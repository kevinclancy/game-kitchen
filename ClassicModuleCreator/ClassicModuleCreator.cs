﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Composition;
using LoveEdit;

namespace ClassicModuleCreator
{
  [Export(typeof(LoveEdit.IModuleCreator))]
  public class ClassicModuleCreator : LoveEdit.IModuleCreator
  {
    private ModuleCreatorControl _currentControl;

    public string GetName()
    { 
      return "Classic Module";
    }

    public Control GetControl()
    {
      _currentControl = new ModuleCreatorControl();
      return _currentControl;
    }

    public bool CreateNewModule(string[] args)
    {
      Studio studio = Studio.Get();
      Project project = studio.CurrentProject;

      string modulePath = args[0].ToLower();
      string moduleName = modulePath.Split('.').Last();
      string moduleDescription = args[1];

      if (!project.IsValidModuleName(modulePath))
      {
        MessageBox.Show("Illegal module name. Module names may only consist of letters, digits, '_', and '.'. Substrings between '.'s must be non-empty."
          + "\nAlso, init is not a legal module name.");
        return false;
      }

      string text = @"
--[[ 
@module
*moduledescription*
]]


local *modulename* = {}



return *modulename*
";

      text = text.Trim();
      text = text.Replace("*modulename*", moduleName);
      text = text.Replace("*moduledescription*", moduleDescription);

      string fileName = project.toFileName(modulePath);
      if (System.IO.File.Exists(fileName))
      {
        MessageBox.Show("A module with name " + modulePath + " already exists.");
        return false;
      }

      studio.CreateNewFile(fileName, text);
      return true;
    }

    public bool CreateNewModuleFromArgs(string[] args)
    {
      return CreateNewModule(args);
    }

    public bool CreateNewModuleFromControl()
    {
      // Contract.Requires(_currentControl != null);
      var args = _currentControl.GetNewModuleArgs();
      return CreateNewModule(args);
    }
  }
}
