﻿namespace ClassicModuleCreator
{
  partial class ModuleCreatorControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._moduleNameTextBox = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this._moduleDescriptionTextBox = new System.Windows.Forms.RichTextBox();
      this.SuspendLayout();
      // 
      // _moduleNameTextBox
      // 
      this._moduleNameTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
      this._moduleNameTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
      this._moduleNameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
      this._moduleNameTextBox.Location = new System.Drawing.Point(82, 7);
      this._moduleNameTextBox.Name = "_moduleNameTextBox";
      this._moduleNameTextBox.Size = new System.Drawing.Size(196, 20);
      this._moduleNameTextBox.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Module Name";
      // 
      // _moduleDescriptionTextBox
      // 
      this._moduleDescriptionTextBox.Location = new System.Drawing.Point(6, 33);
      this._moduleDescriptionTextBox.Name = "_moduleDescriptionTextBox";
      this._moduleDescriptionTextBox.Size = new System.Drawing.Size(652, 472);
      this._moduleDescriptionTextBox.TabIndex = 2;
      this._moduleDescriptionTextBox.Text = "";
      // 
      // ModuleCreatorControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._moduleDescriptionTextBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this._moduleNameTextBox);
      this.Name = "ModuleCreatorControl";
      this.Size = new System.Drawing.Size(672, 517);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox _moduleNameTextBox;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.RichTextBox _moduleDescriptionTextBox;
  }
}
