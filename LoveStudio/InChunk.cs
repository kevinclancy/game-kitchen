﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoveEdit
{
  /// <summary>
  /// A sequence of fields, such as integers and strings, which can be read 
  /// one field at a time. Used to interpret chunks of data received from
  /// TCP sockets.
  /// 
  /// InChunks contain no metadata about what fields they contain; this must
  /// be known from context. For example, without context it would be impossible
  /// to tell whether a 4-byte chunk contains 4 independent, single-byte fields,
  /// or whether it contains a single 4-byte field.
  /// </summary>
  class InChunk
  {
    /// <summary>
    /// The contents of the InChunk
    /// </summary>
    private byte[] _bytes;

    /// <summary>
    /// The length of the InChunk in bytes.
    /// </summary>
    private int _length;

    /// <summary>
    /// The byte position (in _bytes) of the first byte of the next field
    /// to read.
    /// </summary>
    private int _pos;

    /// <summary>
    /// True iff all fields have been read from the chunk.
    /// </summary>
    public bool Empty
    {
      get
      {
        return _pos == _length;
      }
    }

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  Contract.Invariant(_pos <= _length);
    //}
    
    /// <summary>
    /// Constructs an InChunk containing the first length bytes of the specified
    /// array.
    /// </summary>
    /// <param name="bytes">The array containing the bytes to build our chunk from.</param>
    /// <param name="length">The size in bytes of our chunk. (Our chunk takes this many bytes from the front of the first argument.)</param>
    public InChunk(byte [] bytes, int length)
    {
      _length = length;
      _bytes = bytes;
      _pos = 0;
    }

    /// <summary>
    /// Reads an integer.
    /// </summary>
    /// <returns>The integer read.</returns>
    public int ReadInt()
    {
      int ret = BitConverter.ToInt32(_bytes, _pos);
      _pos += sizeof(int);
      return ret;
    }

    /// <summary>
    /// Reads a 64-bit integer.
    /// </summary>
    /// <returns>The 64-bit integer read.</returns>
    public long ReadInt64()
    {
      long ret = BitConverter.ToInt64(_bytes, _pos);
      _pos += sizeof(long);
      return ret;
    }

    /// <summary>
    /// Reads a string.
    /// </summary>
    /// <returns>The string read.</returns>
    public string ReadString()
    {
      int len = BitConverter.ToInt32(_bytes, _pos);
      _pos += sizeof(int);
      string ret = Encoding.ASCII.GetString(_bytes, _pos, len);
      _pos += len;
      return ret;
    }

    /// <summary>
    /// Reads a byte.
    /// </summary>
    /// <returns>The byte read.</returns>
    public byte ReadByte()
    {
      byte ret = _bytes[_pos];
      _pos++;
      return ret;
    }

    /// <summary>
    /// Reads a bool.
    /// </summary>
    /// <returns>The bool read.</returns>
    public bool ReadBool()
    {
      bool ret = _bytes[_pos] == 0 ? false : true;
      _pos++;
      return ret;
    }
  }
}
