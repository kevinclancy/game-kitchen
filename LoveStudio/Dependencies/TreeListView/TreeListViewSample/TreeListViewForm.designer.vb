<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TreeListViewForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TreeListViewForm))
        Me.btn_Fill = New System.Windows.Forms.Button
        Me.btnRead = New System.Windows.Forms.Button
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.btnWrite = New System.Windows.Forms.Button
        Me.nupMain = New System.Windows.Forms.NumericUpDown
        Me.txtName = New System.Windows.Forms.TextBox
        Me.pnlProp = New System.Windows.Forms.Panel
        Me.pgMain = New System.Windows.Forms.PropertyGrid
        Me.cmsHeaders = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SortToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsListViews = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiBeginEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiEndEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.tbpgContainer = New System.Windows.Forms.TabPage
        Me.clvMain = New WinControls.ListView.ContainerListView
        Me.tbpgTree = New System.Windows.Forms.TabPage
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.tlvMain = New WinControls.ListView.TreeListView
        Me.tcMain = New System.Windows.Forms.TabControl
        Me.pnlBottom.SuspendLayout()
        CType(Me.nupMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProp.SuspendLayout()
        Me.cmsHeaders.SuspendLayout()
        Me.cmsListViews.SuspendLayout()
        Me.tbpgContainer.SuspendLayout()
        Me.tbpgTree.SuspendLayout()
        Me.tcMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Fill
        '
        Me.btn_Fill.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Fill.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_Fill.Location = New System.Drawing.Point(398, 9)
        Me.btn_Fill.Name = "btn_Fill"
        Me.btn_Fill.Size = New System.Drawing.Size(75, 23)
        Me.btn_Fill.TabIndex = 1
        Me.btn_Fill.Text = "Populate!"
        Me.btn_Fill.UseVisualStyleBackColor = True
        '
        'btnRead
        '
        Me.btnRead.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRead.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRead.Location = New System.Drawing.Point(504, 9)
        Me.btnRead.Name = "btnRead"
        Me.btnRead.Size = New System.Drawing.Size(75, 23)
        Me.btnRead.TabIndex = 2
        Me.btnRead.Text = "ReadXml"
        Me.btnRead.UseVisualStyleBackColor = True
        '
        'pnlBottom
        '
        Me.pnlBottom.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlBottom.Controls.Add(Me.btnWrite)
        Me.pnlBottom.Controls.Add(Me.nupMain)
        Me.pnlBottom.Controls.Add(Me.btn_Fill)
        Me.pnlBottom.Controls.Add(Me.btnRead)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 552)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(666, 40)
        Me.pnlBottom.TabIndex = 5
        '
        'btnWrite
        '
        Me.btnWrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWrite.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnWrite.Location = New System.Drawing.Point(585, 9)
        Me.btnWrite.Name = "btnWrite"
        Me.btnWrite.Size = New System.Drawing.Size(75, 23)
        Me.btnWrite.TabIndex = 4
        Me.btnWrite.Text = "WriteXml"
        Me.btnWrite.UseVisualStyleBackColor = True
        '
        'nupMain
        '
        Me.nupMain.Location = New System.Drawing.Point(12, 12)
        Me.nupMain.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nupMain.Name = "nupMain"
        Me.nupMain.Size = New System.Drawing.Size(60, 20)
        Me.nupMain.TabIndex = 3
        Me.nupMain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nupMain.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'txtName
        '
        Me.txtName.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtName.Enabled = False
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(0, 0)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(288, 20)
        Me.txtName.TabIndex = 8
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pnlProp
        '
        Me.pnlProp.Controls.Add(Me.pgMain)
        Me.pnlProp.Controls.Add(Me.txtName)
        Me.pnlProp.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlProp.Location = New System.Drawing.Point(666, 0)
        Me.pnlProp.Name = "pnlProp"
        Me.pnlProp.Size = New System.Drawing.Size(288, 592)
        Me.pnlProp.TabIndex = 9
        '
        'pgMain
        '
        Me.pgMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pgMain.LineColor = System.Drawing.SystemColors.Control
        Me.pgMain.Location = New System.Drawing.Point(0, 20)
        Me.pgMain.Name = "pgMain"
        Me.pgMain.Size = New System.Drawing.Size(288, 572)
        Me.pgMain.TabIndex = 6
        '
        'cmsHeaders
        '
        Me.cmsHeaders.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SortToolStripMenuItem})
        Me.cmsHeaders.Name = "cmsHeaders"
        Me.cmsHeaders.Size = New System.Drawing.Size(96, 26)
        '
        'SortToolStripMenuItem
        '
        Me.SortToolStripMenuItem.Name = "SortToolStripMenuItem"
        Me.SortToolStripMenuItem.Size = New System.Drawing.Size(95, 22)
        Me.SortToolStripMenuItem.Text = "Sort"
        '
        'cmsListViews
        '
        Me.cmsListViews.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiBeginEdit, Me.tsmiEndEdit})
        Me.cmsListViews.Name = "cmsListViews"
        Me.cmsListViews.Size = New System.Drawing.Size(128, 48)
        '
        'tsmiBeginEdit
        '
        Me.tsmiBeginEdit.Name = "tsmiBeginEdit"
        Me.tsmiBeginEdit.Size = New System.Drawing.Size(127, 22)
        Me.tsmiBeginEdit.Text = "Begin Edit"
        '
        'tsmiEndEdit
        '
        Me.tsmiEndEdit.Name = "tsmiEndEdit"
        Me.tsmiEndEdit.Size = New System.Drawing.Size(127, 22)
        Me.tsmiEndEdit.Text = "End Edit"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "ClsdFldr.ico")
        Me.ImageList1.Images.SetKeyName(1, "OpenFldr.ico")
        '
        'tbpgContainer
        '
        Me.tbpgContainer.Controls.Add(Me.clvMain)
        Me.tbpgContainer.Location = New System.Drawing.Point(4, 22)
        Me.tbpgContainer.Name = "tbpgContainer"
        Me.tbpgContainer.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpgContainer.Size = New System.Drawing.Size(658, 526)
        Me.tbpgContainer.TabIndex = 1
        Me.tbpgContainer.Text = "ContainerListView"
        Me.tbpgContainer.UseVisualStyleBackColor = True
        '
        'clvMain
        '
        Me.clvMain.BackgroundImage = CType(resources.GetObject("clvMain.BackgroundImage"), System.Drawing.Image)
        Me.clvMain.ColumnHeaderContextMenuStrip = Me.cmsHeaders
        Me.clvMain.ContextMenuStrip = Me.cmsListViews
        Me.clvMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clvMain.EditBackColor = System.Drawing.Color.LightCoral
        Me.clvMain.Location = New System.Drawing.Point(3, 3)
        Me.clvMain.Name = "clvMain"
        Me.clvMain.Size = New System.Drawing.Size(652, 520)
        Me.clvMain.TabIndex = 0
        '
        'tbpgTree
        '
        Me.tbpgTree.Controls.Add(Me.tlvMain)
        Me.tbpgTree.Controls.Add(Me.txtPath)
        Me.tbpgTree.Location = New System.Drawing.Point(4, 22)
        Me.tbpgTree.Name = "tbpgTree"
        Me.tbpgTree.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpgTree.Size = New System.Drawing.Size(658, 526)
        Me.tbpgTree.TabIndex = 0
        Me.tbpgTree.Text = "TreeListView"
        Me.tbpgTree.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.SystemColors.Info
        Me.txtPath.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtPath.Location = New System.Drawing.Point(3, 503)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(652, 20)
        Me.txtPath.TabIndex = 10
        '
        'tlvMain
        '
        Me.tlvMain.CheckBoxes = True
        Me.tlvMain.CheckBoxSelection = System.Windows.Forms.ItemActivation.OneClick
        Me.tlvMain.ColumnHeaderContextMenuStrip = Me.cmsHeaders
        Me.tlvMain.ContextMenuStrip = Me.cmsListViews
        Me.tlvMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlvMain.EditBackColor = System.Drawing.Color.LightCoral
        Me.tlvMain.ImageList = Me.ImageList1
        Me.tlvMain.Location = New System.Drawing.Point(3, 3)
        Me.tlvMain.Name = "tlvMain"
        Me.tlvMain.RowHeight = 24
        Me.tlvMain.Size = New System.Drawing.Size(652, 500)
        Me.tlvMain.TabIndex = 3
        '
        'tcMain
        '
        Me.tcMain.Controls.Add(Me.tbpgTree)
        Me.tcMain.Controls.Add(Me.tbpgContainer)
        Me.tcMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcMain.Location = New System.Drawing.Point(0, 0)
        Me.tcMain.Name = "tcMain"
        Me.tcMain.SelectedIndex = 0
        Me.tcMain.Size = New System.Drawing.Size(666, 552)
        Me.tcMain.TabIndex = 11
        '
        'TreeListViewForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(954, 592)
        Me.Controls.Add(Me.tcMain)
        Me.Controls.Add(Me.pnlBottom)
        Me.Controls.Add(Me.pnlProp)
        Me.Name = "TreeListViewForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TreeListView Sample"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlBottom.ResumeLayout(False)
        CType(Me.nupMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProp.ResumeLayout(False)
        Me.pnlProp.PerformLayout()
        Me.cmsHeaders.ResumeLayout(False)
        Me.cmsListViews.ResumeLayout(False)
        Me.tbpgContainer.ResumeLayout(False)
        Me.tbpgTree.ResumeLayout(False)
        Me.tbpgTree.PerformLayout()
        Me.tcMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Fill As System.Windows.Forms.Button
    Friend WithEvents btnRead As System.Windows.Forms.Button
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents pgMain As System.Windows.Forms.PropertyGrid
    Friend WithEvents nupMain As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents pnlProp As System.Windows.Forms.Panel
    Friend WithEvents btnWrite As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents cmsHeaders As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsListViews As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmiBeginEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiEndEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbpgContainer As System.Windows.Forms.TabPage
    Friend WithEvents clvMain As WinControls.ListView.ContainerListView
    Friend WithEvents tbpgTree As System.Windows.Forms.TabPage
    Friend WithEvents tlvMain As WinControls.ListView.TreeListView
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents tcMain As System.Windows.Forms.TabControl
End Class
