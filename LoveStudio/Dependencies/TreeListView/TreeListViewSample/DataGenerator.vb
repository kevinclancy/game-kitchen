Public Class DataGenerator

#Region " Constructors and Destructors "

#End Region

#Region " Field Declarations "
    Private Shared _Rnd As Random = New Random()

    Private Enum LastNames
        Miller
        Franz
        Osberg
        Favre
        Simmons
        Gastineu
        Lehnerz
        Schowalter
        Simpson
        Hegelbert
        Freeman
        Bullock
        SquarePants
        Starr
        Bertrand
        Merck
        Sinopoli
        Durock
        Hanson
        Borgenstein
        Hastert
        Hannity
        Colmes
        Bagwan
        Ferstenstein
    End Enum

    Private Enum FirstNames_Male
        Bob
        Ted
        Eric
        Curt
        Ravi
        Alex
        Mike
        Bill
        Homer
        Bart
        SpongeBob
        Patrick
        Squidward
        Eugene
    End Enum

    Private Enum FirstNames_FeMale
        Sally
        Becky
        Cindy
        Marcy
        Jill
        Sheila
        Tammy
        Bertha
        Dalia
        Lisa
        Marge
        Gloria
        Serena
        Venus
        Hope
    End Enum

    Private Enum MiddleNames_Male
        Michael
        Thomas
        Edward
        Ricardo
        Sloban
    End Enum

    Private Enum MiddleNames_Female
        Eunice
        Elizabeth
        Lynn
        RoseMarie
        Cynthia
    End Enum
#End Region

#Region " Properties "

#End Region

#Region " Methods "

    Public Shared Function GetFemale(ByVal aMinAge As Integer, ByVal aMaxAge As Integer) As Female
        Return New Female(DirectCast(_Rnd.Next([Enum].GetNames(GetType(FirstNames_FeMale)).Length), FirstNames_FeMale).ToString(), _
                          DirectCast(_Rnd.Next([Enum].GetNames(GetType(MiddleNames_Female)).Length), MiddleNames_Female).ToString(), _
                          DirectCast(_Rnd.Next([Enum].GetNames(GetType(LastNames)).Length), LastNames).ToString(), _
                          _Rnd.Next(aMinAge, aMaxAge))
    End Function

    Public Shared Function GetMale(ByVal aMinAge As Integer, ByVal aMaxAge As Integer) As Male
        Return New Male(DirectCast(_Rnd.Next([Enum].GetNames(GetType(FirstNames_Male)).Length), FirstNames_Male).ToString(), _
                        DirectCast(_Rnd.Next([Enum].GetNames(GetType(MiddleNames_Male)).Length), MiddleNames_Male).ToString(), _
                        DirectCast(_Rnd.Next([Enum].GetNames(GetType(LastNames)).Length), LastNames).ToString(), _
                        _Rnd.Next(aMinAge, aMaxAge))
    End Function

    Public Shared Function GetFamily() As Family
        Dim ChildCount As Integer = _Rnd.Next(0, 6)
        Dim Fam As New Family(GetMale(20, 80), GetFemale(20, 80))

        Fam.Mom.LastName = Fam.Dad.LastName
        If (ChildCount > 0) Then
            Dim Pers As Person
            Dim Oldest As Integer = 0
            Dim Switch As Boolean = False

            For I As Integer = 0 To (ChildCount - 1)
                If (Switch) Then Pers = GetMale(1, 18) Else Pers = GetFemale(1, 18)

                Pers.LastName = Fam.Dad.LastName
                Fam.Kids.Add(Pers)
                Switch = Not Switch
                If (Oldest < Pers.Age) Then Oldest = Pers.Age
            Next

            If ((Fam.Dad.Age - Oldest) < 18) Then Fam.Dad.Age += (Fam.Dad.Age - Oldest)
            If ((Fam.Mom.Age - Oldest) < 18) Then Fam.Mom.Age += (Fam.Mom.Age - Oldest)
        End If


        Return Fam
    End Function


#End Region

#Region " SubClasses and Structures "

    Public Structure Family
        Private _Dad As Male
        Private _Mom As Female
        Private _Kids As Generic.List(Of Person)

        Public Sub New(ByVal aDad As Male, ByVal aMom As Female)
            Me._Dad = aDad
            Me._Mom = aMom
            Me._Kids = New Generic.List(Of Person)
        End Sub

        Public ReadOnly Property Dad() As Male
            Get
                Return Me._Dad
            End Get
        End Property

        Public ReadOnly Property Kids() As Generic.List(Of Person)
            Get
                Return Me._Kids
            End Get
        End Property

        Public ReadOnly Property Mom() As Female
            Get
                Return Me._Mom
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return String.Format("{0} Family", Me.Dad.LastName)
        End Function

    End Structure

    Public MustInherit Class Person
        Private _First, _Middle, _Last As String
        Private _Age As Integer
        Private _Sex As Gender

        Public Enum Gender As Integer
            None
            Male
            Female
        End Enum

        Protected Sub New(ByVal aFirst As String, ByVal aMiddle As String, ByVal aLast As String, ByVal aAge As Integer, ByVal aSex As Gender)
            Me._First = aFirst
            Me._Middle = aMiddle
            Me._Last = aLast
            Me._Age = aAge
            Me._Sex = aSex
        End Sub

        Public Property Age() As Integer
            Get
                Return Me._Age
            End Get
            Friend Set(ByVal value As Integer)
                Me._Age = value
            End Set
        End Property

        Public ReadOnly Property FirstName() As String
            Get
                Return Me._First
            End Get
        End Property

        Public Property LastName() As String
            Get
                Return Me._Last
            End Get
            Friend Set(ByVal value As String)
                Me._Last = value
            End Set
        End Property

        Public ReadOnly Property MiddleName() As String
            Get
                Return Me._Middle
            End Get
        End Property

        Public ReadOnly Property Sex() As Gender
            Get
                Return Me._Sex
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return (String.Format("{0}, {1} {2}", Me._Last, Me._First, Me._Middle.Substring(0, 1)))
        End Function

    End Class

    Public Class Male
        Inherits Person

        Public Sub New(ByVal aFirst As String, ByVal aMiddle As String, ByVal aLast As String, ByVal aAge As Integer)
            MyBase.New(aFirst, aMiddle, aLast, aAge, Gender.Male)
        End Sub
    End Class

    Public Class Female
        Inherits Person

        Public Sub New(ByVal aFirst As String, ByVal aMiddle As String, ByVal aLast As String, ByVal aAge As Integer)
            MyBase.New(aFirst, aMiddle, aLast, aAge, Gender.Female)
        End Sub

    End Class

#End Region

End Class
