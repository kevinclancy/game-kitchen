﻿Imports WinControls.ListView

Public Class testIssue

    Private Sub testIssue_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.tlvMain.Columns.Add(New ContainerColumnHeader("Values"))

        For index As Integer = 1 To 100
            tlvMain.Nodes.Add(New TreeListNode(index.ToString()))
        Next

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        tlvMain.Nodes.Clear()
        tlvMain.Nodes.Add(New TreeListNode("1"))
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        tlvMain.EnsureVisible(45)
    End Sub
End Class