Imports TreeListViewSample.DataGenerator
Imports WinControls.ListView
Imports System.Xml.Serialization

Public Class TreeListViewForm

    Private _CurLstVw As ContainerListView = Nothing
    Private _Path As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) & "\"

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Fill.Click
        Me._CurLstVw.BeginUpdate()
        If (Me._CurLstVw Is Me.tlvMain) Then
            Me.tlvMain.Nodes.Clear()

            For I As Integer = 0 To CType(Me.nupMain.Value, Integer)
                Dim Fam As Family = DataGenerator.GetFamily()
                Dim Nde As New TreeListNode(Fam.ToString())

                Me._addSubItems(Nde.Nodes.Add(Fam.Dad.ToString()), Fam.Dad)
                Me._addSubItems(Nde.Nodes.Add(Fam.Mom.ToString()), Fam.Mom)

                If (Fam.Kids.Count > 0) Then
                    Dim KidsNde As New TreeListNode("Children")

                    For Each Pers As Person In Fam.Kids
                        Dim Kid As New TreeListNode(Pers.ToString)

                        Me._addSubItems(Kid, Pers)
                        KidsNde.Nodes.Add(Kid)
                    Next
                    Nde.Nodes.Add(KidsNde)
                End If
                Me.tlvMain.Nodes.Add(Nde)
            Next
        Else
            Me.clvMain.Items.Clear()
            For I As Integer = 0 To CType(Me.nupMain.Value, Integer)
                Dim Fam As Family = DataGenerator.GetFamily()
                Dim Itm As New ContainerListViewItem(Fam.Dad.LastName)
                With Itm
                    .SubItems.Add(Fam.Dad.FirstName)
                    .SubItems.Add(Fam.Dad.MiddleName)
                    .SubItems.Add(Fam.Dad.Age.ToString())
                End With

                Me.clvMain.Items.Add(Itm)
            Next
        End If
        Me._CurLstVw.EndUpdate()
    End Sub

    Private Sub _addSubItems(ByVal aObj As ContainerListViewObject, ByVal aPers As Person)
        With aObj.SubItems
            .Add(aPers.LastName)
            .Add(aPers.FirstName)
            .Add(aPers.MiddleName)
            .Add(aPers.Age.ToString())
            .Add(aPers.Sex.ToString())
        End With
    End Sub

    Private Sub tlvMain_AfterSelect(ByVal sender As Object, ByVal e As WinControls.ListView.EventArgClasses.ContainerListViewEventArgs) Handles tlvMain.AfterSelect
        Dim Nde As TreeListNode = DirectCast(e.Item, TreeListNode)
        Me.pgMain.SelectedObject = Nde
        If (Not Nde Is Nothing) Then
            Me.txtPath.Text = Nde.FullPath
        Else
            Me.txtPath.Text = String.Empty
        End If
    End Sub

    Private Sub clvMain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clvMain.Click, tlvMain.Click
        Dim Clv As ContainerListView = DirectCast(sender, ContainerListView)

        If (Clv.EditedObject IsNot Nothing) Then Clv.EditedObject.EndEdit()
    End Sub

    Private Sub tlvMain_ColumnHeaderClicked(ByVal aSender As Object, ByVal aE As WinControls.ListView.EventArgClasses.ContainerColumnHeaderEventArgs) Handles tlvMain.ColumnHeaderClicked, clvMain.ColumnHeaderClicked
        If (Not aE.Column Is Nothing) Then
            Me.pgMain.SelectedObject = aE.Column
            Me.txtName.Text = aE.Column.GetType.ToString()
        End If
    End Sub

    Private Sub btnWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWrite.Click
        Dim Settings As New Xml.XmlWriterSettings

        With Settings
            .Encoding = System.Text.Encoding.Default
            .Indent = True
            .OmitXmlDeclaration = True
            .IndentChars = "    "
            .CloseOutput = True
        End With

        Me._CurLstVw.WriteXml(Xml.XmlWriter.Create(Me._Path & Me._CurLstVw.GetType.Name & ".xml", Settings))
        MsgBox("Write Complete")
    End Sub

    Private Sub btnRead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRead.Click
        Me._CurLstVw.ReadXml(Xml.XmlReader.Create(Me._Path & Me._CurLstVw.GetType.Name & ".xml"))
        MsgBox("Read Complete!")
    End Sub

    Private Sub tlvMain_ItemDrop(ByVal aSender As Object, ByVal aE As WinControls.ListView.EventArgClasses.TreeListViewItemDropEventArgs) Handles tlvMain.ItemDrop
        Me.tlvMain.BeginUpdate()
        For Each Nde As TreeListNode In aE.Item
            Nde.Remove()
            aE.TargetNode.Nodes.Add(Nde)
        Next
        Me.tlvMain.EndUpdate()
    End Sub

    Private Sub tcMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcMain.SelectedIndexChanged
        If (Me.tcMain.SelectedTab Is Me.tbpgTree) Then
            Me._CurLstVw = Me.tlvMain
            Me.pgMain.SelectedObject = Me.tlvMain
        Else
            Me._CurLstVw = Me.clvMain
            Me.pgMain.SelectedObject = Me.clvMain
        End If
        Me.txtName.Text = Me.pgMain.SelectedObject.GetType.ToString()
    End Sub

    Private Sub TreeListViewForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.tcMain.SelectedIndex = 1
        Me.tcMain.SelectedIndex = 0

        With Me.tlvMain.Columns
            .Add("Family", 250, HorizontalAlignment.Center)
            .Add(New ContainerColumnHeader("Last"))
            .Add(New ContainerColumnHeader("First"))
            .Add(New ContainerColumnHeader("Middle"))
            .Add(New ContainerColumnHeader("Age"))
        End With

        With Me.clvMain.Columns
            .Add(New ContainerColumnHeader("Last"))
            .Add(New ContainerColumnHeader("First"))
            .Add(New ContainerColumnHeader("Middle"))
            .Add(New ContainerColumnHeader("Age"))
        End With
    End Sub

    Private Sub cmsListViews_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsListViews.Opening
        e.Cancel = Me._CurLstVw.SelectedItems.Count = 0
    End Sub

    Private Sub tsmiBeginEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiBeginEdit.Click
        Me._CurLstVw.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub tsmiEndEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiEndEdit.Click
        If (Me._CurLstVw.EditedObject IsNot Nothing) Then
            Me._CurLstVw.EditedObject.EndEdit()
        End If
    End Sub

    Private Sub clvMain_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles clvMain.DoubleClick, tlvMain.DoubleClick
        Dim Clv As ContainerListView = DirectCast(sender, ContainerListView)

        Dim PntVal As Point = Me.clvMain.PointToClient(ContainerListView.MousePosition)
        Dim Clvo As ContainerListViewObject = Clv.GetItemAt(PntVal)

        If (Clvo IsNot Nothing) Then
            Dim SbItm As ContainerListViewItem.ContainerListViewSubItem = Clvo.GetSubItemAt(PntVal)

            If (Clvo.ListView.EditedObject IsNot Nothing) Then Clvo.ListView.EditedObject.EndEdit()
            If (SbItm IsNot Nothing) Then
                Clvo.BeginEdit(SbItm)
            Else
                Clvo.BeginEdit()
            End If
        End If
    End Sub

    Private Sub tlvMain_BeforeEdit(ByVal sender As System.Object, ByVal e As WinControls.ListView.EventArgClasses.ContainerListViewCancelEventArgs) Handles tlvMain.BeforeEdit
        e.Cancel = True
    End Sub
End Class
