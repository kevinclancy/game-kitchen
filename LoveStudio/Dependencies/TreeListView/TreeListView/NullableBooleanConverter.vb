﻿Public Class NullableBooleanConverter
    Inherits BooleanConverter

    Private Shared values As TypeConverter.StandardValuesCollection

    Public Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object

        If TypeOf value Is String Then
            Dim sval As String = DirectCast(value, String)
            If String.IsNullOrEmpty(sval) Then
                Return DirectCast(Nothing, Nullable(Of Boolean))
            End If
        End If
        Return MyBase.ConvertFrom(context, culture, value)
    End Function


    Public Overrides Function GetStandardValues(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.ComponentModel.TypeConverter.StandardValuesCollection

        If values Is Nothing Then
            values = New TypeConverter.StandardValuesCollection(New Object() {"(null)", True, False})
        End If
        Return values
    End Function
End Class
