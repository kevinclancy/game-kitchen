﻿// Learn more about F# at http://fsharp.net
module Util

exception AssertException

/// An assert function that, unlike the default assert function,
/// does not crash the NUnit GUI
let _assert (cond : bool) =
    if cond then
        ()
    else
        raise AssertException
