﻿module ParsingTests

open NUnit.Framework
open NUnit.Framework.Constraints
open HandParser
open Syntax
open SyntaxDiffInfo
open System.IO
open ParseInterface

let viewDOTGraph (string : string) = 
    let outStream = new StreamWriter("temp.dot")
    outStream.Write string
    outStream.Close()
    
    //Create a new process to build an svg based on the graphviz
    //file we have just written.
    let graphvizProc = System.Diagnostics.Process.Start(
        "dot",
        "-Tsvg -o \"temp.svg\" -Kdot \"temp.dot\""
    ) 

    graphvizProc.WaitForExit()

    //Display the svg in a viewer.
    let squiggleProc = System.Diagnostics.Process.Start(
        "batik-squiggle.jar",
        "temp.svg"
    )    

    ()

let viewSyn<'Diffable> (c : 'Diffable) =
     let g = SyntaxBundle.TreeDOTCode c
     viewDOTGraph g

[<TestFixture>]
type ParsingTests() =
    [<Test>]
    member test.helloWorld() =
        let res = ParseInterface.parseString "
            print(\"hello world\")
        "
        
        match res with
        | Success(result) ->
            match result with
            | Sequence(Call(_),_,_) ->
                ()
            | _ ->
                viewSyn result
                Assert.Fail("hello world did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())
    [<Test>]
    member test.doubleEnd() =
        let res = ParseInterface.parseString "
            do end end
        "
        
        match res with
        | Success(result) ->
            viewSyn result
            Assert.Fail("doubleEnd did not parse as expected.")
        | Failure(error) ->
            ()

    [<Test>]
    member test.forNumLoop() =
        let res = ParseInterface.parseString "
            for i = 1,10 do
                print(i)
            end
        "

        match res with
        | Success(result) ->
            match result with
            | Sequence(ForNum(i,Number(_,_), Number(_,_), Number(_,_), _, _),_,_) ->
                ()
            | _ ->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())

    [<Test>]
    member test.forGenLoop() =
        let res = ParseInterface.parseString "
            for j in fun() do
                z = 55
            end
        "

        match res with
        | Success(result) ->
            match result with
            | Sequence(ForGeneric([NameExpr("j",_)],[CallExpr(_,_,_)],_,_),_,_) ->
                ()
            | _ ->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())

    [<Test>]
    member test.functionDef() =
        let res = ParseInterface.parseString "
            function myFunction(a,b,c)
                for i=1,10,-1 do
                  a(i);
                end;
            end;
        "

        match res with
        | Success(result) ->
            match result with
            | Sequence(Assign([NameExpr("myFunction",_)], [Function([_;_;_],false, _, _)], _),_,_) ->
                ()
            | _ ->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())

    [<Test>]
    member test.functionDef2() =
        let res = ParseInterface.parseString "
            function _someFunc1(z, ...)
                do
                    local a = 1;
                    a = a + 1;
                end;
            end;
        "
        match res with
        | Success(result) ->
            match result with
            | Sequence(Assign([NameExpr("_someFunc1",_)], [Function([NameExpr("z",_)],true,_,_)],_),_,_) ->
                ()
            | _->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())
            
    [<Test>]
    member test.functionDef3() =
        let res = ParseInterface.parseString "
            function a.b._c:_someFunc1()
                do
                    local a = 1
                    a = nil
                end
            end
        "
        match res with
        | Success(result) ->
            match result with
            | Sequence(Assign([BinOpExpr(OpInd, BinOpExpr(OpInd, BinOpExpr(OpInd, _, _, _), _, _), _, _)], [Function(_,_,_,_)], _),_,_) ->
                ()
            | _->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())


    [<Test>]
    member test.callTest() =
        let res = ParseInterface.parseString "
            self:DoSomething()
        "
        match res with
        | Success(result) ->
            match result with
            | Sequence(Call(CallExpr(BinOpExpr(OpInd,_,_,_),_,_),_),_,_) ->
                ()
            | _->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())

    [<Test>]
    member test.whileTest() =
        let res = ParseInterface.parseString "
            while a == true do
                self:DoSomething()
                a=false
            end
        "

        //The problem is that it expects a primary expressions where we
        // have nameExpr a. This highlights that PrimaryExpr should not be
        // a part of the abstract syntax tree. True, you cannot use the index
        // operator... actually, you can, just not with the '.' syntax.
        // aha... another idea: [], ., and : should translate to the index 
        // operator.
        //
        // Do I want to have some concrete syntax to work with, though,
        // for example, for collecting all of the methods of a class? 
        // To differentiate between static and '.' and non-static ':' methods?
        // I don't think so. I am not going to add any extra meaning to
        // lua code. Instead, the extra meaning will have to come from 
        // specially-formatted comments. --[[* *]]. Actually, that is too verbose
        // how about the following?

        //
        //        --[[*
        //key : int [The key which was pressed.]
        //unicode : bool [I don't know what this is.]
        //@return : unit [the number of blah blah blah]
        //
        //@effect [Causes the avatar to move in response to a key press.]
        //(We can synthesize a set of data members which may be modified as an "effect type")
        //
        //@latent
        //(latent is just a tag, with no satellite data)
        //
        //When we add dbc, there will be no point in embedding everything in comments.
        //I will add a latent
        //@requires
        //@requires 
        //...
        //@ensures
        //@ensures
        //
        //@maintains ?
        //]]

        match res with
        | Success(result) ->
            match result with
            | Sequence(While(BinOpExpr(OpEq,NameExpr("a",_), True _,_),_,_),_,_) ->
                ()
            | _->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())

    [<Test>]
    member test.ret() =
        let res = ParseInterface.parseString "
            if a.b then return end
        "
        
        match res with
        | Success(result) ->
            match result with
            | Sequence(If(_,_),_,_) ->
                ()
            | _->
                viewSyn result
                Assert.Fail("did not parse as expected.")
        | Failure(error) ->
            Assert.Fail("parse failed with error: " + error.ToString())
