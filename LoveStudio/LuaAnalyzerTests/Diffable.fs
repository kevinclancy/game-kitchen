﻿///
/// Diffable
/// 
/// Diffable is a module that makes it easy to unit test and debug 
/// values belonging to discriminated union types. It does so by providing 
/// functions that generate DOT code representing such values as trees.
///
// Implementation Notes:
//
// It is intended that types used to instantiate the 'Diffable type argument 
// be discriminated unions. 
//
// In comments, when we refer to values of these types, 
// we need to make sure we're not being ambiguous about whether we're referring 
// to the entire tree rooted at that value or only the root. 
// To this end, we use the following terminology:
//
// We refer to the tree rooted at a 'Diffable value t as "the tree t".
//
// We refer to the root of the aforementioned tree simply as "t", or "the node t"
//
module Diffable

open Microsoft.FSharp.Reflection
open System.Collections.Generic

type private Object = System.Object
type private Type = System.Type

let private fst (x, _, _) = x
let private snd (_, x, _) = x
let private thrd (_, _, x) = x

/// Reference to a function which takes a condition and an error message as
/// arguments. If the condition is false, raises an exception containing
/// the given error message. Replace this with the assert function of your 
/// choice. 
let AssertWith = ref (fun cond s -> if cond then () else failwith s)

/// A DiffInfo<'Context> value contains information which is used for
/// generating DOT code for and performing comparison against a node belonging 
/// carrying context of type 'Context.
type DiffInfo<'Context> = {
    /// The label to give the node representing this value in our DOT graph. 
    nodeLabel : string
    
    /// A list of all nodes that are considered children of
    /// this one. These will typically be either fields of a union case or elements
    /// of a containter which is itself a field of a union case.
    ///
    /// With each child, we tuple the context we wish to pass to it, as
    /// well as a string label for the edge incoming from the current(parent)
    /// node.
    children : (Object*'Context*string) list

    /// A list of all fields of this node which are significant for structural 
    /// equivalence. We allow the IDiffable<'Context> implementor to explicitly 
    /// signify these so that they can filter out insignificant fields (such as 
    /// debug fields), as well as child fields, which should instead be placed 
    /// in the children list. 
    compFields : Object list
}

type DiffInfoFunc<'Context> = Object -> 'Context -> DiffInfo<'Context>

type diffInfoBundle<'Context> (emptyContext : 'Context) =

    let diffInfoDictionary = new Dictionary<Type, DiffInfoFunc<'Context>>()
    let colorDictionary = new Dictionary<Type, string>()

    let getDiffInfoFunc (obj : Object) =
        
        let objT = obj.GetType().BaseType 

        let hasDiffInfo = diffInfoDictionary.ContainsKey objT
        let error = ("Type " + objT.ToString() + " does not have a diff info function")
        let error = error + "\n tried to diff " + obj.ToString()
        !AssertWith hasDiffInfo error
        (diffInfoDictionary.Item objT, colorDictionary.Item objT)

    /// Returns true iff the nodes t0 and t1 are equivalent modulo variable
    /// naming and debug info.
    let rootEquiv (t0 : Object)
                  (ctx0 : 'Context)
                  (t1 : Object)
                  (ctx1 : 'Context) =

        if t0.GetType() <> t1.GetType() then 
            false     
        else
            let diffInfoFunc, _ = getDiffInfoFunc t0
        
            let ty = t0.GetType()
            
            let (case0, _) = Reflection.FSharpValue.GetUnionFields(t0, ty)
            let (case1, _) = Reflection.FSharpValue.GetUnionFields(t1, ty)

            let children0 = (diffInfoFunc t0 ctx0).children
            let children1 = (diffInfoFunc t1 ctx1).children

            let compFields0 = (diffInfoFunc t0 ctx0).compFields
            let compFields1 = (diffInfoFunc t1 ctx1).compFields

            if case0 <> case1 then
                false
            else if children0.Length <> children1.Length then
                false
            else if compFields0.Length <> compFields1.Length then
                false
            else
                List.forall2 (=) compFields0 compFields1

    /// Returns a string containing DOT code relevant to the node t.
    ///
    /// Arguments:
    /// - ctx : 'Context -
    /// The context to consider the node t in.
    ///
    /// - t : IDiffable<'Context> - 
    /// The node we are generating DOT code for.
    ///
    /// - name : string -
    /// The unique identifier to give to the node t.
    ///
    /// - color : string -
    /// The color to give node t.
    let rootDOTCode (ctx : 'Context) 
                    (t : Object) 
                    (name : string) =
    
        let diffInfoFunc, color = getDiffInfoFunc t

        let diffInfo = diffInfoFunc t ctx
        let edgeLabels = List.map thrd diffInfo.children

        let quote string =
            "\"" + string + "\""

        let rootDotCode = "\n" + (quote name) + " [label = \"" + diffInfo.nodeLabel + "\", style=filled, color=" + color + "]"
        
        let makeEdge (i : int) =
            let label = edgeLabels.[i]
            (quote name) + " -> " + quote (name + " " + i.ToString()) + " [label = " + quote label + "]\n"
   
        let edgeSeq = seq {
            for i in 0 .. edgeLabels.Length-1 ->
                makeEdge i
        }

        let edgeDotCode = String.concat "" edgeSeq

        rootDotCode + edgeDotCode
    
    /// Returns a string containing DOT code for the tree corresponding to 
    /// the argument t.
    ///
    /// Arguments:
    /// - ctx : 'Context - 
    /// The context to consider the tree t in.
    ///
    /// - t : 'IDiffable<'Context> - 
    /// The tree to return DOT code for.
    ///
    /// - name : string - 
    /// The unique identifier given to the root node of the tree t. Descendant node
    /// names are generated by concatenating onto this identifier. 
    ///
    /// - color : string - 
    /// The color to give the nodes of the tree t.
    let rec treeDOTCodeRec (ctx : 'Context) 
                           (t : Object) 
                           (name : string) =
    
        let diffInfoFunc, _ = getDiffInfoFunc t
        let nodeDotCode = rootDOTCode ctx t name

        let (_, fields) = 
            Reflection.FSharpValue.GetUnionFields(t, t.GetType().BaseType) 
        
        let diffInfo = diffInfoFunc t ctx
        
        let children = List.map fst (diffInfo.children)
        let contexts = List.map snd (diffInfo.children)

        let children = (diffInfoFunc t ctx).children

        let subtreeSeq = seq {
            for i in 0 .. children.Length-1 do
                yield treeDOTCodeRec (snd children.[i])
                                        (fst children.[i])
                                        (name + " " + i.ToString())
        }

        let subtreeDotCode = String.concat "" subtreeSeq

        nodeDotCode + subtreeDotCode

    /// Returns a DOT file displaying the two trees t0 and t1 under contexts
    /// ctx0 and ctx1 respectively, coloring any subtrees in which the two trees 
    /// differ red.
    ///
    /// The roots of the two trees are given the respective DOT node identifiers
    /// ("0s"+name) and ("1s"+name).
    let rec treeDiffRec (ctx0 : 'Context) 
                        (t0 : Object) 
                        (ctx1 : 'Context) 
                        (t1 : Object) 
                        (name : string) =
              
        if rootEquiv t0 ctx0 t1 ctx1 then
            let diffInfoFunc, _ = getDiffInfoFunc t0

            let n0DotCode = rootDOTCode ctx0 t0 ("0s"+name)
            let n1DotCode = rootDOTCode ctx1 t1 ("1s"+name)

            let subtreeSeq = seq {
                let children0 = List.map fst (diffInfoFunc t0 ctx0).children
                let children1 = List.map fst (diffInfoFunc t1 ctx1).children

                let childContexts0 = List.map snd (diffInfoFunc t0 ctx0).children
                let childContexts1 = List.map snd (diffInfoFunc t1 ctx1).children

                for i in 0 .. children0.Length-1 do
                    yield treeDiffRec childContexts0.[i]
                                        children0.[i]
                                        childContexts1.[i]
                                        children1.[i]
                                        (name + " " + i.ToString())
            }

            let subtreeDotCode = String.concat "" subtreeSeq

            n0DotCode + n1DotCode + subtreeDotCode
        else
            let treeCode0 = treeDOTCodeRec ctx0 t0 ("0s"+name)
            let treeCode1 = treeDOTCodeRec ctx1 t1 ("1s"+name)
            treeCode0 + treeCode1

    /// Returns DOT code displaying the tree for t1 under context ctx1,
    /// coloring any subtrees in which t0 and t1 differ red.
    ///
    /// The roots of the two trees are given the respective DOT node identifiers
    /// ("0s"+name) and ("1s"+name).
    let rec asymmTreeDiffRec (ctx0 : 'Context) 
                             (t0 : Object) 
                             (ctx1 : 'Context) 
                             (t1 : Object) 
                             (name : string) =

        if rootEquiv t0 ctx0 t1 ctx1 then
            let diffInfoFunc,_ = getDiffInfoFunc t0   
            let n1DotCode = rootDOTCode ctx1 t1 ("1s"+name)

            let subtreeSeq = seq {
                let children0 = List.map fst (diffInfoFunc t0 ctx0).children
                let children1 = List.map fst (diffInfoFunc t1 ctx1).children

                let childContexts0 = List.map snd (diffInfoFunc t0 ctx0).children
                let childContexts1 = List.map snd (diffInfoFunc t1 ctx1).children

                for i in 0 .. children0.Length-1 do
                    yield asymmTreeDiffRec childContexts0.[i]
                                            children0.[i]
                                            childContexts1.[i]
                                            children1.[i]
                                            (name + " " + i.ToString())
            }

            let subtreeDotCode = String.concat "" subtreeSeq

            n1DotCode + subtreeDotCode
        else
            treeDOTCodeRec ctx1 t1 ("1s" + name)

    /// Returns true if t0 and t1 are structurally equivalent.
    ///    
    /// In Diffable, two trees are considered strucutrally equivalent if
    /// their roots belong to the same union case, the corresponding 
    /// values of their compFields lists are equal (w.r.t. System.Object.Equals),
    /// and each of their corresponding children are structurally equivalent.
    let rec structuralEquivRec  (t0 : Object)
                                (ctx0 : 'Context)
                                (t1 : Object)
                                (ctx1 : 'Context) =

        if not (rootEquiv t0 ctx0 t1 ctx1) then
            false
        else
            let diffInfoFunc,_ = getDiffInfoFunc t0

            let children0 = (diffInfoFunc t0 ctx0).children
            let children1 = (diffInfoFunc t1 ctx1).children

            let isEquiv (f0, c0, _) (f1, c1, _) =
                structuralEquivRec f0 c0 f1 c1

            Seq.forall2 isEquiv children0 children1    

    //TODO: change color to an enum
    member this.AddDiffInfoFunc<'Diffable> (f : 'Diffable -> 'Context -> DiffInfo<'Context>)
                                           (color : string) =
        let fTy = f.GetType().BaseType
        //!AssertWith (Reflection.FSharpType.IsFunction fTy) "tried to add a non-function as a diff info function"
        let diffableTy,_ = Reflection.FSharpType.GetFunctionElements fTy
        
        let wrappedf (o : Object) (c : 'Context) =
            f (o :?> 'Diffable) c

        colorDictionary.Add(diffableTy, color)
        diffInfoDictionary.Add(diffableTy, wrappedf)

    member this.StructuralEquiv (t0 : 'Diffable) (t1 : 'Diffable) =
        structuralEquivRec t0 emptyContext t1 emptyContext

    member this.treeListDOTCode (treeList : 'Diffable list) =
        let trees = [
            for i in 0 .. treeList.Length-1 ->
                if i = 0 then 
                    treeDOTCodeRec emptyContext treeList.[i] (i.ToString()+" ")
                else
                    asymmTreeDiffRec emptyContext treeList.[i-1] emptyContext treeList.[i] (i.ToString()+" ")
        ]

        let forrestCode = String.concat "" trees

        "digraph { " + forrestCode + "\n}"
                
    member this.TreeDOTCode  (t0 : Object) =
        "digraph {" + (treeDOTCodeRec emptyContext t0 "0") + "\n}"
    
    member this.TreeDiff (t0 : Object) (t1 : Object) =
        let body = treeDiffRec emptyContext t0 emptyContext t1 ""  
        "digraph {" + body + "\n}"