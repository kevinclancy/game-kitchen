﻿module LexerTests

open Token
open Microsoft.FSharp.Reflection

open NUnit.Framework
open NUnit.Framework.Constraints

let tokenMatch (t0 : Token) (t1 : Token) =
    t0.GetType() = t1.GetType()

let arrMatch (actual : array<TokenInfo>) (actualLen : int) (expected : array<Token>) =
    Assert.IsTrue( (actualLen = expected.Length) )
    for i in 1..actualLen-1 do
        match actual.[i] with
        | (actualId, _, _) ->
            assert (actualId = expected.[i])
            Assert.IsTrue( (actualId = expected.[i]) )


[<TestFixture>]
type LexerTests() =
    [<Test>]
    member test.HelloWorld() =
        let (res,len) = HandLexer.lex "
            print(\"hello world\")
        "

        let expected = [| 
            Token.NAME
            Token.OPEN_PAREN
            Token.STRING
            Token.CLOSE_PAREN
        |]

        arrMatch res len expected

    [<Test>]
    member test.forLoop1() =
        let (res,len) = HandLexer.lex "
            for i=1,10,-1.3e+10 do 
                local a = 2
                print(a)
            end
        "

        let expected = [| 
            Token.FOR
            Token.NAME
            Token.ASSIGN
            Token.NUMBER
            Token.COMMA
            Token.NUMBER
            Token.COMMA
            Token.MINUS
            Token.NUMBER
            Token.DO
            Token.LOCAL
            Token.NAME
            Token.ASSIGN
            Token.NUMBER
            Token.NAME
            Token.OPEN_PAREN
            Token.NAME
            Token.CLOSE_PAREN
            Token.END
        |]

        arrMatch res len expected

    [<Test>]
    member test.forLoop2() =
        let (res,len) = HandLexer.lex "
            for k,v in pairs(funk) do 
                k.x = 3
                v.x = 33.
            end
        "

        let expected = [| 
            Token.FOR
            Token.NAME
            Token.COMMA
            Token.NAME
            Token.IN
            Token.NAME
            Token.OPEN_PAREN
            Token.NAME
            Token.CLOSE_PAREN
            Token.DO
            Token.NAME
            Token.DOT
            Token.NAME
            Token.ASSIGN
            Token.NUMBER
            Token.NAME
            Token.DOT
            Token.NAME
            Token.ASSIGN
            Token.NUMBER
            Token.END
        |]

        arrMatch res len expected

    [<Test>]
    member test.functionDef1() =
        let (res,len) = HandLexer.lex "
            function a.b.c:d(x,y,...)
                local a = 5.1e-3
                --[==[
                print(\"this code is commented out\")
                ]=]
                and so are these words
                ]]==]
                localb = ...,3
            end
        "

        let expected = [|
            Token.FUNCTION
            Token.NAME
            Token.DOT
            Token.NAME
            Token.DOT
            Token.NAME
            Token.METHOD_IND
            Token.NAME
            Token.OPEN_PAREN
            Token.NAME
            Token.COMMA
            Token.NAME
            Token.COMMA
            Token.VARARGS
            Token.CLOSE_PAREN
            Token.LOCAL
            Token.NAME
            Token.ASSIGN
            Token.NUMBER
            Token.NAME
            Token.ASSIGN
            Token.VARARGS
            Token.COMMA
            Token.NUMBER
            Token.END
        |]

        arrMatch res len expected

    [<Test>]
    member test.longString() =
        let (res,len) = HandLexer.lex "
            a = 2
            b = [=[ helloyalo ]]]=]
            local c = [[ helllo ]=] ]]
        "        

        let expected = [|
            Token.NAME
            Token.ASSIGN
            Token.NUMBER
            Token.NAME
            Token.ASSIGN
            Token.STRING
            Token.LOCAL
            Token.NAME
            Token.ASSIGN
            Token.STRING
        |]

        arrMatch res len expected