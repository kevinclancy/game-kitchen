﻿module SyntaxDiffInfo

open Syntax
open Diffable
open NUnit.Framework

type Object = System.Object

type Context = Unit

Diffable.AssertWith := fun c s -> Assert.That(c,s)

let binOpStr (op : BinOp) =
    match op with
    | OpAdd -> "+"
    | OpSub -> "-"
    | OpMul -> "*"
    | OpDiv -> "/"
    | OpMod -> "%"
    | OpPow -> "^"
    | OpConcat -> ".."
    | OpNe -> "~="
    | OpEq -> "=="
    | OpLt -> "<"
    | OpLe -> "<="
    | OpGt -> ">"
    | OpGe -> ">="
    | OpAnd -> "and"
    | OpOr -> "or"
    | OpInd -> "index"

let unOpStr (op : UnOp) =
    match op with 
    | OpNegate -> "-"
    | OpNot -> "not"
    | OpLen -> "#"

let private sepBy (l : List<'A>) (sep : string) = 
    let append x y = x.ToString() + sep + y.ToString()
    match l with
    | h :: t ->
        List.fold append (h.ToString()) t 
    | [] ->
        ""

let private statementDiffInfo (statement : Statement) (ctxt : Context) =
    match statement with
    | If(condChunkList,_) ->
        let condChunkToChildren (i : int) (cond, chunk) =
            [
                (cond :> Object, (), "x"+i.ToString())
                (chunk :> Object, (), "y"+i.ToString())
            ] 
        {
            nodeLabel = "if x0 then y0 ... end"
            children = List.concat (List.mapi condChunkToChildren condChunkList)
            compFields = []
        }
    | While(cond, chunk,_) ->
        {
            nodeLabel = "while cond do chunk end"
            children = [(cond :> Object, (), "cond"); (chunk :> Object, (), "chunk")]
            compFields = []
        }
    | Do(chunk,_) ->
        {
            nodeLabel = "do chunk end"
            children = [(chunk :> Object, (), "chunk")]
            compFields = []
        }
    | ForNum(name,start,fin,step,chunk,_) ->
        match name with 
        | NameExpr(nameStr,_) ->
        {
            nodeLabel = "for " + nameStr + " = x,y,z do c end"
            children = [
                (start :> Object, (), "x")
                (fin :> Object, (), "y")
                (step :> Object, (), "z")
                (chunk :> Object, (), "c")
            ]
            compFields = [] 
        }
    | ForGeneric(names, gens, chunk, _) ->
        let genChildren = List.mapi (fun i g -> (g :> Object, (), "g"+i.ToString())) gens
        {
            nodeLabel = "for ... in gens do body end"
            children = List.append genChildren [(chunk :> Object,(),"body")]
            compFields = [] 
        }
    | Repeat(cond, body, _) ->
        {
            nodeLabel = "repeat x until y"
            children = [
                (body :> Object, (), "x")
                (cond :> Object, (), "y")
            ]
            compFields = []
        }
    | LocalAssign(names, exprs,_) ->
        let exprToChild (i : int) (e : Expr) =
            (e :> Object, (), "e"+i.ToString()) 
        {
            nodeLabel = "local " + (sepBy names ", ") + " = " + "e0 ... en"
            children = List.mapi exprToChild exprs
            compFields = List.map (fun x -> x :> Object) names
        }
    | Assign(names, exprs,_) ->
        let lhsChildren = List.mapi (fun i e -> (e :> Object, (), "x"+i.ToString())) names
        let rhsChildren = List.mapi (fun i e -> (e :> Object, (), "y"+i.ToString())) exprs
        {
            nodeLabel = "x0 ... xn" + " = " + "y0 ... yn"
            children = List.append lhsChildren rhsChildren
            compFields = List.map (fun x -> x :> Object) names
        }
    | Return(exprs, _) ->
        let exprToChild (i : int) (e : Expr) =
            (e :> Object, (), "e"+i.ToString())
        {
            nodeLabel = "return e0 ... en"
            children = List.mapi exprToChild exprs
            compFields = []
        }
    | Call(expr, _) ->
        {
            nodeLabel = "call"
            children = [(expr :> Object, (), "")]
            compFields = []
        }
    | Sequence (first, rest, _) ->
        {
            nodeLabel = "Sequence"
            children = [
                (first :> Object, (), "first")
                (rest :> Object, (), "rest")
            ]
            compFields =[]
        }
    | DoNothing(rng) ->
        {
            nodeLabel = "DoNothing"
            children = []
            compFields = []
        }
        
let exprDiffInfo (expr : Expr) (ctxt : Context) =
    match expr with
    | Number(n,_) ->
        {
            nodeLabel = n.ToString()
            children = []
            compFields = [n] 
        }
    | String(s,_) ->
        {
            nodeLabel = "'"+s+"'"
            children = []
            compFields = [s]
        }
    | Nil _ ->
        {
            nodeLabel = "nil"
            children = []
            compFields = []
        }
    | True _ ->
        {
            nodeLabel = "true"
            children = []
            compFields = []
        }
    | False _ ->
        {
            nodeLabel = "false"
            children = []
            compFields = []
        }
    | VarArgs _ ->
        {
            nodeLabel = "..."
            children = []
            compFields = []
        }
    | Constructor(fields,_) ->
        let fieldToChild (i : int) (f : ConstructorField) =
            (f :> Object, (), "f"+i.ToString())

        {
            nodeLabel = "{ f0 ... fn }"
            children = List.mapi fieldToChild fields
            compFields = []
        }
    | Function (formals,hasVarArgs,body,_,_) ->
        {
            nodeLabel = "function(args) body end"
            children = [(body :> Object, (), "body")]
            compFields = (hasVarArgs :> Object) :: (List.map (fun x -> x :> Object) formals)
        }
    | UnOpExpr(op, expr,_) ->
        {
            nodeLabel = (unOpStr op) + "e"
            children = [(expr :> Object, (), "e")]
            compFields = []
        }
    | BinOpExpr(op, expr1, expr2,_) ->
        {
            nodeLabel = "e0 " + (binOpStr op) + " e1"
            children = [(expr1 :> Object, (), "e0"); (expr2 :> Object, (), "e1")]
            compFields = []
        }
    | ParenthesizedExpr(expr,_) ->
        {
            nodeLabel = "( e )"
            children = [(expr :> Object, (), "e")]
            compFields = []
        }
    | NameExpr(name,_) ->
        {
            nodeLabel = name
            children = []
            compFields = [name]
        }
    | CallExpr(func, args, _) ->
        let argChildren = List.mapi (fun i a -> (a :> Object, (), "arg"+i.ToString())) args
        {
            nodeLabel = "func(arg0 arg1 ... argn)"
            children = (func :> Object, (), "func") :: []
            compFields = []
        }

let SyntaxBundle = new diffInfoBundle<Context>( () )
SyntaxBundle.AddDiffInfoFunc statementDiffInfo "green"
SyntaxBundle.AddDiffInfoFunc exprDiffInfo "cyan"
