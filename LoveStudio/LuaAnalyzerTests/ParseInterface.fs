﻿///
/// ParseInterface
///
module ParseInterface

open LuaAnalyzer.HandParser
open LuaAnalyzer.HandLexer
open LuaAnalyzer.Syntax
open Microsoft.FSharp.Text.Lexing
open System.IO

type ParseResult =
| Success of Syntax.Statement
| Failure of string

/// Lexes and parses a string containing a program, returning the ETerm for that
/// program if it is well formed and closed with respect to context ctx. 
/// 
/// If it is not well formed and closed with respect to context ctx, 
/// an exception is thrown.
let parseString (s : string) =
    try 
        Success (HandParser.parse s)
    with
        | ParseException(msg,rng) ->
            Failure(msg,rng)
  

/// Lexes and parses a file containing a program, returning the AST for that
/// program if it is well formed. If it is not well formed, an exception is
/// thrown.
let parseFile (fileName : string) =
    let inputFile = new StreamReader(fileName)
    let inputText = inputFile.ReadToEnd()
    
    parseString inputText