﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace LoveEdit
{
  /// <summary>
  /// A dialog which allow the user to perform searching and substitution
  /// operations on their project's source.
  /// 
  /// Instances of this class can be used to search within a single file 
  /// (when _targetFile is non-null) or within the entire project (when 
  /// _targetFile is null).
  /// 
  /// A dialog which contains two edit boxes:
  ///  Find -- Contains the text to search for.
  ///  Replace -- Contains the text which the matches of the "Find" edit box gets replaced with.
  ///  
  /// And contains four buttons:
  ///  Find All
  ///  Find Next
  ///  Replace All
  ///  Replace Next
  /// </summary>
  public partial class FindReplaceDialog : Form
  {
    #region Dependencies
    private readonly SearchResultsPanel _searchResultsPanel;
    #endregion

    #region Data Members

    /// <summary>
    /// If non-null, the file which we have chosen to do finding/replacing in.
    /// If null, the scope of our finding and replacing is the entire project.
    /// </summary>
    private readonly string _targetFile;

    #endregion

    /// <summary>
    /// Creates a new find replace dialog. 
    /// </summary>
    /// <param name="searchResultsPanel"></param>
    /// <param name="targetFile"></param>
    /// <param name="projFile"></param>
    public FindReplaceDialog(SearchResultsPanel searchResultsPanel, string targetFile, string projFile)
    {
      InitializeComponent();
      _searchResultsPanel = searchResultsPanel;
      _targetFile = targetFile;
      Text = "Find and Replace (" + projFile + ")"; 
    }

    /// <summary>
    /// Creates a new FindReplaceDialog.
    /// </summary>
    /// <param name="searchResultsPanel"></param>
    public FindReplaceDialog(SearchResultsPanel searchResultsPanel)
    {
      InitializeComponent();
      _searchResultsPanel = searchResultsPanel;
      _targetFile = null;
      Text = "Find and Replace (Entire Project)";
    }

    /// <summary>
    /// Key Controls:
    /// Return - Search for the text in the find box.
    /// Escape - Close the window without doing anything.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void _inputBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Return)
      {
        if (_findBox.Text.Length == 0)
        {
          Close();
          return;
        }

        _searchResultsPanel.Search(_findBox.Text,_targetFile);
        Close();
      }
      else if (e.KeyCode == Keys.Escape)
      {
        Close();
      }
    }

    /// <summary>
    /// Search for matches of the text contained in the find box. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void _findAllButton_Click(object sender, EventArgs e)
    {
      _searchResultsPanel.Search(_findBox.Text, _targetFile);
      Close();
    }

    /// <summary>
    /// Replace all matches of the text contained in the find box with
    /// the text contained in the replace box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void _replaceAllButton_Click(object sender, EventArgs e)
    {
      _searchResultsPanel.ReplaceAll(_findBox.Text,_replaceBox.Text, _targetFile);
      Close();
    }

    /// <summary>
    /// Open to the next match and perform a replacement.
    /// If no matches are currently being displayed, show an error popup.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void _replaceButton_Click(object sender, EventArgs e)
    {
      // Really, to handle this I need to first run search, recording
      // the ranges of each item to replace. Running search will help 
      // the user track their progress. Then, I just iterate through
      // the search box. I will probably need to add a "skip" or "find next"
      // button as well.

      if (!_searchResultsPanel.ShowingResults)
      {
        _searchResultsPanel.Search(_findBox.Text, _targetFile);
        _searchResultsPanel.GotoFirstMatch();
        Focus();
      }
      else if (_searchResultsPanel.ResultCount > 0)
      {
        _searchResultsPanel.ReplaceSelectionWith(_replaceBox.Text,_findBox.Text);
        Focus();
      }
      else
      {
        MessageBox.Show("No matches found.");
      }
    }

    /// <summary>
    /// When the text in the error Clear the current search results.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void _findBox_TextChanged(object sender, EventArgs e)
    {
      _searchResultsPanel.Clear();
    }

    private void _nextButton_Click(object sender, EventArgs e)
    {
      if (!_searchResultsPanel.ShowingResults)
      {
        _searchResultsPanel.Search(_findBox.Text, _targetFile);
        _searchResultsPanel.GotoFirstMatch();
        Focus();
      }
      else if(_searchResultsPanel.ResultCount > 0)
      {
        _searchResultsPanel.GotoNextMatch();
        Focus();
      }
      else
      {
        MessageBox.Show("No matches found.");
      }

      Focus();
    }

    private void _replaceBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
      {
        Close();
      }
    }
  }
}
