﻿namespace LoveEdit.Forms
{
  partial class OutputPanel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._outputTextBox = new ScintillaNet.Scintilla();
      ((System.ComponentModel.ISupportInitialize)(this._outputTextBox)).BeginInit();
      this.SuspendLayout();
      // 
      // _outputTextBox
      // 
      this._outputTextBox.BackColor = System.Drawing.SystemColors.ScrollBar;
      this._outputTextBox.CallTip.BackColor = System.Drawing.SystemColors.ScrollBar;
      this._outputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this._outputTextBox.IsReadOnly = true;
      this._outputTextBox.Location = new System.Drawing.Point(0, 0);
      this._outputTextBox.Name = "_outputTextBox";
      this._outputTextBox.Size = new System.Drawing.Size(356, 401);
      this._outputTextBox.Styles.BraceBad.FontName = "Verdana";
      this._outputTextBox.Styles.BraceLight.FontName = "Verdana";
      this._outputTextBox.Styles.CallTip.BackColor = System.Drawing.SystemColors.ScrollBar;
      this._outputTextBox.Styles.ControlChar.FontName = "Verdana";
      this._outputTextBox.Styles.Default.FontName = "Verdana";
      this._outputTextBox.Styles.IndentGuide.FontName = "Verdana";
      this._outputTextBox.Styles.LastPredefined.FontName = "Verdana";
      this._outputTextBox.Styles.LineNumber.FontName = "Verdana";
      this._outputTextBox.Styles.Max.FontName = "Verdana";
      this._outputTextBox.TabIndex = 0;
      this._outputTextBox.UseBackColor = true;
      // 
      // OutputPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._outputTextBox);
      this.Name = "OutputPanel";
      this.Size = new System.Drawing.Size(356, 401);
      ((System.ComponentModel.ISupportInitialize)(this._outputTextBox)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private ScintillaNet.Scintilla _outputTextBox;

  }
}
