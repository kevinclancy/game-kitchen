﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;
using System.Runtime.InteropServices;


namespace LoveEdit.Forms
{
 

  public partial class GamePanel : UserControl
  {
    [DllImport("user32.dll", SetLastError = true)]
    private static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

    [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
    private static extern IntPtr SetWindowLong(IntPtr hwnd, int nIndex, int dwNewLong);

    [DllImport("user32.dll", EntryPoint = "MoveWindow", SetLastError = true)]
    private static extern IntPtr MoveWindow(IntPtr hwnd, int x, int y, int w, int h, bool whatIsThis);


    private const int GWL_STYLE = (-16);
    private const int WS_VISIBLE = 0x10000000;

    IntPtr _mainWindowHandle;

    Process _runningProcess;

    public bool Active
    {
      get
      {
        return _mainWindowHandle != IntPtr.Zero;
      }
    }

    public GamePanel(DebugProxy proxy)
    {
      InitializeComponent();
      proxy.OnDisconnect += OnDisconnect;
    }

    internal void SetProcess(System.Diagnostics.Process p)
    {
      _runningProcess = p;
    }

    public void EmbedLoveProcess()
    {
      var p = _runningProcess;
      p.WaitForInputIdle();
      _mainWindowHandle = p.MainWindowHandle;

      SetParent(_mainWindowHandle, this.Handle);
      SetWindowLong(_mainWindowHandle, GWL_STYLE, WS_VISIBLE);
      MoveWindow(_mainWindowHandle, 0, 0, this.Width, this.Height, true);
    }

    private void RemoveProcess()
    {
      if (_mainWindowHandle != IntPtr.Zero)
        _mainWindowHandle = IntPtr.Zero;

      _runningProcess = null;
    }

    public void OnDisconnect()
    {
      RemoveProcess(); 
    }
  }
}
