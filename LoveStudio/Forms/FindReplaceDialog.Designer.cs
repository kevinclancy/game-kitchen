﻿namespace LoveEdit
{
  partial class FindReplaceDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindReplaceDialog));
      this._findBox = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this._replaceBox = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this._replaceAllButton = new System.Windows.Forms.Button();
      this._replaceButton = new System.Windows.Forms.Button();
      this._findAllButton = new System.Windows.Forms.Button();
      this._nextButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // _findBox
      // 
      this._findBox.Location = new System.Drawing.Point(16, 27);
      this._findBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this._findBox.Name = "_findBox";
      this._findBox.Size = new System.Drawing.Size(503, 22);
      this._findBox.TabIndex = 0;
      this._findBox.TextChanged += new System.EventHandler(this._findBox_TextChanged);
      this._findBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this._inputBox_KeyDown);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 7);
      this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 17);
      this.label1.TabIndex = 1;
      this.label1.Text = "Find";
      // 
      // _replaceBox
      // 
      this._replaceBox.Location = new System.Drawing.Point(16, 94);
      this._replaceBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this._replaceBox.Name = "_replaceBox";
      this._replaceBox.Size = new System.Drawing.Size(503, 22);
      this._replaceBox.TabIndex = 2;
      this._replaceBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this._replaceBox_KeyDown);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 74);
      this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(92, 17);
      this.label2.TabIndex = 3;
      this.label2.Text = "Replace With";
      // 
      // _replaceAllButton
      // 
      this._replaceAllButton.Location = new System.Drawing.Point(133, 127);
      this._replaceAllButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this._replaceAllButton.Name = "_replaceAllButton";
      this._replaceAllButton.Size = new System.Drawing.Size(100, 36);
      this._replaceAllButton.TabIndex = 4;
      this._replaceAllButton.Text = "Replace &All";
      this._replaceAllButton.UseVisualStyleBackColor = true;
      this._replaceAllButton.Click += new System.EventHandler(this._replaceAllButton_Click);
      // 
      // _replaceButton
      // 
      this._replaceButton.Location = new System.Drawing.Point(241, 127);
      this._replaceButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this._replaceButton.Name = "_replaceButton";
      this._replaceButton.Size = new System.Drawing.Size(111, 34);
      this._replaceButton.TabIndex = 5;
      this._replaceButton.Text = "&Replace";
      this._replaceButton.UseVisualStyleBackColor = true;
      this._replaceButton.Click += new System.EventHandler(this._replaceButton_Click);
      // 
      // _findAllButton
      // 
      this._findAllButton.Location = new System.Drawing.Point(16, 126);
      this._findAllButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this._findAllButton.Name = "_findAllButton";
      this._findAllButton.Size = new System.Drawing.Size(109, 36);
      this._findAllButton.TabIndex = 6;
      this._findAllButton.Text = "&Find All";
      this._findAllButton.UseVisualStyleBackColor = true;
      this._findAllButton.Click += new System.EventHandler(this._findAllButton_Click);
      // 
      // _nextButton
      // 
      this._nextButton.Location = new System.Drawing.Point(360, 127);
      this._nextButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this._nextButton.Name = "_nextButton";
      this._nextButton.Size = new System.Drawing.Size(109, 34);
      this._nextButton.TabIndex = 7;
      this._nextButton.Text = "&Next";
      this._nextButton.UseVisualStyleBackColor = true;
      this._nextButton.Click += new System.EventHandler(this._nextButton_Click);
      // 
      // FindReplaceDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(536, 166);
      this.Controls.Add(this._nextButton);
      this.Controls.Add(this._findAllButton);
      this.Controls.Add(this._replaceButton);
      this.Controls.Add(this._replaceAllButton);
      this.Controls.Add(this.label2);
      this.Controls.Add(this._replaceBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this._findBox);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.Name = "FindReplaceDialog";
      this.Text = "Find and Replace";
      this.TopMost = true;
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox _findBox;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox _replaceBox;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button _replaceAllButton;
    private System.Windows.Forms.Button _replaceButton;
    private System.Windows.Forms.Button _findAllButton;
    private System.Windows.Forms.Button _nextButton;
  }
}