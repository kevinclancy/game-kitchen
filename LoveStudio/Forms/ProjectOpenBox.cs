﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using LuaAnalyzer;

namespace LoveEdit
{
  /// <summary>
  /// A dialog, triggered by pressing CTRL-O, which conveniently allows the user 
  /// to select a file from the project to open.
  /// 
  /// Specifically, it is a form containing an edit box and a list box. The 
  /// list is populated with the names of all files in the project, and the 
  /// user may select one by either navigating to it, or (preferrably) typing 
  /// it into the edit box and pressing enter.
  /// </summary>
  public partial class ProjectOpenBox : Form
  {
    #region Dependencies
    Studio _studio;
    DebugProxy _debugProxy;

    #endregion

    #region Enums
    public enum Type
    {
      OPEN,
      REQUIRE
    };
    #endregion

    #region Data Members
    /// <summary>
    /// A snapshot of the filesystem at the time this popup was created.
    /// </summary>
    private readonly DirectoryInfo _dir;
    
    private readonly Type _type;

    /// <summary>
    /// This timer is restarted when an item in the module list is selected.
    /// If, after a short interval, no other module is selected, 
    /// we extract information from the selected module.
    /// 
    /// Extracting module information immediately would cause noticeable 
    /// unresponsiveness.
    /// </summary>
    private readonly Timer _selectTimer;

    #endregion

    #region GUI Events

    /// <summary>
    /// When the text box is changed, this ensures that the list box is listing
    /// only files with prefixes which match the contents of the text box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox1_TextChanged(object sender, EventArgs e)
    {
      RefreshModuleList();
    }

    /// <summary>
    /// Controls:
    /// Up Arrow - move list selection up
    /// Down Arrow - move list selection down
    /// Escape - close project open box
    /// Return - open file corresponding to current list selection
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ProjectOpenBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Down && listBox1.Items.Count > 0)
      {
        listBox1.SelectedIndex = (listBox1.SelectedIndex + 1) % listBox1.Items.Count;
        e.SuppressKeyPress = true;
      }
      else if (e.KeyCode == Keys.Up && listBox1.Items.Count > 0)
      {
        listBox1.SelectedIndex = (listBox1.SelectedIndex <= 0) ? listBox1.Items.Count - 1 : listBox1.SelectedIndex - 1;
        e.SuppressKeyPress = true;
      }
      else if (e.KeyCode == Keys.Escape)
      {
        Close();
      }
      else if (e.KeyCode == Keys.Return)
      {
        if (listBox1.SelectedItem != null)
          OpenSelectedModule();
      }
      else if (e.KeyCode == Keys.Delete)
      {
        if (_debugProxy.Connected)
        {
          MessageBox.Show("Cannot delete file while game is running.");
          return;
        }

        if (listBox1.SelectedItem != null)
        {
          DialogResult result = MessageBox.Show(
            "Are you sure you would like to delete " + listBox1.SelectedItem + "?", 
            "Delete Module", 
            MessageBoxButtons.YesNo
          );

          if (result == DialogResult.Yes)
          {
            string file = _studio.CurrentProject.toFileName((string)listBox1.SelectedValue);

            try
            {
              ProjectChecker.invalidateCachedModule(_studio.CurrentProject.toModuleName(file));
              System.IO.File.Delete(file);
            }
            catch(Exception)
            {
            }

            RefreshModuleList();
          }
        }
      }
    }

    /// <summary>
    /// Double clicking the box opens the current list selection.
    /// 
    /// TODO: We should only do this when the user actually clicks an item,
    /// this will act when the user clicks anywhere in the box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listBox1_DoubleClick(object sender, EventArgs e)
    {
      if (listBox1.SelectedItem != null)
        OpenSelectedModule();
    }
    #endregion

    /// <summary>
    /// Create a new project open box.
    /// </summary>
    /// <param name="studio">The main love studio form.</param>
    public ProjectOpenBox(Studio studio, DebugProxy proxy, Type type)
    {
      InitializeComponent();

      _type = type;
      _studio = studio;
      _debugProxy = proxy;

      _dir = new System.IO.DirectoryInfo(_studio.CurrentProject.RootPath);

      _selectTimer = new Timer();
      _selectTimer.Interval = 250;
      _selectTimer.Tick += _selectTimer_Tick;



      if (_type == Type.OPEN)
      {
        Random rnd = new Random();
        int x = rnd.Next(20);

        if (x == 0)
          Text = "♒ Open From Project ♒";
        else if (x == 1)
          Text = "☹ Open From Project ☹";
        else if (x == 2)
          Text = "☺ Open From Project ☺";
        else
          Text = "Open From Project";
      }
      else if (_type == Type.REQUIRE)
      {
        Random rnd = new Random();
        int x = rnd.Next(20);

        if (x == 0)
          Text = "☢ Require From Project ☢";
        else if (x == 1)
          Text = "♪♫ Require From Project ♫♩";
        else if (x == 2)
          Text = "⛺ Require From Project ⛺";
        else
          Text = "Require From Project";
      }

      RefreshModuleList();
    }

    private void _selectTimer_Tick(object sender, EventArgs e)
    {
      if (listBox1.SelectedItem == null)
      {
        //we must have unselected
        _selectTimer.Stop();
        return;
      }
      string modName = (string)listBox1.SelectedItem;

      string fileName = _studio.CurrentProject.toFileName(modName);
      StreamReader reader = new StreamReader(fileName);
      string contents = reader.ReadToEnd();
      reader.Close();
      
      _moduleNameLabel.Text = "Selected Module: " + modName;
      _moduleDescriptionBox.Text = LuaAnalyzer.Analyzer.getModuleDescription(contents);
      
      _selectTimer.Stop();
    }

    ///
    ///

    /// <summary>
    /// Ensures that the list contains only files which contain a prefix matching
    /// the current contents of the text box.
    /// </summary>
    private void RefreshModuleList()
    {
      IEnumerable<string> moduleList;

      // This method assumes that the application has discovery permissions
      // for all folders under the specified path.
      // TODO: what if it doesn't?
      try
      {
        //TODO: need to figure out how .NET patterns work so that I can
        //exclude luaproj files from this match.
        moduleList = from file in _dir.GetFiles("*",SearchOption.AllDirectories)
                     where file.FullName.EndsWith(".lua")
                     let modName = _studio.CurrentProject.toModuleName(file.FullName)
                     let shortName = modName.Split('.').Last()
                     where shortName.StartsWith(textBox1.Text) || modName.StartsWith(textBox1.Text)
                     select modName;
      }
      catch (Exception exception)
      {
        MessageBox.Show(exception.Message);
        return;
      }

      listBox1.DataSource = moduleList.ToList();
    }

    /// <summary>
    /// Opens the selected file and closes the open file dialog.
    /// </summary>
    private void OpenSelectedModule()
    {
      // Contract.Requires(listBox1.SelectedItem != null);
      string modName = (string)listBox1.SelectedItem;

      if (_type == Type.OPEN)
      {
        _studio.OpenFile(_studio.CurrentProject.toFileName(modName), true);
      }
      else if (_type == Type.REQUIRE)
      {
        string currentFileName = _studio.FileEditorMap[_studio.CurrentEditor];
        if (_debugProxy.Connected && !_debugProxy.CanEdit(currentFileName))
        {
          MessageBox.Show("Source files cannot be changed while game is running.");
          return;
        }

        var shortName = modName.Split('.').Last();
        
        string contents = _studio.CurrentEditor.Text;
        var insertPos = LuaAnalyzer.Analyzer.getRequireInsertPos(contents);
        _studio.CurrentEditor.InsertText(insertPos, "local " + shortName + " = require '" + modName + "'\n");
      }

      Close();
    }

    private void listBox1_SelectedValueChanged(object sender, EventArgs e)
    {

      if (listBox1.SelectedItem != null)
      {
        // if the item is non-null, a new item has been selected, 
        // and we should start a timer which triggers its information to
        // be displayed.
        _selectTimer.Stop();
        _selectTimer.Start();
      }
      else
        // otherwise, the current item has been unselected, so we need to
        // stop the timer
        _selectTimer.Stop();
    }
  }
}
