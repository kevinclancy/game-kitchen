﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoveEdit
{
  /// <summary>
  /// A panel which, when the debugger is breaking, displays a list in 
  /// which each item represents a frame in the lua call stack on the target.
  /// Selecting a frame in this list causes the debugger to display 
  /// information about that frame.
  /// </summary>
  public partial class StackPanel : UserControl
  {

    #region Delegates

    /// <summary>
    /// Called when the user selects a stack frame using the stack panel.
    /// </summary>
    /// <param name="fileName">The name of the file containing the function of the frame.</param>
    /// <param name="lineNum">The line number of the current line being executed in the frame.</param>
    /// <param name="stackLevel">The level of the stack frame, where 0 being the leaf and i+1 is the caller of i.</param>
    /// <param name="clearContents">Whether or not to clear the contents of the debug browser.</param>
    public delegate void SelectFrameDelegate(string fileName, int lineNum, int stackLevel, bool clearContents);
    
    /// <summary>
    /// Called when the user selects a stack frame using the stack panel.
    /// </summary>
    public event SelectFrameDelegate SelectFrame;

    //TODO: it's a bit awkward that we have a clearContents argument for SelectFrame.

    /// <summary>
    /// When the user selects a stack frame, this is called with information
    /// about the previously selected frame in order to unselect it.
    /// </summary>
    /// <param name="fileName">The name of the file containing the function of the previously selected frame.</param>
    /// <param name="lineNum">The number of the current executing line in the previously selected frame.</param>
    public delegate void UnselectFrameDelegate(string fileName, int lineNum);
    
    /// <summary>
    /// When the user selects a stack frame, this is called with information
    /// about the previously selected frame in order to unselect it.   
    /// </summary>
    public event UnselectFrameDelegate UnselectFrame;
    #endregion

    #region Data Members
    /// <summary>
    /// If the debugger is breaking, the current call stack of the target's lua state. 
    /// Otherwise, null.
    /// </summary>
    LuaStackFrame[] _frames;
    /// <summary>
    /// If the debugger is breaking, this is the frame the debugger gui is 
    /// currently inspecting. Otherwise, it is null.
    /// </summary>
    LuaStackFrame _currentFrame;
    #endregion

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  Contract.Invariant(_frames == null || _frames.Contains(_currentFrame));
    //  Contract.Invariant(
    //    _frames == null || 
    //    Contract.ForAll<LuaStackFrame>(_frames, (frame) => frame != null)
    //  );
    //}

    /// <summary>
    /// Constructs a new stack panel.
    /// </summary>
    public StackPanel()
    {
      InitializeComponent();
      _frames = null;
      _currentFrame = null;
    }

    /// <summary>
    /// Populates the stack panel with the given stack and selects the stack
    /// frame at index 0.
    /// </summary>
    /// <param name="frames">
    ///   An array of all frames in the stack, where index 0 represents
    ///   the top frame.
    /// </param>
    /// <param name="stepOver">
    ///   Whether or not we are stepping over
    /// </param>
    public void SetFrames(LuaStackFrame[] frames, bool stepOver)
    {
      // Contract.Requires(frames != null);
      // Contract.Requires(Contract.ForAll<LuaStackFrame>(frames, (frame) => frame != null));

      _frames = frames;

      for (int i = 0; i < _frames.Length; ++i)
      {
        listView.Items.Add(
          new ListViewItem(
            new string[] { _frames[i].funcName, _frames[i].lineNumber.ToString(), _frames[i].fileName }
          )
        );
      }

      _currentFrame = _frames[0];
      SelectFrame(_currentFrame.fileName, _currentFrame.lineNumber, _currentFrame.stackLevel, stepOver);
    }

    /// <summary>
    /// Unselect the current frame, remove all items from the stack panel's 
    /// list of frames so that it no longer displays a stack.
    /// </summary>
    public void ClearFrames()
    {
      // Contract.Assert(_frames != null);

      if (_currentFrame != null)
        UnselectFrame(_currentFrame.fileName, _currentFrame.lineNumber);

      listView.Items.Clear();
      _frames = null;
      _currentFrame = null;
    }

    /// <summary>
    /// When the user selects a frame from the list, this is called in order
    /// to call SelectFrame for the newly selected frame and UnselectFrame
    /// for the previous one.
    /// 
    /// When the user unselects the currently selected frame, this is triggers
    /// only the UnselectFrame event.
    /// </summary>
    /// <param name="sender">sender</param>
    /// <param name="e">event args</param>
    private void listView_SelectedIndexChanged(object sender, EventArgs e)
    {
      // Contract.Assert(listView.SelectedIndices.Count <= 1);

      if (_currentFrame != null)
        UnselectFrame(_currentFrame.fileName, _currentFrame.lineNumber);

      if (listView.SelectedIndices.Count == 0)
        return;

      int level = listView.SelectedIndices[0];
      _currentFrame = _frames[level];
      SelectFrame(_currentFrame.fileName, _currentFrame.lineNumber, _currentFrame.stackLevel, true);
    }
  }
}
