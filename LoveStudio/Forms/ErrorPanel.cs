﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using WinControls.ListView;
using LuaAnalyzer;

namespace LoveEdit
{
  /// <summary>
  /// A panel which lists all statically detected errors. Clicking on
  /// an error in the list will open the file containing the error to
  /// the line of the error.
  /// </summary>
  public partial class ErrorPanel : UserControl
  {
    #region Dependencies
    Studio _studio;

    /// <summary>
    /// Maps an error item to the position in its file which
    /// it occurs.
    /// </summary>
    Dictionary<TreeListNode, int> _errorPosMap;
    #endregion

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  Contract.Invariant(_errorPosMap.Count == _errorList.Nodes.Count);
    //}

    /// <summary>
    /// Creates a new error panel.
    /// </summary>
    /// <param name="studio"></param>
    public ErrorPanel(Studio studio)
    {
      InitializeComponent();

      _studio = studio;
      _errorPosMap = new Dictionary<TreeListNode, int>();
    }

    private int GetLineOf(string fileName, int pos)
    {
      StreamReader reader = new StreamReader(fileName);
      string text = reader.ReadToEnd();
      reader.Close();

      var parseResult = HandParser.parse(text,false,fileName);
      var ast = parseResult.Item1;
      var boundaries = parseResult.Item2;

      return -boundaries.BinarySearch(pos) - 2;
    }

    public void AddErrorsFromList(List<Tuple<string, string, Tuple<int, int>>> errorList)
    {
      foreach (var item in errorList)
      {
        string fileName = item.Item1;
        string errorMessage = item.Item2;
        int rangeStart = item.Item3.Item1;
        int rangeFin = item.Item3.Item2;

        TreeListNode newNode = new TreeListNode();
        newNode.Text = _studio.CurrentProject.projRelative(fileName);

        int errorLine = GetLineOf(fileName, rangeStart);

        newNode.SubItems.Add(
          new ContainerListViewObject.ContainerListViewSubItem((errorLine).ToString())
        );

        newNode.SubItems.Add(
          new ContainerListViewObject.ContainerListViewSubItem(errorMessage.Trim())
        );

        _errorList.Nodes.Add(newNode);
        _errorPosMap[newNode] = rangeStart;
      }
    }

    /// <summary>
    /// Extracts all errors from the given source file and adds them to the
    /// error list.
    /// </summary>
    /// <param name="filename">The file to add errors from.</param>
    /// <param name="includeAnnotationErrors">Whether or not to include annotation parsing errors.</param>
    public void AddErrorsFrom(string filename, bool includeAnnotationErrors)
    {
      StreamReader reader = new StreamReader(filename);
      string text = reader.ReadToEnd();

      try
      {
        var parseResult = HandParser.parse(text,false,filename);
        var ast = parseResult.Item1;
        var boundaries = parseResult.Item2;

        var errors = ErrorList.listErrors(ast, includeAnnotationErrors);

        foreach (var error in errors)
        {
          TreeListNode newNode = new TreeListNode();
          newNode.Text = _studio.CurrentProject.projRelative(filename);

          int errorPos = error.Item2.Item1;
          int errorLine = -boundaries.BinarySearch(errorPos) - 2;

          newNode.SubItems.Add(
            new ContainerListViewObject.ContainerListViewSubItem((errorLine).ToString())
          );

          newNode.SubItems.Add(
            new ContainerListViewObject.ContainerListViewSubItem(error.Item1)
          );

          _errorList.Nodes.Add(newNode);
          _errorPosMap[newNode] = errorPos;
        }
      }
      catch (HandLexer.LexError le)
      {
        TreeListNode newNode = new TreeListNode();
        newNode.Text = _studio.CurrentProject.projRelative(filename);

        int errorPos = le.Data1;
        int errorLine = le.Data2;

        newNode.SubItems.Add(
          new ContainerListViewObject.ContainerListViewSubItem(errorLine.ToString())
        );

        newNode.SubItems.Add(
          new ContainerListViewObject.ContainerListViewSubItem(le.Data0)
        );

        _errorList.Nodes.Add(newNode);
        _errorPosMap[newNode] = errorPos;
      }

      reader.Close();
    }

    /// <summary>
    /// Extracts all errors from the project source and displays them
    /// in the error list. Opens the first listed error (if any) in a source
    /// editor.
    /// </summary>
    public void DetectErrors()
    {
      var files = _studio.CurrentProject.GetSourceFiles();

      _errorList.Nodes.Clear();
      _errorPosMap.Clear();
      
      var errors = Analyzer.getProjectErrors(
        _studio.CurrentProject.GetModules(),
        _studio.CurrentProject.RootPath,
        _studio.CurrentProject.TypeCheckingEnabled
      );

      AddErrorsFromList(errors.Item1);

      if (_errorList.Nodes.Count > 0)
      {
        var item = _errorList.Nodes[0];
        int line = Int32.Parse(item.SubItems[0].Text);
        string fileName = item.Text;

        _studio.OpenFileToLine(_studio.CurrentProject.projNameToFileSystem(fileName), line);
      }
    }

    /// <summary>
    /// Returns true if any errors are currently contained in the error list.
    /// </summary>
    /// <returns></returns>
    // [Pure]
    public bool HasErrors()
    {
      return _errorList.Nodes.Count > 0;
    }

    /// <summary>
    /// If an item in the error list is selected, opens the file containing
    /// the error to the line of the error.
    /// 
    /// Otherwise, does nothing.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DoubleClickPanel(object sender, EventArgs e)
    {
      if (_errorList.SelectedItems.Count == 0)
        return;

      var item = _errorList.SelectedItems[0];
      int line = Int32.Parse(item.SubItems[0].Text);
      string fileName = item.Text;

      _studio.OpenFileToLine(_studio.CurrentProject.projNameToFileSystem(fileName), line);
    }

  }
}
