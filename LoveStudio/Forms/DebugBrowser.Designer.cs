﻿namespace LoveEdit
{
  partial class DebugBrowser
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.listView = new WinControls.ListView.TreeListView();
      this.keyColumnHeader = new WinControls.ListView.ContainerColumnHeader();
      this.valueColumnHeader = new WinControls.ListView.ContainerColumnHeader();
      this.typeColumnHeader = new WinControls.ListView.ContainerColumnHeader();
      this.SuspendLayout();
      // 
      // listView
      // 
      this.listView.Columns.AddRange(new WinControls.ListView.ContainerColumnHeader[] {
            this.keyColumnHeader,
            this.valueColumnHeader,
            this.typeColumnHeader});
      this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.listView.Location = new System.Drawing.Point(0, 0);
      this.listView.Name = "listView";
      this.listView.Size = new System.Drawing.Size(621, 406);
      this.listView.TabIndex = 0;
      this.listView.AfterExpand += new WinControls.ListView.Delegates.TreeListViewEventHandler(this.listView_AfterExpand);
      this.listView.BeforeCollapse += new WinControls.ListView.Delegates.TreeListViewCancelEventHandler(this.listView_BeforeCollapse);
      // 
      // keyColumnHeader
      // 
      this.keyColumnHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.keyColumnHeader.Text = "Key";
      this.keyColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      // 
      // valueColumnHeader
      // 
      this.valueColumnHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.valueColumnHeader.Text = "Value";
      this.valueColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      // 
      // typeColumnHeader
      // 
      this.typeColumnHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.typeColumnHeader.Text = "Type";
      this.typeColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.typeColumnHeader.Width = 10000;
      // 
      // DebugBrowser
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.listView);
      this.Name = "DebugBrowser";
      this.Size = new System.Drawing.Size(621, 406);
      this.ResumeLayout(false);

    }

    #endregion

    private WinControls.ListView.TreeListView listView;
    private WinControls.ListView.ContainerColumnHeader keyColumnHeader;
    private WinControls.ListView.ContainerColumnHeader valueColumnHeader;
    private WinControls.ListView.ContainerColumnHeader typeColumnHeader;
  }
}
