﻿namespace LoveEdit.Forms
{
  partial class ProjectSettingsDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectSettingsDialog));
      this._enableTypeCheckingCheckBox = new System.Windows.Forms.CheckBox();
      this._defaultModuleTypeComboBox = new System.Windows.Forms.ComboBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this._applyButton = new System.Windows.Forms.Button();
      this._cancelButton = new System.Windows.Forms.Button();
      this._embedInGamePanelBox = new System.Windows.Forms.CheckBox();
      this._ingoreClassSystemCodeBox = new System.Windows.Forms.CheckBox();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // _enableTypeCheckingCheckBox
      // 
      this._enableTypeCheckingCheckBox.AutoSize = true;
      this._enableTypeCheckingCheckBox.Location = new System.Drawing.Point(16, 85);
      this._enableTypeCheckingCheckBox.Margin = new System.Windows.Forms.Padding(4);
      this._enableTypeCheckingCheckBox.Name = "_enableTypeCheckingCheckBox";
      this._enableTypeCheckingCheckBox.Size = new System.Drawing.Size(172, 21);
      this._enableTypeCheckingCheckBox.TabIndex = 0;
      this._enableTypeCheckingCheckBox.Text = "Enable Type Checking";
      this._enableTypeCheckingCheckBox.UseVisualStyleBackColor = true;
      // 
      // _defaultModuleTypeComboBox
      // 
      this._defaultModuleTypeComboBox.FormattingEnabled = true;
      this._defaultModuleTypeComboBox.Location = new System.Drawing.Point(8, 21);
      this._defaultModuleTypeComboBox.Margin = new System.Windows.Forms.Padding(4);
      this._defaultModuleTypeComboBox.Name = "_defaultModuleTypeComboBox";
      this._defaultModuleTypeComboBox.Size = new System.Drawing.Size(289, 24);
      this._defaultModuleTypeComboBox.TabIndex = 1;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this._defaultModuleTypeComboBox);
      this.groupBox1.Location = new System.Drawing.Point(16, 15);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
      this.groupBox1.Size = new System.Drawing.Size(315, 63);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Default Module Type";
      // 
      // _applyButton
      // 
      this._applyButton.Location = new System.Drawing.Point(13, 176);
      this._applyButton.Margin = new System.Windows.Forms.Padding(4);
      this._applyButton.Name = "_applyButton";
      this._applyButton.Size = new System.Drawing.Size(147, 50);
      this._applyButton.TabIndex = 3;
      this._applyButton.Text = "Apply";
      this._applyButton.UseVisualStyleBackColor = true;
      this._applyButton.Click += new System.EventHandler(this._applyButton_Click);
      // 
      // _cancelButton
      // 
      this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._cancelButton.Location = new System.Drawing.Point(171, 176);
      this._cancelButton.Margin = new System.Windows.Forms.Padding(4);
      this._cancelButton.Name = "_cancelButton";
      this._cancelButton.Size = new System.Drawing.Size(160, 49);
      this._cancelButton.TabIndex = 4;
      this._cancelButton.Text = "Cancel";
      this._cancelButton.UseVisualStyleBackColor = true;
      this._cancelButton.Click += new System.EventHandler(this.cancel_Click);
      // 
      // _embedInGamePanelBox
      // 
      this._embedInGamePanelBox.AutoSize = true;
      this._embedInGamePanelBox.Location = new System.Drawing.Point(16, 113);
      this._embedInGamePanelBox.Name = "_embedInGamePanelBox";
      this._embedInGamePanelBox.Size = new System.Drawing.Size(171, 21);
      this._embedInGamePanelBox.TabIndex = 5;
      this._embedInGamePanelBox.Text = "Embed in Game Panel";
      this._embedInGamePanelBox.UseVisualStyleBackColor = true;
      // 
      // _ingoreClassSystemCodeBox
      // 
      this._ingoreClassSystemCodeBox.AutoSize = true;
      this._ingoreClassSystemCodeBox.Location = new System.Drawing.Point(16, 140);
      this._ingoreClassSystemCodeBox.Name = "_ingoreClassSystemCodeBox";
      this._ingoreClassSystemCodeBox.Size = new System.Drawing.Size(309, 21);
      this._ingoreClassSystemCodeBox.TabIndex = 6;
      this._ingoreClassSystemCodeBox.Text = "Ignore Class System Code When Debugging";
      this._ingoreClassSystemCodeBox.UseVisualStyleBackColor = true;
      // 
      // ProjectSettingsDialog
      // 
      this.AcceptButton = this._applyButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this._cancelButton;
      this.ClientSize = new System.Drawing.Size(343, 234);
      this.Controls.Add(this._ingoreClassSystemCodeBox);
      this.Controls.Add(this._embedInGamePanelBox);
      this.Controls.Add(this._cancelButton);
      this.Controls.Add(this._applyButton);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this._enableTypeCheckingCheckBox);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Margin = new System.Windows.Forms.Padding(4);
      this.Name = "ProjectSettingsDialog";
      this.Text = "ProjectSettingsDialog";
      this.Load += new System.EventHandler(this.ProjectSettingsDialog_Load);
      this.groupBox1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.CheckBox _enableTypeCheckingCheckBox;
    private System.Windows.Forms.ComboBox _defaultModuleTypeComboBox;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button _applyButton;
    private System.Windows.Forms.Button _cancelButton;
    private System.Windows.Forms.CheckBox _embedInGamePanelBox;
    private System.Windows.Forms.CheckBox _ingoreClassSystemCodeBox;
  }
}