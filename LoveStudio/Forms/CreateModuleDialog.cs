﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoveEdit.Forms
{
  public partial class CreateModuleDialog : Form
  {
    ModuleConstructors _moduleConstructors;
    IModuleCreator _selectedModuleCreator;
    Project _project;

    public CreateModuleDialog(ModuleConstructors moduleConstructors, Project project)
    {
      InitializeComponent();
      _moduleConstructors = moduleConstructors;
      _selectedModuleCreator = null;
      _project = project;
    }

    private void CreateModuleDialog_Load(object sender, EventArgs e)
    {
      foreach (IModuleCreator moduleConstructor in _moduleConstructors.GetAllModuleCreators())
      {
        _moduleConstructorComboBox.Items.Add(moduleConstructor.GetName());

        if (moduleConstructor.GetName() == _project.DefaultModuleType)
          _moduleConstructorComboBox.SelectedIndex = _moduleConstructorComboBox.Items.Count - 1;
      }

      if (_moduleConstructorComboBox.SelectedItem == null)
        _moduleConstructorComboBox.SelectedItem = _moduleConstructorComboBox.Items[0];

      SelectItem((string)_moduleConstructorComboBox.SelectedItem);
    }

    private void SelectItem(string name)
    {
      if (!_moduleConstructors.HasModuleCreator(name))
        return;

      panel1.Controls.Clear();

      _selectedModuleCreator = _moduleConstructors.GetModuleCreatorByName(name);
      panel1.Controls.Add(_selectedModuleCreator.GetControl());
    }

    private void comboBox1_TextChanged(object sender, EventArgs e)
    {
      SelectItem(_moduleConstructorComboBox.SelectedItem.ToString());
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (_selectedModuleCreator.CreateNewModuleFromControl())
        Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
