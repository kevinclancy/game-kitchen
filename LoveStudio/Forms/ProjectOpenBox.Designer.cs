﻿namespace LoveEdit
{
  partial class ProjectOpenBox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectOpenBox));
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this._moduleDescriptionBox = new System.Windows.Forms.RichTextBox();
      this._moduleNameLabel = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(12, 12);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(453, 20);
      this.textBox1.TabIndex = 0;
      this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
      // 
      // listBox1
      // 
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new System.Drawing.Point(12, 35);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(453, 303);
      this.listBox1.TabIndex = 1;
      this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
      this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
      // 
      // _moduleDescriptionBox
      // 
      this._moduleDescriptionBox.Location = new System.Drawing.Point(471, 35);
      this._moduleDescriptionBox.Name = "_moduleDescriptionBox";
      this._moduleDescriptionBox.ReadOnly = true;
      this._moduleDescriptionBox.Size = new System.Drawing.Size(547, 300);
      this._moduleDescriptionBox.TabIndex = 2;
      this._moduleDescriptionBox.Text = "";
      // 
      // _moduleNameLabel
      // 
      this._moduleNameLabel.AutoSize = true;
      this._moduleNameLabel.Location = new System.Drawing.Point(469, 12);
      this._moduleNameLabel.Name = "_moduleNameLabel";
      this._moduleNameLabel.Size = new System.Drawing.Size(104, 13);
      this._moduleNameLabel.TabIndex = 3;
      this._moduleNameLabel.Text = "No Module Selected";
      // 
      // ProjectOpenBox
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1030, 357);
      this.Controls.Add(this._moduleNameLabel);
      this.Controls.Add(this._moduleDescriptionBox);
      this.Controls.Add(this.listBox1);
      this.Controls.Add(this.textBox1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.KeyPreview = true;
      this.Name = "ProjectOpenBox";
      this.Text = "ProjectOpenBox";
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ProjectOpenBox_KeyDown);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.RichTextBox _moduleDescriptionBox;
    private System.Windows.Forms.Label _moduleNameLabel;
  }
}