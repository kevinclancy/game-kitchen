﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Text.RegularExpressions;
using WinControls.ListView;
using LuaAnalyzer;

namespace LoveEdit
{
  /// <summary>
  /// A panel containing a list of search results. When the user performs a
  /// search, using the FindReplaceDialog, the list is populated with items
  /// corresponding to occurrences of string searched for. Each item in the list
  /// contains three columns: file name, line number, and line contents.
  /// </summary>
  public partial class SearchResultsPanel : UserControl
  {
    #region Dependencies
    private readonly Studio _studio;
    #endregion

    #region Data Memebers
    
    /// <summary>
    /// Maps entries in the match list to structures which
    /// contain the positions of the matches.
    /// </summary>
    private readonly Dictionary<TreeListNode, MatchPos> _matchMap;

    /// <summary>
    /// Whether or not the list is currently displaying the results
    /// of a (possibly empty) search.
    /// </summary>
    private bool _showingResults;

    #endregion

    #region Properties

    /// <summary>
    /// Whether or not the list is currently displaying the results
    /// of a (possibly empty) search.
    /// </summary>
    public bool ShowingResults { get { return _showingResults; } }

    /// <summary>
    /// The number of search results currently being displayed in the list.
    /// </summary>
    public int ResultCount { get { return _treeListView.Nodes.Count; } }

    #endregion

    /// <summary>
    /// Contains information about the location of a piece of code in the 
    /// project which matches a search query.
    /// </summary>
    class MatchPos
    {
      public MatchPos(string fileName, int start, int length)
      {
        this.fileName = fileName;
        this.start = start;
        this.length = length;
      }

      /// <summary>
      /// The name of the file containing the match.
      /// </summary>
      public string fileName;

      /// <summary>
      /// The start position of the match.
      /// </summary>
      public int start;

      /// <summary>
      /// The length of the match.
      /// </summary>
      public int length;
    }

    /// <summary>
    /// Creates a new search results panel.
    /// </summary>
    /// <param name="studio"></param>
    public SearchResultsPanel(Studio studio)
    {
      InitializeComponent();

      _matchMap = new Dictionary<TreeListNode, MatchPos>();
      _studio = studio;
      _showingResults = true;
    }

    /// <summary>
    /// Replaces all occurrences of a string with occurrences of another string,
    /// either in the entire project or in a specified file.
    /// </summary>
    /// <param name="target">The string to search for.</param>
    /// <param name="replacement">The string to replace the target with.</param>
    /// <param name="targetFile">The file to form the replacements in. (Or null to indicate the entire project.) </param>
    public void ReplaceAll(string target, string replacement, string targetFile)
    {
      var files = _studio.CurrentProject.GetSourceFiles();

      Regex regex = new Regex(Regex.Escape(target));
      
      int count = 0;

      foreach (FileInfo fileInfo in files)
      {
        if (targetFile != null && fileInfo.FullName != targetFile)
          continue;

        string fileName = fileInfo.FullName;

        if (_studio.FileEditorMap.Contains(fileName))
        {
          string text = _studio.FileEditorMap[fileName].Text;

          MatchCollection matches = regex.Matches(text);
          count += matches.Count;
          string replaced = text.Replace(target, replacement);

          _studio.FileEditorMap[fileName].Text = replaced;
        }
        else
        {
          StreamReader reader = new StreamReader(fileName);
          string text = reader.ReadToEnd();
          reader.Close();

          MatchCollection matches = regex.Matches(text);
          count += matches.Count;
          string replaced = text.Replace(target, replacement);

          StreamWriter writer = new StreamWriter(fileName);
          writer.Write(replaced);
          writer.Close();
        }
      }

      if (count == 0)
        MessageBox.Show("No occurrences found.");
      else if (count == 1)
        MessageBox.Show(count.ToString() + " occurrence replaced.");
      else
        MessageBox.Show(count.ToString() + " occurrences replaced.");
    }

    /// <summary>
    /// Updates the search results list so that its entries corresponding
    /// to the current file are up-to-date. This is called whenever a
    /// replacement happens.
    /// </summary>
    /// <param name="pattern"></param>
    /// <param name="indexIncrement"></param>
    public void RefreshCurrentFile(string pattern, int indexIncrement)
    {
      int initialSelectedIndex = _treeListView.SelectedItems[0].Index;

      TreeListNode[] nodes = new TreeListNode[_treeListView.Nodes.Count];
      _treeListView.Nodes.CopyTo(nodes, 0);
      string targetFile = _studio.FileEditorMap[_studio.CurrentEditor];
      string projRelTargetFile = _studio.CurrentProject.projRelative(targetFile);
      int minIndex = Int32.MaxValue;

      foreach (TreeListNode node in nodes)
      {
        minIndex = Math.Min(node.Index, minIndex);
        if (node.Text == projRelTargetFile)
        {
          _treeListView.Nodes.Remove(node);
          _matchMap.Remove(node);
        }
      }

      Regex regex = new Regex(Regex.Escape(pattern));

      //TODO: this is also in search. I need to factor it out.
      string text;
      if (_studio.FileEditorMap.Contains(targetFile))
      {
        text = _studio.FileEditorMap[targetFile].Text;
      }
      else
      {
        StreamReader reader = new StreamReader(targetFile);
        text = reader.ReadToEnd();
        reader.Close();
      }

      var parseResult = HandParser.parse(text,false,targetFile);
      var ast = parseResult.Item1;
      var boundaries = parseResult.Item2;

      int currLine = 1;
      int currTreeListIndex = minIndex;
      MatchCollection matches = regex.Matches(text);
      foreach (Match match in matches)
      {
        int ind = match.Index;

        //advance line iterator past match
        while (boundaries[currLine] < ind)
          currLine++;

        int start = boundaries[currLine - 1] + 1;
        int fin = boundaries[currLine];

        string contents = text.Substring(
          start,
          fin - start
        );

        TreeListNode newNode = new TreeListNode();
        newNode.Text = _studio.CurrentProject.projRelative(targetFile);
        newNode.SubItems.Add(
          new ContainerListViewObject.ContainerListViewSubItem((currLine - 1).ToString())
        );
        newNode.SubItems.Add(
          new ContainerListViewObject.ContainerListViewSubItem(contents)
        );

        _matchMap[newNode] = new MatchPos(targetFile, match.Index, match.Length);
        _treeListView.Nodes.Insert(newNode, currTreeListIndex);
        ++currTreeListIndex;
      }

      _treeListView.SelectedItems.Clear();

      if (_treeListView.Nodes.Count > 0)
      {
        int newSelectedIndex = (initialSelectedIndex + indexIncrement);
        newSelectedIndex %= _treeListView.Nodes.Count;
        _treeListView.Nodes[newSelectedIndex].Selected = true;
        OpenSelection();
      }
    }

    /// <summary>
    /// Searches either the entire project or the specified file for a specified
    /// piece of text, populating the search list with all matches discovered.
    /// </summary>
    /// <param name="pattern">The text to search for.</param>
    /// <param name="targetFile">
    /// If non-null, the file to search in. Otherwise, we search the entire 
    /// project.
    /// </param>
    public void Search(string pattern, string targetFile)
    {
      _treeListView.Nodes.Clear();
      _matchMap.Clear();

      var files = _studio.CurrentProject.GetSourceFiles();

      Regex regex = new Regex(Regex.Escape(pattern));

      foreach (FileInfo fileInfo in files)
      {
        if (targetFile != null && fileInfo.FullName != targetFile)
          continue;

        string fileName = fileInfo.FullName;

        string text;
        if (_studio.FileEditorMap.Contains(fileName))
        {
          text = _studio.FileEditorMap[fileName].Text;
        }
        else
        {
          StreamReader reader = new StreamReader(fileName);
          text = reader.ReadToEnd();
          reader.Close();
        }

        var parseResult = HandParser.parse(text,false,"");
        var ast = parseResult.Item1;
        var boundaries = parseResult.Item2;

        int currLine = 1;

        MatchCollection matches = regex.Matches(text);
        foreach (Match match in matches)
        {
          int ind = match.Index;
          
          //advance line iterator past match
          while (boundaries[currLine] < ind)
            currLine++;

          int start = boundaries[currLine - 1] + 1;
          int fin = boundaries[currLine];

          string contents = text.Substring(
            start,
            fin-start
          );

          TreeListNode newNode = new TreeListNode();
          newNode.Text = _studio.CurrentProject.projRelative(fileName);
          newNode.SubItems.Add(
            new ContainerListViewObject.ContainerListViewSubItem((currLine-1).ToString())
          );
          newNode.SubItems.Add(
            new ContainerListViewObject.ContainerListViewSubItem(contents)
          );

          _matchMap[newNode] = new MatchPos(fileName,match.Index,match.Length);
          _treeListView.Nodes.Add(newNode);
        }
      }

      if (_treeListView.Nodes.Count > 0)
      {
        _studio.ShowSearchPanel();
        _treeListView.Nodes[0].Selected = true;
      }

      _showingResults = true;
    }

    /// <summary>
    /// Clears the search list.
    /// </summary>
    public void Clear()
    {
      _treeListView.Nodes.Clear();
      _showingResults = false;
    }

    /// <summary>
    /// Replaces the current selection (which matches toReplace) with
    /// the speciefied replacement text. Updates the search list to 
    /// take this into account.
    /// </summary>
    /// <param name="replacement">The text that we insert in place of the selection.</param>
    /// <param name="toReplace">The text contained in the selection.</param>
    public void ReplaceSelectionWith(string replacement, string toReplace)
    {
      _studio.ReplaceSelectionWith(replacement);
      Regex regex = new Regex(Regex.Escape(toReplace));
      RefreshCurrentFile(toReplace, regex.Matches(replacement).Count);
    }

    /// <summary>
    /// Selects the first item in the search list, opens an editor
    /// to that item's source file and line number, and selects
    /// the match in the editor.
    /// 
    /// If no items exist in the search list, we instead display a popup.
    /// </summary>
    public void GotoFirstMatch()
    {
      if (_treeListView.SelectedItems.Count == 0)
      {
        MessageBox.Show("No matches found.");
        return;
      }

      _treeListView.Nodes[0].Selected = true;

      OpenSelection();
    }

    /// <summary>
    /// Open and highlight the next match in the match list.
    /// If no matches exist, explain this in a popup.
    /// </summary>
    public void GotoNextMatch()
    {
      if (_treeListView.Nodes.Count == 0)
      {
        MessageBox.Show("No matches found.");
        return;
      }

      if (_treeListView.SelectedItems.Count == 0)
      {
        _treeListView.Nodes[0].Selected = true;
      }
      else
      {
        //Select the next match in the list
        int selectedInd = _treeListView.SelectedItems[0].Index;
        _treeListView.Nodes[(selectedInd + 1) % _treeListView.Nodes.Count].Selected = true;
      }

      // Open an editor and highlight the match
      OpenSelection();
    }

    private void _treeListView_DoubleClick(object sender, EventArgs e)
    {
      // TODO: ick... isn't there a NodeSelected event?
      OpenSelection();
    }

    /// <summary>
    /// Opens an editor corresponding to the file of the current selected node, 
    /// goes to the current selection's line number, and selects the match
    /// corresponding to the current selected node.
    /// 
    /// KLUDGE: If no items are currently selected, nothing happens.
    /// </summary>
    private void OpenSelection()
    {
      if (_treeListView.SelectedItems.Count == 0)
        return;

      var item = (TreeListNode)_treeListView.SelectedItems[0];
      MatchPos pos = _matchMap[item];
      string fileName = item.Text;

      _studio.SelectText(pos.fileName, pos.start, pos.length);
    }
  }
}
