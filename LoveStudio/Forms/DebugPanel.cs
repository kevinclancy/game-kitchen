﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoveEdit
{
  /// <summary>
  /// A panel which allows the user to view the current locals, globals, 
  /// upvalues, and call stack when the app is breaking.
  /// </summary>
  public partial class DebugPanel : UserControl
  {
    #region Browser Indices
    private static byte LOCALS = 0;
    private static byte GLOBALS = 1;
    private static byte UPVALUES = 2;
    private static byte NUM_BROWSER_INDICES = 3;
    #endregion

    #region Dependencies
    private readonly Studio _studio;
    private readonly DebugProxy _debugProxy;
    #endregion
    
    #region Data Members
    /// <summary>
    /// An array containing all debug browsers, indexed by the constants
    /// defined in the "Browser Indices" region.
    /// </summary>
    private readonly DebugBrowser[] _browsers;

    /// <summary>
    /// If we are in debug mode, this is the current frame level being debugged. 
    /// 0 is the level of the current running function, and (n+1) is the level 
    /// of the function which called the function running at level n.
    /// </summary>
    int _currentFrameLevel;
    #endregion

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  Contract.Invariant(_browsers != null);
    //  Contract.Invariant(_browsers.Length == NUM_BROWSER_INDICES);
    //  Contract.Invariant(_browsers[LOCALS] == localsDebugBrowser);
    //  Contract.Invariant(_browsers[GLOBALS] == globalsDebugBrowser);
    //  Contract.Invariant(_browsers[UPVALUES] == upvaluesDebugBrowser);
    //}

    /// <summary>
    /// Creates a new debug panel.
    /// </summary>
    /// <param name="studio">The studio</param>
    /// <param name="debugProxy">An interface to the target-side debugger.</param>
    public DebugPanel(Studio studio, DebugProxy debugProxy)
    {
      InitializeComponent();
      _studio = studio;
      _debugProxy = debugProxy;
      _currentFrameLevel = 0;

      _browsers = new DebugBrowser[NUM_BROWSER_INDICES];
      _browsers[LOCALS] = localsDebugBrowser;
      _browsers[GLOBALS] = globalsDebugBrowser;
      _browsers[UPVALUES] = upvaluesDebugBrowser;

      SetupDelegates();
      SubscribeEvents();
    }

    /// <summary>
    /// Connect RequestChildren delegates so that the debug browsers can
    /// probe the target-side debugger via the debug proxy.
    /// </summary>
    private void SetupDelegates()
    {
      localsDebugBrowser.RequestChildren =
        delegate(long ptr, string path)
        {
          return _debugProxy.GetChildren(ptr, path, LOCALS, _currentFrameLevel);
        };

      globalsDebugBrowser.RequestChildren =
        delegate(long ptr, string path)
        {
          return _debugProxy.GetChildren(ptr, path, GLOBALS, _currentFrameLevel);
        };

      upvaluesDebugBrowser.RequestChildren =
        delegate(long ptr, string path)
        {
          return _debugProxy.GetChildren(ptr, path, UPVALUES, _currentFrameLevel);
        };
    }

    /// <summary>
    /// Subscribe to debugging and stack panel events.
    /// </summary>
    private void SubscribeEvents()
    {
      _debugProxy.OnResume += delegate() {
        _currentFrameLevel = 0;
        stackPanel.ClearFrames();

        foreach (DebugBrowser browser in _browsers) 
          browser.Clear();

        DisableDebugButtons();      
      };

      _debugProxy.OnStepIn += delegate()
      {
        _currentFrameLevel = 0;
        stackPanel.ClearFrames();

        foreach (DebugBrowser browser in _browsers)
          browser.Clear();

        DisableDebugButtons();
      };

      _debugProxy.OnStepOver += delegate()
      {
        _currentFrameLevel = 0;
        stackPanel.ClearFrames();
        DisableDebugButtons();
      };

      _debugProxy.OnDisconnect += delegate()
      {
        foreach (DebugBrowser browser in _browsers)
          browser.Clear();

        DisableDebugButtons();
      };

      stackPanel.SelectFrame +=
        delegate(string fileName, int lineNum, int level, bool clearContents)
        {
          if (fileName == "(tail call)")
            return;

          _currentFrameLevel = level;
          _studio.Highlight(
            _studio.CurrentProject.projNameToFileSystem(fileName), 
            lineNum
          );
          if (!clearContents)
          {
            localsDebugBrowser.RootItem.Collapse();
            globalsDebugBrowser.RootItem.Collapse();
            upvaluesDebugBrowser.RootItem.Collapse();
          }

          RetrieveBrowserValues();

          if (!clearContents)
          {
            localsDebugBrowser.RootItem.Expand();
            globalsDebugBrowser.RootItem.Expand();
            upvaluesDebugBrowser.RootItem.Expand();
          }
        };

      stackPanel.UnselectFrame +=
        delegate(string fileName, int lineNum)
        {
          //TODO: the debug proxy should take care of converting 
          //between path formats
          _studio.Unhighlight(
            _studio.CurrentProject.projNameToFileSystem(fileName),
            lineNum
          );
        };
    }

    /// <summary>
    /// When the target is not currently breaking, it does not make sense
    /// to resume, step over, or step in. Hence, this should be called in order
    /// to disable the aformentioned buttons.
    /// </summary>
    private void DisableDebugButtons()
    {
      resumeButton.Enabled = false;
      stepOverButton.Enabled = false;
      stepInButton.Enabled = false;
    }

    #region GUI Event Responses
    private void resumeButton_Click(object sender, EventArgs e)
    {
      _debugProxy.Resume();
    }

    private void stepOverButton_Click(object sender, EventArgs e)
    {
      _debugProxy.StepOver();
    }

    private void stepInButton_Click(object sender, EventArgs e)
    {
      _debugProxy.StepIn();
    }
    #endregion

    /// <summary>
    /// Updates the debug panel so that the local described by e is displayed
    /// correctly. Requests information about the children of the specified
    /// local if it is visible.
    /// </summary>
    /// <param name="e">
    ///   A struct describing some value reachable from the current set of 
    ///   local variables.
    /// </param>
    public void UpdateBrowserItem(DebugProxy.ChildrenInfo e)
    {
      //this is out of date.
      _browsers[e.browser].UpdateChildren(e);
    }

    /// <summary>
    /// Responds to the event in which the target breaks, either because
    /// a breakpoint was hit or an error occurred.
    /// </summary>
    /// <param name="frames">
    ///   The stack frame of the target's lua state, where element 0 is
    ///   the current running frame, and the higher elements are the chain
    ///   of callers.
    /// </param>
    public void BreakHit(LuaStackFrame[] frames, bool stepOver)
    {
      stackPanel.SetFrames(frames, stepOver);
      EnableDebugButtons();
    }

    /// <summary>
    /// When the debugger breaks, this is called in order to enable the 
    /// debug buttons.
    /// </summary>
    private void EnableDebugButtons()
    {
      resumeButton.Enabled = true;
      stepOverButton.Enabled = true;
      stepInButton.Enabled = true;
    }

    /// <summary>
    /// Synchronously requests children for the root items of each debug browser, 
    /// and then updates the debug browsers accordingly.
    /// </summary>
    private void RetrieveBrowserValues()
    {
      var locals = _debugProxy.GetChildren(0, "Locals", LOCALS, _currentFrameLevel);
      localsDebugBrowser.UpdateChildren(locals);

      var globals = _debugProxy.GetChildren(0, "Globals", GLOBALS, _currentFrameLevel);
      globalsDebugBrowser.UpdateChildren(globals);

      var upvalues = _debugProxy.GetChildren(0, "Upvalues", UPVALUES, _currentFrameLevel);
      upvaluesDebugBrowser.UpdateChildren(upvalues);
    }


  }
}
