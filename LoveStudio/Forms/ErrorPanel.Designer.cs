﻿namespace LoveEdit
{
  partial class ErrorPanel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._errorList = new WinControls.ListView.TreeListView();
      this._fileNameHeader = new WinControls.ListView.ContainerColumnHeader();
      this._lineColumnHeader = new WinControls.ListView.ContainerColumnHeader();
      this._errorMessageColumn = new WinControls.ListView.ContainerColumnHeader();
      this.SuspendLayout();
      // 
      // _errorList
      // 
      this._errorList.Columns.AddRange(new WinControls.ListView.ContainerColumnHeader[] {
            this._fileNameHeader,
            this._lineColumnHeader,
            this._errorMessageColumn});
      this._errorList.Dock = System.Windows.Forms.DockStyle.Fill;
      this._errorList.Location = new System.Drawing.Point(0, 0);
      this._errorList.Name = "_errorList";
      this._errorList.Size = new System.Drawing.Size(150, 150);
      this._errorList.TabIndex = 0;
      this._errorList.DoubleClick += new System.EventHandler(this.DoubleClickPanel);
      // 
      // _fileNameHeader
      // 
      this._fileNameHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._fileNameHeader.Text = "File";
      this._fileNameHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      // 
      // _lineColumnHeader
      // 
      this._lineColumnHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lineColumnHeader.Text = "Line";
      this._lineColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this._lineColumnHeader.Width = 50;
      // 
      // _errorMessageColumn
      // 
      this._errorMessageColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._errorMessageColumn.Text = "Error";
      this._errorMessageColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this._errorMessageColumn.Width = 10000;
      // 
      // ErrorPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._errorList);
      this.Name = "ErrorPanel";
      this.ResumeLayout(false);

    }

    #endregion

    private WinControls.ListView.TreeListView _errorList;
    private WinControls.ListView.ContainerColumnHeader _fileNameHeader;
    private WinControls.ListView.ContainerColumnHeader _errorMessageColumn;
    private WinControls.ListView.ContainerColumnHeader _lineColumnHeader;

  }
}
