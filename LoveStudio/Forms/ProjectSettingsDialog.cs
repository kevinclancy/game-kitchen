﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoveEdit.Forms
{
  public partial class ProjectSettingsDialog : Form
  {
    ModuleConstructors _moduleConstructors;
    Project _project;

    public ProjectSettingsDialog(ModuleConstructors moduleConstructors, Project project)
    {
      InitializeComponent();
      _moduleConstructors = moduleConstructors;
      _project = project;
    }

    private void ProjectSettingsDialog_Load(object sender, EventArgs e)
    {
      foreach (IModuleCreator creator in _moduleConstructors.GetAllModuleCreators())
      {
        _defaultModuleTypeComboBox.Items.Add(creator.GetName());
      }

      _defaultModuleTypeComboBox.SelectedItem = _project.DefaultModuleType;
      _enableTypeCheckingCheckBox.Checked = _project.TypeCheckingEnabled;
      _embedInGamePanelBox.Checked = _project.EmbedInGamePanel;
      _ingoreClassSystemCodeBox.Checked = _project.DebuggerIgnoresClassSystemCode;

      Invalidate();
    }

    private void cancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void _applyButton_Click(object sender, EventArgs e)
    {
      _project.DefaultModuleType = (string)_defaultModuleTypeComboBox.SelectedItem;
      _project.TypeCheckingEnabled =  _enableTypeCheckingCheckBox.Checked;
      _project.EmbedInGamePanel = _embedInGamePanelBox.Checked;
      _project.DebuggerIgnoresClassSystemCode = _ingoreClassSystemCodeBox.Checked;
      _project.SaveSettings();
      Close();
    }
  }
}
