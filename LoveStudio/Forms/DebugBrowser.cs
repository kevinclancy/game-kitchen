﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WinControls.ListView;

namespace LoveEdit
{
  /// <summary>
  /// A control which displays a group of related lua variables
  /// (upvalues, locals), and/or table entries.
  ///
  /// The variables are displayed using three columns:
  /// 
  /// Key - For locals and upvalues this column actually displays
  /// the variable name. For globals or other table entries, this
  /// column displays the key.
  /// 
  /// Value - The value corresponding to the variable or table entry.
  /// 
  /// Type - The type of this item's value.
  /// 
  /// For any non-empty table which is referenced by a variable 
  /// in this group, the entries of the table can be viewed by
  /// clicking on an "expand" icon next to the variable.
  /// </summary>
  public partial class DebugBrowser : UserControl
  {
    #region Delegates
    /// <summary>
    /// Requests the children of a lua value from the target.
    /// 
    /// TODO: is the parentPath param really necessary?
    /// </summary>
    /// <param name="ptr">The address, either 32 or 64 bit, of the lua gcobject to retrieve the children of.</param>
    /// <param name="parentPath">The debug browser address which is displaying the parent value. (used as a return address).</param>
    /// <returns></returns>
    public delegate DebugProxy.ChildrenInfo RequestChildrenDelegate(long ptr, string parentPath);

    /// <summary>
    /// Requests the children of a lua value from the target.
    /// </summary>
    private RequestChildrenDelegate _requestChildren;
    #endregion

    #region Data Members
    /// <summary>
    /// Value for RootItem. 
    /// </summary>
    private TreeListNode _rootItem;

    /// <summary>
    /// Maps TreeListNodes to the target-process pointes of the nodes' values.
    /// (only applicable to garbage collected values).
    /// </summary>
    Dictionary<TreeListNode, long> _itemPtrMap;
    #endregion 

    private string _rootName;

    #region Properties
    /// <summary>
    /// The name of the root item.
    /// </summary>
    [Browsable(true)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    [Category("Appearance")]
    public string RootName {
      get
      {
        return _rootName;
      }
      set
      {
        // assert(RootName == null);
        _rootName = value;
        _rootItem = new TreeListNode(RootName, 0);
        listView.Nodes.Add(_rootItem);
        _itemPtrMap[_rootItem] = 0;
      }
    }

    /// <summary>
    /// The item which all other items in this browser are descendants of.
    /// Initially, this should be the only item visible in the browser; other
    /// items can be found by expanding the the root item.
    /// </summary>
    public TreeListNode RootItem 
    { get { return _rootItem; } }

    /// <summary>
    /// The delegate used to retrieve lua state data from an external source.
    /// </summary>
    public RequestChildrenDelegate RequestChildren
    {
      get
      {
        return _requestChildren;
      }

      set
      {
        // // Contract.Requires(RequestChildren == null);
        _requestChildren = value;
      }
    }
    #endregion

    #region Nested Types

    /// <summary>
    /// This comparer is used to sort the entries in our tree list.
    /// 
    /// It places numbers first, in ascending numeric order.
    /// Then it places all other keys, sorted in ascending lexicographic order.
    /// </summary>
    class Comparer : System.Collections.IComparer
    {
      public int Compare(object x, object y)
      {
        TreeListNode nx = (TreeListNode)x;
        TreeListNode ny = (TreeListNode)y;

        string keyx = nx.Text;
        string keyy = ny.Text;

        double result;

        if (Double.TryParse(keyx, out result) && Double.TryParse(keyy, out result))
        {
          if (keyx.Length < keyy.Length)
            return -1;
          else if (keyx.Length > keyy.Length)
            return 1;
          else
            return keyx.CompareTo(keyy);
        }
        else if (Double.TryParse(keyx, out result))
        {
          return -1;
        }
        else
        {
          return keyx.CompareTo(keyy);
        }
      }
    }

    #endregion

    /// <summary>
    /// Creates a new debug browser.
    /// </summary>
    public DebugBrowser()
    {
      InitializeComponent();
      listView.PathSeparator = "!~g!";
      _itemPtrMap = new Dictionary<TreeListNode, long>();
      _requestChildren = null;
      listView.SortComparer = new Comparer();
      listView.Sorting = SortOrder.Ascending;

      listView.VScroll.Enabled = true;
      listView.VScroll.Visible = true;
    }

    /// <summary>
    /// Adding fields that are too long cause our TreeList control
    /// to explode, so we truncate them with this function first.
    /// 
    /// TODO: map full strings to TreeListNodes so that we
    /// can display full strings (rather than truncated ones) on demand.
    /// </summary>
    /// <param name="toTruncate"></param>
    /// <returns></returns>
    private string Truncate(string toTruncate)
    {
      if (toTruncate.Length > 50)
        return toTruncate.Substring(0, 50) + " ... ";
      else
        return toTruncate;
    }

    /// <summary>
    /// Updates the debug browser display to account for information,
    /// contained in the argument, about the entries of a lua table.
    /// </summary>
    /// <param name="e">Information about the table entries.</param>
    public void UpdateChildren(DebugProxy.ChildrenInfo e)
    {
      // // Contract.Assert(listView.GetItemAt(e.parentPath) != null);

      TreeListNode parentNode = listView.GetItemAt(e.parentPath);

      // Construct a dictionary to give us quick lookup of the previous
      // children by name
      Dictionary<string, TreeListNode> prevChildren = new Dictionary<string,TreeListNode>();
      foreach (TreeListNode child in parentNode.Nodes)
        prevChildren[child.Text] = child;

      // keep track of names of which children are still present after the update
      SortedSet<string> present = new SortedSet<string>();

      // 
      foreach (var item in e.items)
      {
        present.Add(item.keyName);

        if (prevChildren.ContainsKey(item.keyName))
        {
          if (prevChildren[item.keyName].SubItems[0].Text != item.valueString)
            prevChildren[item.keyName].ForeColor = Color.BlueViolet;
          else
            prevChildren[item.keyName].ForeColor = Color.Black;

          TreeListNode node = prevChildren[item.keyName];
          node.SubItems[0].Text = item.valueString;
          node.SubItems[1].Text = item.typeName;

          //TODO: might want to flatten out the list of all visible nodes
          //so that we don't have to make this recursive call.
          if (item.hasPtr && IsVisible(e.parentPath))
            UpdateChildren(RequestChildren(item.ptr, e.parentPath + "!~g!" + item.keyName));
        }
        else
        {
          TreeListNode newNode = new TreeListNode();
          newNode.Text = Truncate(item.keyName);
          
          newNode.SubItems.Add(
            new ContainerListViewObject.ContainerListViewSubItem(Truncate(item.valueString))
          );

          newNode.SubItems.Add(
            new ContainerListViewObject.ContainerListViewSubItem(Truncate(item.typeName))
          );

          newNode.ForeColor = Color.BlueViolet;

          parentNode.Nodes.Add(newNode);

          if (item.hasPtr)
          {
            _itemPtrMap[newNode] = item.ptr;

            if (IsVisible(e.parentPath))
              UpdateChildren(RequestChildren(item.ptr, e.parentPath + "!~g!" + item.keyName));
          }

        }
      }

      //we copy an array of the previous children so that we 
      //can remove those children which are no longer present.
      TreeListNode[] prevChildrenCopy = new TreeListNode[parentNode.Nodes.Count];
      parentNode.Nodes.CopyTo(prevChildrenCopy, 0);
      foreach (TreeListNode prevChild in prevChildrenCopy)
      {
        if (!present.Contains(prevChild.Text))
        {
          RemoveDescendants(prevChild);

          if (_itemPtrMap.ContainsKey(prevChild))
            _itemPtrMap.Remove(prevChild);

          prevChild.Remove();
        }
      }

      listView.Sort(parentNode.Index);
    }


    /// <summary>
    /// Returns true iff the given path is visible in the browser view. 
    /// </summary>
    /// <param name="parentPath">
    ///   The path of the item to check the visibility of.
    ///   
    /// </param>
    /// <returns></returns>
    // [Pure]
    public bool IsVisible(string parentPath)
    {
      // // Contract.Assert(listView.GetItemAt(parentPath) != null);
      TreeListNode parentNode = listView.GetItemAt(parentPath);
      return parentNode.IsExpanded;
    }

    /// <summary>
    /// When a node is expanded, we request information about all of its grand
    /// children from the target, returning only once this information has been
    /// received and integrated. 
    /// 
    /// This has the effect of populating our debug browser with entries lazily.
    /// </summary>
    /// <param name="sender">sender</param>
    /// <param name="e">event args</param>
    private void listView_AfterExpand(object sender, WinControls.ListView.EventArgClasses.TreeListViewEventArgs e)
    {
      TreeListNode[] children = new TreeListNode[e.Item.Nodes.Count];
      e.Item.Nodes.CopyTo(children,0);

      foreach (TreeListNode child in children)
      {
        if (_itemPtrMap.ContainsKey(child))
        {
          long childPtr = _itemPtrMap[child];
          DebugProxy.ChildrenInfo toUpdate = RequestChildren(childPtr, child.FullPath);
          UpdateChildren(toUpdate);
        }
      }
    }

    /// <summary>
    /// Removes all descendents of the specified TreeListNode.
    /// </summary>
    /// <param name="node">The node to remove the descendents of.</param>
    private void RemoveDescendants(TreeListNode node)
    {
      TreeListNode[] children = new TreeListNode[node.Nodes.Count];
      node.Nodes.CopyTo(children, 0);

      foreach (TreeListNode child in children)
      {
        _itemPtrMap.Remove(child);
        RemoveDescendants(child);
        child.Remove();
      }

      node.Collapse();
    }

    /// <summary>
    /// Removes all non-root items from the browser.
    /// </summary>
    public void Clear()
    {
      // // Contract.Requires(RootItem != null);
      // // Contract.Ensures(_itemPtrMap.Count == 1);

      RemoveDescendants(RootItem);
    }

    /// <summary>
    /// When collapsing a tree list item, we remove all of its descendents which
    /// are not direct children.
    /// </summary>
    /// <param name="sender">The list view</param>
    /// <param name="e">Event args</param>
    private void listView_BeforeCollapse(object sender, WinControls.ListView.EventArgClasses.TreeListViewCancelEventArgs e)
    {
      // clean up the grandchildren of the collapsed node.
      foreach (TreeListNode child in e.Item.Nodes)
        RemoveDescendants(child);

      listView.Refresh();
    }
  }
}
