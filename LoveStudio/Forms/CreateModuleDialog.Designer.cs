﻿namespace LoveEdit.Forms
{
  partial class CreateModuleDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateModuleDialog));
      this.panel1 = new System.Windows.Forms.Panel();
      this._moduleConstructorComboBox = new System.Windows.Forms.ComboBox();
      this.button1 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.AutoSize = true;
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.panel1.Location = new System.Drawing.Point(12, 12);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(669, 542);
      this.panel1.TabIndex = 0;
      // 
      // _moduleConstructorComboBox
      // 
      this._moduleConstructorComboBox.FormattingEnabled = true;
      this._moduleConstructorComboBox.Location = new System.Drawing.Point(12, 572);
      this._moduleConstructorComboBox.Name = "_moduleConstructorComboBox";
      this._moduleConstructorComboBox.Size = new System.Drawing.Size(165, 21);
      this._moduleConstructorComboBox.TabIndex = 1;
      this._moduleConstructorComboBox.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(183, 560);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(96, 33);
      this.button1.TabIndex = 2;
      this.button1.Text = "Create";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // button2
      // 
      this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.button2.Location = new System.Drawing.Point(285, 560);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(96, 33);
      this.button2.TabIndex = 3;
      this.button2.Text = "Cancel";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // CreateModuleDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.button2;
      this.ClientSize = new System.Drawing.Size(693, 605);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.button1);
      this.Controls.Add(this._moduleConstructorComboBox);
      this.Controls.Add(this.panel1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "CreateModuleDialog";
      this.Text = "☀ Create Module ☀";
      this.Load += new System.EventHandler(this.CreateModuleDialog_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ComboBox _moduleConstructorComboBox;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button button2;
  }
}