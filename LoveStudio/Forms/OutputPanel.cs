﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoveEdit.Forms
{
  public partial class OutputPanel : UserControl
  {
    public OutputPanel()
    {
      InitializeComponent();
      _outputTextBox.Text = "";
    }

    public void OnDebugStart()
    {
      _outputTextBox.IsReadOnly = false;
      _outputTextBox.Text = "";
      _outputTextBox.IsReadOnly = true;
    }

    public void OnDebugMsgReceived(string msgReceived)
    {
      _outputTextBox.IsReadOnly = false;
      _outputTextBox.InsertText(_outputTextBox.Text.Length, msgReceived+"\n");
      _outputTextBox.IsReadOnly = true;
    }
  }
}
