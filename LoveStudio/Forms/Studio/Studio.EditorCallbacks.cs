﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ScintillaNet;

namespace LoveEdit
{
  public partial class Studio 
  {
    private Dictionary<Tuple<bool, bool, bool, Keys>, Command> _editorInputMap;

    private void BuildEditorInputMap()
    {
      _editorInputMap = new Dictionary<Tuple<bool, bool, bool, Keys>, Command>();
      _editorInputMap[Entry(false, false, true, Keys.OemQuestion)] = SwapCurrentEditorTabContainer;
      _editorInputMap[Entry(false, false, true, Keys.OemPeriod)] = MoveCaretToOtherTab;
      _editorInputMap[Entry(false, false, true, Keys.R)] = RequireFromProject;
      _editorInputMap[Entry(false, false, true, Keys.W)] = ToggleWideMode;
      _editorInputMap[Entry(false, false, true, Keys.T)] = DoNothing; //to remove weird default behavior
      _editorInputMap[Entry(false, false, true, Keys.S)] = SaveAll;
      _editorInputMap[Entry(false, false, true, Keys.Q)] = CloseCurrentEditor;
      _editorInputMap[Entry(false, false, true, Keys.OemQuotes)] = ToggleCurrentFoldPoint;
      _editorInputMap[Entry(false, false, true, Keys.F)] = FindInFile;
    }

    // [Pure]
    public bool HasEditorCommand(KeyEventArgs e)
    {
      var entry = Entry(e.Shift, e.Alt, e.Control, e.KeyCode);
      return _editorInputMap.ContainsKey(entry);
    }

    private void RequireFromProject()
    {
      ProjectOpenBox box = new ProjectOpenBox(this, _debugProxy, ProjectOpenBox.Type.REQUIRE);
      box.Show();
    }

    private Command GetEditorCommand(KeyEventArgs e)
    {
      // Contract.Requires(HasEditorCommand(e));
      var entry = Entry(e.Shift, e.Alt, e.Control, e.KeyCode);
      return _editorInputMap[entry];
    }

    private void CloseCurrentEditor()
    {
      CloseEditor(_currentEditor);
    }

    private void ToggleCurrentFoldPoint()
    {
      _currentEditor.Lines.Current.ToggleFoldExpanded();
    }

    /// <summary>
    /// Moves the current editor from its current tab container to the
    /// other one.
    /// </summary>
    private void SwapCurrentEditorTabContainer()
    {
      _tabManager.MoveToOtherTabControl((TabPage)_currentEditor.Parent);
    }

    /// <summary>
    /// If both tab controls are currently visible, stretches the left one
    /// to cover up the right.
    /// 
    /// If love studio is already in wide mode, unstretch the left control
    /// so that the right one is visible.
    /// </summary>
    private void ToggleWideMode()
    {
      if (tableLayoutPanel1.ColumnStyles[0].Width == 1)
      {
        tableLayoutPanel1.ColumnStyles[0].Width = 0.5f;
        tableLayoutPanel1.ColumnStyles[1].Width = 0.5f;
      }
      else
      {
        tableLayoutPanel1.ColumnStyles[0].Width = 1;
        tableLayoutPanel1.ColumnStyles[1].Width = 0;
      }
    }

    private void DoNothing()
    {
    }

    /// <summary>
    /// Spawns a dialog which allows the user to find and replace within
    /// the currently focused file.
    /// </summary>
    private void FindInFile()
    {
      FindReplaceDialog dialog = new FindReplaceDialog(
        _searchResultsPanel,
        _files[_currentEditor],
        _currentProject.projRelative(_files[_currentEditor])
      );

      dialog.Show();
    }

    /// <summary>
    /// Given a control whose parent is a page in one of the two vertically
    /// oriented tab containers, moves the parent to the other tab container.
    /// </summary>
    /// <param name="control"></param>
    private void MoveToOtherTabControl(Control control)
    {
      // Contract.Requires(control != null);
      // Contract.Requires(control.Parent != null);
      // Contract.Requires(
      //  control.Parent.Parent == tabControlLeft
      //  || control.Parent.Parent == tabControlRight
      // );

      TabPage page = (TabPage)control.Parent;
      TabControl tabControl = (TabControl)page.Parent;
      if (tabControl == tabControlLeft)
      {
        tabControlLeft.Controls.Remove(page);
        tabControlRight.Controls.Add(page);
        tabControlRight.SelectTab(page);

        control.Focus();
      }
      else
      {
        tabControlRight.Controls.Remove(page);
        tabControlLeft.Controls.Add(page);
        tabControlLeft.SelectTab(page);

        control.Focus();
      }
    }

    /// <summary>
    /// Assuming tabControlLeft and tabControlRight are text editors,
    /// moves the focus (and, hence, the text editing caret) to the
    /// other tabControl.
    /// </summary>
    private void MoveCaretToOtherTab()
    {
      // Contract.Requires(_currentEditor != null);

      TabPage page = (TabPage)_currentEditor.Parent;
      TabControl tabControl = (TabControl)page.Parent;
      if (tabControl == tabControlLeft)
      {
        if (tabControlRight.Controls.Count > 0)
          tabControlRight.SelectedTab.Controls[0].Focus();
      }
      else
      {
        if (tabControlLeft.Controls.Count > 0)
          tabControlLeft.SelectedTab.Controls[0].Focus();
      }
    }

    /// <summary>
    /// Closes the specified editor. If the editor contains a file which 
    /// has been modified, asks the user whether or not they want to save the
    /// file and acts according to their response. TODO: we should also allow 
    /// them to cancel.
    /// </summary>
    /// <param name="editor">The editor to close.</param>
    private void CloseEditor(Editor editor)
    {
      // Contract.Requires(_files.Contains(editor));
      // Contract.Ensures(!_files.Contains(editor));

      TabPage page = (TabPage)editor.Parent;
      TabControl tabControl = (TabControl)page.Parent;

      if (editor.Modified)
      {
        DialogResult res = MessageBox.Show(
          "The following file has been modified. Would you like to save?\n" + _files[editor],
          "Save Option",
          MessageBoxButtons.YesNo
        );

        if (res == System.Windows.Forms.DialogResult.Yes)
          Save(editor);
        else
        {
          // the time stamp of the file on disk is going to have an older timestamp than the editor that 
          // we're closing. since we're rewinding the timestamp of this module, we need to invalidate 
          // any cached data in lua analyzer.
          LuaAnalyzer.ProjectChecker.invalidateCachedModule(_currentProject.toModuleName(_files[editor]));
        }
      }

      if (_currentEditor == editor)
        _currentEditor = null;

      editor.Dead = true;
      _files.Remove(editor);
      _tabManager.RemovePageFromControl(page);
    }
  }
}
