﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;
using System.IO;
using System.Reflection;
using ScintillaNet;
using LuaAnalyzer;
using LoveEdit.Forms;

namespace LoveEdit
{
    public partial class Studio : Form
    {
        static Studio _singleton;
        public static Studio Get()
        {
            return _singleton;
        }

        private string saveDirectory;

        #region Dependencies
        private readonly DebugProxy _debugProxy;
        private readonly DebugPanel _debugPanel;
        private readonly OutputPanel _outputPanel;
        private readonly ErrorPanel _errorPanel;
        private readonly SearchResultsPanel _searchResultsPanel;
        private readonly GamePanel _gamePanel;
        private readonly BreakpointManager _breakpointManager;
        #endregion

        #region Constants
        /// <summary>
        /// The scintilla marker index that we are using for the current line arrow.
        /// There are 32 marker indices and the higher ones are already in use.
        /// Since the higher-indexed markers are drawn over lower-indexed ones,
        /// we choose a mid-range index.
        /// </summary>
        private const int CurrentLineMarker = 12;
        #endregion

        #region Data Members
        /// <summary>
        /// 
        /// </summary>
        private ModuleConstructors _moduleConstructors;

        private TabManager _tabManager;

        /// <summary>
        /// The string of the function call expression corresponding to the last calltip query.
        /// </summary>
        private string _exprOfLastCallTipQuery;

        /// <summary>
        /// key - function call expression
        /// value - The index of the overload last selected in a calltip query for any call expression matching key
        /// </summary>
        private Dictionary<string, uint> _indexofLastCallTipQueryToExpr;

        /// <summary>
        /// The scintilla editor which is currently focused, if any.
        /// </summary>
        private Editor _currentEditor;

        /// <summary>
        /// A non-looping timer which is reset whenever the use enters text into 
        /// the editor, and signals the parser (see the LuaAnalyzer project) to run 
        /// whenever it expires.
        /// </summary>
        private Timer _parseTimer;

        /// <summary>
        /// A looping, frequently-updating timer which causes asynchronously-received
        /// messages from the target to be processed. TODO: move to debug proxy?
        /// </summary>
        private Timer _processEventsTimer;

        /// <summary>
        /// Associates full paths of all currently open files with 
        /// the Scintilla editors that are viewing them, tupled with the last
        /// time they were modified.
        /// </summary>
        private BiMap<string, Editor> _files;

        private Project _currentProject;

        private string _loveEditDirectory;

        #endregion

        #region Type Definitions
        /// <summary>
        /// A function returning void and taking no arguments.
        /// TODO: should be able to just use Action instead
        /// </summary>
        public delegate void Command();

        #endregion

        #region Properties
        /// <summary>
        /// The currently opened project, if any. 
        /// </summary>
        public Project CurrentProject
        {
            get
            {
                return _currentProject;
            }
        }

        /// <summary>
        /// The editor which was most recently focused.
        /// </summary>
        public Editor CurrentEditor
        {
            get
            {
                return _currentEditor;
            }
        }

        /// <summary>
        /// For each open file, maps the name of the file to the editor
        /// that it is currently open in.
        /// </summary>
        public BiMap<string, Editor> FileEditorMap
        {
            get
            {
                return _files;
            }
        }

        public GamePanel GamePanel
        {
            get
            {
                return _gamePanel;
            }
        }

        public ModuleConstructors ModuleConstructors
        {
            get
            {
                return _moduleConstructors;
            }
        }
        #endregion

        /// <summary>
        /// Creates the main love studio form.
        /// </summary>
        public Studio()
        {
            InitializeComponent();

            BasePlugin.activateModule();
            Analyzer.init();

            _indexofLastCallTipQueryToExpr = new Dictionary<string, uint>();

            _moduleConstructors = new ModuleConstructors();
            _moduleConstructors.Init();

            BuildGlobalInputMap();
            BuildEditorInputMap();

            _currentEditor = null;
            _loveEditDirectory = System.IO.Directory.GetCurrentDirectory();
            _files = new BiMap<string, Editor>();

            saveDirectory = System.IO.Directory.GetCurrentDirectory();
            _parseTimer = new Timer();
            _parseTimer.Enabled = true;
            _parseTimer.Tick += new EventHandler(errorCheckTimer_Tick);
            _parseTimer.Interval = 1500;

            _processEventsTimer = new Timer();
            _processEventsTimer.Enabled = true;
            _processEventsTimer.Tick += new EventHandler(popupTimer_Tick);
            _processEventsTimer.Interval = 100;

            _breakpointManager = new BreakpointManager();
            _debugProxy = new DebugProxy(this, _breakpointManager);
            _breakpointManager.SubscribeBreakpointAdded(_debugProxy.AddBreakpoint);
            _breakpointManager.SubscribeBreakpointRemoved(_debugProxy.RemoveBreakpoint);

            _errorPanel = new ErrorPanel(this);
            _searchResultsPanel = new SearchResultsPanel(this);
            _debugPanel = new DebugPanel(this, _debugProxy);
            _outputPanel = new OutputPanel();
            _gamePanel = new GamePanel(_debugProxy);

            _tabManager = new TabManager(tabControlLeft, tabControlRight);
            _tabManager.AddPanelToControl(_errorPanel, "Error", tabControlRight);
            _tabManager.AddPanelToControl(_searchResultsPanel, "Search", tabControlRight);
            _tabManager.AddPanelToControl(_debugPanel, "Debug", tabControlRight);
            _tabManager.AddPanelToControl(_outputPanel, "Output", tabControlRight);
            _tabManager.AddPanelToControl(_gamePanel, "Game", tabControlRight);

            _debugProxy.OnBreak += ((stack, stepOver, errorMsg) =>
            {
                ShowDebugPanel();

                if (!_gamePanel.Active)
                    Activate();

                _debugPanel.BreakHit(stack, stepOver);

                if (errorMsg.Length > 0)
                    MessageBox.Show(errorMsg);
            });
            _debugProxy.OnDebugMsgReceived += _outputPanel.OnDebugMsgReceived;

            string dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string gameKitchenDir = dataDir + "\\GameKitchen";

            System.IO.Directory.CreateDirectory(gameKitchenDir);
            DirectoryInfo gameKitchenDirInfo = new System.IO.DirectoryInfo(gameKitchenDir);

            try
            {
                StreamReader reader = new StreamReader(gameKitchenDir + "\\user.cfg");
                string projFile = reader.ReadLine();

                if (File.Exists(projFile))
                    openProject(projFile);

                reader.Close();
            }
            catch (FileNotFoundException)
            {
            }

            _singleton = this;
        }

        //[ContractInvariantMethod]
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
        //private void ObjectInvariant()
        //{
        //  Contract.Invariant(_currentEditor == null || _currentProject != null);
        //  Contract.Invariant(_currentEditor == null || _files.Contains(_currentEditor));
        //}

        #region Timers

        void popupTimer_Tick(object sender, EventArgs e)
        {
            _debugProxy.ProcessEvents();

            if (_currentProject != null)
                _currentProject.ProcessEvents();
        }

        void errorCheckTimer_Tick(object sender, EventArgs e)
        {
            if (_currentEditor == null)
                return;

            CheckErrors();
        }

        #endregion

        #region GUI Events

        void newEditor_GotFocus(object sender, EventArgs e)
        {
            _currentEditor = (Editor)sender;
        }

        private void saveFileAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_currentEditor == null)
            {
                MessageBox.Show("Cannot save: no editor is currently focused. Either create a new file, or open an existing one first.");
                return;
            }

            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Lua Files (*.lua)|*.lua";
            saveDialog.DefaultExt = ".lua";

            DialogResult res = saveDialog.ShowDialog();

            _files.Remove(_currentEditor);

            if (_files.Contains(saveDialog.FileName))
                CloseEditor(_files[saveDialog.FileName]);

            _files[_currentEditor] = saveDialog.FileName;
            TabPage currentPage = (TabPage)_currentEditor.Parent;

            if (_currentProject.isInProj(saveDialog.FileName))
                currentPage.Text = _currentProject.projRelative(saveDialog.FileName);
            else
                currentPage.Text = saveDialog.FileName;

            if (res == DialogResult.OK)
                Save(_currentEditor);
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Lua Files (*.lua)|*.lua";
            saveDialog.DefaultExt = ".lua";

            DialogResult res = saveDialog.ShowDialog();

            if (res == DialogResult.OK)
                CreateNewFile(saveDialog.FileName);
        }

        public void CreateNewFile(string fileName, string text = "")
        {
            string dirName = fileName.Remove(fileName.LastIndexOf('\\'));
            System.IO.Directory.CreateDirectory(dirName);

            StreamWriter writer = new StreamWriter(fileName);
            writer.Write("");
            writer.Close();
            OpenFile(fileName);

            _currentEditor.Text = text;
        }

        void newEditor_Paint(object sender, PaintEventArgs e)
        {
            Editor editor = (Editor)sender;

            if (editor != _currentEditor)
                return;

            int caretCol = editor.GetColumn(editor.Caret.Position);
            lineStatusLabel.Text = "Ln " + editor.Caret.LineNumber;
            columnStatusLabel.Text = "Col " + caretCol;
        }

        private void openFileMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Lua Files (*.lua)|*.lua";
            openDialog.DefaultExt = ".lua";

            DialogResult res = openDialog.ShowDialog();

            if (res == DialogResult.OK)
                OpenFile(openDialog.FileName);
        }

        private void newEditor_MarginClick(object sender, MarginClickEventArgs e)
        {
            if (e.Margin.IsMarkerMargin)
            {
                string fileName = _files[(Editor)sender];
                int lineNumber = e.Line.Number;

                if (_breakpointManager.ContainsBreakpoint(fileName, lineNumber))
                    _breakpointManager.RemoveBreakpoint(fileName, lineNumber);
                else
                    _breakpointManager.AddBreakpoint(fileName, lineNumber);

                e.ToggleMarkerNumber = 2;
            }
        }

        private void newEditor_ModifiedChanged(object sender, EventArgs e)
        {
            Editor editor = (Editor)sender;
            TabPage tab = (TabPage)editor.Parent;

            string title;
            if (_currentProject != null && _currentProject.isInProj(_files[editor]))
                title = _currentProject.toModuleName(_files[editor]);
            else
                title = _files[editor];

            if (editor.Modified == true)
                tab.Text = title + "*";
            else
                tab.Text = title;
        }

        void newEditor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_debugProxy.Connected && _files.Contains((Editor)sender))
            {
                Editor editor = (Editor)sender;

                string file = _files[editor];

                if (_debugProxy.CanEdit(file))
                {
                    // if we're trying to edit a file which is part of the 
                    // level currently being edited, allow the user to edit
                    return;
                }
                else
                {
                    // otherwise block them.
                    e.Handled = true;
                    MessageBox.Show("Source files cannot be changed while game is running.");
                }
            }
        }

        void newEditor_TextChanged(object sender, EventArgs e)
        {
            if (!_files.Contains((Editor)sender)) return;

            Editor editor = (Editor)sender;
            string path = _files[(Editor)editor];

            _breakpointManager.RemoveAllBreakpointsFromFile(path);

            foreach (Line line in editor.Lines)
            {
                if ((line.GetMarkerMask() & (1 << 2)) != 0)
                    _breakpointManager.AddBreakpoint(path, line.Number);
            }
        }

        void newEditor_DwellEnd(object sender, ScintillaMouseEventArgs e)
        {
            Scintilla editor = (Scintilla)sender;
            editor.CallTip.Hide();
        }

        void newEditor_DwellStart(object sender, ScintillaMouseEventArgs e)
        {
            if (e.Position == -1) return;

            Editor editor = (Editor)sender;

            if (!editor.Visible || editor.Dead)
                return;

            if (!editor.DisplayRectangle.Contains(Rectangle.FromLTRB(e.X, e.Y, e.X, e.Y)))
                return;

            if (!(editor.Parent == _tabManager.LeftTop || editor.Parent == _tabManager.RightTop))
                return;

            int pos = editor.ComputeCharOffsetFromByteOffset(e.Position);

            try
            {
                var ast = HandParser.parse(editor.Text, false, _files[editor]).Item1;
                var optError = Analyzer.getErrorFromPos(pos, ast);

                if (optError != null)
                {
                    // If a parsing error occured here, display its error message
                    editor.CallTip.Message = optError;
                    editor.CallTip.Show(e.Position);
                }
                else
                {
                    var errors = Analyzer.getFileErrors(
                      _currentProject.GetModules(),
                      _currentProject.RootPath,
                      _currentProject.TypeCheckingEnabled,
                      _files[editor]
                    );

                    string fileName = _files[(Editor)editor];

                    // Otherwise, if a typechecking error occured at this position, show that.
                    foreach (var error in errors.Item1)
                    {
                        if (pos >= error.Item3.Item1 && pos <= error.Item3.Item2 && error.Item1 == fileName)
                        {
                            editor.CallTip.Message = error.Item2;
                            editor.CallTip.Show(e.Position);
                            return;
                        }
                    }

                    if (!_currentProject.TypeCheckingEnabled)
                        return;

                    // If no errors were found at this position, query for a description
                    var res = ProjectChecker.queryDescAndLoc(
                      _currentProject.GetModules(),
                      _files[(Editor)sender],
                      pos,
                      _currentProject.TypeCheckingEnabled
                    );
                    var desc = res.Item1;
                    var ty = res.Item3;

                    if (desc != "nothing found")
                    {
                        //If desc is non-empty, add a newline after it
                        desc = (desc == "" ? "" : desc);
                        ty = (ty == "" ? "" : ("Type: " + ty));
                        var sep = (desc != "" && ty != "") ? "\n" : "";
                        editor.CallTip.Message = desc + sep + ty;
                        editor.CallTip.Show(e.Position);
                    }

                }
            }
            catch (HandLexer.LexError le)
            {
                if (pos == le.Data1)
                {
                    editor.CallTip.Message = le.Data0;
                    editor.CallTip.Show(editor.ComputeByteOffsetFromCharOffset(le.Data1));
                }
            }
        }

        private object GetLExpSuffixType()
        {
            // Set [j,i] to the range of the expression adjacent and left of the caret position 
            int currentPos = _currentEditor.CurrentPos;
            int i = _currentEditor.ComputeCharOffsetFromByteOffset(currentPos - 1);
            int j = i;
            int squareBracketCount = 0;
            int curlyBracketCount = 0;
            int parenCount = 0;

            while (j > 0)
            {
                --j;

                char c = _currentEditor.Text[j];

                //TODO: This is a hack. It assumes some common formatting idioms:
                //](alpha) does not occur, and that no spaces are placed in the 
                //middle of l-vals
                if (c == ']')
                    squareBracketCount++;
                else if (c == '[')
                    squareBracketCount--;
                else if (c == '(')
                    parenCount--;
                else if (c == ')')
                    parenCount++;
                else if (c == '{')
                    curlyBracketCount--;
                else if (c == '}')
                    curlyBracketCount++;

                {
                    if (curlyBracketCount > 0 || parenCount > 0 || curlyBracketCount > 0 || squareBracketCount > 0)
                        continue;

                    if (curlyBracketCount < 0 || parenCount < 0 || curlyBracketCount < 0 || squareBracketCount < 0)
                        break;

                    if (!(Char.IsLetterOrDigit(c) || c == '_' || c == '.' || c == ':' || c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == ']'))
                        break;
                }
            }

            j = j + 1;
            var iByteOffset = _currentEditor.ComputeByteOffsetFromCharOffset(i);
            int jByteOffset = _currentEditor.ComputeByteOffsetFromCharOffset(j);
            _currentEditor.UndoRedo.IsUndoEnabled = false;

            string lexp = _currentEditor.Text.Substring(j, i - j);
            string parenChar = _currentEditor.Text.Substring(i, 1);

            _currentEditor.Selection.Start = jByteOffset;
            _currentEditor.Selection.End = iByteOffset + 1;
            _currentEditor.Selection.Clear();

            int lastLookupIndex = lexp.LastIndexOfAny(new[] { ':', '.' });

            object ret = null;

            _currentEditor.InsertText(jByteOffset, "lucbdnioua()");

            if (lastLookupIndex > -1)
            {
                string lexpPrefix = lexp.Substring(0, lastLookupIndex);
                string lexpSuffix = lexp.Substring(lastLookupIndex + 1);


                var lexpPrefixAST = HandParser.parseExp(lexpPrefix);

                if (!lexpPrefixAST.IsErrorExpr)
                {
                    try
                    {
                        ret = Analyzer.getFieldOrMethodInfo(_currentProject.GetModules(), lexpPrefixAST, lexpSuffix, _files[_currentEditor]);
                    }
                    catch (InvalidCastException) { }
                }
            }
            else
            {
                var lexpAST = HandParser.parseExp(lexp);

                if (!lexpAST.IsErrorExpr)
                    ret = Analyzer.getCallInfo(_currentProject.GetModules(), lexpAST, _files[_currentEditor]);
            }

            _currentEditor.Selection.Start = jByteOffset;
            _currentEditor.Selection.Length = "lucbdnioua()".Length;
            _currentEditor.Selection.Clear();
            _currentEditor.InsertText(jByteOffset, lexp + parenChar);
            _currentEditor.Selection.Start = currentPos;
            _currentEditor.Selection.Length = 0;
            _currentEditor.UndoRedo.IsUndoEnabled = true;

            return ret;
        }

        Tuple<
          List<Analyzer.CSFunctionType>,
          Microsoft.FSharp.Core.FSharpOption<Analyzer.CSRecordType>,
          bool> GetClosestEnclosingTip()
        {
            // Contract.Assert(!_currentEditor.PositionIsOnComment(_currentEditor.CurrentPos));

            int currentPos = _currentEditor.CurrentPos;
            int i = _currentEditor.ComputeCharOffsetFromByteOffset(currentPos);
            var iByteOffset = _currentEditor.ComputeByteOffsetFromCharOffset(i);

            bool initiallyTyping = _currentEditor.TypingLine;

            _currentEditor.UndoRedo.IsUndoEnabled = false;
            _currentEditor.InsertText(iByteOffset, " lucbdnioua() ");

            var result = Analyzer.getFileCall(
               _currentProject.GetModules(),
               _files[_currentEditor]
            );

            _currentEditor.Selection.Start = iByteOffset;
            _currentEditor.Selection.Length = " lucbdnioua() ".Length;
            _currentEditor.Selection.Clear();

            _currentEditor.Selection.Start = currentPos;
            _currentEditor.Selection.Length = 0;
            _currentEditor.UndoRedo.IsUndoEnabled = true;

            //calling insert text sets TypingLine to true (should it?), so we need to restore this field
            _currentEditor.TypingLine = initiallyTyping;

            return result;
        }

        /// <summary>
        /// Gets the type of the expression which occurs at some interval whose greatest
        /// index is pos-1. Or returns null if no such well formed expression exists.
        /// </summary>
        /// <param name="pos">The position to the right of the expression.</param>
        /// <returns></returns>
        private object GetLExpType(int pos)
        {
            // Set [j,i] to the range of the l-val adjacent to the given
            int i = _currentEditor.ComputeCharOffsetFromByteOffset(pos);
            int j = i;
            int squareBracketCount = 0;
            int curlyBracketCount = 0;
            int parenCount = 0;

            while (j > 0)
            {
                --j;

                char c = _currentEditor.Text[j];

                //TODO: This is a hack. It assumes some common formatting idioms:
                //](alpha) does not occur, and that no spaces are placed in the 
                //middle of l-vals
                if (c == ']')
                    squareBracketCount++;
                else if (c == '[')
                    squareBracketCount--;
                else if (c == '(')
                    parenCount--;
                else if (c == ')')
                    parenCount++;
                else if (c == '{')
                    curlyBracketCount--;
                else if (c == '}')
                    curlyBracketCount++;

                {
                    if (parenCount > 0 || curlyBracketCount > 0 || squareBracketCount > 0)
                        continue;

                    if (parenCount < 0 || curlyBracketCount < 0 || squareBracketCount < 0)
                        break;

                    if (!(Char.IsLetterOrDigit(c) || c == '_' || c == '.' || c == ':' || c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}'))
                        break;
                }

                if (j == 0)
                {
                    //this loop terminates when j is the index directly before the
                    //expression preceding the '.' or ':'. In this case, -1 is appropriate. 
                    j--;
                    break;
                }
            }

            j = j + 1;
            int jByteOffset = _currentEditor.ComputeByteOffsetFromCharOffset(j);

            _currentEditor.UndoRedo.IsUndoEnabled = false;

            string exp = _currentEditor.Text.Substring(j, i - j - 1);
            string indChar = _currentEditor.Text.Substring(i - 1, 1);

            Line currentLine = _currentEditor.Lines.FromPosition(pos);
            string lineContents = currentLine.Text.TrimEnd();
            _currentEditor.Selection.Start = jByteOffset;
            _currentEditor.Selection.End = pos;
            _currentEditor.Selection.Clear();

            _currentEditor.InsertText(jByteOffset, "lucbdnioua()");

            object ret = null;
            var expAST = HandParser.parseExp(exp);
            if (!expAST.IsErrorExpr)
            {
                if (indChar == "(")
                    ret = Analyzer.getCallInfo(_currentProject.GetModules(), expAST, _files[_currentEditor]);
                else
                    ret = Analyzer.getRecordInfo(_currentProject.GetModules(), expAST, _files[_currentEditor]);
            }

            _currentEditor.Selection.Start = jByteOffset;
            _currentEditor.Selection.Length = "lucbdnioua()".Length;
            _currentEditor.Selection.Clear();
            _currentEditor.InsertText(jByteOffset, exp + indChar);
            _currentEditor.Selection.Start = pos;
            _currentEditor.Selection.Length = 0;
            _currentEditor.UndoRedo.IsUndoEnabled = true;

            return ret;
        }


        string GenerateCallTip(Analyzer.CSFunctionType ty)
        {
            string desc = ty.desc;
            string formals = "";
            string rets = "";

            if (ty.formals.Count > 0 || ty.hasVarPars)
            {
                formals += "\nParameters:";

                foreach (var formal in ty.formals)
                {
                    // name : type - description
                    formals += "\n" + formal.Item1 + " : " + formal.Item3 + " - " + formal.Item2;
                }

                if (ty.hasVarPars)
                {
                    formals += "\n" + "... - " + ty.varParsDesc;
                }
            }

            if (ty.rets.Count > 0 || ty.hasVarRets)
            {
                rets = "\nReturn Values:";
                foreach (var ret in ty.rets)
                {
                    // type - description
                    rets += "\n" + ret.Item2 + " - " + ret.Item1;
                }

                if (ty.hasVarRets)
                {
                    rets += "\n" + "... - " + ty.varRetsDesc;
                }
            }

            string callExpr;
            if (ty.callRange.Item1 == -1 && ty.callRange.Item2 == -1)
            {
                callExpr = "";
            }
            else
            {
                var r1 = _currentEditor.ComputeByteOffsetFromCharOffset(ty.callRange.Item1);
                var r2 = _currentEditor.ComputeByteOffsetFromCharOffset(ty.callRange.Item2);

                callExpr = _currentEditor.GetRange(r1, r2).Text + "\n";
                if (desc != "")
                    callExpr += "\n";
            }

            return callExpr + desc + formals + rets + (ty.isLatent ? "\n\n(latent)" : "");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="immediate">
        /// if true, we only spawn a calltip if th closest enclosing function call's last character
        /// is two indices before the caret.
        /// </param>
        void SpawnCallTip(bool immediate = false)
        {
            if (_currentEditor.PositionIsOnComment(_currentEditor.CurrentPos)) return;

            var tip = GetClosestEnclosingTip();
            var overloads = tip.Item1;
            var record = tip.Item2;
            var hasRecord = tip.Item3;

            string recordTyDescription = "";
            if (hasRecord)
            {
                recordTyDescription += "In constructor for type " + record.Value.name + "\n"
                                       + record.Value.desc + "\n";

                foreach (var field in record.Value.fields)
                {
                    var name = field.Item1;
                    var desc = field.Item2;
                    var tyString = field.Item3;

                    recordTyDescription += "\n" + name + " : " + tyString + " - " + desc;
                }

                // it wouldn't make any sense to print methods here, because methods cannot be defined
                // directly in literal tables. should we notify the programmer of this somehow?
                // they will get a type error, so that should be good enough
            }

            if (_currentEditor.Focused && overloads.Count > 0)
            {
                var rng = overloads[0].callRange;
                var r1 = _currentEditor.ComputeByteOffsetFromCharOffset(rng.Item1);
                var r2 = _currentEditor.ComputeByteOffsetFromCharOffset(rng.Item2);
                string callExprText = _currentEditor.GetRange(r1, r2).Text;

                if (immediate && (_currentEditor.Caret.Position < 1 || (_currentEditor.Caret.Position - 1) != r2))
                {
                    return;
                }

                _currentEditor.CallTip.OverloadList = new OverloadList();

                foreach (Analyzer.CSFunctionType ty in overloads)
                {
                    string overload = GenerateCallTip(ty) +
                                      (recordTyDescription == "" ? "" : "\n--------\n") +
                                      recordTyDescription;

                    if (overload != "")
                        _currentEditor.CallTip.OverloadList.Add(overload);
                }

                var overloadList = _currentEditor.CallTip.OverloadList;
                if (overloadList.Count == 1)
                    _currentEditor.CallTip.Show(overloadList[0]);
                else if (overloadList.Count > 1)
                {
                    // if the call expression matches the previous one, we restore the overload index from the previous calltip
                    // in order to maintain continuity for the programmer
                    if (_indexofLastCallTipQueryToExpr.ContainsKey(callExprText))
                    {
                        _currentEditor.CallTip.ShowOverload(
                          _currentEditor.CallTip.OverloadList,
                          _currentEditor.Caret.Position,
                          _indexofLastCallTipQueryToExpr[callExprText],
                          0,
                          0
                        );
                    }
                    else
                    {
                        _currentEditor.CallTip.ShowOverload();
                    }

                }

                _exprOfLastCallTipQuery = callExprText;
            }
            else if (hasRecord)
            {
                _currentEditor.CallTip.Show(recordTyDescription);
            }
            else
            {
                _currentEditor.CallTip.Hide();
            }
        }

        void newEditor_CharAdded(object sender, CharAddedEventArgs e)
        {
            if (_currentEditor.Snippets.IsActive) return;
            if (_currentEditor.PositionIsOnComment(_currentEditor.Caret.Position)) return;

            if (e.Ch == '\n')
            {
                LinesCollection lines = _currentEditor.Lines;
                Line currLine = lines[lines.Current.Number];
                Line prevLine = lines[lines.Current.Number - 1];

                char[] prevLineChars = prevLine.Text.ToCharArray();
                int parenCount = 0;
                int curlyCount = 0;

                for (int i = 0; i < prevLineChars.Length; ++i)
                {
                    if (prevLine.Text[i] == '(') ++parenCount;
                    if (prevLine.Text[i] == ')') --parenCount;
                    if (prevLine.Text[i] == '{') ++curlyCount;
                    if (prevLine.Text[i] == '}') --curlyCount;
                }

                if (
                  (prevLine.Text.TrimStart().StartsWith("function") && !prevLine.Text.TrimEnd().EndsWith(","))
                  || (prevLine.Text.TrimStart().StartsWith("local function"))
                  || (prevLine.Text.TrimEnd().EndsWith("do"))
                  || (prevLine.Text.TrimEnd().EndsWith("then"))
                )
                {
                    //the line being split might not be complete, so we remove the portion after the split
                    //before parsing. if it is complete, its unlikely that removing the trailing portion will
                    //cause parse errors.
                    int startChar = _currentEditor.ComputeCharOffsetFromByteOffset(lines.Current.StartPosition);
                    int endChar = _currentEditor.ComputeCharOffsetFromByteOffset(lines.Current.EndPosition);
                    string insertEnd =
                      _currentEditor.Text.Substring(0, startChar)
                      + " end " +
                      _currentEditor.Text.Substring(endChar);

                    var ast = HandParser.parse(insertEnd, false, _files[_currentEditor]).Item1;

                    if (!Analyzer.hasErrors(ast, _currentProject.TypeCheckingEnabled) && currLine.Text.Trim() == "")
                    {
                        lines.Current.Text = new string(' ', prevLine.Indentation + 2) +
                                             "\r\n" +
                                             new string(' ', prevLine.Indentation) +
                                             "end";
                    }
                    else
                    {
                        lines.Current.Text = new string(' ', prevLine.Indentation + 2) + lines.Current.Text.TrimEnd();
                    }
                }
                else if (parenCount == 1)
                {
                    int startChar = _currentEditor.ComputeCharOffsetFromByteOffset(lines.Current.StartPosition);
                    int endChar = _currentEditor.ComputeCharOffsetFromByteOffset(lines.Current.EndPosition);
                    string insertEnd =
                      _currentEditor.Text.Substring(0, startChar)
                      + " ) " +
                      _currentEditor.Text.Substring(endChar);

                    var ast = HandParser.parse(insertEnd, false, _files[_currentEditor]).Item1;

                    if (!Analyzer.hasErrors(ast, _currentProject.TypeCheckingEnabled) && currLine.Text.Trim() == "")
                    {
                        lines.Current.Text = new string(' ', prevLine.Indentation + 2) +
                                             "\r\n" +
                                             new string(' ', prevLine.Indentation) +
                                             ")";
                    }
                    else
                    {
                        lines.Current.Text = new string(' ', prevLine.Indentation + 2) + lines.Current.Text.TrimEnd();
                    }
                }
                else if (curlyCount == 1)
                {
                    int startChar = _currentEditor.ComputeCharOffsetFromByteOffset(lines.Current.StartPosition);
                    int endChar = _currentEditor.ComputeCharOffsetFromByteOffset(lines.Current.EndPosition);
                    string insertEnd =
                      _currentEditor.Text.Substring(0, startChar)
                      + " } " +
                      _currentEditor.Text.Substring(endChar);

                    var ast = HandParser.parse(insertEnd, false, _files[_currentEditor]).Item1;

                    if (!Analyzer.hasErrors(ast, _currentProject.TypeCheckingEnabled) && currLine.Text.Trim() == "")
                    {
                        lines.Current.Text = new string(' ', prevLine.Indentation + 2) +
                                             "\r\n" +
                                             new string(' ', prevLine.Indentation) +
                                             "}";
                    }
                    else
                    {
                        string insertEnd2 =
                          _currentEditor.Text.Substring(0, startChar)
                          + " }, " +
                          _currentEditor.Text.Substring(endChar);

                        var ast2 = HandParser.parse(insertEnd2, false, _files[_currentEditor]).Item1;

                        if (!Analyzer.hasErrors(ast2, _currentProject.TypeCheckingEnabled) && currLine.Text.Trim() == "")
                        {
                            lines.Current.Text = new string(' ', prevLine.Indentation + 2) +
                                   "\r\n" +
                                   new string(' ', prevLine.Indentation) +
                                   "},";
                        }
                        else
                            lines.Current.Text = new string(' ', prevLine.Indentation + 2) + lines.Current.Text.TrimEnd();
                    }
                }
                else
                {
                    lines.Current.Text = new string(' ', prevLine.Indentation) + lines.Current.Text.TrimEnd();
                }

                _currentEditor.TypingLine = false;
                _currentEditor.Caret.Position = currLine.IndentPosition;
                _currentEditor.Selection.Start = _currentEditor.Caret.Position;
                _currentEditor.Selection.End = _currentEditor.Caret.Position;
            }
            else if (e.Ch == ':' || e.Ch == '.' && !_currentEditor.Lines.Current.Text.Trim().StartsWith("--"))
            {
                try
                {
                    Analyzer.CSRecordType ty = (Analyzer.CSRecordType)GetLExpType(_currentEditor.CurrentPos);

                    List<string> autoCompleteList = new List<string>();

                    if (ty != null)
                    {
                        var list = new List<Tuple<string, string, string>>();

                        if (e.Ch == ':')
                            list = ty.methods;
                        else
                        {
                            // Contract.Assert(e.Ch == '.');
                            list = ty.fields;
                        }

                        foreach (var item in list)
                            autoCompleteList.Add(item.Item1);

                        autoCompleteList.Sort();
                        _currentEditor.AutoComplete.List = autoCompleteList;
                    }

                    if (autoCompleteList.Count != 0)
                        _currentEditor.AutoComplete.Show();
                }
                catch (InvalidCastException)
                {
                }
            }
            else if (e.Ch == '(')
            {
                SpawnCallTip(true);
                // parse timers can trigger call tip spawns, so we need to do this to prevent double spawning
                _parseTimer.Stop();
            }
        }

        private void tabPage_keyDown(object sender, KeyEventArgs e)
        {
            if (_currentProject == null && !e.Alt)
            {
                MessageBox.Show("This feature cannot be used until a project has been opened. Goto 'Project' -> 'New Project'.");
                return;
            }

            if (_debugProxy.Connected)
            {
                var editor = (Editor)sender;

                if (!(editor != null && _debugProxy.CanEdit(_files[editor])))
                {
                    if (!(e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up || e.KeyCode == Keys.Down))
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }

            _parseTimer.Stop();
            _parseTimer.Start();

            if (HasEditorCommand(e))
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                GetEditorCommand(e)();
            }
        }

        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Lua Project Files (*.luaproj)|*.luaproj";
            saveDialog.DefaultExt = ".luaproj";
            saveDialog.Title = "New Project";

            DialogResult res = saveDialog.ShowDialog();

            if (res == DialogResult.OK)
            {
                // Save and close current project.
                SaveAndCloseAllEditors();

                openProject(saveDialog.FileName);

                string dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\GameKitchen";
                string templateDir = dataDir + "\\template";

                // copy main.lua, globals,lua, etc. into the project dir
                DirectoryInfo templateDirInfo = new System.IO.DirectoryInfo(templateDir);
                var files = templateDirInfo.EnumerateFiles("*", SearchOption.AllDirectories);

                foreach (var file in files)
                {
                    string relPath = file.FullName.Substring(templateDir.Length);
                    string targetFileName = _currentProject.RootPath + relPath;
                    string targetPathName = targetFileName.Substring(0, targetFileName.LastIndexOf('\\'));
                    Directory.CreateDirectory(targetPathName);
                    if (!System.IO.File.Exists(_currentProject.RootPath + relPath))
                        System.IO.File.Copy(templateDir + relPath, _currentProject.RootPath + relPath);
                }
            }
        }

        private void studio_keyDown(object sender, KeyEventArgs e)
        {
            if (_currentProject == null && !e.Alt)
            {
                MessageBox.Show("This feature cannot be used until a project has been opened. Goto 'Project' -> 'New Project'.");
                return;
            }

            if (HasGlobalCommand(e))
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                GetGlobalCommand(e)();
            }
            if (_currentEditor != null && HasEditorCommand(e))
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                GetEditorCommand(e)();
            }
        }

        private void openProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Lua Project Files (*.luaproj)|*.luaproj";
            openDialog.DefaultExt = ".luaproj";

            DialogResult res = openDialog.ShowDialog();


            if (res == DialogResult.OK)
            {
                SaveAndCloseAllEditors();

                openProject(openDialog.FileName);

                string dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                StreamWriter writer = new StreamWriter(dataDir + "\\GameKitchen\\user.cfg");
                writer.Write(openDialog.FileName);
                writer.Close();
            }
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_debugProxy.Connected)
                RunTarget();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"
        Controls:
          Ctrl-Q       Close Current Editor
          Ctrl-S       Save Current Editor
          Ctrl-W       Toggle Wide Mode
          Ctrl-O       Open Module
          Ctrl-R       Require Module
          Ctrl-N       Create New Module
          Ctrl-L       Cut Current Line
          Ctrl-,       Move Caret to Other Tab
          Ctrl-/       Move Editor To Other Tab
          Ctrl-'       Collapse/Expand Folded Code
          Ctrl-G       Goto Line
          Ctrl-F       Find in file
          Ctrl-Shift-F Find in project
          Ctrl-I       Incremental Search
          Ctrl-Wheel   Zoom
      ");
        }

        #endregion

        /// <summary>
        /// Places a brown box around the given line number in the given file;
        /// this distinguishes it from the other lines in the file.
        /// </summary>
        /// <param name="fileName">The full path of the file containing the line to highlight.</param>
        /// <param name="lineNum">The number of the line to highlight.</param>
        public void Highlight(string fileName, int lineNum)
        {
            OpenFile(fileName);

            Scintilla editor = _files[fileName];

            TabPage page = (TabPage)editor.Parent;
            if (page.Parent == tabControlRight)
                _tabManager.MoveToOtherTabControl(page);

            editor.Focus();
            editor.Lines[lineNum].EnsureVisible();
            editor.Lines[lineNum].Goto();
            editor.Lines[lineNum].Range.SetIndicator(2);
        }

        /// <summary>
        /// Replaces the current selection with the given string.
        /// </summary>
        /// <param name="replacement">The string used to replace the current selection.</param>
        public void ReplaceSelectionWith(string replacement)
        {
            // Contract.Assert(_currentEditor.Selection != null);
            _currentEditor.Selection.Text = replacement;
        }

        /// <summary>
        /// Opens the specified file, ensures the specified text is visible,
        /// and selects the specified text.
        /// </summary>
        /// <param name="fileName">The file containing the text we wish to select.</param>
        /// <param name="start">The start offset of the text we wish to select.</param>
        /// <param name="length">The length of the text we wish to select.</param>
        public void SelectText(string fileName, int start, int length)
        {
            OpenFile(fileName);
            Editor editor = _files[fileName];
            editor.TypingLine = false;
            int byteOffset = editor.ComputeByteOffsetFromCharOffset(start);
            Line line = editor.Lines.FromPosition(byteOffset);
            line.EnsureVisible();
            line.Goto();
            editor.GetRange(byteOffset, byteOffset + length).Select();
        }

        /// <summary>
        /// Opens a file (or just brings it to the front if it's already open) and 
        /// ensures that the given line number is visible.
        /// </summary>
        /// <param name="fileName">The file to open.</param>
        /// <param name="lineNum">The line number to open to.</param>
        public void OpenFileToLine(string fileName, int lineNum, TabControl hostControl = null)
        {
            OpenFile(fileName, false, hostControl);

            Editor editor = _files[fileName];
            editor.TypingLine = false;
            editor.Focus();
            editor.Lines[lineNum].EnsureVisible();
            editor.Lines[lineNum].Goto();

            _parseTimer.Start();
        }

        /// <summary>
        /// Opens a file (or just brings it to the front if it's already open) and 
        /// ensures that the given character index position is visible.
        /// </summary>
        /// <param name="fileName">The file to open.</param>
        /// <param name="lineNum">The line number to open to.</param>
        public void OpenFileToPos(string filename, int position, TabControl hostControl = null)
        {
            OpenFile(filename, false, hostControl);
            Editor editor = _files[filename];
            Line lineOfPos = editor.Lines.FromPosition(editor.ComputeByteOffsetFromCharOffset(position));
            lineOfPos.EnsureVisible();
            lineOfPos.Goto();

            _parseTimer.Start();
        }

        /// <summary>
        /// Removes a highlight placed by an earlier call to highlight.
        /// </summary>
        /// <param name="fileName">The project-relative file containing the highlighted line.</param>
        /// <param name="lineNum">The number of the highlighted line.</param>
        public void Unhighlight(string fileName, int lineNum)
        {
            if (_files.Contains(fileName))
            {
                Scintilla editor = _files[fileName];
                editor.Lines[lineNum].Range.ClearIndicator(2);
            }
        }

        /// <summary>
        /// Ensures that the error panel is open and visible for the user to 
        /// interact with.
        /// </summary>
        private void ShowErrorPanel()
        {
            TabPage errorPage = (TabPage)_errorPanel.Parent;
            // Contract.Assert(errorPage.Parent == tabControlRight);
            _tabManager.BringPageToFront(errorPage);
        }

        /// <summary>
        /// Brings the debug page to the front of the right tab control if it is not
        /// already there.
        /// </summary>
        private void ShowDebugPanel()
        {
            TabPage debugPage = (TabPage)_debugPanel.Parent;
            // Contract.Assert(debugPage.Parent == tabControlRight);
            _tabManager.BringPageToFront(debugPage);
        }

        /// <summary>
        /// Brings the search page to the front of the right tab control if it
        /// is not already there.
        /// </summary>
        public void ShowSearchPanel()
        {
            TabPage searchPage = (TabPage)_searchResultsPanel.Parent;
            // Contract.Assert(searchPage.Parent == tabControlRight);
            _tabManager.BringPageToFront(searchPage);
        }

        /// <summary>
        /// Brings the game page to the front of the right tab control if it
        /// is not already there.
        /// </summary>
        public void ShowGamePanel()
        {
            TabPage gamePage = (TabPage)_gamePanel.Parent;
            // Contract.Assert(gamePage.Parent == tabControlRight);
            _tabManager.BringPageToFront(gamePage);
        }

        /// <summary>
        /// Custom folding. Not currently used.
        /// </summary>
        private void ComputeFolds()
        {
            //Call out to F# to get a list of levels
            //List<bool> folds = RoughParser.computeFolds(currentEditor.Text);
            //if (folds[0])
            //{
            //  currentEditor.Lines[0].IsFoldPoint = true;
            //  currentEditor.Lines[0].FoldLevel = 0;
            //}

            //for (int i = 1; i < currentEditor.Lines.Count; ++i)
            //{
            //  Line currentLine = currentEditor.Lines[i];

            //  if (!folds[i - 1] && folds[i])
            //  {
            //    currentLine.IsFoldPoint = true;
            //    currentLine.FoldLevel = 0;
            //  }
            //  else if (folds[i - 1] && !folds[i])
            //  {
            //    if (currentLine.IsFoldPoint && !currentLine.FoldExpanded)
            //      currentLine.ToggleFoldExpanded();

            //    currentEditor.Lines[i].IsFoldPoint = false;
            //    currentEditor.Lines[i].FoldLevel = 1;
            //  }
            //  else if (!folds[i])
            //  {
            //    if (currentLine.IsFoldPoint && !currentLine.FoldExpanded)
            //      currentLine.ToggleFoldExpanded();

            //    currentEditor.Lines[i].IsFoldPoint = false;
            //    currentEditor.Lines[i].FoldLevel = 0;
            //  }
            //  else
            //  {
            //    currentEditor.Lines[i].IsFoldPoint = false;
            //    currentEditor.Lines[i].FoldLevel = 1;
            //  }
            //}
        }

        /// <summary>
        /// Creates a new text editor containing the contents of the file specified
        /// by fileName.
        /// </summary>
        /// <param name="fileName">
        ///   The full path of the file that our new editor will be
        ///   used to edit.
        /// </param>
        /// <returns>An editor used to edit the file specified by fileName.</returns>
        private Editor SpawnEditor(string fileName, TabControl containingControl)
        {
            // Contract.Requires(!_files.Contains(fileName));
            // Contract.Ensures(_files.Contains(fileName));

            Editor newEditor = new Editor();

            string keywords = "";
            keywords += "and break do else elseif end false ";
            keywords += "for function if in local nil not or repeat return ";
            keywords += "then true until while ";
            newEditor.ConfigurationManager.Language = "lua";
            newEditor.Lexing.SetKeywords(0, keywords);

            newEditor.ConfigurationManager.IsUserEnabled = true;
            newEditor.Dock = DockStyle.Fill;
            newEditor.Indentation.UseTabs = false;
            newEditor.Indentation.TabIndents = false;
            newEditor.Indentation.TabWidth = 2;
            newEditor.Folding.IsEnabled = true;
            newEditor.Folding.MarkerScheme = FoldMarkerScheme.BoxPlusMinus;

            newEditor.Margins.Margin1.Width = 12;
            newEditor.Margins.Margin1.IsMarkerMargin = true;
            newEditor.Margins.Margin1.IsClickable = true;
            newEditor.Markers[CurrentLineMarker].ForeColor = Color.Red;
            newEditor.Markers[CurrentLineMarker].Symbol = MarkerSymbol.Arrow;
            newEditor.Markers[CurrentLineMarker].BackColor = Color.Red;

            newEditor.Indicators[2].Style = IndicatorStyle.Box;
            newEditor.Indicators[2].Color = Color.SaddleBrown;

            //Give the fold margin some width.
            newEditor.Margins.Margin2.Width = 12;
            newEditor.MarginClick += new EventHandler<MarginClickEventArgs>(newEditor_MarginClick);

            newEditor.CharAdded += new EventHandler<CharAddedEventArgs>(newEditor_CharAdded);
            newEditor.GotFocus += new EventHandler(newEditor_GotFocus);
            newEditor.LostFocus += newEditor_LostFocus;
            newEditor.KeyDown += new KeyEventHandler(tabPage_keyDown);
            newEditor.KeyPress += new KeyPressEventHandler(newEditor_KeyPress);
            newEditor.Paint += new PaintEventHandler(newEditor_Paint);
            newEditor.ModifiedChanged += new EventHandler(newEditor_ModifiedChanged);
            newEditor.NativeInterface.SetMouseDwellTime(1100);
            newEditor.DwellStart += new EventHandler<ScintillaMouseEventArgs>(newEditor_DwellStart);
            newEditor.DwellEnd += new EventHandler<ScintillaMouseEventArgs>(newEditor_DwellEnd);
            newEditor.TextChanged += new EventHandler<EventArgs>(newEditor_TextChanged);
            newEditor.NativeInterface.UsePopUp(false);
            newEditor.MouseClick += newEditor_MouseClick;
            newEditor.CallTipClick += newEditor_CallTipClick;

            newEditor.Snippets.IsEnabled = true;
            newEditor.Snippets.List.Add("pr", "--@param($name$ : $type$) $description$");
            newEditor.Snippets.List.Add("vr", "--@var($type$) $description$");
            newEditor.Snippets.List.Add("rt", "--@ret($type$) $description$");
            newEditor.Snippets.List.Add("cn", "--@const($type$) $description$");
            newEditor.Snippets.List.Add("lt", "--@latent");

            TabPage newTabPage;
            if (_currentProject != null && _currentProject.isInProj(fileName))
                newTabPage = new TabPage(_currentProject.toModuleName(fileName));
            else
                newTabPage = new TabPage(fileName);

            newTabPage.Controls.Add(newEditor);

            if (containingControl == null)
                _tabManager.AddPageToControl(newTabPage, tabControlLeft);
            else
                _tabManager.AddPageToControl(newTabPage, containingControl);

            _files[(Editor)newEditor] = fileName;

            return newEditor;
        }

        void newEditor_LostFocus(object sender, EventArgs e)
        {
            _indexofLastCallTipQueryToExpr.Clear();
        }

        void newEditor_CallTipClick(object sender, CallTipClickEventArgs e)
        {
            _indexofLastCallTipQueryToExpr[_exprOfLastCallTipQuery] = (uint)e.NewIndex;
        }

        void newEditor_MouseClick(object sender, MouseEventArgs e)
        {
            if (!_currentProject.TypeCheckingEnabled)
                return;

            Editor editor = (Editor)sender;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                EventHandler GotoDefinitionDelegate =
                  delegate (object gotoDefSender, EventArgs gotoDefArgs)
                  {
                      int charOffset = editor.ComputeCharOffsetFromByteOffset(editor.PositionFromPoint(e.X, e.Y));
                      var query = ProjectChecker.queryDescAndLoc(
                _currentProject.GetModules(),
                _files[editor],
                charOffset,
                _currentProject.TypeCheckingEnabled
              );

                      var filename = query.Item2.Item1;
                      var position = query.Item2.Item2.Item1;

                      var clickControl = editor.Parent.Parent;

                      TabControl newPageHostControl;

                      if (clickControl == _tabManager.LeftControl)
                      {
                          if (filename == _files[editor])
                              newPageHostControl = _tabManager.LeftControl;
                          else
                              newPageHostControl = _tabManager.RightControl;
                      }
                      else
                      {
                          if (filename == _files[editor])
                              newPageHostControl = _tabManager.RightControl;
                          else
                              newPageHostControl = _tabManager.LeftControl;
                      }

                      if (filename != "")
                          OpenFileToPos(filename, position, newPageHostControl);
                  };

                MenuItem[] menuItems = {
          new MenuItem("Go to definition", GotoDefinitionDelegate)
        };

                var newMenu = new ContextMenu(menuItems);
                newMenu.Show(editor, e.Location);
            }
        }

        /// <summary>
        /// Folds all foldpoints in the given editor.
        /// 
        /// TODO: fix this. it is currently buggy.
        /// </summary>
        /// <param name="editor">The editor we wish to fold all fold points of.</param>
        private void FoldAll(Scintilla editor)
        {
            ComputeFolds();

            int originalZoom = editor.Zoom;

            //Only visible lines get folded, so we zoom way out :)
            editor.Zoom = -1000000000;
            editor.Refresh();

            bool folded = false;
            do
            {
                folded = false;
                editor.Lines[0].Goto();
                editor.Refresh();

                foreach (Line l in editor.Lines)
                {
                    // don't fold headers
                    if (l.Number == 0) continue;

                    if (l.IsFoldPoint && l.FoldExpanded == true && l.FoldParent == null)
                    {
                        folded = true;
                        //HACK: only visible lines get folded if we don't do this.
                        //TODO: check scintilla docs for a less hacky solution to this
                        l.Goto();
                        l.EnsureVisible();

                        l.ToggleFoldExpanded();
                    }
                }
            } while (folded);

            editor.Lines[0].Goto();

            editor.Zoom = originalZoom;
            editor.Refresh();
        }

        /// <summary>
        /// Saves the contents of the specified editor.
        /// </summary>
        /// <param name="editor">The editor we wish to save the contents of.</param>
        private void Save(Editor editor)
        {
            string fileName = _files[editor];


            // We need to do this so that we don't have a cached file info list
            // with out-of-date timestamps
            _currentProject.InvalidateFileInfoCache();
            _currentProject.FileWatcher.EnableRaisingEvents = false;

            try
            {
                StreamWriter writer;
                if (System.IO.Path.IsPathRooted(fileName))
                    writer = new StreamWriter(fileName);
                else
                    writer = new StreamWriter(_currentProject.projNameToFileSystem(fileName));

                writer.Write(editor.Text);
                writer.Close();

                LuaAnalyzer.ProjectChecker.updateModule(CurrentProject.GetModules(), fileName);

                editor.Modified = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            _currentProject.FileWatcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Opens the file if it is not already open. Selects the page containing
        /// the file's editor so that the user can see it.
        /// </summary>
        /// <param name="fileName">
        ///   The name of the file we wish to open.
        /// </param>
        /// <param name="autoFold">
        ///   When this is true, the file's outer-level functions are folded
        ///   in addition to the file being opened and displayed.
        /// </param>
        public void OpenFile(string fileName, bool autoFold = false, TabControl containingControl = null)
        {
            if (_currentProject == null)
            {
                MessageBox.Show("Cannot open: no project is currently open. Either create a new project, or open an existing one first.");
                return;
            }

            if (_files.Contains(fileName))
            {
                Editor editor = _files[fileName];
                TabPage page = (TabPage)editor.Parent;
                editor.TypingLine = false;

                if (containingControl != null)
                    _tabManager.MovePageToFrontOfControl(page, containingControl);
                else
                    _tabManager.MovePageToFrontOfControl(page, _tabManager.LeftControl);

                if (autoFold)
                    FoldAll(editor);

                return;
            }

            Editor newEditor = SpawnEditor(fileName, containingControl);
            StreamReader reader = new StreamReader(fileName);
            newEditor.Text = reader.ReadToEnd();
            reader.Close();
            newEditor.Modified = false;
            newEditor.Refresh();
            _currentEditor = (Editor)newEditor;
            FoldAll(newEditor);
            CheckErrors();
            newEditor.Focus();
        }

        /// <summary>
        /// Saves all open files, closes all editors.
        /// </summary>
        private void SaveAndCloseAllEditors()
        {
            // Contract.Ensures(_files.Count == 0);

            Editor[] editors = new Editor[_files.EnumerateB().Count];
            _files.EnumerateB().CopyTo((Editor[])editors, 0);
            foreach (Editor editor in editors)
                CloseEditor(editor);
        }

        private bool CurrentLineBalanced()
        {
            string line = _currentEditor.Lines.Current.Text;

            int squareCount = 0;
            int parenCount = 0;
            int curlyCount = 0;

            for (int i = 0; i < line.Length; ++i)
            {
                if (line[i] == '[')
                    squareCount++;
                else if (line[i] == ']')
                    squareCount--;
                else if (line[i] == '(')
                    parenCount++;
                else if (line[i] == ')')
                    parenCount--;
                else if (line[i] == '{')
                    curlyCount++;
                else if (line[i] == '}')
                    curlyCount--;
            }

            return (squareCount == 0) && (parenCount == 0) && (curlyCount == 0);
        }

        private void CheckErrors()
        {
            ComputeFolds();

            _parseTimer.Stop();

            SpawnCallTip();

            _currentEditor.Indicators[1].Color = Color.Red;
            _currentEditor.GetRange().ClearIndicator(1);

            //try
            //{
            var errors = Analyzer.getFileErrors(
              _currentProject.GetModules(),
              _currentProject.RootPath,
              _currentProject.TypeCheckingEnabled,
              _files[_currentEditor]
            );

            // don't display errors while typing a line
            // it's tempting to display type errors only, but unfinished code
            // often manifests as type errors. Consider the scenario in which
            // the user is in the middle of typing the first line the following code:
            // self.
            // self.x = 3
            // it parses correctly, but may generate an error "self does not have a self field"
            if (_currentEditor.TypingLine) return;

            foreach (var error in errors.Item1)
            {
                string fileName = error.Item1;
                string errorMessage = error.Item2;

                if (fileName != _files[_currentEditor])
                    continue;

                int l = error.Item3.Item1;
                int r = error.Item3.Item2;

                int caretLine = _currentEditor.Caret.LineNumber;
                int caretLineStart = _currentEditor.Lines[caretLine].StartPosition;
                int caretByteIndex = _currentEditor.ComputeByteOffsetFromCharOffset(_currentEditor.Caret.Position);

                if (caretLineStart < l && l <= caretByteIndex && caretByteIndex < r) continue;

                int byteL = _currentEditor.ComputeByteOffsetFromCharOffset(l);
                int byteR = _currentEditor.ComputeByteOffsetFromCharOffset(r);

                _currentEditor.GetRange(byteL, byteR).SetIndicator(1);
            }
            //}      
            //catch(HandLexer.LexError err)
            //{
            //editor.CallTip.Message = le.Data0;
            //  editor.CallTip.Show(editor.ComputeByteOffsetFromCharOffset(le.Data1));
            //}
        }

        /// <summary>
        /// Opens the specified project file, which consists of creating a new _currentProject object (from stored luaproj settings), 
        /// clearing out all intellisense cache, and updating the title bar.
        /// </summary>
        /// <param name="fileName">The filename for the project file we wish to open.</param>
        private void openProject(string fileName)
        {
            _currentProject = new Project(fileName, this);
            ProjectChecker.switchProject(_currentProject.RootPath);
            var rnd = new Random();
            //this.Text = "- chrømitrøn - (" + fileName + ")";
            //this.Text = "ꙛ Burger ꙛ (" + fileName + ")"; 
            //this.Text = "✩✭✮ chromitron ✮✭✩ (" + fileName + ")";
            //this.Text = "⥍⥍ chromitron ⥍⥍ (" + fileName + ")";
            var x = rnd.Next(3);
            if (x == 0)
                this.Text = "😎 Game Kitchen 😎 (" + fileName + ")";
            else if (x == 1)
                this.Text = "✩✭✮ Game Kitchen ✮✭✩ (" + fileName + ")";
            else
                this.Text = "⥍⥍ Game Kitchen ⥍⥍ (" + fileName + ")";
        }

        /// <summary>
        /// When the FileSystemWatcher (see Project.cs) detecteds that a lua file
        /// under the root project directory has changed, this method is called,
        /// which reloads the file if the user wants.
        /// </summary>
        /// <param name="path">The path of the file which changed.</param>
        public void ReloadFile(string path)
        {
            if (!_files.Contains(path))
                return;

            var result = MessageBox.Show(
              "\"" + path + "\" has changed outside of LӦVE Studio. Would you like to reload it?",
              "File Changed",
              MessageBoxButtons.YesNo
            );

            Scintilla editor = _files[path];

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                if (!_debugProxy.Connected)
                {
                    StreamReader reader = new StreamReader(path);
                    editor.Text = reader.ReadToEnd();
                    editor.UndoRedo.EmptyUndoBuffer();
                    reader.Close();
                    editor.Modified = false;
                    editor.Refresh();
                    FoldAll(editor);
                }
                else
                {
                    MessageBox.Show("Cannot reload while debugging.");
                    editor.Modified = true;
                }
            }
            else
            {
                editor.Modified = true;
            }
        }

        /// <summary>
        /// Saves all currently-open editors in the project.
        /// </summary>
        private void SaveAll()
        {
            foreach (Editor editor in _files.EnumerateB())
            {
                if (editor.Modified)
                    Save(editor);
            }
        }

        private void collectTypes()
        {
            var files = _currentProject.GetSourceFiles();


            foreach (var file in files)
            {
                var fileName = _currentProject.projRelative(file.FullName);
                StreamReader reader = new StreamReader(file.FullName);
                var contents = reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Returns the contents of the specified line of the specified source file.
        /// </summary>
        /// <param name="fileName">The file containing the line to retrieve.</param>
        /// <param name="lineNum">The number of the line to retrieve.</param>
        /// <returns></returns>
        public string GetLineContents(string fileName, int lineNum)
        {
            // Contract.Assert(_files.Contains(fileName));
            Scintilla editor = _files[fileName];
            return editor.Lines[lineNum].Text;
        }

        private bool IsTextAltering(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
                return true;
            else if (e.KeyCode == Keys.Return)
                return true;
            else if (e.KeyCode == Keys.X && e.Control)
                return true;
            else if (e.KeyCode == Keys.Z && e.Control)
                return true;
            else if (e.KeyCode == Keys.Y && e.Control)
                return true;
            else if (e.KeyCode == Keys.L && e.Control)
                return true;

            return false;
        }

        private bool IsAlpha(KeyEventArgs e)
        {
            return e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z;
        }

        private void Studio_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_debugProxy.Connected)
                _debugProxy.TerminateTarget();
        }

        private void Studio_Paint(object sender, PaintEventArgs e)
        {
            statusStrip.Top = Bottom - statusStrip.Height;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_currentProject != null)
            {
                ProjectSettingsDialog newDialog = new ProjectSettingsDialog(_moduleConstructors, _currentProject);
                newDialog.Show();
            }
            else
            {
                MessageBox.Show("Cannot edit project settings because no project is currently opened.");
            }
        }

        private void saveFileMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void closeF4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TerminateTarget();
        }
    }
}
