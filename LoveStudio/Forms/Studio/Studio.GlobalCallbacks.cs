﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;
using LuaAnalyzer;

namespace LoveEdit
{
  public partial class Studio
  {
    private Dictionary<Tuple<bool, bool, bool, Keys>, Command> _globalInputMap;

    private Tuple<bool, bool, bool, Keys> Entry(bool shiftDown, bool altDown, bool ctrlDown, Keys keyCode)
    {
      return new Tuple<bool, bool, bool, Keys>(shiftDown, altDown, ctrlDown, keyCode);
    }

    private void BuildGlobalInputMap()
    {
      _globalInputMap = new Dictionary<Tuple<bool, bool, bool, Keys>, Command>();
      _globalInputMap[Entry(false, false, true, Keys.O)] = OpenFromProject;
      _globalInputMap[Entry(false, false, true, Keys.N)] = SpawnCreateModuleBox;
      _globalInputMap[Entry(false, false, false, Keys.F4)] = TerminateTarget;
      _globalInputMap[Entry(false, false, false, Keys.F5)] = StartOrResumeDebugging;
      _globalInputMap[Entry(false, false, false, Keys.F6)] = StepOver;
      _globalInputMap[Entry(false, false, false, Keys.F7)] = StepIn;
      _globalInputMap[Entry(true, false, true, Keys.F)] = FindInProject;
      _globalInputMap[Entry(false, false, true, Keys.P)] = DebugTypeCheck;
    }

    private void TerminateTarget()
    {
      if (_debugProxy.Running)
        _debugProxy.TerminateTarget();
    }

    // [Pure]
    public bool HasGlobalCommand(KeyEventArgs e)
    {
      var entry = Entry(e.Shift, e.Alt, e.Control, e.KeyCode);
      return _globalInputMap.ContainsKey(entry);
    }

    private Command GetGlobalCommand(KeyEventArgs e)
    {
      // Contract.Requires(HasGlobalCommand(e));
      var entry = Entry(e.Shift, e.Alt, e.Control, e.KeyCode);
      return _globalInputMap[entry];
    }

    private void DebugTypeCheck()
    {
      var errors = Analyzer.getProjectErrors(
        _currentProject.GetModules(),
        _currentProject.RootPath,
        _currentProject.TypeCheckingEnabled
      );
      _errorPanel.AddErrorsFromList(errors.Item1);
    }

    /// <summary>
    /// Spawns a popup which allows the user to open up a file
    /// by typing in its name.
    /// </summary>
    private void OpenFromProject()
    {
      ProjectOpenBox box = new ProjectOpenBox(this,_debugProxy,ProjectOpenBox.Type.OPEN);
      box.Show();
    }

    /// <summary>
    /// If the game isn't already being debugged, begins debugging.
    /// If the debugger is breaking, resumes from breakpoint.
    /// If the debugger is trapping an error, clears debug info and returns 
    /// to edit mode.
    /// </summary>
    private void StartOrResumeDebugging()
    {
      if (_currentProject.EmbedInGamePanel)
          ShowGamePanel();

      if (!_debugProxy.Connected)
        RunTarget();
      else if (_debugProxy.Breaking)
        _debugProxy.Resume();
    }

    /// <summary>
    /// 
    /// </summary>
    private void RunTarget()
    {
      // Contract.Requires(!_debugProxy.Connected);
      // Contract.Ensures(_debugProxy.Connected || CurrentProject == null || _errorPanel.HasErrors());

      if (CurrentProject == null)
      {
        MessageBox.Show("'Run' cannot be used when no project is currently opened.");
        return;
      }

      SaveAll();
      _errorPanel.DetectErrors();
      if (_errorPanel.HasErrors())
      {
        TabPage errorPage = (TabPage)_errorPanel.Parent;
        TabControl tabControl = (TabControl)errorPage.Parent;
        tabControl.SelectTab(errorPage);

        MessageBox.Show("Project has errors. Fix these before running.");
        return;
      }

      string rootPath = CurrentProject.RootPath;
      // remove the trailing '\\' for love
      rootPath = rootPath.Substring(0, rootPath.Length - 1);
      _outputPanel.OnDebugStart();

      ProcessStartInfo info = new ProcessStartInfo(
        _loveEditDirectory + "\\love\\love.exe", 
        "\"" + rootPath + "\"" //quotes necessary to handle spaces in project path
      );

      info.WorkingDirectory = rootPath;
      var p = Process.Start(info);
      _gamePanel.SetProcess(p);

      while (!_debugProxy.Connected) { }
    }

    /// <summary>
    /// Tells the debugger to step over.
    /// </summary>
    private void StepOver()
    {
      if (_debugProxy.Breaking)
        _debugProxy.StepOver();
    }

    /// <summary>
    /// Tells the debugger to step in.
    /// </summary>
    private void StepIn()
    {
      if (_debugProxy.Breaking)
        _debugProxy.StepIn();
    }

    /// <summary>
    /// Spawns a find/replace dialog for the entire project.
    /// </summary>
    private void FindInProject()
    {
      FindReplaceDialog frDialog = new FindReplaceDialog(_searchResultsPanel);
      frDialog.Show();
    }

    /// <summary>
    /// Spawns the new module creation dialog.
    /// </summary>
    private void SpawnCreateModuleBox()
    {
      Form createModuleDialog = new Forms.CreateModuleDialog(_moduleConstructors,_currentProject);
      createModuleDialog.Show();
    }
  }
}
