﻿

namespace LoveEdit
{
  partial class Studio
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Studio));
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.openFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.closeF4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.lineStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.columnStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.tabControlLeft = new System.Windows.Forms.TabControl();
      this.tabControlRight = new System.Windows.Forms.TabControl();
      this.quoteStrip = new System.Windows.Forms.StatusStrip();
      this.quoteLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.menuStrip1.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.quoteStrip.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.projectToolStripMenuItem,
            this.helpToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(977, 24);
      this.menuStrip1.TabIndex = 3;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.openFileMenuItem});
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // newFileToolStripMenuItem
      // 
      this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
      this.newFileToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
      this.newFileToolStripMenuItem.Text = "New File";
      this.newFileToolStripMenuItem.Click += new System.EventHandler(this.newFileToolStripMenuItem_Click);
      // 
      // openFileMenuItem
      // 
      this.openFileMenuItem.Name = "openFileMenuItem";
      this.openFileMenuItem.Size = new System.Drawing.Size(124, 22);
      this.openFileMenuItem.Text = "Open &File";
      this.openFileMenuItem.Click += new System.EventHandler(this.openFileMenuItem_Click);
      // 
      // projectToolStripMenuItem
      // 
      this.projectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.openProjectToolStripMenuItem,
            this.runToolStripMenuItem,
            this.closeF4ToolStripMenuItem,
            this.settingsToolStripMenuItem});
      this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
      this.projectToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
      this.projectToolStripMenuItem.Text = "Project";
      // 
      // newProjectToolStripMenuItem
      // 
      this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
      this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.newProjectToolStripMenuItem.Text = "New Project";
      this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
      // 
      // openProjectToolStripMenuItem
      // 
      this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
      this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.openProjectToolStripMenuItem.Text = "Open Project";
      this.openProjectToolStripMenuItem.Click += new System.EventHandler(this.openProjectToolStripMenuItem_Click);
      // 
      // runToolStripMenuItem
      // 
      this.runToolStripMenuItem.Name = "runToolStripMenuItem";
      this.runToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.runToolStripMenuItem.Text = "Run (F5)";
      this.runToolStripMenuItem.Click += new System.EventHandler(this.runToolStripMenuItem_Click);
      // 
      // closeF4ToolStripMenuItem
      // 
      this.closeF4ToolStripMenuItem.Name = "closeF4ToolStripMenuItem";
      this.closeF4ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.closeF4ToolStripMenuItem.Text = "Close Game (F4)";
      this.closeF4ToolStripMenuItem.Click += new System.EventHandler(this.closeF4ToolStripMenuItem_Click);
      // 
      // settingsToolStripMenuItem
      // 
      this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      this.settingsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
      this.settingsToolStripMenuItem.Text = "Settings";
      this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
      // 
      // helpToolStripMenuItem
      // 
      this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
      this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
      this.helpToolStripMenuItem.Text = "Help";
      this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.statusStrip, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.tabControlLeft, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.tabControlRight, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.quoteStrip, 1, 1);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(977, 552);
      this.tableLayoutPanel1.TabIndex = 4;
      // 
      // statusStrip
      // 
      this.statusStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.statusStrip.Dock = System.Windows.Forms.DockStyle.None;
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lineStatusLabel,
            this.columnStatusLabel});
      this.statusStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
      this.statusStrip.Location = new System.Drawing.Point(0, 530);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(488, 22);
      this.statusStrip.TabIndex = 5;
      this.statusStrip.Text = "statusStrip";
      // 
      // lineStatusLabel
      // 
      this.lineStatusLabel.Name = "lineStatusLabel";
      this.lineStatusLabel.Size = new System.Drawing.Size(0, 17);
      // 
      // columnStatusLabel
      // 
      this.columnStatusLabel.Name = "columnStatusLabel";
      this.columnStatusLabel.Size = new System.Drawing.Size(0, 17);
      // 
      // tabControlLeft
      // 
      this.tabControlLeft.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControlLeft.Location = new System.Drawing.Point(3, 3);
      this.tabControlLeft.Name = "tabControlLeft";
      this.tabControlLeft.SelectedIndex = 0;
      this.tabControlLeft.Size = new System.Drawing.Size(482, 524);
      this.tabControlLeft.TabIndex = 4;
      // 
      // tabControlRight
      // 
      this.tabControlRight.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControlRight.Location = new System.Drawing.Point(491, 3);
      this.tabControlRight.Name = "tabControlRight";
      this.tabControlRight.SelectedIndex = 0;
      this.tabControlRight.Size = new System.Drawing.Size(483, 524);
      this.tabControlRight.TabIndex = 5;
      // 
      // quoteStrip
      // 
      this.quoteStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.quoteStrip.Dock = System.Windows.Forms.DockStyle.None;
      this.quoteStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quoteLabel});
      this.quoteStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
      this.quoteStrip.Location = new System.Drawing.Point(488, 530);
      this.quoteStrip.Name = "quoteStrip";
      this.quoteStrip.Size = new System.Drawing.Size(489, 22);
      this.quoteStrip.TabIndex = 7;
      this.quoteStrip.Text = ":)";
      // 
      // quoteLabel
      // 
      this.quoteLabel.BackColor = System.Drawing.SystemColors.Control;
      this.quoteLabel.ForeColor = System.Drawing.Color.Gainsboro;
      this.quoteLabel.Name = "quoteLabel";
      this.quoteLabel.Size = new System.Drawing.Size(0, 17);
      this.quoteLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // Studio
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(977, 576);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Controls.Add(this.menuStrip1);
      this.KeyPreview = true;
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "Studio";
      this.Text = "😎 Game Kitchen 😎";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Studio_FormClosed);
      this.Paint += new System.Windows.Forms.PaintEventHandler(this.Studio_Paint);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.studio_keyDown);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.quoteStrip.ResumeLayout(false);
      this.quoteStrip.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openFileMenuItem;
    private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.TabControl tabControlLeft;
    private System.Windows.Forms.TabControl tabControlRight;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripStatusLabel lineStatusLabel;
    private System.Windows.Forms.ToolStripStatusLabel columnStatusLabel;
    private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    private System.Windows.Forms.StatusStrip quoteStrip;
    private System.Windows.Forms.ToolStripStatusLabel quoteLabel;
    private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem closeF4ToolStripMenuItem;

  }
}

