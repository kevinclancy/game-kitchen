﻿namespace LoveEdit
{
  partial class DebugPanel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.resumeButton = new System.Windows.Forms.Button();
      this.debugTable = new System.Windows.Forms.TableLayoutPanel();
      this.stackPanel = new LoveEdit.StackPanel();
      this.debugTabs = new System.Windows.Forms.TabControl();
      this.localsPage = new System.Windows.Forms.TabPage();
      this.localsDebugBrowser = new LoveEdit.DebugBrowser();
      this.globalsPage = new System.Windows.Forms.TabPage();
      this.globalsDebugBrowser = new LoveEdit.DebugBrowser();
      this.upvaluesPage = new System.Windows.Forms.TabPage();
      this.upvaluesDebugBrowser = new LoveEdit.DebugBrowser();
      this.commandButtonPanel = new System.Windows.Forms.TableLayoutPanel();
      this.stepOverButton = new System.Windows.Forms.Button();
      this.stepInButton = new System.Windows.Forms.Button();
      this.debugTable.SuspendLayout();
      this.debugTabs.SuspendLayout();
      this.localsPage.SuspendLayout();
      this.globalsPage.SuspendLayout();
      this.upvaluesPage.SuspendLayout();
      this.commandButtonPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // resumeButton
      // 
      this.resumeButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this.resumeButton.Enabled = false;
      this.resumeButton.Location = new System.Drawing.Point(3, 3);
      this.resumeButton.Name = "resumeButton";
      this.resumeButton.Size = new System.Drawing.Size(166, 24);
      this.resumeButton.TabIndex = 0;
      this.resumeButton.Text = "Resume (F5)";
      this.resumeButton.UseVisualStyleBackColor = true;
      this.resumeButton.Click += new System.EventHandler(this.resumeButton_Click);
      // 
      // debugTable
      // 
      this.debugTable.BackColor = System.Drawing.Color.Transparent;
      this.debugTable.ColumnCount = 1;
      this.debugTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.debugTable.Controls.Add(this.stackPanel, 0, 2);
      this.debugTable.Controls.Add(this.debugTabs, 0, 1);
      this.debugTable.Controls.Add(this.commandButtonPanel, 0, 0);
      this.debugTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.debugTable.Location = new System.Drawing.Point(0, 0);
      this.debugTable.Name = "debugTable";
      this.debugTable.RowCount = 3;
      this.debugTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
      this.debugTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
      this.debugTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.debugTable.Size = new System.Drawing.Size(523, 458);
      this.debugTable.TabIndex = 2;
      // 
      // stackPanel
      // 
      this.stackPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.stackPanel.Location = new System.Drawing.Point(3, 355);
      this.stackPanel.Name = "stackPanel";
      this.stackPanel.Size = new System.Drawing.Size(517, 100);
      this.stackPanel.TabIndex = 0;
      // 
      // debugTabs
      // 
      this.debugTabs.Controls.Add(this.localsPage);
      this.debugTabs.Controls.Add(this.globalsPage);
      this.debugTabs.Controls.Add(this.upvaluesPage);
      this.debugTabs.Dock = System.Windows.Forms.DockStyle.Fill;
      this.debugTabs.Location = new System.Drawing.Point(3, 39);
      this.debugTabs.Name = "debugTabs";
      this.debugTabs.SelectedIndex = 0;
      this.debugTabs.Size = new System.Drawing.Size(517, 310);
      this.debugTabs.TabIndex = 1;
      // 
      // localsPage
      // 
      this.localsPage.Controls.Add(this.localsDebugBrowser);
      this.localsPage.Location = new System.Drawing.Point(4, 22);
      this.localsPage.Name = "localsPage";
      this.localsPage.Padding = new System.Windows.Forms.Padding(3);
      this.localsPage.Size = new System.Drawing.Size(509, 284);
      this.localsPage.TabIndex = 0;
      this.localsPage.Text = "Locals";
      this.localsPage.UseVisualStyleBackColor = true;
      // 
      // localsDebugBrowser
      // 
      this.localsDebugBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
      this.localsDebugBrowser.Location = new System.Drawing.Point(3, 3);
      this.localsDebugBrowser.Name = "localsDebugBrowser";
      this.localsDebugBrowser.RequestChildren = null;
      this.localsDebugBrowser.RootName = "Locals";
      this.localsDebugBrowser.Size = new System.Drawing.Size(503, 278);
      this.localsDebugBrowser.TabIndex = 0;
      // 
      // globalsPage
      // 
      this.globalsPage.Controls.Add(this.globalsDebugBrowser);
      this.globalsPage.Location = new System.Drawing.Point(4, 22);
      this.globalsPage.Name = "globalsPage";
      this.globalsPage.Padding = new System.Windows.Forms.Padding(3);
      this.globalsPage.Size = new System.Drawing.Size(509, 284);
      this.globalsPage.TabIndex = 1;
      this.globalsPage.Text = "Globals";
      this.globalsPage.UseVisualStyleBackColor = true;
      // 
      // globalsDebugBrowser
      // 
      this.globalsDebugBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
      this.globalsDebugBrowser.Location = new System.Drawing.Point(3, 3);
      this.globalsDebugBrowser.Name = "globalsDebugBrowser";
      this.globalsDebugBrowser.RequestChildren = null;
      this.globalsDebugBrowser.RootName = "Globals";
      this.globalsDebugBrowser.Size = new System.Drawing.Size(503, 278);
      this.globalsDebugBrowser.TabIndex = 0;
      // 
      // upvaluesPage
      // 
      this.upvaluesPage.Controls.Add(this.upvaluesDebugBrowser);
      this.upvaluesPage.Location = new System.Drawing.Point(4, 22);
      this.upvaluesPage.Name = "upvaluesPage";
      this.upvaluesPage.Padding = new System.Windows.Forms.Padding(3);
      this.upvaluesPage.Size = new System.Drawing.Size(509, 284);
      this.upvaluesPage.TabIndex = 2;
      this.upvaluesPage.Text = "Upvalues";
      this.upvaluesPage.UseVisualStyleBackColor = true;
      // 
      // upvaluesDebugBrowser
      // 
      this.upvaluesDebugBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
      this.upvaluesDebugBrowser.Location = new System.Drawing.Point(3, 3);
      this.upvaluesDebugBrowser.Name = "upvaluesDebugBrowser";
      this.upvaluesDebugBrowser.RequestChildren = null;
      this.upvaluesDebugBrowser.RootName = "Upvalues";
      this.upvaluesDebugBrowser.Size = new System.Drawing.Size(503, 278);
      this.upvaluesDebugBrowser.TabIndex = 0;
      // 
      // commandButtonPanel
      // 
      this.commandButtonPanel.ColumnCount = 3;
      this.commandButtonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.commandButtonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.commandButtonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.commandButtonPanel.Controls.Add(this.stepOverButton, 1, 0);
      this.commandButtonPanel.Controls.Add(this.resumeButton, 0, 0);
      this.commandButtonPanel.Controls.Add(this.stepInButton, 2, 0);
      this.commandButtonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.commandButtonPanel.Location = new System.Drawing.Point(3, 3);
      this.commandButtonPanel.Name = "commandButtonPanel";
      this.commandButtonPanel.RowCount = 1;
      this.commandButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.commandButtonPanel.Size = new System.Drawing.Size(517, 30);
      this.commandButtonPanel.TabIndex = 2;
      // 
      // stepOverButton
      // 
      this.stepOverButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this.stepOverButton.Enabled = false;
      this.stepOverButton.Location = new System.Drawing.Point(175, 3);
      this.stepOverButton.Name = "stepOverButton";
      this.stepOverButton.Size = new System.Drawing.Size(166, 24);
      this.stepOverButton.TabIndex = 3;
      this.stepOverButton.Text = "Step Over (F6)";
      this.stepOverButton.UseVisualStyleBackColor = true;
      this.stepOverButton.Click += new System.EventHandler(this.stepOverButton_Click);
      // 
      // stepInButton
      // 
      this.stepInButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this.stepInButton.Enabled = false;
      this.stepInButton.Location = new System.Drawing.Point(347, 3);
      this.stepInButton.Name = "stepInButton";
      this.stepInButton.Size = new System.Drawing.Size(167, 24);
      this.stepInButton.TabIndex = 4;
      this.stepInButton.Text = "Step In (F7)";
      this.stepInButton.UseVisualStyleBackColor = true;
      this.stepInButton.Click += new System.EventHandler(this.stepInButton_Click);
      // 
      // DebugPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.debugTable);
      this.Name = "DebugPanel";
      this.Size = new System.Drawing.Size(523, 458);
      this.debugTable.ResumeLayout(false);
      this.debugTabs.ResumeLayout(false);
      this.localsPage.ResumeLayout(false);
      this.globalsPage.ResumeLayout(false);
      this.upvaluesPage.ResumeLayout(false);
      this.commandButtonPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button resumeButton;
    private System.Windows.Forms.TableLayoutPanel debugTable;
    private System.Windows.Forms.TableLayoutPanel commandButtonPanel;
    private System.Windows.Forms.Button stepOverButton;
    private System.Windows.Forms.Button stepInButton;
    private StackPanel stackPanel;
    private System.Windows.Forms.TabControl debugTabs;
    private System.Windows.Forms.TabPage localsPage;
    private DebugBrowser localsDebugBrowser;
    private System.Windows.Forms.TabPage globalsPage;
    private DebugBrowser globalsDebugBrowser;
    private System.Windows.Forms.TabPage upvaluesPage;
    private DebugBrowser upvaluesDebugBrowser;
  }
}
