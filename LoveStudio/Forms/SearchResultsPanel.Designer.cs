﻿namespace LoveEdit
{
  partial class SearchResultsPanel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._treeListView = new WinControls.ListView.TreeListView();
      this._fileNameHeader = new WinControls.ListView.ContainerColumnHeader();
      this._lineNumHeader = new WinControls.ListView.ContainerColumnHeader();
      this._lineContentsHeader = new WinControls.ListView.ContainerColumnHeader();
      this.SuspendLayout();
      // 
      // _treeListView
      // 
      this._treeListView.Columns.AddRange(new WinControls.ListView.ContainerColumnHeader[] {
            this._fileNameHeader,
            this._lineNumHeader,
            this._lineContentsHeader});
      this._treeListView.Dock = System.Windows.Forms.DockStyle.Fill;
      this._treeListView.Location = new System.Drawing.Point(0, 0);
      this._treeListView.Name = "_treeListView";
      this._treeListView.Size = new System.Drawing.Size(379, 325);
      this._treeListView.TabIndex = 0;
      this._treeListView.DoubleClick += new System.EventHandler(this._treeListView_DoubleClick);
      // 
      // _fileNameHeader
      // 
      this._fileNameHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._fileNameHeader.Text = "File";
      this._fileNameHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      // 
      // _lineNumHeader
      // 
      this._lineNumHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lineNumHeader.Text = "Line";
      this._lineNumHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this._lineNumHeader.Width = 50;
      // 
      // _lineContentsHeader
      // 
      this._lineContentsHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lineContentsHeader.Text = "Contents";
      this._lineContentsHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this._lineContentsHeader.Width = 10000;
      // 
      // SearchResultsPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._treeListView);
      this.Name = "SearchResultsPanel";
      this.Size = new System.Drawing.Size(379, 325);
      this.ResumeLayout(false);

    }

    #endregion

    private WinControls.ListView.TreeListView _treeListView;
    private WinControls.ListView.ContainerColumnHeader _fileNameHeader;
    private WinControls.ListView.ContainerColumnHeader _lineNumHeader;
    private WinControls.ListView.ContainerColumnHeader _lineContentsHeader;
  }
}
