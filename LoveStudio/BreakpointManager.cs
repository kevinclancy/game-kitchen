﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoveEdit
{
  /// <summary>
  /// Stores the file name and line number of a breakpoint which
  /// the user has set by clicking in the margin of an editor.
  /// </summary>
  public class Breakpoint : EventArgs
  {
    /// <summary>
    /// Create a new breakpoint in the specified file on the specified line
    /// number.
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="lineNumber"></param>
    public Breakpoint(string fileName, int lineNumber)
    {
      this.fileName = fileName;
      this.lineNumber = lineNumber;
    }

    /// <summary>
    /// The name of the file that the breakpoint is contained in.
    /// </summary>
    public readonly string fileName;

    /// <summary>
    /// The line number that the breakpoint is on.
    /// </summary>
    public readonly int lineNumber;
  }

  /// <summary>
  /// Stores all current breakpoints, and publishes events for when breakpoints
  /// are added and removed.
  /// </summary>
  public class BreakpointManager
  {
    /// <summary>
    /// An event which is triggered when a breakpoint is added.
    /// </summary>
    event EventHandler<Breakpoint> _raiseBreakpointAdded;

    /// <summary>
    /// An event which is triggered when a breakpoint is removed.
    /// </summary>
    event EventHandler<Breakpoint> _raiseBreakpointRemoved;

    /// <summary>
    /// Maps a file name to the set of breakpoints currently set in that file.
    /// If no breakpoints are set in a file, the file is mapped to null.
    /// </summary>
    readonly Dictionary<string, SortedSet<int>> _breakpointMap; 

    /// <summary>
    /// For each file containing breakpoints, maps the name of the file to
    /// the set containing all lines numbers which contain break points.
    /// </summary>
    public Dictionary<string, SortedSet<int>> BreakpointMap
    {
      get
      {
        return _breakpointMap;
      }
    }

    /// <summary>
    /// Creates a breakpoint manager.
    /// </summary>
    public BreakpointManager()
    {
      _breakpointMap = new Dictionary<string, SortedSet<int>>();
    }

    /// <summary>
    /// True iff there is a breakpoint on the specified line number of the
    /// specified file.
    /// </summary>
    /// <param name="fileName">The name of the file.</param>
    /// <param name="lineNumber">The line number.</param>
    /// <returns></returns>
    // [Pure]
    public bool ContainsBreakpoint(string fileName, int lineNumber)
    {
      return _breakpointMap.ContainsKey(fileName) &&
             _breakpointMap[fileName].Contains(lineNumber);
    }

    /// <summary>
    /// Subscribes to an event which is triggered whenever a breakpoint is 
    /// added.
    /// </summary>
    /// <param name="subscriber"></param>
    public void SubscribeBreakpointAdded(EventHandler<Breakpoint> subscriber)
    {
      _raiseBreakpointAdded += subscriber;
    }

    /// <summary>
    /// Subscribes to an event which is triggered whenever a breakpoint is 
    /// removed.
    /// </summary>
    /// <param name="subscriber"></param>
    public void SubscribeBreakpointRemoved(EventHandler<Breakpoint> subscriber)
    {
      _raiseBreakpointRemoved += subscriber;
    }

    /// <summary>
    /// Sets a new breakpoint, publishing the location of the breakpoint
    /// to subscribers such as the debug client and breakpoint window.
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="lineNumber"></param>
    public void AddBreakpoint(string fileName, int lineNumber)
    {
      // Contract.Requires(!ContainsBreakpoint(fileName, lineNumber));
      // Contract.Ensures(ContainsBreakpoint(fileName, lineNumber));

      if (!_breakpointMap.ContainsKey(fileName))
        _breakpointMap[fileName] = new SortedSet<int>();

      _breakpointMap[fileName].Add(lineNumber);

      if (_raiseBreakpointAdded != null)
        _raiseBreakpointAdded(this, new Breakpoint(fileName, lineNumber));
    }

    /// <summary>
    /// Removes a set breakpoint, publishing the location of the breakpoint
    /// to subscribers such as the debug client and breakpoint window. 
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="lineNumber"></param>
    public void RemoveBreakpoint(string fileName, int lineNumber)
    {
      // Contract.Requires(ContainsBreakpoint(fileName, lineNumber));
      // Contract.Ensures(!ContainsBreakpoint(fileName, lineNumber));

      _breakpointMap[fileName].Remove(lineNumber);

      if (_breakpointMap[fileName].Count == 0)
        _breakpointMap.Remove(fileName);

      if (_raiseBreakpointRemoved != null)
        _raiseBreakpointRemoved(this, new Breakpoint(fileName, lineNumber));
    }

    /// <summary>
    /// Removes all breakpoints from a specified file.
    /// </summary>
    /// <param name="fileName">The name of the file to remove all breakpoints from.</param>
    public void RemoveAllBreakpointsFromFile(string fileName)
    {
      if (!_breakpointMap.ContainsKey(fileName))
        return;

      int[] breakpoints = new int[_breakpointMap[fileName].Count];
      _breakpointMap[fileName].CopyTo(breakpoints);

      foreach (int i in breakpoints)
        RemoveBreakpoint(fileName, i);
    }
  }
}
