﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LoveEdit
{
  class PathUtils
  {
    /// <summary>
    /// Given the full path of a file, returns the directory that it is 
    /// contained in, i.e. it removes everything beyond the last \.
    /// </summary>
    /// <param name="filePath">The file to get the directory of.</param>
    static public string ContainingDirectory(string filePath)
    {
      //TODO: copy .NET regex docs onto laptop;
      //Regex regex = new Regex("" 
      
      int lastSlashInd = -1;

      // I don't know how to use .NET regexes, so for now, I'll do it 
      // the stupid way.
      for(int i = 0; i < filePath.Length; ++i)
      {
        if (filePath[i] == '\\')
          lastSlashInd = i;
      }

      return filePath.Substring(0, lastSlashInd + 1); 
    }

    /// <summary>
    /// Given the full path of a file, return the file's path relative to 
    /// 
    /// </summary>
    /// <param name="projRoot">The root path of the project being edited.</param>
    /// <param name="filePath">The path to extract a file name from.</param>
    /// <returns></returns>
    static public string StripFileName(string projRoot, string filePath)
    {
      int lastSlashInd = -1;

      // I don't know how to use .NET regexes, so for now, I'll do it 
      // the stupid way.
      for (int i = 0; i < filePath.Length; ++i)
      {
        if (filePath[i] == '\\')
          lastSlashInd = i;
      }

      return filePath.Substring(lastSlashInd+1); 
    }
  }
}
