﻿module LuaAnalyzer.Syntax

[<System.Runtime.CompilerServices.InternalsVisibleTo("Microsoft.VisualStudio.TestTools")>]
do ()

open Annotations
open Operators

type Name = string

/// FunctionName(fieldNames, opt methodName)
and FunctionName = List<Name>*Option<Name>

and Statement =
    /// If((condition,chunk) list, elseExplicit, rng)
    /// all if statements have an else clause. If the programmer inserted
    /// it explicitly, elseExplicit is true. Otherwise, the else clause is
    /// inserted by the type checker, and elseExplicit is false.
    | If of List<Expr*Statement>*bool*Range
    /// While(cond,body)
    | While of Expr*Statement*Range
    /// Do(body)
    | Do of Statement*Range
    /// ForNum(var, start, fin, step, body)
    | ForNum of Expr*Expr*Expr*Expr*Statement*Range
    /// ForGeneric(vars, gens, )
    | ForGeneric of List<Expr>*List<Expr>*Statement*Range
    /// Repeat(cond,body)
    | Repeat of Expr*Statement*Range
    /// assign.
    | LocalAssign of List<Expr>*List<Expr>*Option<Annotations.Annotation>*Range
    /// Assign(lvalList, rvalList,annotation,range)
    | Assign of List<Expr>*List<Expr>*Option<Annotations.Annotation>*Range
    /// Return(exps) 
    | Return of List<Expr>*Range
    /// Break
    | Break of Range
    /// Call(callExp)
    | Call of Expr*Range
    /// Sequence(header,statements,range)
    | Sequence of Option<Annotation>*GenList<Statement>*Range
    | ErrorStatement of string*Range

and Expr =
    | Number of double*Range
    | String of string*Range
    | Nil of Range
    | True of Range
    | False of Range
    | VarArgs of Range
    | Constructor of List<ConstructorField>*Range
    /// selfName,formals,hasVarargs,body,annotation, range
    | Function of Option<string>*List<Expr>*bool*Statement*Option<Annotations.Annotation>*Range
    | UnOpExpr of UnOp*Expr*Range
    | BinOpExpr of BinOp*Expr*Expr*Range
    | ParenthesizedExpr of Expr*Range
    | NameExpr of Name*Range
    ///CallExpr(func,
    | CallExpr of Expr*List<Expr>*Range
    | ErrorExpr of string*Range

and ConstructorField =
    /// ListField(annotation,value,rng)
    | ListField of Option<Annotation>*Expr*Range
    /// RecField(annotation,key,value,rng)
    | RecField of Option<Annotation>*Expr*Expr*Range
    | ErrorField of string*Range

/// Statement(exprs,stats)
///
/// Matches a statement in which exprs contains all of the expressions
/// which are direct subterms of the scrutinee and stats contains all
/// statements which are direct subterms of the scrutinee.
///
/// This active pattern allows the user to decompose a statement into
/// its subexpresions and substatements in a way that is homogenous
/// across all Statement constructs. This is useful for searching
/// syntax trees for things like errors.
let (|Statement|) (stat : Statement) =
    match stat with
    /// If((condition,chunk) list)
    | If(clauses,_, _) ->
        let exprs,stats = List.unzip clauses
        (None,exprs,stats)
    | While(cond,body,_) ->
        (None,[cond],[body])
    | Do(stmt,_) ->
        (None,[],[stmt])
    | ForNum(var,start,fin,step,body,_) ->
        (None,[var;start;fin;step], [body])
    | ForGeneric(vars, gens, stmt, _) ->
        (None,List.append vars gens,[stmt])
    | Repeat(expr,stat,_) ->
        (None,[expr],[stat])
    | LocalAssign(lvals,rvals,optAnnotation,_) ->
        (optAnnotation,List.append lvals rvals,[])
    | Assign(lvals,rvals,optAnnotation,_) ->
        (optAnnotation,List.append lvals rvals, [])
    | Return(exprs,_) ->
        (None,exprs, [])
    | Break(_) ->
        (None,[],[])
    | Call(expr, _) ->
        (None,[expr],[])
    | Sequence(optAnnotation,stats,_) ->
        (optAnnotation,[],List.ofSeq stats)
    | ErrorStatement(_,_) ->
        (None,[],[])

/// Expr(exprs,statement,fields)
///
/// Matches an expr in which exprs contains all of the expressions
/// which are direct subterms of the scrutinee, stats contains all
/// statements which are direct subterms of the scrutinee, and fields
/// contains all constructor fields which are direct subterms of the 
/// scrutinee.
///
/// This active pattern allows the user to decompose an expression into
/// its subexpresions and substatements in a way that is homogenous
/// across all Expr constructs. This is useful for searching
/// syntax trees for things like errors.
let (|Expr|) (expr : Expr) =
    match expr with
    | Number(_,_)
    | String(_,_)
    | Nil(_)
    | True(_)
    | False(_)
    | VarArgs(_) 
    | NameExpr(_,_)
    | ErrorExpr(_,_) ->
        ([],[],[])
    | Constructor(fields,_) ->
        ([],[],fields)
    | Function(_,args, _, stat,_,_) ->
        (args,[stat],[])
    | UnOpExpr(_,expr,_) ->
        ([expr],[],[])
    | BinOpExpr(_,e0,e1,_) ->
        ([e0;e1],[],[])
    | ParenthesizedExpr(e,_) ->
        ([e],[],[])
    | CallExpr(func,args,_) ->
        (func :: args, [], [])

let (|ConstructorField|) (field : ConstructorField) : List<Expr>*List<Statement>*List<ConstructorField> =
    match field with
    | ListField(_,expr,_) ->
        ([expr],[],[])
    | RecField(_,e0,e1,_) ->
        ([e0;e1],[],[])
    | ErrorField(_,_) ->
        ([],[],[])

/// Gets the range of character indices that a statement occupies in
/// a file.
let getStatementRange (stmt : Statement) : Range =
    match stmt with
    | If(_,_,rng)
    | While(_,_,rng)
    | Do(_,rng)
    | ForNum(_,_,_,_,_,rng)
    | ForGeneric(_,_,_,rng)
    | Repeat(_,_,rng)
    | LocalAssign(_,_,_,rng)
    | Assign(_,_,_,rng)
    | Return(_,rng)
    | Break(rng)
    | Call(_,rng)
    | Sequence(_,_,rng)
    | ErrorStatement(_,rng) ->
        rng

/// Gets the range of character indices that an expression occupies
/// in a file.
let getExprRange (expr : Expr) : Range =
    match expr with
    | Number(_,rng)
    | String(_,rng)
    | Nil(rng)
    | True(rng)
    | False(rng)
    | VarArgs(rng)
    | Constructor(_,rng)
    | Function(_,_,_,_,_,rng)
    | UnOpExpr(_,_,rng)
    | BinOpExpr(_,_,_,rng)
    | ParenthesizedExpr(_,rng)
    | NameExpr(_,rng)
    | CallExpr(_,_,rng)
    | ErrorExpr(_,rng) ->
        rng
