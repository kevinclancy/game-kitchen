﻿module LuaAnalyzer.BasePlugin

open Type
open Context
open TypeChecker

open System.Xml.Linq
open System.ComponentModel.Composition


let (!!) (str : string) =
    XName.Get(str)

type System.String with
    member str.TrimLines() =
        let lines = str.Split('\n')
        let trimmed = Seq.map (fun (ln : string) -> ln.Trim()) lines
        (String.concat "\n" trimmed).Trim()

/// Returns the value of the child element, or def if no such child element exists
let valOrDef (element : XElement) (name : XName) (def : string) =
    if element.Element(name) = null then
        def.TrimLines()
    else
        element.Element(name).Value.TrimLines()

let refReadType = 
    ref (fun (env : TypeEnvironment) (element : XElement) (isMethod : bool) -> UnknownTy)

let readType (env : TypeEnvironment) (element : XElement) (isMethod : bool) = 
    (!refReadType) env element isMethod

let readParameter (env : TypeEnvironment) (element : XElement) =
    let name = element.Element(!!"name").Value
    let desc = element.Element(!!"description").Value.TrimLines()
    let ty = readType env (element.Element(!!"type")) false
    (name,desc,ty)

let readRet (env : TypeEnvironment) (element : XElement) =
    let name = element.Element(!!"name").Value
    let desc = element.Element(!!"description").Value.TrimLines()
    let ty = readType env (element.Element(!!"type")) false
    (desc,ty)

let readFunctionType (env : TypeEnvironment) (element : XElement) (isMethod : bool) =
    let desc = valOrDef element !!"description" ""
    let parameters = element.Elements(!!"parameter")
    let rets = element.Elements(!!"return")
    let latent = (element.Element(!!"latent") <> null)
    let varPars = 
        if (element.Element(!!"varpars") <> null) then 
            if element.Element(!!"varpars").Element(!!"description") <> null then
                Some(element.Element(!!"varpars").Element(!!"description").Value.Trim())
            else
                Some("") 
        else 
            None
    let varRets = 
        if (element.Element(!!"varrets") <> null) then 
            if element.Element(!!"varrets").Element(!!"description") <> null then
                Some(element.Element(!!"varrets").Element(!!"description").Value.Trim())
            else
                Some("") 
        else 
            None

    FunctionTy(
        desc,
        List.map (readParameter env) (Seq.toList parameters),
        varPars,
        List.map (readRet env) (Seq.toList rets),
        varRets,
        isMethod,
        latent,
        NoLocation
    )

let readField (env : TypeEnvironment) (element : XElement) =
    let name = element.Element(!!"name").Value
    let description = element.Element(!!"description").Value.TrimLines()
    let ty = readType env (element.Element(!!"type")) false
    let visible = (element.Element(!!"invisible") = null)
    let newField = {
        Field.OfType(ty) with
            desc = description
            isVisible = visible
    }
    (name, newField)

let readMethod (env : TypeEnvironment) (element : XElement) =
    let name = element.Element(!!"name").Value
    let description = element.Element(!!"description").Value.TrimLines()
    let ty = readType env (element.Element(!!"type")) true

    match ty with
    | FunctionTy(tyDesc,formals,varPars,rets,varRets,true,latent,loc) ->
        let newTy = 
            FunctionTy(
                (if tyDesc <> "" then tyDesc else description), 
                formals,
                varPars,
                rets,
                varRets,
                true,
                latent,
                loc
            )

        (name, {desc=description;ty=newTy;isAbstract=false;loc=NoLocation})
    | OverloadTy(overloads) ->
        let insertSelf (ty : Type) =
            match ty with
            | FunctionTy(tyDesc,formals,varPars,rets,varRets,true,latent,loc) ->
                FunctionTy(
                    tyDesc, 
                    formals,
                    varPars,
                    rets,
                    varRets,
                    true,
                    latent,
                    loc
                )                
            | _ ->
                failwith "overloads must have function type"

        let newTy = OverloadTy(List.map insertSelf overloads)
        (name, {desc=description;ty=newTy;isAbstract=false;loc=NoLocation}) 
    | UnknownTy ->
        (name, {desc=description;ty=UnknownTy;isAbstract=false;loc=NoLocation}) 
    | _ ->
        failwith "error reading XML: methods must have function type."
        
let readSuperType (env : TypeEnvironment) (element : XElement) =
    match Type.Unfold env (UserDefinedTy(element.Value,(0,0))) with
    | RecordTy(_,_,_,_,Closed,_,fields,_,methods,_,_) ->
        fields,methods
    | UserDefinedTy(name,_) ->
        failwith ("type " + name + " not found")
    | _ ->
        failwith "only record types can be used as supertypes"

let readRecordSupertypeNames (element : XElement) =
    Seq.map (fun (x : XElement) -> x.Value) (element.Elements(!!"supertype")) 

let readRecordTy (env : TypeEnvironment) (element : XElement) =
    let hasName = element.Element(!!"name") <> null
    let name = if hasName then element.Element(!!"name").Value else "*UnnamedRecord*"
    let description = element.Element(!!"description").Value.TrimLines()
    let structural = element.Element(!!"structural") <> null


    let supertypes = Seq.map (readSuperType env) (element.Elements(!!"supertype"))
    let supertypeFieldsSeq = Seq.map fst supertypes
    let supertypeMethodsSeq = Seq.map snd supertypes

    //TODO: detect collisions
    let supertypeFields = Seq.fold cover Map.empty supertypeFieldsSeq
    let supertypeMethods = Seq.fold cover Map.empty supertypeMethodsSeq

    let fields = 
        if element.Element(!!"fields") = null then
            seq []
        else
            element.Element(!!"fields").Elements()

    let fields = new Map<string,Field>(List.map (readField env) (Seq.toList fields))
        
    let methods = 
        if element.Element(!!"methods") = null then
            seq []
        else
            element.Element(!!"methods").Elements()
        
    let methods = new Map<string,Method>(List.map (readMethod env) (Seq.toList methods))

    RecordTy(
        name, 
        description, 
        (not hasName) || structural,
        not hasName,
        Closed,
        MetamethodSet.empty, 
        cover supertypeFields fields,
        Map.toList (cover supertypeFields fields),
        cover supertypeMethods methods,
        [],
        NoLocation
    )

let readMapTy (env : TypeEnvironment) (element : XElement) =
    let keyTy = readType env (element.Element(!!"keytype").Element(!!"type")) false
    let valTy = readType env (element.Element(!!"valtype").Element(!!"type")) false

    let metamethodSet = {
        MetamethodSet.empty with
            Index = Some("",keyTy,Type.NillableTy(valTy),NoLocation)
    }

    RecordTy(
        "",
        "",
        true,
        true,
        Closed,
        metamethodSet,
        Map.empty,
        [],
        Map.empty,
        [],
        NoLocation
    )

let readArrayTy (env : TypeEnvironment) (element : XElement) =
    let elemTy = readType env (element.Element(!!"elemtype").Element(!!"type")) false

    let metamethodSet = {
        MetamethodSet.empty with
            Index = Some("",NumberTy,elemTy,NoLocation)
            Len = Some("",NumberTy,NoLocation)
    }

    RecordTy(
        "", 
        "", 
        true,
        true,
        Closed,
        metamethodSet, 
        Map.empty,
        [],
        Map.empty,
        [],
        NoLocation
    )


refReadType := fun (env : TypeEnvironment) (element : XElement) (isMethod : bool) ->
    let variant = element.Attribute(!!"variant").Value
    let nillable, variant = 
        if variant.[0] = '?' then
            true, variant.Substring(1)
        else
            false, variant

    let baseTy = 
        match variant with
        | "array" ->
            readArrayTy env element
        | "map" ->
            readMapTy env element
        | "function" ->
            readFunctionType env element isMethod
        | "usertype" ->
            let name = element.Element(!!"name").Value
            if name.[0] = '?' then
                NillableTy(UserDefinedTy(name.Substring(1),(0,0)))
            else
                UserDefinedTy(name,(0,0))
        | "overload" ->
            let overloads = ref []
            let overloadElements = element.Elements(!!"type")
            for overloadElement in overloadElements do
                overloads := (readType env overloadElement isMethod) :: !overloads
            OverloadTy(List.rev !overloads)
        | "unknown" ->
            UnknownTy
        | "record" ->
            readRecordTy env element
        | "number" ->
            NumberTy
        | "string" ->
            UserDefinedTy("string",(0,0))
        | "boolean" ->
            BoolTy 
        | _ ->
            UnknownTy

    if nillable then
        NillableTy(baseTy)
    else
        baseTy
    
let includeAPI (ctxt : Context) (fileName : string) =
    let reader = new System.Xml.XmlTextReader(fileName)    
    let root = XElement.Load(reader)
    
    let foldDefinition (env : TypeEnvironment) (typeDef : XElement) =
        let name = typeDef.Element(!!"name").Value
        {
        env with
            instanceTypes = env.instanceTypes.Add(name)
            typeMap = env.typeMap.Add(name, readType env typeDef false)
            edges = env.edges.Add(
                name,
                Seq.toList (readRecordSupertypeNames typeDef)
            )
        }

    let tenv = Seq.fold foldDefinition ctxt.tenv (root.Element(!!"TypeDefinitions").Elements())

    let foldGlobal (venv : ValueEnvironment) (def : XElement) =
        if def.Name = !!"global" then
            let globalId = def.Element(!!"name").Value
            let globalDesc = 
                if not (def.Element(!!"description") = null) then
                    def.Element(!!"description").Value.Trim().Replace("	","")
                else
                    ""
            let ty = readType tenv (def.Element(!!"type")) false
            let newField = {
                Field.OfType(Type.Unfold tenv ty) with
                    desc = globalDesc
            }

            venv.Add(globalId, newField)
        else
            venv

    let venv = Seq.fold foldGlobal ctxt.venv (root.Element(!!"globals").Elements())

    { 
        ctxt with
            venv = venv
            tenv = tenv
    }

let activateModule () =
    let ctxt = getBaseContext()
    let ctxt = includeAPI ctxt "LuaAPI.xml"
    
    let strTy = ctxt.tenv.typeMap.Item "string"
    let strTy =
        match strTy with
        | RecordTy(name,desc,_,_,Closed,metaset,fields,fieldGenList,methods,[],defLocation) ->
            let stringTy = UserDefinedTy("string",(0,0))
            let metaset =
                {
                MetamethodSet.empty with
                    Concat = Some ("concatenate two strings", stringTy, stringTy,NoLocation)
                    Len = Some ("get the length of the string", NumberTy,NoLocation)
                    Lt = Some ("is the left string lexicographically less than the right string?",stringTy,BoolTy,NoLocation)
                    Le = Some ("is the left number lexicographically less than or equal to the right number?",stringTy,BoolTy,NoLocation)
                    Gt = Some ("is the left number lexicographically greater than the right number?",stringTy,BoolTy,NoLocation)
                    Ge = Some ("is the left number lexicographically less than or equal to the right number?",stringTy,BoolTy,NoLocation)
                }
            RecordTy(name,desc,false,false,Closed,metaset,fields,fieldGenList,methods,[],defLocation)
        | _ ->
            failwith "unreachable"

    let tenv = ctxt.tenv
    
    let tenv = 
        {
        tenv with
            typeMap = tenv.typeMap.Add("string",strTy)
        }
    
    let ctxt = 
        {
        ctxt with
            tenv = tenv
        }
    
    let ctxt = includeAPI ctxt "LoveAPI.xml"

    setBaseContext ctxt