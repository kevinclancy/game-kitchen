﻿///
/// Functions for extracting semantic annotations and documentation metadata
/// from specially formatted comments.
///
module LuaAnalyzer.Annotations

open System.ComponentModel
open Type
open Utils


type AnnotationField =
    /// VarPars(desc) 
    /// This function has additional formal parameters, 
    /// the purpose of which is described by desc
    | VarPars of string
    /// VarRets(desc)
    /// This function has additional formal returns, the purpose of which
    /// is described by desc
    | VarRets of string
    /// Annotation for functions which are latent.
    /// A latent function is one which  may yield execution to another coroutine
    /// before returning.
    | Latent
    /// Param(name,type,description,nameRng)
    /// Name - the name of the formal parameter
    /// Type - the name of the type of the formal parameter
    /// Description - a description of the formal parameter
    /// nameRng - the range in source of the formal parameter
    | Param of string*Type*string*Range
    /// Ret(type,description)
    /// type - the name of the type of the return value
    /// description - a decription of the return value
    | Ret of Type*string
    /// Var(type,description)
    /// type - the type of the variable (if any is given)
    /// description - a decription of the variable
    | Var of Option<Type>*string
    /// Const(type,description)
    /// type - the name of the type of the constant (if any is given)
    /// description - a decription of the constant
    | Const of Option<Type>*string
    /// A tag which contains a description of a module
    | Module of string
    /// An association of a type name with a type structure
    | Typedef of List<string*Type>
    /// A list of name/description pairs for the type parameters of this module
    | TypeParams of List<string*string*DefinitionLocation>
    /// TODO: For now, we're not actually reporting these. This is because
    /// we are not tracking comment positions, and doing so would require
    /// us to change the TokenInfo type.
    | Error of int*string


/// FieldAnnotation(type,description,isVariable)
/// type - type ascribed to the field (or None if no type is ascribed)
/// description - description of the variable
/// isVariable - true if variable, false if const
type FieldAnnotation = Option<Type>*string*bool

type TagSet = 
    /// FunctionTagSet(params,varParsDesc,rets,varRetsDesc,isLatent) 
    /// params - the formal parameters of the function, paired with their ranges the annotation
    /// varParsDesc - Some(description) if this function has varpars, None otherwise
    /// rets - the formal returns of the function
    /// varRetsDesc - Some(description) if this function has varrets, None otherwise
    /// isLatent - whether or not this function is latent.
    | FunctionTagSet of List<Param*Range>*Option<string>*List<Ret>*Option<string>*bool
    | VarConstTagSet of List<FieldAnnotation>
    /// ModuleHeaderTagSet(typedefs,typeparams)
    /// typedefs is a list of type definitions, associating names with types.
    /// typeparams is a list of type parameters, associating type variable names with descriptions
    /// and definition locations.
    | ModuleHeaderTagSet of List<string*Type>*List<string*string*DefinitionLocation>
    | EmptyTagSet

type AnnotationCommentType =
    | ShortCommentAnnotation
    | LongCommentAnnotation

/// Annotation(description,structured annotation form, annotation parsing errors,range)
type Annotation = string*TagSet*List<string*Range>*Range

/// HeaderAnnotation(description)
/// description - a description of the current module
type HeaderAnnotation = string
    
//let EmptyAnnotation = ("",EmptyTagSet)
let EmptyFunctionTagSet = ([],None,[],None,false)

type TableEntryType =
    | Field
    | ConstField
    | Method

/// AnnotationParsingException(position,errorMessage)
exception AnnotationParsingException of Range*string

type Type with            

    /// annotation - the annotation to extract types from
    /// n (positive integer) - the number of types to extract
    static member TypesFromAnnotation (annotation : Option<Annotation>) (n : int) : List< Option<Type> > =
        // Contract.Requires(n > 0)
        // Contract.Ensures(Contract.Result< List<Option<Type>> >().Length = n)

        match annotation with
        | Some(desc,FunctionTagSet(paramList,varPars,retList,varRets,latent),_,_) ->
            Some(FunctionTy(desc,List.map fst paramList,varPars,retList,varRets,false,latent,NoLocation)) :: List.init (n-1) (fun _ -> None)
        | Some(desc, VarConstTagSet(fieldList),_,_) ->
            let optTys = List.map (fun (opTy,_,_) -> opTy) fieldList
            sizeListTo optTys n None
        | _ ->
            List.init n (fun _ -> None)
    
    static member TypeFromAnnotation (annotation : Option<Annotation>) =
        (Type.TypesFromAnnotation annotation 1).[0]

    static member InterpretFunctionAnnotation (annotation : Option<Annotation>) : Option< string*List<Param>*Option<string>*List<Ret>*Option<string>*bool > =
        let (desc,paramList,varPars,retList,varRets,latent) = 
            match annotation with
            | Some(desc,FunctionTagSet(paramList,varPars,retList,varRets,latent),_,_) ->
                desc,paramList,varPars,retList,varRets,latent
            | Some(desc,_,_,_) ->
                desc,[],None,[],None,false
            | None ->
                "",[],None,[],None,false

        Some (desc,List.map fst paramList,varPars,retList,varRets,latent)


    static member InterpretVarAnnotation (annotation : Option<Annotation>) (length : int) : List<string>*List<Option<Type>>*List<bool> =
        let (desc,fieldList) =
            match annotation with
            | Some(desc, VarConstTagSet(fieldList),_,_) ->
                desc,fieldList
            | Some(desc,_,_,_) ->
                desc,[]
            | None ->
                "",[] 

        let opTypes, descriptions, isConstList =
            if fieldList.Length = 0 then
                [None], [desc], [false]
            else
                List.unzip3 fieldList

        let descriptions = sizeListTo (List.rev descriptions) length ""
        let isConstList = sizeListTo (List.rev isConstList) length false
        let opTypes = sizeListTo (List.rev opTypes) length None
        descriptions,opTypes,isConstList

/// Index of the next character to lex
let private i = ref 0

/// The annotation string that we are lexing
let private s = ref ""

/// The start character index in the source file of the annotation
/// we are parsing.
let private p = ref 0
       
/// the name of the file containing the annotation being parsed
let private currentFileName = ref ""

/// The number of the current line relative to the top of the annotation
let metacomment = ref None

let ch x =
    (!s).Chars x

/// returns true iff x is a whitespace character
// [<Pure>]
let isSpace(x : char) : bool =
    (x = '\t') || (x = '\r') || (x = '\n') || (x = ' ')

let lastCharRng () =
    let ret = ref None
    let indices = List.rev (List.init (!s).Length (fun i -> i))

    for i in indices do
        if not (isSpace (ch i)) then
            if (!ret).IsNone then
                ret := Some(!p + i,!p + i + 1)

    match !ret with
    | Some rng ->
        rng
    | None ->
        (!p + 0,!p + 1)

let eat (c : char) =
    if !i >= (!s).Length then
        let msg = "expected '" + c.ToString() + "'. Instead reach end of annotation."
        let rng = lastCharRng()
        raise( AnnotationParsingException(rng, msg) )
    elif ( (!s).Chars !i ) = c then
        i := !i + 1
    else 
        let actual = (!s).Chars !i
        let msg = "Expected '" + c.ToString() + "'. Got '" + actual.ToString() + "'."
        let rng = (!p + !i,!p + !i + 1)
        raise( AnnotationParsingException(rng,msg) )

let expect (c : char) =
    if !i >= (!s).Length then
        let msg = "expected '" + c.ToString() + "'. Instead reach end of annotation."
        let rng = lastCharRng()
        raise( AnnotationParsingException(rng, msg) )
    elif ( (!s).Chars !i ) = c then
        ()
    else 
        let actual = (!s).Chars !i
        let msg = "Expected '" + c.ToString() + "'. Got '" + actual.ToString() + "'."
        let rng = (!p + !i,!p + !i + 1)
        raise( AnnotationParsingException(rng,msg) )

let lexMetaComment() =
    let str = !s
    let commentStart = !i
    while not (!i >= str.Length || ((str.Chars !i) = '\n')) do
        i := !i + 1
    i := !i + 1
    match !metacomment with
    | Some(contents) ->
        metacomment := Some( contents + "\n" + str.Substring(commentStart+2, !i-1-(commentStart+2)) )
    | None ->
        metacomment := Some ( str.Substring(commentStart+2, !i-1-(commentStart+2)) )

/// Advances the character iterator until it reaches a non-whitespace
/// character.
let eatWhitespace (lexMetaComments : bool) =
    // Contract.Ensures (!i = (!s).Length || not (isSpace ((!s).Chars !i)))

    let lastNewLine = ref !i

    if lexMetaComments then
        while !i < ((!s).Length-1) && (!s).Chars !i = '-' && (!s).Chars (!i + 1) = '-' do
            lexMetaComment()

    while (!i < (!s).Length) && (isSpace ((!s).Chars !i)) do
        if (!s).Chars !i = '\n' then
            let lineContents = (!s).Substring(!lastNewLine+1,!i - !lastNewLine)
            if lexMetaComments && lineContents.Trim() = "" then
                metacomment := None
                lastNewLine := !i
            
        i := !i + 1

        if lexMetaComments then
            while !i < ((!s).Length-1) && (!s).Chars !i = '-' && (!s).Chars (!i + 1) = '-' do
                lexMetaComment()

/// Returns ture iff x is an alphabet character OR an underscore
let isAlpha(x : char) =
    (x >= 'a' && x <= 'z') ||
    (x >= 'A' && x <= 'Z') ||
    x = '_'

let isDigit(x : char) =
    (x >= '0' && x <= '9')

/// raise a parsing exception of the current character is not _ or in the ranges a-z or A-Z
let expectAlpha () =
    if !i >= (!s).Length then
        let msg = "Expected alphabetic character. Instead reach end of annotation."
        let rng = lastCharRng()
        raise( AnnotationParsingException(rng, msg) )
    elif not (isAlpha (ch !i)) then
        let msg = "Expected alphabetic character, got '" + (ch !i).ToString() + "'." 
        let rng = ( !p + !i, !p + !i+1 )
        raise(AnnotationParsingException(rng,msg))    

let parseName () =
    eatWhitespace false
    expectAlpha()
    let nameStart = !i
    while isAlpha (ch !i) || isDigit (ch !i) do  
      i := !i + 1

    (!s).Substring(nameStart,!i-nameStart), (!currentFileName, (!p+nameStart, !p + !i))

let refParseType = ref (fun (name : Option<string*DefinitionLocation>) (desc : Option<string>) -> UnknownTy)

let parseType (name : Option<string*DefinitionLocation>) (desc : Option<string>) =
    let ty = (!refParseType) name desc
    let args = ref []
    if (ch !i) = '<' then
        eat '<'
        eatWhitespace false
        args := (!refParseType) None None :: !args
        while (ch !i) = ',' do
            eat ','
            args := (!refParseType) None None :: !args
            eatWhitespace false
        eatWhitespace false
        eat '>'

    if (!args).Length > 0 then
        TypeAppTy(ty,List.rev !args)
    else
        ty
        
let parseTypeList () : List<Type>*Option<string> =
    eat('[')
    eatWhitespace false

    let ret = ref []
    let indefinite = ref None
    while (!i) < (!s).Length && not ((ch !i) = ']') do
        if !i + 3 <= (!s).Length && (!s).Substring(!i,3) = "..." then
            indefinite := Some("")
            i := !i + 3
            eatWhitespace false
            expect ']'
        else
            ret := (parseType None None) :: !ret
            
            eatWhitespace false
            
            if (!i) < (!s).Length && not ((ch !i) = ']') then
                eat(',')
        
            eatWhitespace false
                                
            if !i >= (!s).Length then
                expect ']'  
    
    eat(']')

    List.rev !ret, !indefinite

let parseFunctionType (isMethod : bool) (name : Option<string*DefinitionLocation>) (desc : Option<string>) =
    let pars,varPars = parseTypeList()
    eatWhitespace false

    eat '-'
    let latent =
        if (ch !i) = 'L' then
            eat 'L'
            true
        else
            false
    eat '>'
        
    eatWhitespace false
    let rets,varRets = parseTypeList()
    let pars = List.mapi (fun i ty -> ("p" + i.ToString(),"",ty)) pars
    let rets = List.mapi (fun i ty -> ("",ty)) rets
    FunctionTy(
        (if desc.IsSome then desc.Value else ""),
        pars,
        varPars,
        rets,
        varRets,
        isMethod,
        latent,
        (if name.IsSome then snd name.Value else NoLocation)
    )

let parseRecordType (name : Option<string*DefinitionLocation>) (description : Option<string>) =
    i := !i + 1
    eatWhitespace true

    let fields = ref []
    let methods = ref Map.empty
    let finished = ref false

    while not !finished do
        eatWhitespace false
        let name,nameRng = parseName()
        eatWhitespace false           

        let entryType =
            expect ':'
            if (ch !i) = ':' && (ch (!i + 1)) = '~' then
                i := !i + 2
                ConstField
            elif (ch !i) = ':' && (ch (!i + 1)) = ':' then
                i := !i + 2
                Method
            else
                i := !i + 1
                Field
        
        eatWhitespace false

        if entryType = Method then
            let ty = parseFunctionType true None None
            let desc = if (!metacomment).IsSome then (!metacomment).Value else ""
            methods := (!methods).Add(name, { desc=desc ; isAbstract=false ; ty = ty; loc = nameRng })
        else
            let ty = parseType None None
            let desc = if (!metacomment).IsSome then (!metacomment).Value else ""
            fields := (name, { Field.OfType(ty) with isConst = (entryType = ConstField); desc = desc; loc = nameRng }) :: !fields
                
        eatWhitespace false

        if (ch !i) = ',' then
            eat(',')
            eatWhitespace true
        else
            finished := true
            eat('}')    

    RecordTy(
        (if name.IsSome then fst name.Value else "*UnnamedRecord*"),
        (if description.IsSome then description.Value else "user-defined type"),
        true,
        true,
        Closed,
        MetamethodSet.empty,
        Map.ofList(!fields),
        List.rev !fields,
        !methods,
        [],
        (if name.IsSome then snd name.Value else NoLocation)
    )

let parseArrayType (name : Option<string*DefinitionLocation>) (desc : Option<string>) =
    i := !i + 1
    eatWhitespace false

    let elemTy = parseType None None

    eatWhitespace false

    eat '}'

    RecordTy(
        (if name.IsSome then fst name.Value else "*UnnamedRecordType*"),
        (if desc.IsSome then desc.Value else "user-defined type"),
        true,
        true,
        Closed, 
        MetamethodSet.ForArrayTy(elemTy), 
        Map.empty, 
        [],
        Map.empty, 
        [],
        (if name.IsSome then snd name.Value else NoLocation)
    )

let parseMapType (name : Option<string*DefinitionLocation>) (desc : Option<string>) =
    i := !i + 1
    eatWhitespace false

    let domType = parseType None None

    eatWhitespace false
    eat '='
    eat '>'
    eatWhitespace false

    let codType = parseType None None

    eatWhitespace false

    eat '}'

    RecordTy(
        (if name.IsSome then fst name.Value else "*UnnamedMapType*"),
        (if desc.IsSome then desc.Value else "user-defined type"),
        true,
        true,
        Closed,
        MetamethodSet.ForMap(domType,codType),
        Map.empty,
        [],
        Map.empty, 
        [],
        (if name.IsSome then snd name.Value else NoLocation)
    )

/// Reads a typename beginning at the current position of the input
/// iterator. Advances the input iterator past the typename. Returns
/// the read typename.
refParseType := fun (name : Option<string*DefinitionLocation>) (desc : Option<string>) ->
    
    if (!i >= (!s).Length) then
        let msg = "expected type expression. instead reached end of annotation"
        let rng = lastCharRng()
        raise( AnnotationParsingException(rng,msg) )
    if (ch !i) = '?' then
        i := !i + 1
        NillableTy(parseType name desc)
    elif (ch !i) = '[' then
        parseFunctionType false name desc
    elif (ch !i) = '{' then
        //both record and array annotations start with {
        //we must now figure out which one it is
        let ind = ref (!i+1)
        let count = ref 1

        //true if record, false if array
        let isRecord = ref false
        let isMap = ref false

        while !count > 0 && !ind < (!s).Length do 
            if (ch !ind) = ':' && !count = 1 then
                isRecord := true
            elif (ch !ind) = '=' && !count = 1 then
                isMap := true
            elif (ch !ind) = '{' then
                count := !count + 1
            elif (ch !ind) = '}' then
                count := !count - 1

            ind := !ind + 1

        if (!ind = (!s).Length) then
            let msg = "No matching } found for {"
            let rng = !p + !i,!p + !i + 1
            raise(AnnotationParsingException(rng,msg))
        elif !isMap then
            parseMapType name desc
        elif !isRecord then
            parseRecordType name desc
        else
            parseArrayType name desc
    else
        let startPos = !i

        if not (isAlpha (ch !i)) then
            let msg = "Expecting type expression. Got '" + (ch !i).ToString() + "'."
            let rng = !p + !i,!p + !i + 1
            raise(AnnotationParsingException(rng,msg))

        while isAlpha (ch !i) || isDigit (ch !i) || (ch !i = '.') do  
          i := !i + 1

        let name = (!s).Substring(startPos,!i-startPos)

        match name with
        | "nil" ->
            NilTy
        | "number" ->
            NumberTy
        | "boolean" ->
            BoolTy
        | "unknown" ->
            UnknownTy
        | _ ->
            UserDefinedTy(name,(!p+startPos,!p + !i))

let parseOptType () : Option<Type> =
    if (ch !i) = ')' then
        None
    else
        Some(parseType None None)

/// Parses a string of the form "(typename) description"
/// Returns a pairs of strings (typeName,description) each elements containing
/// the corresponding section of the input text.
///
/// Throws an AnnotationParsing exception if the input string does not
/// match the format "(typename) description"
let parseFieldAnnotation () =
    eatWhitespace false
    eat '('
    eatWhitespace false
    let typeName = parseOptType()
    eatWhitespace false
    eat ')'
    let description = (!s).Substring(!i).Trim()
    typeName, description

/// Reads a variable annotation "var(typename) description" from the input string, 
/// returning a representation of the annotation which was read.
///
/// If the input string does not contain a properly formatted variable annotation, 
/// throws an exception.
let parseVarAnnotation () =
    i := "var".Length
    let typename, description = parseFieldAnnotation()
    Var(typename,description)

/// Reads a const annotation "const(typename) description" from the input string,
/// returning a representation of the annotation which was read.
///
/// If the input string does not contain a properly formatted variable annotation, 
/// throws an exception.
let parseConstAnnotation () =
    i := "const".Length
    let typename, description = parseFieldAnnotation()
    Const(typename,description)

// ret(typeName) description
let parseRetAnnotation () =
    i := "ret".Length
    eatWhitespace false
    eat '('
    eatWhitespace false
    let typeName = parseType None None
    eatWhitespace false
    eat ')'
    let description = (!s).Substring(!i).Trim()
    Ret(typeName,description)

// param(argName : typeName) description
let parseParamAnnotation () =
    i := "param".Length
    eatWhitespace false
    eat '('
    eatWhitespace false
    let nameStart = !i
    expectAlpha()
    let nameStart = !i
    while isAlpha (ch !i) || isDigit (ch !i) do  
      i := !i + 1
    let nameEnd = !i
    let name = (!s).Substring(nameStart,nameEnd-nameStart)
    eatWhitespace false
    eat ':'
    eatWhitespace false
    let ty = parseType None None
    eatWhitespace false
    eat ')'
    let description = (!s).Substring(!i).Trim()
    Param(name,ty,description,(!p + nameStart,!p + nameEnd))

let parseModuleAnnotation () = 
    i := "module".Length
    Module( (!s).Substring(!i).Trim() )

let parseTypedefAnnotation () =
    i := "types".Length

    let typedefs = ref []
    
    eatWhitespace true

    while (!i) < (!s).Length do
        expectAlpha()
        let name,loc = parseName()
        eatWhitespace false
        eat '='
        eatWhitespace false
        let ty = parseType (Some (name,loc)) (!metacomment)
        typedefs := (name,ty) :: !typedefs
        eatWhitespace true
        

    Typedef(!typedefs)

let parseTypeParamsAnnotation () =
    i := "typeparams".Length

    let typeparams = ref []

    eatWhitespace true
    while (!i) < (!s).Length do
        expectAlpha()
        let name,loc = parseName()
        let comment = if (!metacomment).IsSome then (!metacomment).Value else ""
        typeparams := (name,comment,loc) :: (!typeparams)
        eatWhitespace true

    TypeParams(List.rev !typeparams)

let parseAnnotationField (annotation : string) (position : int) =
    s := annotation
    i := 0
    p := position

    if (!s).StartsWith "param" then
        parseParamAnnotation()
    elif (!s).StartsWith "module" then
        parseModuleAnnotation()
    elif (!s).StartsWith "types" then
        parseTypedefAnnotation()
    elif (!s).StartsWith "typeparams" then
        parseTypeParamsAnnotation()
    elif (!s).StartsWith "ret" then
        parseRetAnnotation()
    elif (!s).StartsWith "varargs" then
        VarPars((!s).Substring("varargs".Length).Trim())
    elif (!s).StartsWith "varrets" then
        VarRets((!s).Substring("varrets".Length).Trim())
    elif (!s).StartsWith "var" then
        parseVarAnnotation()
    elif (!s).StartsWith "const" then
        parseConstAnnotation()
    elif (!s).StartsWith "latent" then
        Latent
    else
        Error(777, "annotation type not recognized")

/// splits toSplit at every position where the '@' character occurs
/// returns a list of string*int pairs, where each strings is a chunks of text
/// between contiguous '@' characters, ande each integers is the character index
/// in the text file at which the correspo
let private split (toSplit : string) (commentType : AnnotationCommentType) : List<string*int> =
    let rest = ref toSplit
    let pos = ref (if commentType = ShortCommentAnnotation then 2 else 0)
    let ret = ref []
    let foundSep = ref true

    while !foundSep do
        if (!rest).Contains("@") then 
           let nextSeparatorInd = (!rest).IndexOf("@")
           let nextChunk =  (!rest).Substring(0,nextSeparatorInd)
           ret := (nextChunk, !pos) :: !ret
           rest := (!rest).Substring(nextSeparatorInd+1)

           //if this is a short-comment style annotation, at each newline, we have 
           //removed two hyphens from toSplit, so we need to take that into account 
           //in our position counter
           let commentHyphens = ref 0
           if commentType = ShortCommentAnnotation then 
             String.iter (fun ch -> if ch = '\n' then commentHyphens := !commentHyphens + 2) nextChunk
           
           pos := !pos + (nextSeparatorInd + 1) + !commentHyphens
        else 
            ret := (!rest,!pos) :: !ret
            rest := ""
            foundSep := false 

    List.rev (!ret)


let parseAnnotation (contents : string) (startPosition : int) (commentType : AnnotationCommentType) (fileName : string) : Annotation =
    let chunks,positions = List.unzip (split contents commentType)
    
    currentFileName := fileName

    let description = chunks.[0].Trim()
    let annotations = ref []
    let paramList = ref []
    let retList = ref []
    let fieldList = ref []
    let errorList = ref []
    let isLatent = ref false
    let varPars = ref None
    let varRets = ref None
    let moduleDesc = ref None
    let typedefList = ref []
    let typeParams = ref []
    for i in 1..chunks.Length-1 do
        try
            let annotationField = parseAnnotationField 
                                    chunks.[i] 
                                    (startPosition + positions.[i])

            match annotationField with
            | Latent ->
                isLatent := true
            | VarPars(desc) ->
                varPars := Some(desc)
            | VarRets(desc) ->
                varRets := Some(desc)
            | Param(name,ty,desc,nameRng) ->
                paramList := ( (name,desc,ty),nameRng ) :: !paramList
            | Ret(ty,desc) ->
                retList := (desc,ty) :: (!retList)
            | Var(opTy,desc) ->
                fieldList := (opTy,desc,false) :: (!fieldList)
            | Const(opTy,desc) ->
                fieldList := (opTy,desc,true) :: (!fieldList)
            | Typedef(list) ->
                typedefList := list
            | TypeParams(pars) ->
                typeParams := pars
            | Module(desc) ->
                moduleDesc := Some(desc)
            | _ ->
                ()
        with
        | AnnotationParsingException(rng,msg) ->
            errorList := (msg,rng) :: !errorList

    let hasFuncAnnotations = (!paramList).Length > 0 || (!varPars).IsSome || (!retList).Length > 0 || (!varRets).IsSome || !isLatent
    let hasNonFuncAnnotations = (!fieldList).Length > 0
    let range = (startPosition, startPosition + contents.Length)
    if hasFuncAnnotations && (not hasNonFuncAnnotations) then
        description,
        FunctionTagSet(List.rev !paramList,!varPars,List.rev !retList,!varRets,!isLatent),
        !errorList,
        range
    elif hasNonFuncAnnotations && (not hasFuncAnnotations) then
        description,VarConstTagSet(!fieldList),!errorList,range
    elif (!moduleDesc).IsSome || (not (!typedefList).IsEmpty) then
        (if (!moduleDesc).IsSome then (!moduleDesc).Value else description),
        ModuleHeaderTagSet(!typedefList, !typeParams),
        !errorList,
        range
    else
        description,EmptyTagSet,!errorList,range
        