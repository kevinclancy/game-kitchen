﻿module LuaAnalyzer.ErrorList

type GenList<'a> = System.Collections.Generic.List<'a>

open Syntax
open Annotations

/// a list of detected type collection errors
/// msg,filename,range
let errors = new GenList<string*string*(int*int)>([])

/// a list of detected parse errors
/// msg,filename,range
let parseErrors = new GenList<string*string*(int*int)>([])

/// addError fileName msg rng
let addError (fileName : string) (msg : string) (rng : int*int) =
    errors.Add(fileName,msg,rng)

/// addErrors( (fileName,msg,rng) :: ...)
let addErrors (errors : List<Error>) =
    let addError' ((fileName,msg,rng) : Error) =
        addError fileName msg rng

    List.iter addError' errors

/// addError fileName msg rng
let addParseError (fileName : string) (msg : string) (rng : int*int) =
    parseErrors.Add(fileName,msg,rng)

let private ref_listStatErrors : Ref<bool -> Statement -> List<LocalError>> = 
    ref (fun x y -> [])

let listStatErrors (checkAnnotations : bool)  (stat : Statement) =
    !ref_listStatErrors checkAnnotations stat 

let private ref_listExprErrors : Ref<bool -> Expr -> List<LocalError>> =
    ref (fun x y -> [])
    
let private listExprErrors (checkAnnotations : bool) (expr : Expr) =
    !ref_listExprErrors checkAnnotations expr

let rec private listFieldErrors (checkAnnotations : bool) (field : ConstructorField) : List<LocalError> =
    match field with
    | ListField(ann,e,_) ->
        let annotationErrors =
            match ann, checkAnnotations with
            | Some(_,_,errors,_), true ->
                errors
            | _ ->
                []

        List.concat [annotationErrors; listExprErrors checkAnnotations e]
    | RecField(ann,e0,e1,_) ->
        let annotationErrors =
            match ann, checkAnnotations with
            | Some(_,_,errors,_), true ->
                errors
            | _ ->
                []

        let exprErrors = List.append (listExprErrors checkAnnotations e0) 
                                     (listExprErrors checkAnnotations e1)

        List.concat [annotationErrors; exprErrors]
    | ErrorField(msg,rng) ->
        [(msg,rng)]

ref_listExprErrors := fun checkAnnotations exp ->
    match exp with
    | ErrorExpr(msg,rng) ->
        [(msg,rng)]
    | Expr(exprs, stats, fields) ->
        let exprErrors = List.concat (List.map (listExprErrors checkAnnotations) exprs)
        let statErrors = List.concat (List.map (listStatErrors checkAnnotations) stats)
        let fieldErrors = List.concat (List.map (listFieldErrors checkAnnotations) fields)
        List.concat [exprErrors;statErrors;fieldErrors]

ref_listStatErrors := fun checkAnnotations ast ->
    match ast with
    | ErrorStatement(str,rng) ->
        [(str,rng)]
    | Statement(optAnnotation,exprs,stats) ->
        let annotationErrors =
            match optAnnotation, checkAnnotations with
            | Some(_,_,errors,_), true ->
                errors
            | _ ->
                []
                 
        let exprErrors = List.concat (List.map (listExprErrors checkAnnotations) exprs)
        let statErrors = List.concat (List.map (listStatErrors checkAnnotations) stats)
        List.concat [annotationErrors;exprErrors;statErrors]

let listErrors (ast : Statement) (shouldCheckAnnotations : bool) =
    
    let errors = listStatErrors shouldCheckAnnotations ast 
    let ret = new System.Collections.Generic.List<LocalError>()
    for i = 0 to errors.Length-1 do
        ignore( ret.Add(errors.[i]) )
    ret

let private ref_getExprError : Ref<int -> Expr -> Option<string>> = 
    ref (fun x y -> None)

let getExprError (pos : int) (expr : Expr) : Option<string> =
    !ref_getExprError pos expr

let private ref_getStatError : Ref<int -> Statement -> Option<string>> = 
    ref (fun x y -> None)

let getStatError (pos : int) (stat : Statement) : Option<string> =
    !ref_getStatError pos stat 

let private ref_getError 
    : Ref<int -> Option<Annotation> -> List<Expr> -> List<Statement> -> List<ConstructorField> -> Option<string>> =
    
    ref (fun v w x y z -> None)

let private getError (pos : int)
                     (optAnnotation : Option<Annotation>)
                     (exprs : List<Expr>)
                     (stats : List<Statement>)
                     (fields : List<ConstructorField>) =

    !ref_getError pos optAnnotation exprs stats fields

let private getAnnotationError (pos : int) (ann : Annotation) =
    let _,_,errors,_ = ann
    let msgRng = List.tryFind (snd >> (inRange pos)) errors
    Option.map fst msgRng

let rec private getFieldError (pos : int) (field : ConstructorField) =
    match field with
    | ErrorField(msg,rng) when inRange pos rng ->
        Some msg
    | ConstructorField(exprs,stats,fields) ->
        getError pos None exprs stats fields      
    
ref_getStatError := fun (pos : int) (stat : Statement) ->
    match stat with
    | ErrorStatement(msg,rng) when inRange pos rng ->
        Some msg
    | Statement(optAnn,exprs,stats) ->
        getError pos optAnn exprs stats []

ref_getExprError := fun (pos : int) (expr : Expr) ->
    match expr with
    | ErrorExpr(msg,rng) when inRange pos rng ->
        Some msg
    | Expr(exprs,stats,fields) ->
        getError pos None exprs stats fields

ref_getError := fun (pos : int) 
                    (optAnnotation : Option<Annotation>)
                    (exprs : List<Expr>) 
                    (stats : List<Statement>) 
                    (fields : List<ConstructorField>) ->
             
    let opStatErrors = List.map (getStatError pos) stats
    let opExprErrors = List.map (getExprError pos) exprs
    let opFieldErrors = List.map (getFieldError pos) fields

    let annErr = Option.bind (getAnnotationError pos) optAnnotation
    let statErr = List.tryFind (fun x -> Option.isSome x) opStatErrors
    let exprErr = List.tryFind (fun x -> Option.isSome x) opExprErrors
    let fieldErr = List.tryFind (fun x -> Option.isSome x) opFieldErrors

    match (statErr,exprErr,fieldErr,annErr) with
    | (Some(Some x), None, None, None)
    | (None, Some(Some x), None, None)
    | (None, None, Some(Some x), None) 
    | (None, None, None, Some x)->
        Some x
    | (None, None, None, None) ->
        None
    | _ ->
        failwith "unreachable" 