﻿module LuaAnalyzer.Context

open Type

type ValueEnvironment = Map<string,Field>

/// A structure which serves as an argument to typechecking functions. It stores
/// all information necessary for typechecking (but external to) the term being typechecked.
///
/// Context structures are modified in a purely functional manner when passed to
/// recursive invocations of the typechecker on the children of the syntax node being
/// checked. 
type Context = {
    
    /// value environment -- maps variable names to variable types. 
    venv : ValueEnvironment

    /// Type Environment -- Contains all known information about types
    tenv : TypeEnvironment

    /// The position in the source file of the term that the user has 
    /// requested information about.
    queryPos : Option<int>

    /// True iff we should generate are messages whenever errors are encountered
    trackErrors : bool

    /// true if we are set to record stub context when we encounter stub
    recordStub : bool

    /// The return types expected from the chunk being checked (or None, if we have no expectations)
    expectedReturnTypes : Option< List<Type> >

    /// Whether or not we are in the body of a latent function. If we are in one,
    /// we can call other latent functions. Otherwise, we can't.
    inLatent : bool

    /// True iff we are checking an expression on the left-hand side of an assignment
    isLeftExpr : bool

    /// True tells the typechecker that assigning variables which are not 
    /// in the context should add them to the context.
    /// (this is true when checking globals.lua, but should be false most of the time)
    addGlobals : bool

    /// the (optional) field, source range, and type of the closest enclosing called function
    closestEnclosingFunctionCall : Option<Option<Field>*Range*Type>

    // the type of the closest enclosing record constructor which has an expected type from
    // bidirectional type checking. note that this type is only recorded for structural records 
    closestEnclosingExpectedRecord : Option<Type>

    /// During internal type collection, we want to record which types the currently-collected type
    /// depends on the structure of. (This allows us to determine when our cached internal type
    /// becomes invalidated). 
    ///
    /// When this is the case, this option has value Some(func),
    /// where is a function which takes fileNames for the files that the currently-collected
    /// internal type depends on.
    ///
    /// When we are not tracking dependencies, this is equal to None.
    dependencyRecorder : Option<string -> unit>
}

type Context with

    static member empty
        with get () =
            {
                venv = Map.empty
                tenv = TypeEnvironment.empty
                queryPos = None
                trackErrors = false
                recordStub = true
                expectedReturnTypes = None
                closestEnclosingFunctionCall = None
                closestEnclosingExpectedRecord = None
                inLatent = false
                isLeftExpr = false
                addGlobals = false
                dependencyRecorder = None
            }

    /// Adds a value to the value environment with the 
    /// specified name and field.
    member this.AddValue (name : string, field : Field) =
        {
        this with
            venv = this.venv.Add(name,field)
        }

    member this.HasValPath (path : List<string>) : bool =
        (this.FieldFromPath path).IsSome

    member this.IsNewField (path : List<string>) : bool =
        let rec isNewField (path : List<string>) (field : Field) =
            match path with
            | [last] ->
                match Type.Coerce this.tenv field.ty with
                | RecordTy(_,_,_,_,Open,_,fields,_,_,_,_) when not (fields.ContainsKey last) ->
                    true
                | _ ->
                    false
            | head :: rest ->
                match Type.Coerce this.tenv field.ty with
                | RecordTy(_,_,_,_,_,_,fields,_,_,_,_) ->
                    if fields.ContainsKey head then
                        isNewField rest fields.[head]
                    else
                        false
                | _ ->
                    false
            | [] ->
                false
            

        if this.venv.ContainsKey path.[0] then
            if path.Tail.IsEmpty then
                false
            else
                isNewField path.Tail this.venv.[path.[0]]
        else
            if path.Length = 1 && this.addGlobals then
                //for globals
                true 
            else
                false

    member this.FieldFromPath (path : List<string>) : Option<Field> =
        // Contract.Requires(path.Length > 0)
        let rec fieldFromPath (path : List<string>) (field : Field) =
            match path with
            | head :: rest ->
                let ty = Type.Coerce this.tenv field.ty
                match ty with
                | RecordTy(_,_,_,_,_,_,fields,_,_,_,_) ->
                    if fields.ContainsKey head then
                        fieldFromPath rest fields.[head]
                    else
                        None
                | _ ->
                    None
            | [] ->
                Some field

        if this.venv.ContainsKey path.[0] then
            fieldFromPath path.Tail this.venv.[path.[0]]
        else
            None

    member this.ExpectReturnTypes (returnTypes : List<Type>) =
        {
        this with
            expectedReturnTypes = Some (returnTypes)
        }

    /// Undoes all types in the type environment (in effect,
    /// reducing all accumulated types to their permanent types).
    member this.UndoAllAccumulations
        with get () =
            let mapEntry (key : string) (field : Field) : Field =
                { field with ty = Type.UndoAccumulations field.ty }

            {
            this with
                venv = Map.map mapEntry this.venv
            }

    /// A context identical to this one, except that it tracks errors
    member this.TrackErrors
        with get () =
            {
            this with
                trackErrors = true
            }
    
    /// A context identical to this one, except that it does not track errors
    member this.DontTrackErrors
        with get () =
            {
            this with
                trackErrors = false
            }

    /// A context identical to this one, except that it does not track errors
    member this.RecordStub
        with get () =
            {
            this with
                recordStub = true
            }

    /// A context identical to this one, except that it does not track errors
    member this.DontRecordStub
        with get () =
            {
            this with
                recordStub = false
            }

    /// A context identical to this one, except intended for left expressions
    member this.IsLeftExpr
        with get () =
            {
            this with
                isLeftExpr = true
            }

    /// A context identical to this one, except intended for non-left expressions
    member this.IsNotLeftExpr
        with get () =
            {
            this with
                isLeftExpr = false
            }

    /// A context identical to this one, except inside a latent function body                
    member this.InLatent
        with get () =
            {
            this with
                inLatent = true
            }

    /// A context identical to this one, except not in a latent function body
    member this.NotInLatent
        with get () =
            {
            this with
                inLatent = false
            }


    /// A context identical to this one, but which tells the typechecker to
    /// add new global assignments to the value environment rather than
    /// generating "identifier not in context" errors
    member this.AddGlobals
        with get () =
            {
            this with
                addGlobals = true
            }
    
    /// A context identical to this one, but which tells the typechecker
    /// to generate errors upon encountering the assignment of names
    /// which are not currently in the value environment.
    member this.DontAddGlobals
        with get () =
            {
            this with
                addGlobals = false
            }

    /// Returns true iff there is a reason for the type checker
    /// to enter function bodies.
    member this.MustEnterFunctionBodies
        with get () =
            this.queryPos.IsSome || this.trackErrors

/// Contains all globals loaded from API definition XML files.
let baseContext : Ref< Context > = ref (Context.empty)

/// Gets the base context, which is the portion of the context consisting
/// of values defined by various external APIs and standard libraries, and
/// which is not affected by the project at all.
let getBaseContext () =
    !baseContext

/// Sets the base context, which is the portion of the context consisting
/// of values defined by various external APIs and standard libraries, and
/// which is not affected by the projected at all.
let setBaseContext (ctxt : Context) =
    baseContext := ctxt