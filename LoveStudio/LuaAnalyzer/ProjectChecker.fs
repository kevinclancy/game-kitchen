﻿module LuaAnalyzer.ProjectChecker

open Type
open TypedSyntax
open ErrorList
open Context
open TypeCollector
open SubtypeGraphBuilder
open TypeChecker
open Plugins

type GenList<'T> = System.Collections.Generic.List<'T>

/// maps each module name to:
/// 1.) the instance type which was last extracted from this module
/// 2.) the constructor type which was last extracted from this module
/// 3.) the list of errors which was generated the last time we tried 
/// to extract external types from this module
let externalTypeCache : Ref< Map<string,Type*Type*List<string*Type>*List<Error>> > = ref Map.empty
 
 /// maps each module name to: 
 /// 1.) A (name->type) map of all internal types which were last extracted from this module
 /// 2.) A list of all errors which were generated the last time we extracted internal types
 /// 3.) A list of the names of all modules which define types which the last internal type computation
 ///     for this module depended upon; ancestor types are excluded because we always treat those as dependencies.
let internalTypeCache : Ref< Map<string,Map<string,Type>*List<Error>*List<string>> > = ref Map.empty

/// maps a module to the time at which that module's external types were last cached.
let timeCachedExt : Ref< Map<string,int64> > = ref Map.empty

/// maps a module to the time at which that module's internal types were last cached.
let timeCachedInt : Ref< Map<string,int64> > = ref Map.empty

/// maps a type name to the time at which that type was last cached
let timeCachedTy : Ref< Map<string,int64> > = ref Map.empty

/// Maps a module name to the typed ast that resulted the last time that 
/// file's contents were parsed and typed, paired with the last-modified time
/// for the contents.
let astCache = ref (new Map<string, int64*TypedStatement*GenList<LocalError>>([]))

/// A set of all modules defining types which the last computation
/// of the globals context depended on the structure of
let globalsDependencies : Ref< Set<string> > = ref Set.empty

/// The tick count of the last time at which the globals context was recomputed
let userGlobalsTimeStamp : Ref<int64> = ref 0L

let userGlobalsCached : Ref<ValueEnvironment> = ref Map.empty

/// whether or not typecheck was enabled the last time we tried to get project errors
let typeCheckCached = ref false

/// The directory of the currently open project (or "" if no project is opened)
let currentProjectDir = ref ""

/// Clears all type collection caching. We call this whenever the current project is switched.
/// Because caching uses module names as lookups, and because separate projects can (and usually do) 
/// have identically named modules, not calling this upon project switches would lead to bugs.
let clearCaches () = 
    externalTypeCache := Map.empty
    internalTypeCache := Map.empty
    timeCachedExt := Map.empty
    timeCachedInt := Map.empty
    timeCachedTy := Map.empty
    astCache := Map.empty
    globalsDependencies := Set.empty
    userGlobalsTimeStamp := 0L
    userGlobalsCached := Map.empty
    SubtypeGraphBuilder.clearCaches()

let switchProject (newProjectDir : string) =
    currentProjectDir := newProjectDir
    clearCaches()

let refRenameType = ref (fun (renamings : Map<string,string>) (ty : Type) -> ty)

let private renameType (renamings : Map<string,string>) (ty : Type) =
    (!refRenameType) renamings ty

let rec private renameMetamethodSet (renamings : Map<string,string>) (metaset : MetamethodSet) =
    let renameBinOp (desc,rhsTy,retTy,loc) =
        (desc, renameType renamings rhsTy, renameType renamings retTy, loc)
    
    let renameUnOp (desc,retTy,loc) =
        (desc, renameType renamings retTy, loc)

    let renameFunc (desc,pars,varpars,rets,varrets,isMethod,isLatent,defLocation) =
        let pars' = List.map (fun (nm,desc,ty) -> (nm,desc,renameType renamings ty)) pars
        let rets' = List.map (fun (desc,ty) -> (desc, renameType renamings ty)) rets
        (desc,pars',varpars,rets',varrets,isMethod,isLatent,defLocation)

    {
        Add = Option.map renameBinOp metaset.Add
        Sub = Option.map renameBinOp metaset.Sub
        Mul = Option.map renameBinOp metaset.Mul
        Div = Option.map renameBinOp metaset.Div
        Mod = Option.map renameBinOp metaset.Mod
        Pow = Option.map renameBinOp metaset.Pow
        Unm = Option.map renameUnOp metaset.Unm
        Concat = Option.map renameBinOp metaset.Concat
        Len = Option.map renameUnOp metaset.Len
        Lt = Option.map renameBinOp metaset.Lt
        Le = Option.map renameBinOp metaset.Le
        Gt = Option.map renameBinOp metaset.Gt
        Ge = Option.map renameBinOp metaset.Ge
    
        And = Option.map renameBinOp metaset.And
        Or = Option.map renameBinOp metaset.Or
        Not = Option.map renameUnOp metaset.Not

        Index = Option.map renameBinOp metaset.Index

        Call = Option.map renameFunc metaset.Call
    }

refRenameType := fun (renamings : Map<string,string>) (ty : Type) ->
    match ty with
    | RecordTy(name,desc,isStructural,displayStructural,openStatus,metamethods,fieldMap,fieldsList,methodMap,typeArgs,defloc) ->
        RecordTy(
            name,
            desc,
            isStructural,
            displayStructural,
            openStatus,
            renameMetamethodSet renamings metamethods,
            Map.map (fun key field -> {field with ty = renameType renamings field.ty}) fieldMap,
            List.map (fun (name,field) -> (name, {field with ty = renameType renamings field.ty})) fieldsList,
            Map.map (fun key field -> {field with ty = renameType renamings field.ty}) methodMap,
            List.map (renameType renamings) typeArgs,
            defloc
        )
    | FunctionTy(desc,pars,varPars,rets,varrets,isMethod,latent,loc) ->
        FunctionTy(
            desc, 
            List.map (fun (nm,desc,ty) -> (nm,desc,renameType renamings ty)) pars,
            varPars,
            List.map (fun (desc,ty) -> (desc,renameType renamings ty)) rets,
            varrets,
            isMethod,
            latent,
            loc
        )
    | TupleTy(types, indefinite) ->
        TupleTy(List.map (renameType renamings) types, indefinite)
    | OverloadTy(tyList) ->
        OverloadTy(List.map (renameType renamings) tyList)
    | NillableTy(ty) ->
        NillableTy(renameType renamings ty)
    | UserDefinedTy(name,rng) ->
        if renamings.ContainsKey name then 
            UserDefinedTy(renamings.[name],rng)
        else
            ty
    | AccumulatedTy(perm,temp) ->
        AccumulatedTy(renameType renamings perm, renameType renamings temp)
    | ErrorTy(_)
    | TypeVarTy(_,_,_,_)
    | TypeAbsTy(_,_)
    | NewFieldTy
    | NumberTy
    | StringTy
    | NilTy
    | BoolTy
    | UnknownTy ->
        ty

type private TypeVar = string*string*DefinitionLocation

//let private refEmbedTypeVarsTy = ref (fun (typeVarNames : Set<TypeVar>) (ty : Type) -> ty)
//
//let private embedTypeVarsTy (typeVarNames : Set<TypeVar>) (ty : Type) =
//    (!refEmbedTypeVarsTy) typeVarNames ty
//
//let emberTypeVarsMetamethodSet (typeVarNames : Set<TypeVar>) (metaset : MetamethodSet) =
//    let embedBinOp (desc,rhsTy,retTy,loc) =
//        (desc, embedTypeVarsTy typeVarNames rhsTy, embedTypeVarsTy typeVarNames retTy, loc)
//    
//    let embedUnOp (desc,retTy,loc) =
//        (desc, embedTypeVarsTy typeVarNames retTy, loc)
//
//    let embedFunc (desc,pars,varpars,rets,varrets,isMethod,isLatent,defLocation) =
//        let pars' = List.map (fun (nm,desc,ty) -> (nm,desc,embedTypeVarsTy typeVarNames ty)) pars
//        let rets' = List.map (fun (desc,ty) -> (desc,embedTypeVarsTy typeVarNames ty)) rets
//        (desc,pars',varpars,rets',varrets,isMethod,isLatent,defLocation)
//
//    {
//        Add = Option.map embedBinOp metaset.Add
//        Sub = Option.map embedBinOp metaset.Sub
//        Mul = Option.map embedBinOp metaset.Mul
//        Div = Option.map embedBinOp metaset.Div
//        Mod = Option.map embedBinOp metaset.Mod
//        Pow = Option.map embedBinOp metaset.Pow
//        Unm = Option.map embedUnOp metaset.Unm
//        Concat = Option.map embedBinOp metaset.Concat
//        Len = Option.map embedUnOp metaset.Len
//        Lt = Option.map embedBinOp metaset.Lt
//        Le = Option.map embedBinOp metaset.Le
//        Gt = Option.map embedBinOp metaset.Gt
//        Ge = Option.map embedBinOp metaset.Ge
//    
//        And = Option.map embedBinOp metaset.And
//        Or = Option.map embedBinOp metaset.Or
//        Not = Option.map embedUnOp metaset.Not
//
//        Index = Option.map embedBinOp metaset.Index
//
//        Call = Option.map embedFunc metaset.Call
//    }


let private embedTypeVarsStat (typeVars : Map<string,TypeVar>) (ast : TypedStatement) =
    let embedVars (ty : Type) =
        match ty with
        | UserDefinedTy(name,rng) when typeVars.ContainsKey name ->
            let nm,desc,loc = typeVars.[name]
            Some( TypeVarTy(name,desc,loc,rng) )
        | _ ->
            None
    TransformTypes.transform embedVars ast

let private renameStat (renamings : Map<string,string>) (ast : TypedStatement) =
    let renameTy (ty : Type) =
        match ty with
        | UserDefinedTy(name,rng) when renamings.ContainsKey name ->
            Some( UserDefinedTy(renamings.[name],rng) )
        | _ ->
            None

    TransformTypes.transform renameTy ast

//let renameAnnotation (renamings : Map<string,string>) (ann : Annotations.Annotation) =
//    let (desc,tagset,errors,rng) = ann
//    let tagset' = 
//        match tagset with
//        | Annotations.FunctionTagSet(pars,varpars,rets,varrets,isLatent) ->
//            let renamePar ((a,b,ty),c) =
//                ((a,b,renameType renamings ty),c)
//            let renameRet (desc,ty) =
//                (desc,renameType renamings ty)
//            let pars' = List.map renamePar pars
//            let rets' = List.map renameRet rets
//            Annotations.FunctionTagSet(pars',varpars,rets',varrets,isLatent)
//        | Annotations.VarConstTagSet(fields) ->
//            let renameField (optTy : Option<Type>,a,b) =
//                (Option.map (renameType renamings) optTy,a,b)
//            Annotations.VarConstTagSet(List.map renameField fields)
//        | Annotations.ModuleHeaderTagSet(typedefs,typeparams) ->
//            Annotations.ModuleHeaderTagSet(
//                List.map (fun (nm,ty) -> (nm,renameType renamings ty)) typedefs,
//                typeparams
//            )
//        | _ ->
//            tagset
//    (desc,tagset',errors,rng)
//
//let refRenameExpr = ref (fun (renamings : Map<string,string>) (expr : TypedExpr) -> expr)
// 
//let private renameExpr (renamings : Map<string,string>) (expr : TypedExpr) =
//    (!refRenameExpr) renamings expr
//
///// renames all type aliases which appear in the statement, as according to the renamings maps
//let rec private renameStat (renamings : Map<string,string>) (stat : TypedStatement) =
//    match stat with
//    | If(clauses,elseExplicit,rng) ->
//        let renameClause ((cond,body) : TypedExpr*TypedStatement) = 
//            renameExpr renamings cond,
//            renameStat renamings body
//            
//        let newClauses = List.map renameClause clauses
//        If(newClauses,elseExplicit,rng)
//    | While(cond,body,rng) ->
//        let cond' = renameExpr renamings cond
//        let body' = renameStat renamings body
//        While(cond',body',rng)
//    | Do(body,rng) ->
//        let body' = renameStat renamings body
//        Do(body',rng)
//    | ForNum(var,start,fin,step,body,rng) ->
//        let var' = renameExpr renamings var
//        let start' = renameExpr renamings start
//        let fin' = renameExpr renamings fin
//        let step' = renameExpr renamings step
//        let body' = renameStat renamings body
//        ForNum(var',start',fin',step',body',rng)
//    | ForGeneric(vars,gens,body,rng) ->
//        let vars' = List.map (fun (expr) -> renameExpr renamings expr) vars
//        let gens' = List.map (fun (expr) -> renameExpr renamings expr) gens
//        let body' = renameStat renamings body
//        ForGeneric(vars',gens',body',rng)
//    | Repeat(cond,body,rng) ->
//        let cond' = renameExpr renamings cond
//        let body' = renameStat renamings body
//        Repeat(cond',body',rng)
//    | LocalAssign(annotation,names,exprs,rng) ->
//        LocalAssign(
//            Option.map (renameAnnotation renamings) annotation,
//            List.map (renameExpr renamings) names,
//            List.map (renameExpr renamings) exprs,
//            rng
//        ) 
//    | Assign(annotation,lvals,rvals,rng) ->
//        Assign(
//            Option.map (renameAnnotation renamings) annotation,
//            List.map (renameExpr renamings) lvals,
//            List.map (renameExpr renamings) rvals,
//            rng
//        )
//    | Return(rets,rng) ->
//        Return(List.map (renameExpr renamings) rets, rng)
//    | Break(rng) ->
//        Break(rng)
//    | Call(callExp,rng) ->
//        Call(renameExpr renamings callExp,rng)
//    | Sequence(optAnn,stats,rng) ->
//        let stats' = Seq.cast stats
//        let stats'' = new GenList<TypedStatement>(Seq.map (renameStat renamings) stats')
//        Sequence(
//            Option.map (renameAnnotation renamings) optAnn,
//            stats'',
//            rng
//        ) 
//    | ErrorStatement(msg,rng) ->
//        ErrorStatement(msg,rng)
//
//let rec private renameConsField (renamings : Map<string,string>) (field : TypedConstructorField) =
//    match field with
//    | ListField(optAnn, valExpr, rng) ->
//        ListField(Option.map (renameAnnotation renamings) optAnn, renameExpr renamings valExpr, rng)
//    | RecField(optAnn,keyExpr,valExpr,rng) ->
//        RecField(
//            Option.map (renameAnnotation renamings) optAnn, 
//            renameExpr renamings keyExpr, 
//            renameExpr renamings valExpr, 
//            rng
//        )
//    | ErrorField(_,_) ->
//        field
//
//refRenameExpr := fun (renamings : Map<string,string>) (expr : TypedExpr) ->
//    match expr with
//    | Constructor(consFields,rng) ->
//        Constructor(List.map (renameConsField renamings) consFields, rng)
//    | Function(optAnn,selfName,latent,formals,varpars,rets,varrets,body,rng) ->
//        Function(
//            Option.map (renameAnnotation renamings) optAnn,
//            selfName,
//            latent,
//            List.map (fun (a,b,ty) -> (a,b,renameType renamings ty)) formals,
//            varpars,
//            List.map (fun (a,ty) -> (a,renameType renamings ty)) rets,
//            varrets,
//            renameStat renamings body,
//            rng
//        )
//    | UnOpExpr(unop,expr,rng) ->
//        UnOpExpr(unop,renameExpr renamings expr, rng)
//    | BinOpExpr(binop,exprA,exprB,rng) ->
//        BinOpExpr(binop,renameExpr renamings exprA,renameExpr renamings exprB,rng)
//    | ParenthesizedExpr(expr,rng) ->
//        ParenthesizedExpr(renameExpr renamings expr,rng)
//    | Ascription(expr,ty,rng) ->
//        Ascription(renameExpr renamings expr, renameType renamings ty, rng)
//    | CallExpr(callTargetExpr,argExprs,rng) ->
//        CallExpr(
//            renameExpr renamings callTargetExpr,
//            List.map (renameExpr renamings) argExprs,
//            rng
//        )
//    | ErrorExpr(_,_)
//    | Number(_,_) 
//    | String(_,_)
//    | Nil(_)
//    | True(_)
//    | False(_)
//    | VarArgs(_)
//    | NameExpr(_,_) ->
//        expr    

/// renames all type aliases contained in all ast's in astMap from local aliases to global ones
//let private renameExternalTypes (tenv : TypeEnvironment) (astMap : Map<string,TypedStatement>) =
//    
//    let renameAST (definedTypeNames : Set<string>) (ast : TypedStatement) =
//        match ast with
//        | 
//
//    let foldModule (acc : Map<string,TypedStatement>) (moduleName : string) =
//        let ast = acc.[moduleName]
//        let typeDefs =
//            match ast with
//            | Sequence(Some(_,Annotations.ModuleHeaderTagSet(typeDefs),_,_),_,_) ->
//                Map.ofList typeDefs
//            | _ ->
//                []



/// env - A partially-constructed type environment which contains a complete subtype
/// graph, but nothing else.
///
/// collectorMap - maps each type-collected module name to the type collector which
/// collects it.
///
/// astMap - Maps each module to a typed ast for that module's code
///
/// timeMap - Maps each module name to the time that module was last modified
/// these times come directly from the client, and hence may be more up-to-date
/// than the cached times.
///
/// Iterates through each module mentioned in the subtype graph contained in tenv,
/// collecting an instance and a constructor type from each one, accumulating these
/// types into the consMap and typeMap fields of the environment.
let private buildExternalTypes (tenv : TypeEnvironment)
                               (collectorMap : Map<string,TypeCollector>)
                               (astMap : Map<string,TypedStatement>)
                               (timeMap : Map<string,int64>)
                               : TypeEnvironment =

    //In tenv, all built-in, API, and project-defined types have
    //been recorded in instanceTypes, but actual types for the project-defined
    //ones have not been entered into typeMap
    // Contract.Requires(tenv.typeMap.Count <= tenv.instanceTypes.Count) 
    
    // In the result, all project-defined instance types have been entered
    // into the typeMap. Constructor types have potentially been added
    // to the typeMap as well, so strict equality is not guaranteed.
    // Contract.Ensures(Contract.Result<TypeEnvironment>().typeMap.Count >= Contract.Result<TypeEnvironment>().instanceTypes.Count)
    
    let foldType (tenv : TypeEnvironment) (typeName : string) : TypeEnvironment =
        if tenv.instanceTypes.Contains typeName then
            let fileName,moduleName = tenv.typeFiles.Item typeName
            let ast = astMap.Item typeName
            let collector = collectorMap.Item typeName
            let clientTimeStamp = timeMap.[typeName]

            /// compute the instance and constructor types for this module
            let computeResults (newTimeStamp : int64) =
                currentFileName := fileName
                let newTypes,errors = collector.externalTypeBuilder tenv typeName fileName ast
                
                let typeDefs =
                    match ast with
                    | Sequence(Some(_,Annotations.ModuleHeaderTagSet(typeDefs,_),_,_),_,_) ->
                        typeDefs
                    | _ ->
                        []

                let newTypes,typeDefs =
                    match newTypes with
                    | Some(instTy, consTy) ->
                        timeCachedTy := (!timeCachedTy).Add(typeName,newTimeStamp)
                        timeCachedTy := (!timeCachedTy).Add(consTy.Name,newTimeStamp)
                        timeCachedExt := (!timeCachedExt).Add(typeName,newTimeStamp)

                        let cacheTypeDef ( (name,ty) : string*Type ) = 
                            timeCachedTy := (!timeCachedTy).Add(typeName + "." + name, newTimeStamp)

                        List.iter cacheTypeDef typeDefs

                        externalTypeCache := (!externalTypeCache).Add(typeName,(instTy,consTy,typeDefs,errors))
                        newTypes,typeDefs
                    | None ->
                        if (!externalTypeCache).ContainsKey typeName then
                            let (instTy,consTy,typeDefs,errors) = (!externalTypeCache).[typeName]
                            timeCachedTy := (!timeCachedTy).Remove(typeName)
                            timeCachedTy := (!timeCachedTy).Remove(consTy.Name)
                            timeCachedExt := (!timeCachedExt).Remove(typeName)
                            let removeTypeDefFromCache ( (name,ty) : string*Type ) = 
                                timeCachedTy := (!timeCachedTy).Remove(typeName + "." + name)
                            List.iter removeTypeDefFromCache typeDefs
                            externalTypeCache := (!externalTypeCache).Remove(typeName)
                        None,[]

                newTypes,typeDefs,errors

            // An option containing a possible (instanceType, constructorType) pair
            let instConsTypes,typeDefs,errors =
                if (!externalTypeCache).ContainsKey typeName then
                    let cachedInstTy,cachedConsTy,cachedTypeDefs,cachedErrors = (!externalTypeCache).[typeName]
                    let cachedTimeStamp = (!timeCachedExt).[typeName]

                    let getTimeStamp (ancestorName : string) =
                        (!timeCachedTy).[ancestorName]

                    let ancestorTimeStamps = Set.map getTimeStamp ((tenv.AncestorsOfInclusive typeName).Remove(typeName))
                    let maxAncestorTimeStamp = ancestorTimeStamps.Add(clientTimeStamp).MaximumElement

                    if cachedTimeStamp >= maxAncestorTimeStamp then
                        Some(cachedInstTy,cachedConsTy),cachedTypeDefs,cachedErrors
                    else
                        computeResults(maxAncestorTimeStamp)
                else
                    computeResults(clientTimeStamp)
            
            addErrors(errors)

            // type environment with instance and constructor type added
            let tenv1 = 
                match instConsTypes with
                | Some (instTy, consTy) ->
                    {
                        tenv with
                            typeMap = tenv.typeMap.Add(typeName,instTy)
                            consMap = tenv.consMap.Add(typeName,consTy)
                            typeFiles = tenv.typeFiles.Add(consTy.Name, (fileName,moduleName))
                    }
                | None ->
                    tenv

            let foldTypeDef (tenv : TypeEnvironment) ((typeName,typeDef) : string*Type) =
                if tenv.instanceTypes.Contains(moduleName+"."+typeName) then
                    addError
                        fileName
                        ("type " + typeName + " defined in module '" + moduleName + "' as well as module '" + typeName + "'.")
                        (0,0)
                    tenv
                else
                    {
                        tenv with
                            typedefNames = tenv.typedefNames.Add(moduleName+"."+typeName)
                            typeMap = tenv.typeMap.Add(moduleName+"."+typeName,typeDef)
                            typeFiles = tenv.typeFiles.Add(moduleName+"."+typeName, (fileName,moduleName))
                    }

            // type environment with typedefs added
            List.fold foldTypeDef tenv1 typeDefs
        else
            tenv


    let tenv1 = 
        List.fold foldType tenv tenv.TopSortedTypeNames

    /// add all of the types in the typedef section of the specified module's header to
    /// the type environment
    let addGlobalTypedefsFromModule (tenv : TypeEnvironment) (moduleName : string) = 
        let typeDefs = TypedSyntax.getTypedefs astMap.[moduleName]

        let foldTypeDef (tenv : TypeEnvironment) ((typeName,typeDef) : string*Type) =
            if tenv.instanceTypes.Contains(typeName) then
                addError
                    (!currentProjectDir + moduleName + ".lua")
                    ("type " + typeName + " defined in module '" + moduleName + "' as well as module '" + typeName + "'.")
                    (0,0)
                tenv
            else
                {
                    tenv with
                        typedefNames = tenv.typedefNames.Add(typeName)
                        typeMap = tenv.typeMap.Add(typeName,typeDef)
                        typeFiles = tenv.typeFiles.Add(typeName, (!currentProjectDir + moduleName,moduleName)) 
                }

        List.fold foldTypeDef tenv typeDefs

    // global types can be defined as typedefs of main.lua or globals.lua
    let tenv2 = addGlobalTypedefsFromModule tenv1 "globals"
    addGlobalTypedefsFromModule tenv2 "main"



/// ctxt - The initial context, which we add global bindings to
/// modules - a sequence of, for all modules in the project, 
///   (fullPath,moduleName,contents,lastModifiedTime) quadruples
/// globalsModName - the name of the globals module
///
/// If a module with the name globalsModName exists, we extract the
/// name/field pairs of all globals which are assigned in this module,
/// adding them to ctxt, replacing any existing entries that have identical names.
/// Returns the resulting context.
let buildGlobalCtxt (ctxt : Context) (modules : seq<string*string*TypedStatement*int64>) (globalsModName : string) =
    
    // Contract.Ensures(Contract.Result<Context>().tenv === ctxt.tenv)
    // Contract.Ensures(Contract.Result<Context>().venv.Count >= ctxt.venv.Count)

    let globalsModule = Seq.tryFind (fun (fileName,modName,_,_) -> modName = globalsModName) modules
    
    match globalsModule with
    | Some(fileName,_,ast,clientTimeStamp) ->

        // given the name of an external type dependency, get the last time
        // that external type was cached. This should be up-to-date,
        // because external types are computed before internal types.
        //
        // note that constructor types can be dependencies, so we can't
        // just take this from timeCachedTy
        let getTimeStampExt (dep : string) =
            if dep = "globals" then
                clientTimeStamp
            else
                if ctxt.tenv.typeFiles.ContainsKey(dep) then
                    (!timeCachedTy).[snd (ctxt.tenv.typeFiles.[dep])]
                else
                    0L
        
        // the maximum timestamp of any type dependencies of the globals module (or 0 if there aren't any)
        let maxTyDepTimeStamp = (Set.map getTimeStampExt (!globalsDependencies)).Add(0L).MaximumElement
        // incorporate the other dependency timestamp -- that of the globals.lua source file
        let maxDepTimeStamp = max clientTimeStamp maxTyDepTimeStamp

        if (!userGlobalsTimeStamp) < maxDepTimeStamp then
            currentFileName := fileName
            //note that we can't just plant a stub at the end of the
            //ast and typecheck; that would put locals into our context
            //as well.
            let userGlobals = Map.map (fun name field -> { field with dependencies = field.dependencies.Add("globals") })
                                      (getAssigns ctxt ast)
            
            globalsDependencies := Set.unionMany (List.map (fun (name,field) -> field.dependencies) (Map.toList userGlobals))

            userGlobalsTimeStamp := maxDepTimeStamp
            userGlobalsCached := userGlobals 
            { ctxt with venv = cover ctxt.venv userGlobals}
        else
            { ctxt with venv = cover ctxt.venv (!userGlobalsCached) }
    | _ ->
        ctxt

/// ctxt - The initial context, which we add internal types to.
/// collectorMap - maps each external type name to the name of the collector which 
///                collected its type.
/// astMap - Maps each module to a typed abstract syntax tree of that module's code
///
/// Given a context for which external types have been collected, visits all modules
/// which have been determined to define types, collecting their internal types and
/// adding them to the context.
///
/// timeMap - maps each module to the time that module was last modified. these times 
/// come directly from the client, and hence may be more up-to-date than the cached times.
///
/// Internal type collection topologically sorts the subtype graph and visits 
/// modules in this order. This is necessary, because class constructors, which define
/// internal types, may reference the internal types of parent classes in their bodies.
let buildInternalTypes (ctxt : Context) 
                       (collectorMap : Map<string,TypeCollector>) 
                       (astMap : Map<string,TypedStatement>) 
                       (timeMap : Map<string,int64>)
                       (optTargetModuleName : Option<string*string*TypedStatement*int64>)
                       : Context  =
    
    // Contract.Ensures (Contract.Result<Context>().tenv.typeMap.Count >= ctxt.tenv.typeMap.Count)

    let foldModule (ctxt : Context) (modName : string) : Context =
        let tenv = ctxt.tenv
        let fileName = tenv.typeFiles.Item modName

        let isAncestorOfTarget =
            match optTargetModuleName with
            | Some(_,targetModuleName,_,_) when tenv.instanceTypes.Contains targetModuleName ->
                // if there is a target, we only collect internal types of it and its ancestor modules
                let targetAncestors = tenv.AncestorsOfInclusive targetModuleName
                targetAncestors.Contains modName
            | Some(_,_,_,_) ->
                // in this case, the target does not define internal types, so we don't need to do 
                // internal type collection at all
                false
            | _ ->
                // if there is no target, we don't impose this constraint
                true

        if tenv.instanceTypes.Contains modName && isAncestorOfTarget then
            let fileName,_ = tenv.typeFiles.Item modName
            let ast = astMap.Item modName
            let collector = collectorMap.Item modName
            let clientTimeStamp = timeMap.[modName]

            // given the name of an internal type dependency, get the last time 
            // that internal type was cached
            //
            // this should be up-to-date, because internal type dependencies
            // must have been processed before this type in topological order
            //
            // TODO: the below explanation is kind of weird. I need to clean this up.
            // we don't use timeCachedTy here, because we do not set up nominal
            // subtype relationships between internal types across modules. However,
            // when an internal type is a nominal descendent of an external type defined 
            // in another module, it usually means that there is a dependency
            // upon the internal types in that module.
            let getTimeStampInt (dep : string) =
                let modName = snd (tenv.typeFiles.[dep])
                (!timeCachedInt).[modName]
            
            /// given the name of an external type dependency, get the last time
            /// that external type was cached. This should be up-to-date,
            /// because external types are computed before internal types.
            ///
            /// If the dependency is globals, we just get the last time globals.lua
            /// was updated. We track globals dependencies on the identifier level of granularity;
            /// if a global identifier changes due to one of its external dependencies 
            /// (rather than a change to globals.lua), the all of the modules depending upon 
            /// the changed identifier will depend directly upon the identifier's external dependencies.
            /// This way, modules do not depend upon global variables that they do not use. 
            ///
            // note that constructor types can be dependencies, so we can't
            // just take this from timeCachedTy
            let getTimeStampExt (dep : string) =
                if dep = "globals" then
                    timeMap.["globals"]
                else
                    (!timeCachedTy).[snd (tenv.typeFiles.[dep])]

            let computeAndCacheResults (timeStamp : int64) =
                currentFileName := fileName
                let newTypes,errors,nonAncestorDependencies = collector.internalTypeBuilder ctxt modName fileName ast

                let foldInternalType (cacheTime : Map<string,int64>) (name : string) (ty : Type) =
                    cacheTime.Add(name,timeStamp) 
                
                System.Console.WriteLine("caching internal type for: " + modName)

                timeCachedTy := Map.fold foldInternalType (!timeCachedTy) newTypes 
                timeCachedInt := (!timeCachedInt).Add(modName,timeStamp)
                internalTypeCache := (!internalTypeCache).Add(modName,(newTypes,errors,nonAncestorDependencies))
                newTypes,errors,nonAncestorDependencies

            let collectedTypeMap, errors, _ =
                let ancestorTimeStamps = Set.map getTimeStampInt ((tenv.AncestorsOfInclusive modName).Remove(modName))
                let maxTimeStamp = ancestorTimeStamps.Add(0L).MaximumElement
                let maxTimeStamp = max maxTimeStamp clientTimeStamp

                if (!internalTypeCache).ContainsKey modName then
                    let _,_,oldNonAncestorDependencies = (!internalTypeCache).[modName]

                    // up to date time stamps, but most-recently-cached dependency list
                    let nonAncestorTimeStamps = List.map getTimeStampExt oldNonAncestorDependencies
                    let maxNonAncestorTimeStamp = List.max (0L :: nonAncestorTimeStamps)
                    let maxTimeStamp = max maxTimeStamp maxNonAncestorTimeStamp
                        
                    let cacheTimeStamp = (!timeCachedInt).[modName]
                    if cacheTimeStamp < maxTimeStamp then
                        computeAndCacheResults (maxTimeStamp)
                    else
                        (!internalTypeCache).[modName]
                        
                else
                    computeAndCacheResults (maxTimeStamp)
            
            addErrors(errors)

            let tenv = 
                {
                tenv with
                    typeMap = (cover tenv.typeMap collectedTypeMap)
                }

            {
            ctxt with
                tenv = tenv
            }
        else
            ctxt
    
    List.fold foldModule ctxt ctxt.tenv.TopSortedTypeNames

/// Sets currentFileName to fileName and then type checks ast w.r.t. baseCtxt
let typeCheck (baseCtxt : Context) (fileName : string) (ast : TypedStatement) =
    currentFileName := fileName
    typeCheckStat baseCtxt ast

/// collectorMap - maps each type-collected module name to the type collector
///                which collected it.
/// preGlobalsCtxt - A context with a complete type environment and a value environment
///                  that contains external API definition values, but no globals.
///
/// ctxt - A context with a complete type environment and a value environment that
///        contains API definitions AND globals.
///
/// modules - a list containing, for each module in the project, a 
///          (fullPathName,moduleName,typedSyntaxTree,lastModifiedTime) quadruple.
///
/// Typechecks each file in the modules list, adding all encountered typing errors
/// to the error list. Depending on the nature of the files, they will be conditioned
/// for typechecking differently. 
///
/// - Files which define types are first decorated and checked
/// for miscellaneous errors. Then, they are checked w.r.t. ctxt.
///
/// - The module called "globals" is checked w.r.t. preGlobalsCtxt, with the
///   addGlobals flag set to true.
///
/// - All non-type-defining, non-globals files are checked wr.t. ctxt.
let private runTypeChecker (collectorMap : Map<string,TypeCollector>) 
                           (preGlobalsCtxt : Context)
                           (ctxt : Context) 
                           (modules : seq<string*string*TypedStatement*int64>) =
    
    // Contract.Requires(preGlobalsCtxt.venv.Count <= ctxt.venv.Count)

    let foldTypeVar tenv (nm,desc,loc) =
        {
            tenv with
                typeParamNames = tenv.typeParamNames.Add(nm)
                typeMap = tenv.typeMap.Add(nm, TypeVarTy(nm,desc,loc,snd loc))
        }

    for fileName, moduleName, ast, lastMod in modules do
        if ctxt.tenv.instanceTypes.Contains moduleName then
            let collector = collectorMap.Item moduleName
            let ctxt' = { ctxt with tenv = List.fold foldTypeVar ctxt.tenv (getTypeVars ast) }
            let decoratedAST = collector.decorate ctxt' moduleName ast
            collector.detectMiscErrors ctxt' moduleName fileName decoratedAST
            typeCheck ctxt'.TrackErrors fileName decoratedAST
        else
            if moduleName = "globals" then
                typeCheck preGlobalsCtxt.AddGlobals.TrackErrors fileName ast
            else 
                typeCheck ctxt.TrackErrors fileName ast

let private renameAST (ast : TypedStatement) (moduleName : string) =
    let renamings = 
        match ast with
        | Sequence(optAnn,stats,rng) ->
            match optAnn with
            | Some(_,Annotations.ModuleHeaderTagSet(typedefs,_),_,_) ->
                List.fold (fun mp (nm,ty) -> Map.add nm (moduleName+"."+nm) mp) Map.empty typedefs
            | _ ->
                Map.empty
        | _ ->
            Map.empty
    if moduleName = "globals" || moduleName = "main" then
        ast
    else
        renameStat renamings ast

/// If a valid cached AST exists, return that along with its parse errors and time stamp. 
/// Otherwise, return parse a new ast for the given file. 
/// The return type of this function is weird, and I am currently too lazy to fix it.
let getAST (parseRepair : bool)
           (targetFileName : Option<string>)
           (generateAnnotationErrors : bool) 
           ((fileName,modName,modContents,lastMod) : string*string*Lazy<string>*int64) =
    
    let ast,errors =
        if parseRepair && (fileName = targetFileName.Value) then
            let parsed,_ = HandParser.parse (modContents.Value) true fileName
            let errors = ErrorList.listErrors parsed generateAnnotationErrors
            let ast = renameAST (typeStatement parsed) modName
            let typeVarMap = List.fold (fun (mp : Map<string,TypeVar>) (name,desc,loc) -> mp.Add (name,(name,desc,loc))) Map.empty (getTypeVars ast)
            let ast = embedTypeVarsStat typeVarMap ast
            ast, errors
        elif (!astCache).ContainsKey modName then
            let stamp,cachedAST,errors = (!astCache).Item modName
 
            if lastMod <> stamp then
                let parsed,_ = HandParser.parse (modContents.Value) false fileName
                let errors = ErrorList.listErrors parsed generateAnnotationErrors
                let ast = renameAST (typeStatement parsed) modName
                let typeVarMap = List.fold (fun (mp : Map<string,TypeVar>) (name,desc,loc) -> mp.Add (name,(name,desc,loc))) Map.empty (getTypeVars ast)
                let ast = embedTypeVarsStat typeVarMap ast
                astCache := astCache.contents.Add(modName, (lastMod,ast,errors))
                ast,errors
            else
                cachedAST,errors
        else
            let contents = modContents.Value
            let parsed,_ = HandParser.parse contents false fileName
            let ast = renameAST (typeStatement parsed) modName
            let typeVarMap = List.fold (fun (mp : Map<string,TypeVar>) (name,desc,loc) -> mp.Add (name,(name,desc,loc))) Map.empty (getTypeVars ast)
            let ast = embedTypeVarsStat typeVarMap ast
            let errors = ErrorList.listErrors parsed generateAnnotationErrors
            astCache := astCache.contents.Add(modName, (lastMod,ast,errors))
            ast,errors
    
    (fileName,modName,ast,lastMod), (modName,ast), errors 

/// Copies all library modules from the UserData directory to the project directory
/// Returns a list of project-relative file names of all library modules
let syncLibModules () =
    let projectDir = !currentProjectDir

    let libModules = Set.ofSeq (Seq.collect (fun (x : ITypeCollectorPlugin) -> x.GetLibPaths()) !plugins)
    
    for modFile : string in libModules do
        // make sure the directory containing the file (in the project) exists 
        let dirChain = modFile.Split('\\')
        let dir = ref ""
        for i in 0 .. dirChain.Length-2 do
            dir := !dir + "\\" + dirChain.[i]

            if not (System.IO.Directory.Exists(projectDir + !dir)) then
                ignore (System.IO.Directory.CreateDirectory(projectDir + !dir))

        let writeTime = System.IO.File.GetLastWriteTime
        let exists = System.IO.File.Exists
        let projFile = projectDir + "\\" + modFile
        let pluginsFile = pluginsDir + "\\" + modFile

        // copy from plugin directory to project directory
        if (not (exists(projectDir + "\\" + modFile))) ||
           (not (writeTime(projectDir + "\\" + modFile) = writeTime(pluginsDir + "\\" + modFile))) then
            System.IO.File.Copy(pluginsDir + "\\" + modFile, projectDir + "\\" + modFile,true)

    Set.map (fun x -> projectDir + x) libModules

/// filters out all modules which are associated with plugins from typechecking
let filterLibModules (modules : seq<LuaModule>) =
    let projectDir = !currentProjectDir
    let projRelativeLibFileNames = Set.ofSeq (Seq.collect (fun (x : ITypeCollectorPlugin) -> x.GetLibPaths()) !plugins)
    let filterLibModules (fileName : string,_,_,_) = not (projRelativeLibFileNames.Contains (fileName.Substring(projectDir.Length)))
    Seq.filter filterLibModules modules

/// modules - a sequence of all modules in the project
///
/// targetFile - None signifies that the entire project should be typechecked. 
/// Otherwise, we only check the specified file (but still collect from the entire project)
///
/// queryPos - If targetFile is used, we may be checking it so that information about some
/// word in the program can be retrieved. If so, the some position in the word is used as this argument.
///
/// shouldCheck - true if we want to run the typechecker (for generating errors or getting the context
/// at a certain point). false if we're just extract type environment and type collector information.
///
/// checkAnnotations - true if we want to generate parse errors for annotations as well as lua syntax
///
/// repairParse - true iff we want to attempt repairing the parse near the occurrence of the
/// marker expression ("lucbdnioua") in the target file. this is used to handle intellisense 
/// queries, which are often made while the user is in the middle of typing incomplete expressions 
/// and statements.
///
/// Runs the typechecker--either for generating type checking errors or querying information
/// about the program. Note that this function performs everything needed for typechecking, 
/// including type collection.
let runTypeCollectors (modules : seq<LuaModule>) 
                      (targetFile : Option<string>) 
                      (queryPos : Option<int>) 
                      (shouldCheck : bool)
                      (checkAnnotations : bool)
                      (parseRepair : bool) 
                      : Option<TypeEnvironment*Map<string,TypeCollector>> =

    // Contract.Requires(queryPos.IsSome ==> targetFile.IsSome)
    // Contract.Requires(targetFile.IsSome ==> shouldCheck)
    // Contract.Requires(checkAnnotations ==> shouldCheck)
    // Contract.Requires(parseRepair ==> targetFile.IsSome)

    System.Console.WriteLine("running type collectors with target " + (if targetFile.IsSome then targetFile.Value else "none"))
    let libFiles = syncLibModules()

    let modules = filterLibModules modules
    let mapped = List.ofSeq (Seq.map (getAST parseRepair targetFile checkAnnotations) modules)

    let fst3 (x,_,_) = x
    let snd3 (_,x,_) = x
    let thrd3 (_,_,x) = x

    let modules = List.map fst3 mapped
    let astMap = Map.ofSeq (Seq.map snd3 mapped)
    
    let concatAllErrors (fileName,_,_,_) (fileErrors : GenList<LocalError>) =
        for (msg,rng) in fileErrors do
            addParseError fileName msg rng
    
    Seq.iter2 concatAllErrors (List.map fst3 mapped) (List.map thrd3 mapped)

    let isTarget (fileName,_,_,_) =
        fileName = targetFile.Value

    if parseErrors.Count = 0 || targetFile.IsNone || Seq.exists isTarget modules then
        // if there isn't a target, or the target is a non-lib file in the project
        let optTargetModule = 
            if targetFile.IsSome then
                Seq.tryFind (fun (fileName,_,_,_) -> fileName = targetFile.Value) modules
            else
                None

        try 
            let addTimeStamp (acc : Map<string,int64>) (_,modName,_,lastModTime)  =
                 acc.Add(modName,lastModTime)

            let timeMap = Seq.fold addTimeStamp Map.empty modules  
            let tenv, collectorMap = buildSubtypeGraph modules
            let tenv = buildExternalTypes tenv collectorMap astMap timeMap
        
            let preGlobalsCtxt = 
                {
                !baseContext with
                    tenv = tenv
                }

            let ctxt = buildGlobalCtxt preGlobalsCtxt modules "globals"
        

            let ctxt = buildInternalTypes ctxt collectorMap astMap timeMap optTargetModule
        
            if not (targetFile.IsSome) then
                // it might be nice to get interactive errors for these. they are too slow to compute, though.
                // maybe I could pass in the target file name and do a targeted version of this
                ctxt.tenv.CheckStructuralSubtyping()
        
            let modules = 
                match optTargetModule with
                | Some(targetModule) ->
                    Seq.ofList [targetModule]
                | _ ->
                    Seq.ofList modules
        
            let ctxt = { ctxt with queryPos = queryPos }
            //it's important to inject queryPos into the preGlobalsCtxt as well, in case
            //the query is being made in globals.lua.
            let preGlobalsCtxt = { preGlobalsCtxt with queryPos = queryPos }

            if shouldCheck then
                runTypeChecker collectorMap preGlobalsCtxt ctxt modules

            Some (tenv, collectorMap)
        with
        | BrokenEdgeException(child,parent,file,rng) ->
            addError 
                file
                ( child + " inherits from non-existant or non-instance type " + parent )
                rng
            None
        | TypeGraphDiamondException(source,dest,pathA,pathB,file,rng) ->
            addError
                file 
                ("diamond in subtype graph from " + source + " to " + dest + "\n" + pathA.ToString() + "\n" + pathB.ToString())
                rng
            None
        | TypeGraphCycleException(cycle,file,rng) ->
            addError
                file
                ("cycle in subtype graph: " + cycle.ToString())
                rng
            None
        | CollectionConflict(collectorName1, collectorName2, moduleName,file,rng) ->
            addError
                file
                ("two different type collection plugins, " + collectorName1 + " and " + collectorName2 + ", collected a type from " + moduleName + ".")
                rng
            None
        | StructuralSubtypingError(msg,file,rng) ->
            addError
                file
                msg
                rng
            None
        | Failure(msg) ->
            System.Console.WriteLine("collection error: " + msg)
            failwith msg
    else
        //if the target is a lib, or a file not in the project, return None.
        //lib files are not meant to be edited, so we don't need to provide intellisense features for them.
        //it wouldn't really make sense to provide intellisense for non-project files
        None

/// modules - sequence of all lua modules in the project
/// typeCheck - whether or not to generate type errors
/// fileName - the full path name of the single file to check (or None if we want to check all files)
///
/// Generates a list of errors in the project. If typeCheck is true and no syntax errors exist, 
/// it generates type errors. If typeCheck is false, it generates syntax errors only.
/// The boolean returned is true iff the errors list contains syntax errors. Otherwise,
/// it contains type errors.
let typeCheckProject (modules : seq<LuaModule>) 
                     (typeCheck : bool) 
                     (fileName : Option<string>) 
                     : GenList<string*string*Range>*bool =
    
    Type.clearAssumptions()

    if !typeCheckCached <> typeCheck then
        clearCaches()

    typeCheckCached := typeCheck

    errors.Clear()
    parseErrors.Clear()

    //generate errors
    let a = runTypeCollectors modules fileName None true typeCheck false

    if (parseErrors.Count = 0) && typeCheck then                 
        errors, false
    else
        parseErrors, true
    
/// caches type data for module
let updateModule (modules: seq<LuaModule>) (fileName : string) =
    ignore (typeCheckProject modules false (Some fileName))

/// modules - a sequence of all modules in the project
/// targetFile - the full path name of the file to query from
/// queryPos - the position (char index) in the target file which we are querying
///
/// Retrieves a description (and definition location) of the program element containing 
/// the specified position in the specified file. This description is generated from the 
/// element's type and/or field when possible.
///
/// Returns a (identifier description, definition location, type description) triple.
/// When any of the aforementioned items cannot be obtained, it returns "",NoLocation, and ""
/// respectively.  
let queryDescAndLoc (modules : seq<LuaModule>) (targetFile : string) (queryPos : int) (checkAnnotations : bool) =
    
    refQueryResponse := None

    let result = 
        runTypeCollectors 
            modules 
            (Some(targetFile)) 
            (Some(queryPos)) 
            true 
            checkAnnotations
            false

    let queryResponse = !refQueryResponse

    match !refQueryResponse with
    | Some(response) ->
        let superTypesDesc = 
            match (result,queryResponse) with
            | Some(tenv,_),Some(response) ->
                if response.ty.IsSome && response.ty.Value.HasName && 
                   tenv.edges.ContainsKey response.ty.Value.Name then
                    let tyName = response.ty.Value.Name
                    let ancestors = Set.filter (fun x -> x <> tyName) (tenv.AncestorsOfInclusive(tyName))
                    if ancestors.Count > 0 then
                        "\nSupertypes of " + tyName + ": " + (String.concat ", " ancestors)
                    else
                        "" 
                else
                    ""
            | _ ->
                ""
        /// avoid redundant descriptions by removing description from type if necessary
        let ty = 
            match response.ty with
            | Some(FunctionTy(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation))
                when desc = response.desc ->
                Some(FunctionTy("",formals,varPars,rets,varRets,isMethod,latent,defLocation))
            | _ ->
                response.ty

        let typeDescription = 
            if ty.IsSome then
                ty.Value.ToStringElaborate()+superTypesDesc
            else
                ""

        response.desc,response.loc,typeDescription
    | None ->
        "nothing found",NoLocation,""

/// modules - a sequence of all lua modules in the project
/// targetFile - the full path of the lua file containing the context 
/// stub (a call to lucbdnioua) to extract from.
///
/// Given a project and a file within that project, typechecks the specified
/// file, recording the context when the typechecker reaches a call to "lucbdnioua",
/// It is assumed that exactly one call to lucbdnioua exists in the file.
let getStubContextInProject (modules : seq<LuaModule>) (targetFile : string) =
    refStubContext := getBaseContext()
    ignore (runTypeCollectors modules (Some(targetFile)) None true false true)
    !refStubContext

/// modules - a sequence of all lua modules in the project
/// targetCollectorName - The name of the collector from which obtain
///   a list of all known module names.
///
/// Given a project and the name of a type collector, returns a list of names
/// of all modules which belong to that type collector.
let getModulesCollectedBy (modules : seq<LuaModule>) (targetCollectorName : string) =
    match runTypeCollectors modules None None false false false with
    | Some(tenv,collectorMap) ->
        let moduleCollectedByTarget modName =
            collectorMap.ContainsKey modName && collectorMap.[modName].name = targetCollectorName

        let result = Seq.filter moduleCollectedByTarget (Seq.map (fun (_,x,_,_) -> x) modules)
        new GenList<string>(result)
    | None ->
        new GenList<string>([])

/// ctxt - a context to typecheck ast with
/// ast - a syntax tree with an embedded placeholder to extract the context at
///
/// Typechecks the supplied statement w.r.t. the supplied context, recording
/// the context when the typechecker reaches a call to "lucbdnioua". It is assumed
/// that exactly one such call exists in the statement.
let getStubContext (ctxt : Context) (ast : TypedStatement) (fileName : string) =
    currentFileName := fileName
    ignore (typeCheckStat ctxt ast)
    !refStubContext

/// ctxt - a context to typecheck ast with
/// ast - a syntax tree whose effect on the ctxt we are measuring
///
/// Returns the updated context after typechecking ast w.r.t. ctxt
let getContextAfter (ctxt : Context) (ast : TypedStatement) (fileName : string) =
    let z = (0,0)
    let ast = Sequence(
        None,
        new GenList<TypedStatement>([
            ast;
            Call(CallExpr(NameExpr("lucbdnioua",z),[],z),z)
        ]),
        (0,0)
    )

    getStubContext ctxt ast fileName

let invalidateCachedModule (moduleToInvalidate : string) =
    
    if (!externalTypeCache).ContainsKey moduleToInvalidate then
        // remove external types timestamps
        let _,consTy,typeDefs,_ = (!externalTypeCache).[moduleToInvalidate]
        let consName = consTy.Name
        timeCachedTy := (!timeCachedTy).Remove(moduleToInvalidate).Remove(consName)
        List.iter (fun (name,_) -> timeCachedTy := (!timeCachedTy).Remove(moduleToInvalidate + "." + name)) typeDefs

        //remove internal type timestamps
        if (!internalTypeCache).ContainsKey moduleToInvalidate then
            let internalTypeMap,_,_ = (!internalTypeCache).[moduleToInvalidate]
            for kv in internalTypeMap do
                timeCachedTy := (!timeCachedTy).Remove(kv.Key)

        // remove module-based caches
        externalTypeCache := (!externalTypeCache).Remove moduleToInvalidate
        internalTypeCache := (!internalTypeCache).Remove moduleToInvalidate
        timeCachedExt := (!timeCachedExt).Remove moduleToInvalidate
        timeCachedInt := (!timeCachedInt).Remove moduleToInvalidate
        astCache := (!astCache).Remove moduleToInvalidate

        //remove globals caches
        globalsDependencies := Set.empty
        userGlobalsTimeStamp := 0L
        userGlobalsCached := Map.empty
    
        SubtypeGraphBuilder.invalidateCachedModule moduleToInvalidate
    elif moduleToInvalidate = "globals" then
        globalsDependencies := Set.empty
        userGlobalsTimeStamp := 0L
        userGlobalsCached := Map.empty
    else
        // if it isn't cached, it must be a library or external module
        ()