﻿/// Provides the Transform function, which allows the user to transform all type annotations
/// in an AST using a transformation function of their choosing

module LuaAnalyzer.TransformTypes

open Type
open TypedSyntax

let refTransformType = ref (fun (transform : Type -> Option<Type>) (ty : Type) -> ty)

let private transformType (transform : Type -> Option<Type>) (ty : Type) =
    (!refTransformType) transform ty

let rec private transformMetamethodSet (transform : Type -> Option<Type>) (metaset : MetamethodSet) =
    let transformBinOp (desc,rhsTy,retTy,loc) =
        (desc, transformType transform rhsTy, transformType transform retTy, loc)
    
    let transformUnOp (desc,retTy,loc) =
        (desc, transformType transform retTy, loc)

    let transformFunc (desc,pars,varpars,rets,varrets,isMethod,isLatent,defLocation) =
        let pars' = List.map (fun (nm,desc,ty) -> (nm,desc,transformType transform ty)) pars
        let rets' = List.map (fun (desc,ty) -> (desc, transformType transform ty)) rets
        (desc,pars',varpars,rets',varrets,isMethod,isLatent,defLocation)

    {
        Add = Option.map transformBinOp metaset.Add
        Sub = Option.map transformBinOp metaset.Sub
        Mul = Option.map transformBinOp metaset.Mul
        Div = Option.map transformBinOp metaset.Div
        Mod = Option.map transformBinOp metaset.Mod
        Pow = Option.map transformBinOp metaset.Pow
        Unm = Option.map transformUnOp metaset.Unm
        Concat = Option.map transformBinOp metaset.Concat
        Len = Option.map transformUnOp metaset.Len
        Lt = Option.map transformBinOp metaset.Lt
        Le = Option.map transformBinOp metaset.Le
        Gt = Option.map transformBinOp metaset.Gt
        Ge = Option.map transformBinOp metaset.Ge
    
        And = Option.map transformBinOp metaset.And
        Or = Option.map transformBinOp metaset.Or
        Not = Option.map transformUnOp metaset.Not

        Index = Option.map transformBinOp metaset.Index

        Call = Option.map transformFunc metaset.Call
    }

refTransformType := fun (transform : Type -> Option<Type>) (ty : Type) ->
    match transform ty with
    | Some(result) ->
        result
    | None ->
        match ty with
        | RecordTy(name,desc,isStructural,displayStructural,openStatus,metamethods,fieldMap,fieldList,methodMap,typeArgs,defloc) ->
            RecordTy(
                name,
                desc,
                isStructural,
                displayStructural,
                openStatus,
                transformMetamethodSet transform metamethods,
                Map.map (fun key field -> {field with ty = transformType transform field.ty}) fieldMap,
                List.map (fun (key,field) -> (key, {field with ty = transformType transform field.ty})) fieldList,
                Map.map (fun key field -> {field with ty = transformType transform field.ty}) methodMap,
                List.map (transformType transform) typeArgs,
                defloc
            )
        | FunctionTy(desc,pars,varPars,rets,varrets,isMethod,latent,loc) ->
            FunctionTy(
                desc, 
                List.map (fun (nm,desc,ty) -> (nm,desc,transformType transform ty)) pars,
                varPars,
                List.map (fun (desc,ty) -> (desc,transformType transform ty)) rets,
                varrets,
                isMethod,
                latent,
                loc
            )
        | TupleTy(types, indefinite) ->
            TupleTy(List.map (transformType transform) types, indefinite)
        | OverloadTy(tyList) ->
            OverloadTy(List.map (transformType transform) tyList)
        | NillableTy(ty) ->
            NillableTy(transformType transform ty)
        | AccumulatedTy(perm,temp) ->
            AccumulatedTy(transformType transform perm, transformType transform temp)
        | TypeAppTy(applicand,args) ->
            TypeAppTy(transformType transform applicand, List.map (transformType transform) args)
        | TypeAbsTy(varNames,body) ->
            // type abstractions cannot appear in annotations for now
            failwith "unreachable" 
        | TypeVarTy(_,_,_,_)
        | UserDefinedTy(_,_)
        | ErrorTy(_)
        | TypeVarTy(_,_,_,_)
        | TypeAbsTy(_,_)
        | NewFieldTy
        | NumberTy
        | StringTy
        | NilTy
        | BoolTy
        | UnknownTy ->
            ty

let private transformAnnotation (transform : Type -> Option<Type>) (ann : Annotations.Annotation) =
    let (desc,tagset,errors,rng) = ann
    let tagset' = 
        match tagset with
        | Annotations.FunctionTagSet(pars,varpars,rets,varrets,isLatent) ->
            let transformPar ((a,b,ty),c) =
                ((a,b,transformType transform ty),c)
            let transformRet (desc,ty) =
                (desc,transformType transform ty)
            let pars' = List.map transformPar pars
            let rets' = List.map transformRet rets
            Annotations.FunctionTagSet(pars',varpars,rets',varrets,isLatent)
        | Annotations.VarConstTagSet(fields) ->
            let transformField (optTy : Option<Type>,a,b) =
                (Option.map (transformType transform) optTy,a,b)
            Annotations.VarConstTagSet(List.map transformField fields)
        | Annotations.ModuleHeaderTagSet(typedefs,typeparams) ->
            Annotations.ModuleHeaderTagSet(
                List.map (fun (nm,ty) -> (nm,transformType transform ty)) typedefs,
                typeparams
            )
        | _ ->
            tagset
    (desc,tagset',errors,rng)

let private refTransformExpr = ref (fun (transform : Type -> Option<Type>) (expr : TypedExpr) -> expr)
 
let private transformExpr (transform : Type -> Option<Type>) (expr : TypedExpr) =
    (!refTransformExpr) transform expr

/// renames all type aliases which appear in the statement, as according to the renamings maps
let rec private transformStat (transform : Type -> Option<Type>) (stat : TypedStatement) =
    match stat with
    | If(clauses,elseExplicit,rng) ->
        let renameClause ((cond,body) : TypedExpr*TypedStatement) = 
            transformExpr transform cond,
            transformStat transform body
            
        let newClauses = List.map renameClause clauses
        If(newClauses,elseExplicit,rng)
    | While(cond,body,rng) ->
        let cond' = transformExpr transform cond
        let body' = transformStat transform body
        While(cond',body',rng)
    | Do(body,rng) ->
        let body' = transformStat transform body
        Do(body',rng)
    | ForNum(var,start,fin,step,body,rng) ->
        let var' = transformExpr transform var
        let start' = transformExpr transform start
        let fin' = transformExpr transform fin
        let step' = transformExpr transform step
        let body' = transformStat transform body
        ForNum(var',start',fin',step',body',rng)
    | ForGeneric(vars,gens,body,rng) ->
        let vars' = List.map (fun (expr) -> transformExpr transform expr) vars
        let gens' = List.map (fun (expr) -> transformExpr transform expr) gens
        let body' = transformStat transform body
        ForGeneric(vars',gens',body',rng)
    | Repeat(cond,body,rng) ->
        let cond' = transformExpr transform cond
        let body' = transformStat transform body
        Repeat(cond',body',rng)
    | LocalAssign(annotation,names,exprs,rng) ->
        LocalAssign(
            Option.map (transformAnnotation transform) annotation,
            List.map (transformExpr transform) names,
            List.map (transformExpr transform) exprs,
            rng
        ) 
    | Assign(annotation,lvals,rvals,rng) ->
        Assign(
            Option.map (transformAnnotation transform) annotation,
            List.map (transformExpr transform) lvals,
            List.map (transformExpr transform) rvals,
            rng
        )
    | Return(rets,rng) ->
        Return(List.map (transformExpr transform) rets, rng)
    | Break(rng) ->
        Break(rng)
    | Call(callExp,rng) ->
        Call(transformExpr transform callExp,rng)
    | Sequence(optAnn,stats,rng) ->
        let stats' = Seq.cast stats
        let stats'' = new GenList<TypedStatement>(Seq.map (transformStat transform) stats')
        Sequence(
            Option.map (transformAnnotation transform) optAnn,
            stats'',
            rng
        ) 
    | ErrorStatement(msg,rng) ->
        ErrorStatement(msg,rng)

let rec private transformConsField (transform : Type -> Option<Type>) (field : TypedConstructorField) =
    match field with
    | ListField(optAnn, valExpr, rng) ->
        ListField(Option.map (transformAnnotation transform) optAnn, transformExpr transform valExpr, rng)
    | RecField(optAnn,keyExpr,valExpr,rng) ->
        RecField(
            Option.map (transformAnnotation transform) optAnn, 
            transformExpr transform keyExpr, 
            transformExpr transform valExpr, 
            rng
        )
    | ErrorField(_,_) ->
        field

refTransformExpr := fun (transform : Type -> Option<Type>) (expr : TypedExpr) ->
    match expr with
    | Constructor(consFields,rng) ->
        Constructor(List.map (transformConsField transform) consFields, rng)
    | Function(optAnn,selfName,latent,formals,varpars,rets,varrets,body,rng) ->
        Function(
            Option.map (transformAnnotation transform) optAnn,
            selfName,
            latent,
            List.map (fun (a,b,ty) -> (a,b,transformType transform ty)) formals,
            varpars,
            List.map (fun (a,ty) -> (a,transformType transform ty)) rets,
            varrets,
            transformStat transform body,
            rng
        )
    | UnOpExpr(unop,expr,rng) ->
        UnOpExpr(unop,transformExpr transform expr, rng)
    | BinOpExpr(binop,exprA,exprB,rng) ->
        BinOpExpr(binop,transformExpr transform exprA,transformExpr transform exprB,rng)
    | ParenthesizedExpr(expr,rng) ->
        ParenthesizedExpr(transformExpr transform expr,rng)
    | Ascription(expr,ty,rng) ->
        Ascription(transformExpr transform expr,transformType transform ty, rng)
    | CallExpr(callTargetExpr,argExprs,rng) ->
        CallExpr(
            transformExpr transform callTargetExpr,
            List.map (transformExpr transform) argExprs,
            rng
        )
    | ErrorExpr(_,_)
    | Number(_,_) 
    | String(_,_)
    | Nil(_)
    | True(_)
    | False(_)
    | VarArgs(_)
    | NameExpr(_,_) ->
        expr  
        
/// Recursively descends through ast, mapping all types encountered through the transform function
let public transform (transform : Type -> Option<Type>) (ast : TypedStatement) =
    transformStat transform ast  