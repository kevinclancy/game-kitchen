﻿module LuaAnalyzer.SubtypeGraphBuilder

open Type
open TypedSyntax
open TypeCollector
open Plugins
open Context

/// The data we need to track during the construction of the
/// subtype graph.
type TypeGraphBuilderState = {

    /// the type environment containing the graph we are building
    tenv : TypeEnvironment
    
    /// all type names collected so far as keys
    allTypeNames : Set<string>


    /// maps each module name to the time at which that module 
    /// had its subtype graph data incorporated into this record
    timeStamps : Map<string,int64>

    /// maps each module name to a list of the names of internal
    /// types defined by that module
    internalTypes : Map<string,List<string>>

    /// maps each module name to the list of errors that module last produced
    errors : Map<string,List<Error>>

    //TODO: perhaps this should be included in type environment
    //except that would have to map module names to type collector names
    /// maps module names to the collectors which collected them
    collectedFrom : Map<string,TypeCollector>
}

type TopSortColor =
    /// gray nodes are currently being processed
    | GRAY
    /// black nodes have already been processed
    | BLACK

/// BrokenEdgeException(child,parent,fileName,rng) 
/// Thrown when a class inherits from a class which was not collected
exception BrokenEdgeException of string*string*string*Range

exception TypeGraphCycleException of List<string>*string*Range

/// TypeGraphDiamondException(source, dest, pathA, pathB,fileName,rng)
///
/// thrown when the type named source can reach the type named dest
/// via two different paths: pathA and pathB
exception TypeGraphDiamondException of string*string*List<string>*List<string>*string*Range

/// CollectionConflict(collectorName1,collectorName2,moduleName,fileName,range)
///
/// thrown when two different collectors both successfully collect
/// types from a module.
exception CollectionConflict of string*string*string*string*Range

/// Used for errors in the implementations of type collectors.
exception CollectorError of string

let baseTypeGraphCache = {
    tenv = getBaseContext().tenv
    allTypeNames = getBaseContext().tenv.instanceTypes
    timeStamps = Map.empty
    internalTypes = Map.empty
    errors = Map.empty
    collectedFrom = Map.empty 
}

let fullTypeGraphCache = ref baseTypeGraphCache

/// If any edge leads to an edge which is not an instance type (or doesn't exist),
/// raise an exception
let checkBrokenEdges (tenv : TypeEnvironment) =
    for kv in tenv.edges do
        let srcName = kv.Key
        let edges = kv.Value         

        for edge in edges do
            if not ( tenv.instanceTypes.Contains edge ) then
                let file,_ = tenv.typeFiles.Item srcName
                let rng = tenv.typeDeclarationRanges.Item srcName
                raise( BrokenEdgeException(srcName,edge,file,rng) )

let undoPrevEffect (acc : TypeGraphBuilderState) 
                   (moduleName : string) 
                   : TypeGraphBuilderState =
        
        if acc.allTypeNames.Contains moduleName then
            let internalTypeNames = 
                if acc.internalTypes.ContainsKey moduleName then
                    acc.internalTypes.[moduleName]
                else
                    []

            let tenv = acc.tenv

            let tenv =
                {
                tenv with
                    instanceTypes = tenv.instanceTypes.Remove(moduleName)
                    typeFiles = tenv.typeFiles.Remove(moduleName)
                    edges = tenv.edges.Remove(moduleName)
                    typeDeclarationRanges = tenv.typeDeclarationRanges.Remove(moduleName)
                }
            
            let acc =
                {
                acc with
                    tenv = tenv
                    collectedFrom = acc.collectedFrom.Remove(moduleName)
                    allTypeNames = acc.allTypeNames.Remove(moduleName)
                    timeStamps = acc.timeStamps.Remove(moduleName)
                    internalTypes = acc.internalTypes.Remove(moduleName)
                    errors = acc.errors.Remove(moduleName)
                }

            let foldInternalTypeName (acc : TypeGraphBuilderState) (typeName : string) =
                let tenv =
                    {
                    acc.tenv with
                        typeFiles = acc.tenv.typeFiles.Remove(typeName)
                        typeDeclarationRanges = acc.tenv.typeDeclarationRanges.Remove(typeName)
                        edges = acc.tenv.edges.Remove(typeName)              
                    }

                {
                acc with
                    tenv = tenv
                    allTypeNames = acc.allTypeNames.Remove(typeName)
                    collectedFrom = acc.collectedFrom.Remove(typeName)
                }

            List.fold foldInternalTypeName acc internalTypeNames
        else
            acc


/// If the supplied module is collected by collector, accumulate its type graph data into acc
let foldModule (acc : TypeGraphBuilderState) ((fileName,moduleName,ast,lastMod) : string*string*TypedStatement*int64) =

    let tenv = acc.tenv
    let collectedFrom = acc.collectedFrom

    let getCollectionResult () =
        let mapCollector = (fun x -> x.typeGraphBuilder moduleName fileName ast)
        let collectorResults = List.map mapCollector (!typeCollectors)
                
        let isSignificant (result : Option<'T>, errors : List<Error>) =
            result.IsSome || errors.Length > 0

        //a list of all (result,collector) pairs
        let pairs = List.zip collectorResults (!typeCollectors)
        //the above list, filtered to only contain significant results 
        let significants = List.filter (fst >> isSignificant) pairs
                
        if significants.Length > 1 then
            let s0 = snd significants.[0]
            let s1 = snd significants.[1]

            let file = tenv.typeFiles.Item moduleName
            let rng = tenv.typeDeclarationRanges.Item moduleName

            raise(
                CollectionConflict(
                    s0.name,
                    s1.name,
                    moduleName,
                    fileName,
                    rng
                )
            )
        elif significants.Length = 1 then
            match (fst significants.[0]) with
            | Some(extRng,internalTypeNames,edges),errors ->
                let collector = snd significants.[0]
                Some(extRng,internalTypeNames,edges,collector),errors
            | None,errors ->
                None,errors 
        else
            None,[]        

    let newResult,errors =
        if acc.timeStamps.ContainsKey moduleName then
            let timeStamp, errors = 
                acc.timeStamps.[moduleName], 
                acc.errors.[moduleName]

            if lastMod > timeStamp then
                let mapCollector = (fun x -> x.typeGraphBuilder moduleName fileName ast)
                let collectorResults = List.map mapCollector (!typeCollectors)
                
                //a list of all (result,collector) pairs
                let pairs = List.zip collectorResults (!typeCollectors)
                //the above list, filtered to only contain positive results 
                let positives = List.filter (fst >> fst >> Option.isSome) pairs
                
                let newRes,newErrors = getCollectionResult()

                Some(newRes),newErrors
            else
                None,errors
        else
            let newRes,newErrors = getCollectionResult()
            Some(newRes),newErrors

    ErrorList.addErrors errors

    match newResult with
    | Some (result) ->
        match result with
        | Some(extRng,internalTypeNames,edges,collector) ->
            let acc = undoPrevEffect acc moduleName
            let tenv = acc.tenv

            let tenv =
                {
                tenv with
                    instanceTypes = tenv.instanceTypes.Add(moduleName)
                    typeFiles = tenv.typeFiles.Add(moduleName,(fileName,moduleName))
                    edges = tenv.edges.Add(moduleName,[])
                    typeDeclarationRanges = tenv.typeDeclarationRanges.Add(moduleName,extRng)
                }
            
            let acc =
                {
                acc with
                    tenv = tenv
                    collectedFrom = acc.collectedFrom.Add(moduleName,collector)
                    allTypeNames = acc.allTypeNames.Add(moduleName)
                    internalTypes = acc.internalTypes.Add(moduleName,List.map fst internalTypeNames)
                    errors = acc.errors.Add(moduleName,errors)
                    timeStamps = acc.timeStamps.Add(moduleName,lastMod)
                }

            let foldInternalTypeName (acc : TypeGraphBuilderState) (typeName,rng) =
                let tenv =
                    {
                    acc.tenv with
                        typeFiles = acc.tenv.typeFiles.Add(typeName,(fileName,moduleName))
                        typeDeclarationRanges = acc.tenv.typeDeclarationRanges.Add(typeName,rng)
                        edges = acc.tenv.edges.Add(typeName,[])
                    }

                {
                acc with
                    tenv = tenv
                    allTypeNames = acc.allTypeNames.Add(typeName)
                    collectedFrom = acc.collectedFrom.Add(typeName,collector)
                }


            let acc = List.fold foldInternalTypeName acc internalTypeNames

            let foldEdge (acc : TypeGraphBuilderState) ((t0,t1) : string*string) =
                let tenv =
                    {
                    acc.tenv with
                        edges = acc.tenv.edges.Add(t0,t1 :: (acc.tenv.edges.Item t0))
                    }

                { acc with tenv = tenv }

            let acc = List.fold foldEdge acc edges

            acc
        | None ->
            {
            undoPrevEffect acc moduleName with
                timeStamps = acc.timeStamps.Add(moduleName, lastMod)
                errors = acc.errors.Add(moduleName,errors)
            }
    | None ->
        acc

/// If the root's type collector requires a tree ancestor graph, we raise an 
/// exception if there are cycles or diamonds in its the ancestor graph.
///
/// Otherwise, we raise an exception if there are cycles in its ancestor graph.
let checkStructureErrors (tenv : TypeEnvironment) (collectedFrom : Map<string,TypeCollector>) (root : string) : unit =
    
    let parent : Ref< Map<string,string> > = ref Map.empty
    let color : Ref< Map<string,TopSortColor> > = ref Map.empty

    let typeGraphIsTree = 
        if collectedFrom.ContainsKey root then
            (collectedFrom.Item root).typeGraphIsTree
        else 
            false

    let rec traceParents (src : string) (dst : string) =
        if src = dst then
            [src]
        else
            src :: (traceParents ((!parent).Item src) dst) 

    let rec checkStructureErrorsAux (node : string) =
        color := (!color).Add(node, TopSortColor.GRAY)

        let children = 
            match tenv.edges.ContainsKey node with
            | true ->
                tenv.edges.Item node
            | false ->
                []

        for adj in children do
            match (!color).TryFind adj with
            | Some(TopSortColor.GRAY) ->
                let fileName,moduleName = tenv.typeFiles.Item adj
                let rng = tenv.typeDeclarationRanges.Item adj
                raise( TypeGraphCycleException(adj :: (traceParents node adj),fileName,rng) )
            | Some(TopSortColor.BLACK) ->
                if typeGraphIsTree then
                    let fileName,moduleName = tenv.typeFiles.Item adj
                    let rng = tenv.typeDeclarationRanges.Item adj
                    raise(
                        TypeGraphDiamondException(
                            root, 
                            adj, 
                            (traceParents adj root),
                            adj :: (traceParents node root),
                            fileName,
                            rng
                        )
                    )
                else
                    color := (!color).Add(adj, TopSortColor.GRAY)
                    parent := (!parent).Add(adj,node)
                    checkStructureErrorsAux adj      
            | None ->
                parent := (!parent).Add(adj,node)
                checkStructureErrorsAux adj

        color := (!color).Add(node,TopSortColor.BLACK) 

    checkStructureErrorsAux root

/// Raise an exception if there are any broken edges or cycles in the subtype graph.
/// If some type collector requires ancestor graphs to be trees, we raise an exception
/// if diamonds are found in the subtype graph.
let checkGraphErrors (state : TypeGraphBuilderState) =
    checkBrokenEdges state.tenv

    // check for errors in subtype graph
    for typeName in state.allTypeNames do
        checkStructureErrors state.tenv state.collectedFrom typeName

/// modules - a sequence of (filename,modulename,ast,timestamp) quadruples
/// for all modules in the project
///
/// Starting with the subtypegraph from the previous run, accumulates a subtype graph computed
/// from the lua project consisting of those modules in the modules sequence.
/// Returns the resulting type environment, along with a map which records which
/// modules were collected by which type collectors.
let buildSubtypeGraph (modules : seq<string*string*TypedStatement*int64>) 
                      : TypeEnvironment*Map<string,TypeCollector> =
    
    let result = Seq.fold foldModule (!fullTypeGraphCache) modules
    fullTypeGraphCache := result
    checkGraphErrors result
    result.tenv,result.collectedFrom

/// Clears caches upon project switch
let clearCaches () =
    fullTypeGraphCache := baseTypeGraphCache

let invalidateCachedModule (moduleToInvalidate : string) =
    fullTypeGraphCache := undoPrevEffect (!fullTypeGraphCache) moduleToInvalidate