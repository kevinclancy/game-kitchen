﻿module LuaAnalyzer.Plugins

type ITypeCollectorPlugin =
    /// Activates the type collector
    abstract member Init : unit -> unit

    /// Returns a sequence of all relative paths of lua library
    /// files to copy from the plugin directory to the project 
    /// directory
    abstract member GetLibPaths : unit -> seq<string>

    /// Returns the name of the type collector
    abstract member GetName : unit -> string

let plugins : Ref< List<ITypeCollectorPlugin> > = ref []

let private appDataDir = System.Environment.SpecialFolder.ApplicationData

/// The directory which the plugin dlls are contained in, under appdata\GameKitchen
let pluginsDir = System.Environment.GetFolderPath(appDataDir) + "\\GameKitchen"