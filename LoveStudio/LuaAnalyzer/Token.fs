﻿module LuaAnalyzer.Token

type Pos = int
type Annotation = Option<string>
type ErrorExplanation = string

type Token =
    | NAME = 0
    | NUMBER = 1
    | EOF = 2
    | AND = 3
    | BREAK = 4
    | DO = 5
    | ELSEIF = 6
    | ELSE = 7
    | END = 8
    | FALSE = 9
    | FOR = 10
    | FUNCTION = 11
    | IF = 12
    | IN = 13
    | LOCAL = 14
    | NIL = 15
    | NOT = 16
    | OR = 17
    | REPEAT = 18
    | RETURN = 19
    | THEN = 20
    | TRUE = 21
    | UNTIL = 22
    | WHILE = 23
    | VARARGS = 24
    | CONCAT = 25
    | DOT = 26
    | EQUAL = 27
    | GREATER_OR_EQUAL = 28
    | LESS_OR_EQUAL = 29
    | NOT_EQUAL = 30
    | GREATER = 31
    | LESS = 32 
    | LENGTH = 33
    | ASSIGN = 34
    | PLUS = 35
    | MINUS = 36
    | MULT = 37
    | DIV = 38
    | POW = 39
    | MOD = 40
    | COMMA = 41
    | OPEN_PAREN = 42
    | CLOSE_PAREN = 43
    | OPEN_BRACKET = 44
    | CLOSE_BRACKET = 45
    | OPEN_SQUARE_BRACKET = 46
    | CLOSE_SQUARE_BRACKET = 47
    | METHOD_IND = 48
    | SEMI_COLON = 49
    | GOTO = 50
    | STRING = 51
    | BAD_TOKEN = 52
    | NUM_TOKENS = 53

type TokenInfo = Token*Option<string>*int

let getTokenRange (inf : TokenInfo) =
    match inf with
    | (tok, str, pos) ->
        match tok with
        | Token.NAME ->
            let s = Option.get str
            (pos, pos+s.Length)
        | Token.NUMBER ->
            let s = Option.get str
            (pos, pos+s.Length)
        | Token.EOF ->
            (pos, pos)
        | Token.AND ->
            (pos, pos+3)
        | Token.BREAK ->
            (pos, pos+5)
        | Token.DO ->
            (pos, pos+2) 
        | Token.ELSEIF ->
            (pos, pos+6)
        | Token.ELSE ->
            (pos, pos+4)
        | Token.END ->
            (pos, pos+3)
        | Token.FALSE ->
            (pos, pos+5)
        | Token.FOR ->
            (pos, pos+3)
        | Token.FUNCTION ->
            (pos, pos+8)
        | Token.IF ->
            (pos, pos+2)
        | Token.IN ->
            (pos, pos+2)
        | Token.LOCAL ->
            (pos, pos+5)
        | Token.NIL ->
            (pos, pos+3)
        | Token.NOT ->
            (pos, pos+3)
        | Token.OR ->
            (pos, pos+2)
        | Token.REPEAT ->
            (pos, pos+3)
        | Token.RETURN ->
            (pos, pos+6)
        | Token.THEN ->
            (pos, pos+4)
        | Token.TRUE ->
            (pos, pos+4)
        | Token.UNTIL ->
            (pos, pos+5)
        | Token.WHILE ->
            (pos, pos+5)
        | Token.VARARGS ->
            (pos, pos+3)
        | Token.CONCAT ->
            (pos, pos+2)
        | Token.DOT ->
            (pos, pos+1)
        | Token.EQUAL ->
            (pos, pos+2)
        | Token.GREATER_OR_EQUAL ->
            (pos, pos+2)
        | Token.LESS_OR_EQUAL ->
            (pos, pos+2)
        | Token.NOT_EQUAL ->
            (pos, pos+2)
        | Token.GREATER ->
            (pos, pos+1)
        | Token.LESS ->
            (pos, pos+1)
        | Token.LENGTH ->
            (pos, pos+1)
        | Token.ASSIGN ->
            (pos, pos+1)
        | Token.PLUS ->
            (pos, pos+1)
        | Token.MINUS ->
            (pos, pos+1)
        | Token.MULT ->
            (pos, pos+1)
        | Token.DIV ->
            (pos, pos+1)
        | Token.POW ->
            (pos, pos+1)
        | Token.MOD ->
            (pos, pos+1)
        | Token.COMMA ->
            (pos,pos+1)
        | Token.OPEN_PAREN ->
            (pos, pos+1)
        | Token.CLOSE_PAREN ->
            (pos, pos+1)
        | Token.OPEN_BRACKET ->
            (pos, pos+1)
        | Token.CLOSE_BRACKET ->
            (pos, pos+1)
        | Token.OPEN_SQUARE_BRACKET ->
            (pos, pos+1)
        | Token.CLOSE_SQUARE_BRACKET ->
            (pos, pos+1)
        | Token.METHOD_IND ->
            (pos, pos+1)
        | Token.SEMI_COLON ->
            (pos, pos+1)
        | Token.GOTO ->
            (pos, pos+4)
        | Token.STRING ->
            let s = Option.get str
            (pos, pos+s.Length)
        | Token.BAD_TOKEN ->
            (pos, pos+1)
        | _ ->
            failwith "unreachable"

/// Given a token, returns Some([closerTokens]) if the token is a syntactic opener which
/// is closed by the sequence of tokens closerTokens. 
///
/// If the token is not an syntactic opener, returns None
let getCloser (tok : Token) =
    match tok with
    | Token.OPEN_PAREN -> Some(Token.CLOSE_PAREN)
    | Token.OPEN_BRACKET -> Some(Token.CLOSE_BRACKET)
    | Token.OPEN_SQUARE_BRACKET -> Some(Token.CLOSE_SQUARE_BRACKET)
    | Token.FUNCTION
    | Token.DO 
    | Token.ELSE
    | Token.THEN ->
        Some(Token.END)
    | Token.IF
    | Token.ELSEIF ->
        Some(Token.THEN)
    | Token.REPEAT ->
        Some(Token.UNTIL)
    | _ ->
        None