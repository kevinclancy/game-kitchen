﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace LoveEdit
{
  public interface IModuleCreator
  {
    string GetName();
    Control GetControl();

    /// <summary>
    /// Instead of retrieving the args from the control, we retrieve them
    /// from another source (e.g. via a TCP socket from the level editor).
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    bool CreateNewModuleFromArgs(string[] args);

    /// <summary>
    /// If the module definition fields are correct, creates a new file for the 
    /// module containing a basic template and returns true. Otherwise, returns false.
    /// </summary>
    /// <returns>True iff module creation was successful.</returns>
    bool CreateNewModuleFromControl();
  }

  public class ModuleConstructors
  {
    // The plan:
    // TODO:
    //
    // Create an add-in point for module constructor controls
    // Note that we are keeping this separate from LuaAnalyzer
    // because LuaAnalyzer is purely a type checker, and knows
    // nothing about the love studio IDE.

    Dictionary<string,IModuleCreator> _moduleConstructors;
    IModuleCreator _currentModuleConstructor;

    [ImportMany]
    IEnumerable<IModuleCreator> _allModuleCreators = null;

    private void InitCatalog()
    {
      var catalog = new AggregateCatalog();
      var appDataDir = System.Environment.SpecialFolder.ApplicationData;
      var gameKitchenPath = System.Environment.GetFolderPath(appDataDir) + "\\GameKitchen";
      
      //love studio's directory under AppData for plugins
      catalog.Catalogs.Add(new DirectoryCatalog(gameKitchenPath));
      
      //Create the CompositionContainer with the parts in the catalog
      CompositionContainer container = new CompositionContainer(catalog);

      //Fill the imports of this object
      try
      {
        container.ComposeParts(this);
      }
      catch (CompositionException compositionException)
      {
        Console.WriteLine(compositionException.ToString());
      }
    }

    public void Init()
    {
      InitCatalog();

      _moduleConstructors = new Dictionary<string, IModuleCreator>();
      foreach (IModuleCreator creator in _allModuleCreators)
      {
        if (!_moduleConstructors.ContainsKey(creator.GetName()))
          _moduleConstructors.Add(creator.GetName(), creator);
      }

      _currentModuleConstructor = _moduleConstructors.First().Value;
    }

    public IModuleCreator GetCurrentModuleCreator()
    {
      return _currentModuleConstructor;
    }

    public IEnumerable<IModuleCreator> GetAllModuleCreators()
    {
      return _allModuleCreators;
    }

    public IModuleCreator GetModuleCreatorByName(string name)
    {
      return _moduleConstructors[name];
    }

    public bool HasModuleCreator(string name)
    {
      return _moduleConstructors.ContainsKey(name);
    }
  }
}
