local Class = require "class.lua"

local CharSet = Class{

interface = {

Load = {},

-- Returns the number of characters in the char set
--yoyoyo
GetCharCount = {},

--GetChar(ind) returns the char at index ind
GetChar = {
	pre = function(obj,ind)
		assert(0 < ind and ind <= obj:GetCharCount())
  end,
  
  post = function(obj,ret,ind)
		assert(ret:typeOf("Image"))
  end
},

GetCharDims = {}

}

}

function CharSet:constructor(charWidth,charHeight)
	self.charWidth = charWidth
  self.charHeight = charHeight
end

function CharSet:Load()
  self.chars = {
		love.graphics.newImage("c00.png"),
    love.graphics.newImage("c01.png"),
    love.graphics.newImage("c02.png"),
    love.graphics.newImage("c03.png"),
    love.graphics.newImage("c04.png"),
    love.graphics.newImage("c05.png"),
    love.graphics.newImage("c06.png"),
    love.graphics.newImage("c07.png"),
    love.graphics.newImage("c08.png")
  }
end

function CharSet:GetChar(ind)
  return self.chars[ind]
end

function CharSet:GetCharCount()
	return #self.chars
end

function CharSet:GetCharDims()
  return self.charWidth, self.charHeight
end

return CharSet