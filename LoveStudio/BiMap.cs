﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoveEdit
{
  /// <summary>
  /// Only appropriate for when A and B are unique.
  /// </summary>
  /// <typeparam name="A"></typeparam>
  /// <typeparam name="B"></typeparam>
  public class BiMap<A,B>
  {
    /// <summary>
    /// For each entry, associates the A component to the B component.
    /// </summary>
    private readonly Dictionary<A, B> _aToB;

    /// <summary>
    /// For each entry, associates the B component to the A component.
    /// </summary>
    private readonly Dictionary<B, A> _bToA;

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  // excluded invariant -- _aToB and _bToA are inverses
    //  Contract.Invariant(_aToB != null);
    //  Contract.Invariant(_bToA != null);
    //  Contract.Invariant(_aToB.Count == _bToA.Count);
    //}

    /// <summary>
    /// Creates a new BiMap.
    /// </summary>
    public BiMap()
    {
      _aToB = new Dictionary<A, B>();
      _bToA = new Dictionary<B, A>();
    }

    /// <summary>
    /// Returns true iff the BiMap contains an entry whose A component is a.
    /// </summary>
    /// <param name="a"></param>
    /// <returns></returns>
    // [Pure]
    public bool Contains(A a)
    {
      return _aToB.ContainsKey(a);
    }

    /// <summary>
    /// Returns true iff the BiMap contains an entry whose B component is B.
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    // [Pure]
    public bool Contains(B b)
    {
      return _bToA.ContainsKey(b);
    }

    /// <summary>
    /// The number of entries in the map.
    /// </summary>
    // [Pure]
    public int Count
    {
      get
      {
        return _aToB.Count;
      }
    }

    /// <summary>
    /// Returns a collection of all A components of the entries of the map.
    /// </summary>
    /// <returns></returns>
    // [Pure]
    public ICollection<A> EnumerateA()
    {
      return _aToB.Keys;
    }

    /// <summary>
    /// Returns a collection of all B components of the entries of the map.
    /// </summary>
    /// <returns></returns>
    // [Pure]
    public ICollection<B> EnumerateB()
    {
      return _bToA.Keys;
    }

    /// <summary>
    /// Remove the entry of the map that has an A component equal to a.
    /// </summary>
    /// <param name="a"></param>
    public void Remove(A a)
    {
      //Contract.Requires(Contains(a));
      //Contract.Ensures(!Contains(a));

      B b = _aToB[a];
      _aToB.Remove(a);
      _bToA.Remove(b);
    }

    /// <summary>
    /// Remove the entry of the map that has a B component equal to b.
    /// </summary>
    /// <param name="b"></param>
    public void Remove(B b)
    {
      //Contract.Requires(Contains(b));
      //Contract.Ensures(!Contains(b));

      A a = _bToA[b];
      _bToA.Remove(b);
      _aToB.Remove(a);
    }

    /// <summary>
    /// Removes all entries of the map.
    /// </summary>
    public void Clear()
    {
      //Contract.Ensures(EnumerateA().Count() == 0);
      //Contract.Ensures(EnumerateB().Count() == 0);

      _aToB.Clear();
      _bToA.Clear();
    }

    /// <summary>
    /// The A component of the entry of the map with a B component equal to b.
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public A this[B b]
    {
      get
      {
        // Contract.Ensures(Contract.Result<A>() != null);
        return _bToA[b];
      }

      set
      {
        // Contract.Requires(b != null);
        // Contract.Requires(!Contains(b));
        // Contract.Requires(!Contains(value));
        _bToA[b] = value;
        _aToB[value] = b;
      }
    }

    /// <summary>
    /// The B component of the entry of the map with an A component equal to a.
    /// </summary>
    /// <param name="a"></param>
    /// <returns></returns>
    public B this[A a]
    {
      get
      {
        // Contract.Ensures(Contract.Result<B>() != null);
        return _aToB[a];
      }

      set
      {
        // Contract.Requires(a != null);
        // Contract.Requires(!Contains(a));
        _aToB[a] = value;
        _bToA[value] = a;
      }
    }
  }
}
