﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace LoveEdit
{
  /// <summary>
  /// State object for receiving target data asynchronously
  /// </summary>
  public class StateObject
  {
    /// Size of receive buffer.
    public const int BufferSize = 65536;
    /// Receive buffer.
    public byte[] buffer = new byte[BufferSize];
  }

  /// <summary>
  /// Represents a stack frame from the target's lua state.
  /// </summary>
  public class LuaStackFrame
  {
    /// <summary>
    /// The name of the active function.
    /// </summary>
    public string funcName;
    /// <summary>
    /// The name of the file containing the active function.
    /// </summary>
    public string fileName;
    /// <summary>
    /// The line number being executed in this frame.
    /// </summary>
    public int lineNumber;
    /// <summary>
    /// The stack level of this frame, where 0 is the current
    /// executing function and level n+1 is the active function
    /// which called level n.
    /// </summary>
    public int stackLevel;
  }

  /// <summary>
  /// An instance of the network object pattern, which love studio uses as the
  /// interface to the app's debugger module. 
  /// 
  /// The methods of the DebugProxy allow the various components of love studio 
  /// to add and remove breakpoints, probe the state of the target-side lua VM
  /// while it is breaking, and detect and resume from target-side breaks.
  /// </summary>
  public class DebugProxy
  {
    #region Constants

    /// <summary>
    /// The number of the port used to send data from the app process to the 
    /// debugger process.
    /// </summary>
    private const int DEBUGGER_PORT = 57778;
    /// <summary>
    /// The number of the port used to send data from the debugger process to
    /// the app process.
    /// </summary>
    private const int APP_PORT = 57777;

    /// <summary>
    /// The port that love studio uses to receive data from the target.
    /// </summary>
    private EndPoint localEP = new IPEndPoint(
      new IPAddress(new byte[] { 127, 0, 0, 1 }),
      DEBUGGER_PORT
    );

    /// <summary>
    /// The port that love studio uses to send data to the target.
    /// </summary>
    private EndPoint remoteEP = new IPEndPoint(
      new IPAddress(new byte[] { 127, 0, 0, 1 }),
      APP_PORT
    );

    enum State
    {
      BREAK_SETUP,
      BREAKING,
      STEPPING_OVER,
      STEPPING_IN,
      RUNNING,
      PERFORMING_SYNCHRONIZED_ACTION,
      DISCONNECTED
    }

    /// <summary>
    /// The message header used to describe the event in which a breakpoint
    /// has been added via love studio.
    /// </summary>
    private const byte BREAKPOINT_ADDED = 0;
    /// <summary>
    /// The message header used to describe the event in which a breakpoint
    /// has been removed via love studio.
    /// </summary>
    private const byte BREAKPOINT_REMOVED = 1;
    /// <summary>
    /// The message used to describe the event in which the lua VM is about
    /// to execute a line which has been marked as a breakpoint.
    /// </summary>
    private const byte BREAKPOINT_HIT = 2;
    /// <summary>
    /// The message header used to describe the event in which the user has signaled
    /// the target to resume.
    /// </summary>
    private const byte RESUME = 3;
    /// <summary>
    /// The message header used to describe the event in which the target-side debugger
    /// provides information about a lua value that was requested from the studio.
    /// </summary>
    private const byte CHILDREN_RESPONSE = 4;
    /// <summary>
    /// The message header used to describe the event in which love studio requests 
    /// information about a lua value existing in the target.
    /// </summary>
    private const byte REQUEST_CHILDREN = 5;
    /// <summary>
    /// The message header used to describe the event in which the user has signaled
    /// the target to execute the current line before breaking upon hitting the next
    /// line.
    /// 
    /// If the current line that the debugger is breaking at is the last line of
    /// a function, we advance the debugger to the next line to be executed as
    /// part of any active function call in the stack.
    /// </summary>
    private const byte STEP_OVER = 6;

    /// <summary>
    /// Signals the debugger to break on the next line executed -- wherever it 
    /// may be.
    /// </summary>
    private const byte STEP_IN = 7;

    /// <summary>
    /// Tells the debugger that all debug information that it requires has
    /// been sent and that it may begin executing.
    /// </summary>
    private const byte INIT_FINISH = 8;

    /// <summary>
    /// Love Studio has signaled the target to shut down. 
    /// </summary>
    private const byte TERMINATE = 9;

    /// <summary>
    /// A debug message has been received from the target.
    /// </summary>
    private const byte DEBUG_PRINT = 10;

    private const byte ENTER_LEVEL_EDIT_MODE = 11;

    private const byte ENTER_LEVEL_PLAY_MODE = 12;

    /// <summary>
    /// A request from the running game for the dimensions of the game window
    /// </summary>
    private const byte REQUEST_EMBEDDED_SCREEN_SIZE = 13;

    private const byte CREATE_NEW_MODULE = 14;

    private const byte OPEN_MODULE = 15;

    private const byte SYNC = 16;

    #endregion

    #region Dependencies
    private readonly Studio _studio;
    private readonly BreakpointManager _breakpointManager;
    #endregion

    Socket listener;
    Socket appSocket;
    Socket debuggerSocket;

    delegate void DebugEvent();

    /// <summary>
    /// When the target breaks (whether due to a breakpoint, runtime error, or some other cause),
    /// Delegates of this type are called by the OnBreak event.
    /// </summary>
    /// <param name="stack">An array of every frame in the callstack.</param>
    /// <param name="stepOver">Whether or not this break resulted from stepping over.</param>
    /// <param name="errorMessage">If this break is due to an error, this message describes the error. Otherwise, it's null.</param>
    public delegate void BreakEvent(LuaStackFrame[] stack, bool stepOver, string errorMessage);
    /// <summary>
    /// Triggered whenever the debugger breaks.
    /// </summary>
    public event BreakEvent OnBreak;

    /// <summary>
    /// When the target prints a debug message (by calling "print"), it
    /// sends the message to the DebugProxy, which propogates it via this event.
    /// </summary>
    /// <param name="msg">The message received</param>
    public delegate void DebugMsgReceivedEvent(string msg);
    /// <summary>
    /// Triggered when a debug message has been received.
    /// </summary>
    public event DebugMsgReceivedEvent OnDebugMsgReceived;

    public delegate void LevelAction(string levelName);
    public delegate void ModuleAction(string moduleName);
    public delegate void NewModuleAction(string pluginName, string[] args);

    public event LevelAction OnEnterLevelEditMode;
    public event LevelAction OnEnterLevelPlayMode;

    public event Action OnRequestEmbeddedScreenSize;
    public event NewModuleAction OnCreateNewModule;
    public event ModuleAction OnOpenModule;

    /// <summary>
    /// A description of the entries of a lua value, produced by probing the
    /// target's lua state and intended for consumption by a DebugBrowser instance.
    /// </summary>
    public struct ChildrenInfo
    {
      /// <summary>
      /// An identifier for the DebugBrowser instance that this ChildrenInfo 
      /// is addressed to.
      /// </summary>
      public byte browser;
      /// <summary>
      /// A string identifiying the tree position of the debug browser item whose 
      /// children this struct describes.
      /// 
      /// The string is formatted using a sequence of keys separated by the '.'
      /// character. For example, "Locals.self.position" would represent an item
      /// called "position" whose parent is "self" and whose grandparent is "Locals".
      /// </summary>
      public string parentPath;

      /// <summary>
      /// Describes an entry of a lua value.
      /// </summary>
      public struct ItemDesc
      {
        /// <summary>
        /// The entry's key, converted to a string using the tostring
        /// function from the target's lua state.
        /// </summary>
        public string keyName;
        /// <summary>
        /// The name of the entry's type, retrieved in the target's lua state by 
        /// calling the "type" function on this entry's value.
        /// </summary>
        public string typeName;
        /// <summary>
        /// The entry's value, converted to a string using the tostring function
        /// from the target's lua state.
        /// </summary>
        public string valueString;
        /// <summary>
        /// If the entry's value is a table, this is true, which signals
        /// that the ptr field contains meaningful data. Otherwise, it is false.
        /// </summary>
        public bool hasPtr;
        /// <summary>
        /// If this entry's value is a table, this contains the target
        /// process address of the value. Otherwise, it is undefined.
        /// </summary>
        public long ptr;
      }

      /// <summary>
      /// The entries of the lua value.
      /// </summary>
      public ItemDesc[] items;
    }

    /// <summary>
    /// Delegates of this type subscribe to events corresponding to the different
    /// ways in which the debugger resumes executing (OnResume, OnStepOver, etc.) 
    /// </summary>
    public delegate void ResumeDelegate();

    #region Events
    /// <summary>
    /// Called when the user commands the debugger to resume.
    /// </summary>
    public event ResumeDelegate OnResume;
    /// <summary>
    /// Called when the user commands the debugger to step over the current line.
    /// </summary>
    public event ResumeDelegate OnStepOver;
    /// <summary>
    /// Called when the use tells the debugger to step into the current line.
    /// </summary>
    public event ResumeDelegate OnStepIn;
    /// <summary>
    /// Called when the debugger disconnects from the target due to the fact
    /// that the target has terminated.
    /// </summary>
    public event ResumeDelegate OnDisconnect;
    #endregion

    #region Data Members
    /// <summary>
    /// When debugging, the name of the file which contains the function 
    /// corresponding to the stack frame at the top of the stack (i.e. the leaf). 
    /// When not debugging, this is null.
    /// </summary>
    private string _currentFile;
    /// <summary>
    /// When breaking, contains the line number of the top (leaf) stack frame.
    /// Otherwise, -1.
    /// </summary>
    private int _currentLineNum;
    /// <summary>
    /// The state that the debugger is currently in.
    /// </summary>
    State _state;

    /// <summary>
    /// True if we are in level edit mode.
    /// False if we are in level play mode.
    /// When disconnected, this data member is meaningless.
    /// </summary>
    bool _inLevelEditMode;

    /// <summary>
    /// The level that is currently being edited.
    /// </summary>
    string _currentLevel;

    /// <summary>
    /// Asynchronous data transfers from the target are handled using 
    /// multithreading. But we want to minimize the total amount of 
    /// multithreading used in this application, so delegates for repsonses to 
    /// asynchronously received messages are stored in this queue, and are
    /// dequeued and called by the main thread at a timed interval.
    /// </summary>
    private Queue<DebugEvent> _eventQueue;
    /// <summary>
    /// Stores data received asynchronously from debuggerSocket.
    /// </summary>
    private StateObject _asyncState;
    #endregion

    #region Properties

    /// <summary>
    /// This is true iff the debugger is in its breaking state.
    /// 
    /// During the breaking state, the debug browsers are activated
    /// and their items can be expanded. This allows the user to explore the
    /// target's lua state. All communication with the target during this time 
    /// is done synchronously; any requests to the target are expected to be
    /// responded to within a short amount of time.
    /// 
    /// When Breaking is false, the target is either executing or disconnected. 
    /// 
    /// When the target is executing, the debugger then expects the next message 
    /// received from the target to convey a "break" event, for when a breakpoint 
    /// is hit or an error occurs. We do not know when this event will take 
    /// place, so the reception is done asynchronously.
    /// </summary>
    public bool Breaking
    {
      get
      {
        return _state == State.BREAKING;
      }
    }

    public bool Running
    {
      get
      {
        return _state == State.RUNNING || _state == State.STEPPING_OVER || _state == State.STEPPING_IN;
      }
    }

    public bool InLevelEditMode
    {
      // [Pure]
      get
      {
        // Contract.Requires(Connected);
        return _inLevelEditMode;
      }
    }

    public string CurrentLevel
    {
      get
      {
        // Contract.Requires(InLevelEditMode);
        return _currentLevel;
      }
    }

    public bool CanEdit(string fileName)
    {
      var modName = _studio.CurrentProject.toModuleName(fileName);
      return InLevelEditMode && _studio.CurrentProject.ModuleInCurrentLevel(modName, CurrentLevel);
    }

    /// <summary>
    /// True while the target is running and connected to love studio
    /// via TCP sockets.
    /// </summary>
    public bool Connected
    {
      get
      {
        return appSocket != null && debuggerSocket != null &&
               appSocket.Connected && debuggerSocket.Connected;
      }
    }

    #endregion

    /// <summary>
    /// Creates a new debug proxy with given dependencies.
    /// </summary>
    /// <param name="app">The studio object.</param>
    /// <param name="bpm">The breakpoint manager.</param>
    public DebugProxy(Studio app, BreakpointManager bpm)
    {
      _studio = app;
      _breakpointManager = bpm;

      // Create a TCP/IP socket.
      appSocket = new Socket(
        AddressFamily.InterNetwork,
        SocketType.Stream, 
        ProtocolType.Tcp
      );

      debuggerSocket = null;

      listener = new Socket(
        AddressFamily.InterNetwork,
        SocketType.Stream,
        ProtocolType.Tcp
      );

      listener.Bind(localEP);
      listener.Listen(100);

      listener.BeginAccept(
        new AsyncCallback(AppAccepted),
        listener
      );

      OnBreak += DebugProxy_OnBreak;

      OnEnterLevelEditMode += DebugProxy_OnEnterLevelEditMode;
      OnCreateNewModule += DebugProxy_OnCreateNewModule;
      OnRequestEmbeddedScreenSize += DebugProxy_OnRequestEmbeddedScreenSize;
      _eventQueue = new Queue<DebugEvent>();

      _currentFile = null;
      _currentLineNum = -1;
      _state = State.DISCONNECTED;
    }

    void DebugProxy_OnRequestEmbeddedScreenSize()
    {
      var embedInGamePanel = _studio.CurrentProject.EmbedInGamePanel;

      int width = embedInGamePanel  ? _studio.GamePanel.Width : -1;
      int height = embedInGamePanel ? _studio.GamePanel.Height : -1;

      OutChunk chunk = new OutChunk();
      chunk.WriteByte(SYNC);

      //write length of message
      chunk.WriteInt(sizeof(int) * 2);
      chunk.WriteInt(width);
      chunk.WriteInt(height);
      SendChunk(chunk);

      int numBytes = debuggerSocket.Receive(_asyncState.buffer);
      InChunk screenSizeResponse = new InChunk(_asyncState.buffer, numBytes);

      byte msgType = screenSizeResponse.ReadByte();
      // Contract.Assert(msgType == SYNC);

      if (embedInGamePanel)
        _studio.GamePanel.EmbedLoveProcess();

      _state = State.RUNNING;
      BeginDebuggerReceive();
    }

    void DebugProxy_OnCreateNewModule(string pluginName, string[] args)
    {
      var plugin = _studio.ModuleConstructors.GetModuleCreatorByName(pluginName);
      plugin.CreateNewModuleFromArgs(args);
    }

    void DebugProxy_OnEnterLevelEditMode(string levelName)
    {
      _inLevelEditMode = true;
      _currentLevel = levelName;
    }

    void DebugProxy_OnEnterLevelPlayMode(string levelName)
    {
      _inLevelEditMode = false;
      _currentLevel = levelName;
    }

    void DebugProxy_OnBreak(LuaStackFrame[] stack, bool stepOver, string errorMessage)
    {
      _state = State.BREAKING;
    }

    /// <summary>
    /// Sends all breakpoints from love studio to the target. This is called 
    /// when the target launches, in order to synchronize it with love studio.
    /// </summary>
    private void SendBreakpointsToApp()
    {
      foreach (var x in _breakpointManager.BreakpointMap)
      {
        string fileName = x.Key;
        SortedSet<int> lineNumbers = x.Value;

        foreach (int lineNumber in lineNumbers)
        {
          AddBreakpoint(
            this,
            new Breakpoint(fileName, lineNumber)
          );
        }
      }
    }

    /// <summary>
    /// Sends the target a message telling it that all preliminary data, 
    /// (e.g. breakpoint data) has been sent, and that it can begin executing.
    /// </summary>
    private void SendInitFinished()
    {
      OutChunk data = new OutChunk();
      data.WriteByte(INIT_FINISH);
      data.WriteBool(_studio.CurrentProject.TypeCheckingEnabled);
      
      data.WriteBool(_studio.CurrentProject.DebuggerIgnoresClassSystemCode);

      List<string> classSystemModulePaths = new List<string>();
      foreach (LuaAnalyzer.Plugins.ITypeCollectorPlugin plugin in LuaAnalyzer.Plugins.plugins.Value)
      {
        classSystemModulePaths.AddRange(plugin.GetLibPaths());
      }
      data.WriteInt(classSystemModulePaths.Count);

      foreach (string path in classSystemModulePaths)
        data.WriteString(path.Replace('\\','/'));

      SendChunk(data);
    }

    #region Connection

    private void ConnectCallback(IAsyncResult ar)
    {
      appSocket.EndConnect(ar);

      _state = State.RUNNING;

      SendBreakpointsToApp();
      SendInitFinished();
    }

    private void AppAccepted(IAsyncResult ar)
    {
      // Get the socket that handles the client request.
      debuggerSocket = listener.EndAccept(ar);

      // Create the state object.
      _asyncState = new StateObject();

      // Connect to the remote endpoint.
      appSocket.BeginConnect(
        remoteEP,
        new AsyncCallback(ConnectCallback),
        appSocket
      );

      BeginDebuggerReceive();
    }

    #endregion

    /// <summary>
    /// Sends the given chunk to the target.
    /// </summary>
    /// <param name="chunk"></param>
    private void SendChunk(OutChunk chunk)
    {
      SocketError errorCode;
      appSocket.Send(chunk.Bytes, 0, chunk.Length, 0, out errorCode);

      if (errorCode == SocketError.ConnectionAborted)
        ResetConnections();

      // Contract.Assert(errorCode == SocketError.Success);
    }

    #region Requests

    /// <summary>
    /// Signals the target-side debugger to add the specified breakpoint.
    /// </summary>
    /// <param name="sender">msdn told me to put this param here</param>
    /// <param name="bp">The breakpoint which we are adding.</param>
    public void AddBreakpoint(object sender, Breakpoint bp)
    {
      if (!appSocket.Connected)
        return;

      OutChunk data = new OutChunk();
      
      data.WriteByte(BREAKPOINT_ADDED);
      data.WriteInt(bp.lineNumber+1);
      data.WriteString(_studio.CurrentProject.projRelative(bp.fileName));

      SendChunk(data);
    }

    /// <summary>
    /// Signals the target-side debugger to remove the specified breakpoint.
    /// </summary>
    /// <param name="sender">msdn told me to put this param here</param>
    /// <param name="bp">The breakpoint which we are removing.</param>
    public void RemoveBreakpoint(object sender, Breakpoint bp)
    {
      if (!appSocket.Connected)
        return;

      OutChunk data = new OutChunk();

      data.WriteByte(BREAKPOINT_REMOVED);
      data.WriteInt(bp.lineNumber+1);
      data.WriteString(_studio.CurrentProject.projRelative(bp.fileName));

      SendChunk(data);
    }

    /// <summary>
    /// Signals the target-side debugger to resume execution.
    /// </summary>
    public void Resume()
    {
      // Contract.Requires(Breaking);
      // Contract.Ensures(!Breaking);

      OnResume();

      _state = State.RUNNING;

      OutChunk data = new OutChunk(1);
      data.WriteByte(RESUME);

      SendChunk(data);

      // Start a receiver for the next breakpoint which gets hit
      BeginDebuggerReceive();
    }

    /// <summary>
    /// Signals the target-side debugger to step over the current line,
    /// skipping over any function calls.
    /// </summary>
    public void StepOver()
    {
      // Contract.Requires(Breaking);
      // Contract.Ensures(!Breaking);

      string currentLineContents = _studio.GetLineContents(
        _studio.CurrentProject.projNameToFileSystem(_currentFile),
        _currentLineNum
      );

      if (currentLineContents.Contains("return") || currentLineContents.Contains("break"))
      {
        StepIn();
        return;
      }

      _state = State.STEPPING_OVER;

      OnStepOver();

      OutChunk data = new OutChunk();
      data.WriteByte(STEP_OVER);

      SendChunk(data);

      // Start a receiver for the next breakpoint which gets hit
      BeginDebuggerReceive();
    }

    /// <summary>
    /// Signals the target-side debugger to step to the next line or
    /// into a function call.
    /// </summary>
    public void StepIn()
    {
      // Contract.Requires(Breaking);
      // Contract.Ensures(!Breaking);

      _state = State.STEPPING_IN;

      OnStepIn();

      OutChunk data = new OutChunk();
      data.WriteByte(STEP_IN);

      SendChunk(data);

      // Start a receiver for the next breakpoint which gets hit
      BeginDebuggerReceive();
    }

    /// <summary>
    /// Sends the target a message telling it to terminate.
    /// </summary>
    public void TerminateTarget()
    {
      OutChunk data = new OutChunk();
      data.WriteByte(TERMINATE);
      SendChunk(data);
    }

    /// <summary>
    /// Gets descriptions of the children of the specified lua table.
    /// </summary>
    /// <param name="ptr">
    ///   The target process address of the table we are getting the children of.
    ///   If ptr is 0, then we are requesting the children of an imaginary table 
    ///   like locals or globals; these imaginary tables are handled as a special
    ///   case by the target.
    /// </param>
    /// <param name="path">The debug browser path of the table we are getting the children of.</param>
    /// <param name="browser">The debug browser identifier for the browser containing the table we are getting the children of.</param>
    /// <param name="frameIndex">
    ///   If we are requesting locals, this is the index of the stack
    ///   frame we are requesting the locals of.
    /// </param>
    /// <returns></returns>
    public ChildrenInfo GetChildren(long ptr, string path, byte browser, int frameIndex)
    {
      OutChunk data = new OutChunk();
      data.WriteByte(REQUEST_CHILDREN);
      data.WriteLong(ptr);
      data.WriteString(path);
      data.WriteByte(browser);
      data.WriteInt(frameIndex);

      //TODO: catch any lost connection exceptions or socket errors
      SendChunk(data);
      int numBytes = debuggerSocket.Receive(_asyncState.buffer);
      InChunk childrenResponse = new InChunk(_asyncState.buffer, numBytes);

      byte msgType = childrenResponse.ReadByte();
      // Contract.Assert(msgType == CHILDREN_RESPONSE);

      ChildrenInfo e = new ChildrenInfo();
      e.browser = childrenResponse.ReadByte();
      e.parentPath = childrenResponse.ReadString();
      int numItems = childrenResponse.ReadInt();
      e.items = new ChildrenInfo.ItemDesc[numItems];

      for (int i = 0; i < numItems; ++i)
      {
        var item = new ChildrenInfo.ItemDesc();
        item = new ChildrenInfo.ItemDesc();
        item.keyName = childrenResponse.ReadString();
        item.typeName = childrenResponse.ReadString();
        item.valueString = childrenResponse.ReadString();
        item.hasPtr = childrenResponse.ReadBool();
        item.ptr = childrenResponse.ReadInt64();
        e.items[i] = item;
      }

      // Contract.Assert(childrenResponse.Empty);

      return e;
    }

    #endregion

    private void HandleSynActionInitiated()
    {
      _state = State.PERFORMING_SYNCHRONIZED_ACTION;
    }

    private void HandleBreakpointHit(InChunk data)
    {
      string errorMessage = data.ReadString();
      int stackLevels = data.ReadInt();

      LuaStackFrame[] stack = new LuaStackFrame[stackLevels];

      for (int i = 0; i < stackLevels; ++i)
      {
        stack[i] = new LuaStackFrame();
        stack[i].funcName = data.ReadString();
        stack[i].fileName = data.ReadString();
        stack[i].lineNumber = data.ReadInt() - 1;
        stack[i].stackLevel = data.ReadInt();
      }

      if (stack.Count() == 0)
      {
        //HACK: This handles errors in built-in lua code (e.g. boot.lua)
        //Since the user does not have access to it, we cannot break 
        //on the error.
        TerminateTarget();
        _eventQueue.Enqueue(ResetConnections);
        MessageBox.Show(errorMessage, "Error");
      }
      else
      {
        //Do not add this as a callback -- could cause weird thread errors
        _currentFile = stack[0].fileName;
        _currentLineNum = stack[0].lineNumber;
        var isSteppingOver = _state == State.STEPPING_OVER;
        _state = State.BREAK_SETUP;
        ///

        _eventQueue.Enqueue(() => OnBreak(stack, isSteppingOver, errorMessage));
      }
    }

    /// <summary>
    /// Handles a message received asynchronously from the target.
    /// </summary>
    /// <param name="ar">Message received from target.</param>
    private void ReceiveCallback(IAsyncResult ar)
    {
      --receiveCount;
      String content = String.Empty;

      // Retrieve the state object and the handler socket
      // from the asynchronous state object.
      StateObject state = (StateObject)ar.AsyncState;

      try
      {
        // Read data from the client socket. 
        int bytesRead = debuggerSocket.EndReceive(ar);

        if (bytesRead > 0)
        {
          InChunk data = new InChunk(state.buffer, bytesRead);

          while (!data.Empty)
          {
            byte msgType = data.ReadByte();

            if (msgType == BREAKPOINT_HIT)
            {
              HandleBreakpointHit(data);
            }
            else if (msgType == DEBUG_PRINT)
            {
              string lineToPrint = data.ReadString();
              _eventQueue.Enqueue(() => OnDebugMsgReceived(lineToPrint));
            }
            else if (msgType == ENTER_LEVEL_EDIT_MODE)
            {
              string levelName = data.ReadString();
              _eventQueue.Enqueue(() => OnEnterLevelEditMode(levelName));
            }
            else if (msgType == ENTER_LEVEL_PLAY_MODE)
            {
              string levelName = data.ReadString();
              _eventQueue.Enqueue(() => OnEnterLevelPlayMode(levelName));
            }
            else if (msgType == REQUEST_EMBEDDED_SCREEN_SIZE)
            {
              int width = _studio.GamePanel.Width;
              int height = _studio.GamePanel.Height;
              
              HandleSynActionInitiated();

              _eventQueue.Enqueue(() => OnRequestEmbeddedScreenSize());
            }
            else if (msgType == CREATE_NEW_MODULE)
            {
              string modulePluginName = data.ReadString();
              int numArgs = data.ReadInt();

              string[] args = new string[numArgs];
              for (int i = 0; i < numArgs; ++i)
                args[i] = data.ReadString();

              _eventQueue.Enqueue(() => OnCreateNewModule(modulePluginName, args));
            }
            else if (msgType == OPEN_MODULE)
            {
              string moduleName = data.ReadString();
              _eventQueue.Enqueue(() => OnOpenModule(moduleName));
            }
            else
            {
              // Assert(false); // unreachable
            }
          }

          if (Running)
          {
            BeginDebuggerReceive();
          }
        }
        else
          _eventQueue.Enqueue(ResetConnections);
      }
      catch (SocketException)
      {
        //10054 - the winsock error code for when the socket
        //is closed by the remote host (or process in this case).
        // Contract.Assert(e.ErrorCode == 10054);
        _eventQueue.Enqueue(ResetConnections);
      }
    }

    private int receiveCount = 0;
    private void BeginDebuggerReceive()
    {
      // Contract.Ensures(receiveCount <= 1);
      receiveCount++;
      // kick off next async receive (is there a more lightweight way to do this?)
      debuggerSocket.BeginReceive(
        _asyncState.buffer,
        0,
        _asyncState.buffer.Length,
        0,
        new AsyncCallback(ReceiveCallback),
        _asyncState
      );
    }

    /// <summary>
    /// When the target crashes or closes, this is called in order to clear out 
    /// all data related to the connection to the target and begin listening
    /// for a new connection.
    /// </summary>
    private void ResetConnections()
    {
      _state = State.DISCONNECTED;
      _currentFile = null;
      _currentLineNum = -1;

      appSocket.Disconnect(true);
      debuggerSocket.Disconnect(true);
      OnDisconnect();
      listener.BeginAccept(
        new AsyncCallback(AppAccepted),
        listener
      );
    }

    /// <summary>
    /// Responds to all pending asynchronously-received messages. This is called
    /// from the main thread.
    /// </summary>
    public void ProcessEvents()
    {
      while (_eventQueue.Count != 0)
      {
        DebugEvent e = _eventQueue.Dequeue();
        e();
      }
    }
  }
}
