﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ScintillaNet;

namespace LoveEdit
{
  public class Editor : Scintilla
  {
    /// <summary>
    /// The tick count for the last modified time.
    /// </summary>
    public long LastModified { get; private set; }

    public bool Dead { get; set; }

    /// <summary>
    /// True if the last interaction with the editor involved adding non-newline text.
    /// </summary>
    public bool TypingLine { get; set; }

    /// <summary>
    /// HACK: if OnTextAdded detects that a key was pressed which adds text to a line,
    /// it sets this to true so that the OnKeyDown event handler knows not to set TypingLine
    /// to false.
    /// </summary>
    private bool _lineTypeKeyPressed;

    public override string Text
    {
      get
      {
        
        return base.Text;
      }
      set
      {
        base.Text = value;
        OnTextChanged();
      }
    }

    public Editor()
    {
      LastModified = DateTime.Now.Ticks;
      TypingLine = false;
    }

    /// <summary>
    /// Given an index of a character in the document, 
    /// returns the byte offset of the specified character. 
    /// </summary>
    /// <param name="charInd">The character index of some character in the string</param>
    public int ComputeByteOffsetFromCharOffset(int charInd)
    {
      // Contract.Requires(0 <= charInd && charInd <= Text.Length);
      // Contract.Ensures(Contract.Result<int>() <= RawText.Length);
      char[] chars = Text.ToCharArray();
      return UTF8Encoding.UTF8.GetByteCount(chars, 0, charInd);
    }

    /// <summary>
    /// Given a byte offset, return the character index of the character beginning 
    /// at the given byte offset.
    /// </summary>
    /// <param name="byteOffset">The byte offset of the character in question.</param>
    public int ComputeCharOffsetFromByteOffset(int byteOffset)
    {
      // Contract.Requires(byteOffset <= RawText.Length);
      // Contract.Ensures(Contract.Result<int>() <= Text.Length);

      char[] chars = Text.ToCharArray();
      int currByteOffset = 0;
      for (int i = 0; i < chars.Length; ++i)
      {
        if (byteOffset == currByteOffset)
          return i;

        currByteOffset += UTF8Encoding.UTF8.GetByteCount(chars, i, 1);
      }

      return chars.Length;
    }

    protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
    {
      base.OnKeyDown(e);

      if (_lineTypeKeyPressed)
      {
        _lineTypeKeyPressed = false;
        return;
      }
      _lineTypeKeyPressed = false;

      if (AutoComplete.IsActive && (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down))
        return;

      if (AutoComplete.IsActive && (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return))
        return;

      if (e.KeyCode == Keys.Back)
        return;

      if (e.Alt || e.Control || e.Shift)
        return;
     
      TypingLine = false;
    }

    public void Indent()
    {
      Line prevLine = Lines.Current.Previous;
      
      TypingLine = false;
    }

    protected override void OnTextInserted(TextModifiedEventArgs e)
    {
      base.OnTextInserted(e);
      
      if (e.IsUserChange && e.LinesAddedCount == 0)
      {
        _lineTypeKeyPressed = true;
        TypingLine = true;
      }
    }

    protected override void OnTextChanged()
    {
      base.OnTextChanged();

      LastModified = DateTime.Now.Ticks;
    }
  }
}
