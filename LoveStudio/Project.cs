﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

using System.Xml;

namespace LoveEdit
{
  /// <summary>
  /// To the user, a project is a collection of source files and resources
  /// used to make a love application.
  /// 
  /// This class contains all necessary information about a project,
  /// which right now is just a root directory. It provides methods for 
  /// converting between absolute and project-relative paths and is responsible
  /// for watching the files in the project to keep the files on disk in sync
  /// with files which are being modified in memory.
  /// </summary>
  public class Project
  {
    #region Data Members
    //see property
    readonly private DirectoryInfo _rootPathInfo;
    //see property
    readonly private FileSystemWatcher _fileWatcher;
    //The full path of the project file
    readonly private string _path;

    /// <summary>
    /// A list of all file paths which have been altered and may need to
    /// be reloaded (depending on how the user answers a prompt).
    /// </summary>
    readonly private SynchronizedCollection<string> _reloadBuffer;

    /// <summary>
    /// Info for all lua files rooted in the project directory. This is cached so that we 
    /// do not need to go through the expensive process of collecting these files unless
    /// something actually changes.
    /// </summary>
    private List<FileInfo> _fileInfoCache;

    #endregion

    #region Dependencies

    private readonly Studio _studio;

    #endregion


    /// <summary>
    /// An object which can traverse the project directory.
    /// </summary>
    public DirectoryInfo RootPathInfo { get { return _rootPathInfo; } }

    /// <summary>
    /// Monitors files in project to make sure that any external changes to
    /// them are taken into account.
    /// </summary>
    public FileSystemWatcher FileWatcher { get { return _fileWatcher; } }

    /// <summary>
    /// The path which all of the project's contents are contained in-- either
    /// directly, or indirectly via subdirectories. Directly contains the
    /// project's luaproj file.
    /// </summary>
    public string RootPath { get { return _rootPathInfo.FullName; } }

    public string DefaultModuleType { get; set; }

    public bool TypeCheckingEnabled { get; set; }

    /// <summary>
    /// Whether or not we should embed the love process into the game
    /// panel when it is running.
    /// </summary>
    public bool EmbedInGamePanel { get; set; }

    /// <summary>
    /// Whether or not to allow the debugger to step into modules which
    /// define class systems and whether to include those modules
    /// in the stack browser.
    /// </summary>
    public bool DebuggerIgnoresClassSystemCode { get; set; }


    /// <summary>
    /// Creates a new project corresponding to the specified root path.
    /// </summary>
    /// <param name="rootPath">The path of the project file.</param>
    /// <param name="studio">The studio object.</param>
    public Project(string path, Studio studio)
    {
      _path = path;
      string rootPath = PathUtils.ContainingDirectory(path);
      _studio = studio;

      _rootPathInfo = new System.IO.DirectoryInfo(rootPath);
      _reloadBuffer = new SynchronizedCollection<string>();

      _fileWatcher = new FileSystemWatcher(rootPath, "*.lua");
      _fileWatcher.IncludeSubdirectories = true;
      _fileWatcher.Changed += new FileSystemEventHandler(_fileWatcher_Changed);
      
      _fileWatcher.Created += _fileWatcher_InvalidateCache;
      _fileWatcher.Deleted += _fileWatcher_InvalidateCache;
      _fileWatcher.Renamed += _fileWatcher_InvalidateCache;

      // begin watching
      _fileWatcher.EnableRaisingEvents = true;

      LoadSettings();
    }

    /// <summary>
    /// Sets _fileInfoCache to null so that it is recomputed the next time it is accessed
    /// </summary>
    public void InvalidateFileInfoCache()
    {
      _fileInfoCache = null;
    }

    // When files or folders in the project directory are modified externally
    // from LOVE Studio, this invalidates the file info cache.
    private void _fileWatcher_InvalidateCache(object sender, FileSystemEventArgs e)
    {
      InvalidateFileInfoCache();
    }

    private void LoadSettings()
    {
      if (System.IO.File.Exists(_path))
      {
        XmlReader reader = new XmlTextReader(_path);

        var doc = new XmlDocument();
        doc.Load(reader);

        var settings = doc.DocumentElement;
        DefaultModuleType = settings.SelectSingleNode("DefaultModuleType").InnerText;
        TypeCheckingEnabled = settings.SelectSingleNode("TypeCheckingEnabled").InnerText == "True" ? true : false;

        if (settings.SelectSingleNode("EmbedInGamePanel") != null)
        {
          EmbedInGamePanel = settings.SelectSingleNode("EmbedInGamePanel").InnerText == "True" ? true : false;
        }
        else
        {
          EmbedInGamePanel = false;
        }

        if (settings.SelectSingleNode("DebuggerIgnoresClassSystemCode") != null)
        {
          DebuggerIgnoresClassSystemCode = settings.SelectSingleNode("DebuggerIgnoresClassSystemCode").InnerText == "True" ? true : false;
        }
        else
        {
          DebuggerIgnoresClassSystemCode = true;
        }

        reader.Close();
      }
      else
      {
        //This must be a brand new project. Set default settings and save.
        TypeCheckingEnabled = true;
        EmbedInGamePanel = false;
        DebuggerIgnoresClassSystemCode = true;
        DefaultModuleType = "";
        SaveSettings();
      }
    }

    public void SaveSettings()
    {
      XmlDocument doc = new XmlDocument();
      XmlElement settings = (XmlElement)doc.AppendChild(doc.CreateElement("ProjectSettings"));
      settings.AppendChild(doc.CreateElement("DefaultModuleType")).InnerText = DefaultModuleType;
      settings.AppendChild(doc.CreateElement("TypeCheckingEnabled")).InnerText = TypeCheckingEnabled.ToString();
      settings.AppendChild(doc.CreateElement("EmbedInGamePanel")).InnerText = EmbedInGamePanel.ToString();
      
      while (true)
      {
        try
        {
          doc.Save(_path);
        }
        catch (IOException e)
        {
          var res = MessageBox.Show(e.Message, "error", MessageBoxButtons.RetryCancel);
          if (res == DialogResult.Retry)
            continue;
        }

        break;
      }
    }

    /// <summary>
    /// When a file contained in this project changes on disk, we add it to 
    /// a reload buffer; the main thread will then prompt the user to reload
    /// this file if it is currently being edited.
    /// </summary>
    /// <param name="sender">sender</param>
    /// <param name="e">event args</param>
    private void _fileWatcher_Changed(object sender, FileSystemEventArgs e)
    {
      if(!_reloadBuffer.Contains(e.FullPath))
        _reloadBuffer.Add(e.FullPath);
    }

    /// <summary>
    /// Gets a collection of all source files in the project.
    /// </summary>
    /// <returns>A collection of all source (.lua) files in the project. </returns>
    public IEnumerable<FileInfo> GetSourceFiles()
    {
      if (_fileInfoCache == null)
      {
        _fileInfoCache = new List<FileInfo>(
          from file in _rootPathInfo.GetFiles("*", SearchOption.AllDirectories)
          where file.FullName.EndsWith(".lua")
          select file
        );
      }

      return _fileInfoCache;
    }

    /// <summary>
    /// Yields (File Name, Module Name, File Contents, Last Modified) quadriples for each
    /// module in the project. If an editor is currently open for a given
    /// file, the contents are taken from the editor rather than the disk.
    /// </summary>
    public List<Tuple<string, string, Lazy<string>, long>> GetModules()
    {
      IEnumerable<FileInfo> files = GetSourceFiles();

      var moduleInfoList = new List<Tuple<string, string, Lazy<string>, long>>(files.Count());

      foreach (FileInfo file in files)
      {
        Lazy<string> fileContents;
        long lastModified;

        if (_studio.FileEditorMap.Contains(file.FullName))
        {
          fileContents = new Lazy<string>(() => _studio.FileEditorMap[file.FullName].Text);
          lastModified = _studio.FileEditorMap[file.FullName].LastModified;
        }
        else
        {
          fileContents = new Lazy<string>(delegate() {
              StreamReader reader = new StreamReader(file.FullName);
              string ret = reader.ReadToEnd();
              reader.Close();
              return ret;
            }
          );
          
          lastModified = file.LastWriteTime.Ticks;
        }

        // Contract.Assert(fileContents != null);

        moduleInfoList.Add(
          new Tuple<string, string, Lazy<string>, long>(
            file.FullName,
            toModuleName(file.FullName),
            fileContents,
            lastModified
          )
        );
      }

      return moduleInfoList;
    }

    /// <summary>
    /// Returns a sequence of all source paths in the project.
    /// </summary>
    /// <returns>
    /// A sequence of all paths rooted by the project path which
    /// contain at least one lua file.
    /// </returns>
    public IEnumerable<string> GetModulePaths()
    {
      IEnumerable<FileInfo> files = GetSourceFiles();
      HashSet<string> pathsEncountered = new HashSet<string>();

      foreach (FileInfo file in files)
      {
        string moduleName = toModuleName(file.FullName);

        if (!moduleName.Contains('.'))
          continue;

        string path = moduleName.Remove(moduleName.LastIndexOf("."));

        if (pathsEncountered.Contains(path))
          continue;

        pathsEncountered.Add(path);
        yield return path;
      }
    }

    // [Pure]
    public bool IsValidModuleName(string name)
    {
      bool legalChars = name.All(c => c == '.' || c == '_' || Char.IsLetterOrDigit(c));
      bool nonEmptyNames = name.Split('.').All(s => s.Length > 0);
      return legalChars && nonEmptyNames && !(name == "init");
    }

    // [Pure]
    public bool ModuleInCurrentLevel(string moduleName, string currentLevelName)
    {
      string[] parts = moduleName.Split('.');
      if (parts[0] != "levels")
        return false;

      if (parts[1] != currentLevelName)
        return false;

      return true;
    }

    public IEnumerable<string> GetModuleNames()
    {
      IEnumerable<FileInfo> files = GetSourceFiles();
      foreach (FileInfo file in files)
        yield return toModuleName(file.FullName);  
    }

    public IEnumerable<string> GetFileNames()
    {
      IEnumerable<FileInfo> files = GetSourceFiles();
      foreach (FileInfo file in files)
       yield return file.FullName;
    }

    /// <summary>
    /// Given the full path of a file, return the file's path relative to the 
    /// root directory of the project, converted to the format used by love.
    /// </summary>
    /// <param name="filePath">The path to extract a file name from.</param>
    /// <returns></returns>
    public string projRelative(string filePath)
    {
      return filePath.Substring(RootPath.Length).Replace('\\','/');
    }

    ///
    public string toFileName(string moduleName)
    {
      return RootPath + moduleName.Replace(".", "\\") + ".lua";
    }

    /// <summary>
    /// Converts a source file path to a lua module name.
    /// </summary>
    /// <param name="filePath">The file path to convert.</param>
    /// <returns></returns>
    public string toModuleName(string filePath)
    {
      // Contract.Requires(isInProj(filePath));
      string projFilePath = projRelative(filePath);
     
      // remove ".lua"
      projFilePath = projFilePath.Substring(0, projFilePath.Length - 4);

      // replace "/" with "."
      projFilePath = projFilePath.Replace('/', '.');

      // "init.lua" is used for packages; hence, we chop "init" off of the end
      // of the string if it is there.
      if (projFilePath.Length >= 4 && projFilePath.Substring(projFilePath.Length - 4) == "init")
        projFilePath = projFilePath.Substring(0, projFilePath.Length - 4);

      return projFilePath;
    }

    /// <summary>
    /// Returns true iff the given absolute file name is located in a (not necessarily proper)
    /// subdirectory of the project root.
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    // [Pure]
    public bool isInProj(string filePath)
    {
      return (filePath.Length > RootPath.Length) &&
             filePath.Substring(0, RootPath.Length) == RootPath;
    }

    /// <summary>
    /// Given a project-relative file path, generates the full filesystem path 
    /// by prepending the project root.
    /// </summary>
    /// <param name="filePath">A project-relative file name.</param>
    /// <returns></returns>
    public string projNameToFileSystem(string filePath)
    {
      return RootPath + filePath.Replace('/', '\\');
    }

    /// <summary>
    /// External file modifications are detected in separate threads 
    /// (managed by FileSystemWatchers). The main thread occasionally calls
    /// this method, which triggers all buffered file modification events
    /// to get handled.
    /// </summary>
    public void ProcessEvents()
    {
      // Contract.Ensures(_reloadBuffer.Count == 0);

      string [] toReload = new string[_reloadBuffer.Count];
      _reloadBuffer.CopyTo(toReload,0);
      _reloadBuffer.Clear();

      foreach (string path in toReload)
          _studio.ReloadFile(path);
    }
  }
}
