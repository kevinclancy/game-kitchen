﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace LoveEdit
{
  public class TabManager
  {
    public readonly TabControl LeftControl;
    public readonly TabControl RightControl;

    private readonly List<TabPage> _lStack;
    private readonly List<TabPage> _rStack;

    private bool _removingPage;

    public TabPage LeftTop
    {
      get
      {
        if (_lStack.Count == 0)
          return null;
        else
          return _lStack.Last();
      }
    }

    public TabPage RightTop
    {
      get
      {
        if (_rStack.Count == 0)
          return null;
        else
          return _rStack.Last();
      }
    }


    private readonly Dictionary<TabControl, List<TabPage>> _stackOf;

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  Contract.Invariant(LeftControl.TabCount == _lStack.Count);
    //  Contract.Invariant(RightControl.TabCount == _rStack.Count);
    //  Contract.Invariant(LeftControl.TabCount == 0 || LeftControl.SelectedTab == _lStack.Last());
    //  Contract.Invariant(RightControl.TabCount == 0 || RightControl.SelectedTab == _rStack.Last());
    //}

    public TabManager(TabControl left, TabControl right)
    {
      LeftControl = left;
      RightControl = right;

      left.Selected += left_Selected;
      right.Selected += right_Selected;

      _removingPage = false;

      _lStack = new List<TabPage>();
      _rStack = new List<TabPage>();

      _stackOf = new Dictionary<TabControl, List<TabPage>>();

      _stackOf[LeftControl] = _lStack;
      _stackOf[RightControl] = _rStack;
    }

    void right_Selected(object sender, TabControlEventArgs e)
    {
      if (_removingPage)
        return;

      _rStack.Remove(e.TabPage);
      _rStack.Add(e.TabPage);
    }

    void left_Selected(object sender, TabControlEventArgs e)
    {
      if (_removingPage)
        return;

      _lStack.Remove(e.TabPage);
      _lStack.Add(e.TabPage);
    }

    public void AddPanelToControl(Control panel, string title, TabControl control)
    {
      // Contract.Requires(control == LeftControl || control == RightControl);

      TabPage page = new TabPage(title);
      panel.Dock = DockStyle.Fill;
      page.Controls.Add(panel);

      control.TabPages.Add(page);
      _stackOf[control].Add(page);
      SelectTop(control);
    }

    public void AddPageToControl(TabPage page, TabControl control)
    {
      //Contract.Requires(control == LeftControl || control == RightControl);
      //Contract.Requires(!(LeftControl.TabPages.Contains(page) || RightControl.TabPages.Contains(page)));
      //Contract.Ensures(control.TabPages.Contains(page));
      //Contract.Ensures((control == LeftControl && LeftTop == page) || (control == RightControl && RightTop == page));

      control.TabPages.Add(page);
      _stackOf[control].Add(page);
      SelectTop(control);
    }

    public void RemovePageFromControl(TabPage page)
    {
      //Contract.Requires(page.Parent != null);
      //Contract.Ensures(
      //  !LeftControl.TabPages.Contains(page) &&
      //  !RightControl.TabPages.Contains(page)
      //);

      TabControl control = (TabControl)page.Parent;
      _removingPage = true;
      control.TabPages.Remove(page);
      _removingPage = false;
      _stackOf[control].Remove(page);
      SelectTop(control);
    }

    public void BringPageToFront(TabPage page)
    {
      // Contract.Requires(page.Parent == LeftControl || page.Parent == RightControl);
      // Contract.Ensures(
      //   (page.Parent == LeftControl && LeftTop == page) ||
      //   (page.Parent == RightControl && RightTop == page)
      // );

      if (page.Parent == LeftControl)
      {
        LeftControl.SelectTab(page);
        page.Controls[0].Focus();

        // Move this page to top of stack
        _lStack.Remove(page);
        _lStack.Add(page);
      }
      else
      {
        RightControl.SelectTab(page);
        page.Controls[0].Focus();

        // Move this page to top of stack
        _rStack.Remove(page);
        _rStack.Add(page);
      }
    }

    public void MovePageToFrontOfControl(TabPage page, TabControl control)
    {
      //Contract.Requires(
      //  page.Parent == LeftControl
      //  || page.Parent == RightControl
      //);

      // Contract.Requires(control == LeftControl || control == RightControl);

      if (page.Parent == control)
        BringPageToFront(page);
      else
        MoveToOtherTabControl(page);
    }

    public void MoveToOtherTabControl(TabPage page)
    {
      // Contract.Requires(
      //  page.Parent == LeftControl
      //  || page.Parent == RightControl
      // );

      TabControl tabControl = (TabControl)page.Parent;
      if (tabControl == LeftControl)
      {
        RemovePageFromControl(page);
        AddPageToControl(page, RightControl);
      }
      else
      {
        RemovePageFromControl(page);
        AddPageToControl(page, LeftControl);
      }
    }

    private void SelectTop(TabControl control)
    {
      if (_stackOf[control].Count > 0)
      {
        control.SelectTab(_stackOf[control].Last());
        _stackOf[control].Last().Controls[0].Focus();
      }
    }
  }
}
