﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoveEdit
{
  /// <summary>
  /// Stores a serialized sequence of fields, such as integers and strings,
  /// used for sending data through TCP sockets.
  /// 
  /// Provides methods for appending fields.
  /// </summary>
  public class OutChunk
  {
    /// <summary>
    /// The data being written.
    /// </summary>
    private byte[] _bytes;

    /// <summary>
    /// The contents of the OutChunk.
    /// </summary>
    public byte[] Bytes
    {
      get
      {
        byte[] bytes = new byte[Length];
        Array.Copy(_bytes, bytes, Length);
        return _bytes;
      }
    }

    /// <summary>
    /// The position (in bytes) of the next position to be written to.
    /// </summary>
    private int _pos;

    /// <summary>
    /// The total number of bytes written so far.
    /// </summary>
    public int Length
    {
      get
      {
        return _pos;
      }
    }

    //[ContractInvariantMethod]
    //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
    //private void ObjectInvariant()
    //{
    //  Contract.Invariant(_pos <= _bytes.Length);
    //  Contract.Invariant(_pos >= 0);
    //  Contract.Invariant(_bytes.Length > 0);
    //}

    /// <summary>
    /// Construct a new outChunk with the given initial capacity.
    /// </summary>
    /// <param name="byteLength">The capacity of the new chunk in bytes.</param>
    public OutChunk(int byteLength)
    {
      // Contract.Requires(byteLength > 0);

      _bytes = new byte[byteLength];
      _pos = 0; 
    }

    /// <summary>
    /// Construct a new OutChunk with an initial capacity of 10.
    /// </summary>
    public OutChunk()
    {
      _bytes = new byte[10];
      _pos = 0;
    }

    /// <summary>
    /// Writes a specified number of bytes from the given array, starting
    /// at a specified location.
    /// </summary>
    /// <param name="src">The array to write bytes from.</param>
    /// <param name="startIndex">The index in src to start copying bytes from.</param>
    /// <param name="length">The number of bytes to copy.</param>
    public void WriteBytes(byte[] src, int startIndex, int length)
    {
      //Contract.Requires(startIndex >= 0);
      //Contract.Requires(length > 0);
      //Contract.Requires(startIndex+length <= src.Length);
      //Contract.Ensures(Length == Contract.OldValue(Length) + length);

      while (_pos + length >= _bytes.Length)
        Grow();

      Array.Copy(src, startIndex, _bytes, _pos, length);
      _pos += length;
    }

    /// <summary>
    /// Writes an array of bytes to the OutChunk.
    /// </summary>
    /// <param name="src">An array containing the bytes to write.</param>
    public void WriteBytes(byte[] src)
    {
      // Contract.Requires(src.Length > 0);
      // Contract.Ensures(Length == Contract.OldValue(Length) + src.Length);

      WriteBytes(src, 0, src.Length);
    }

    /// <summary>
    /// Writes a single byte to the OutChunk.
    /// </summary>
    /// <param name="toWrite">The byte to write.</param>
    public void WriteByte(byte toWrite)
    {
      // Contract.Ensures(Length == Contract.OldValue(Length) + 1);

      if (_pos == _bytes.Length)
        Grow();

      _bytes[_pos] = toWrite;
      _pos++;
    }

    /// <summary>
    /// Writes a single bool to the OutChunk.
    /// </summary>
    /// <param name="toWrite">The byte to write.</param>
    public void WriteBool(bool toWrite)
    {
      // Contract.Ensures(Length == Contract.OldValue(Length) + 1);

      if (_pos == _bytes.Length)
        Grow();

      _bytes[_pos] = toWrite ? (byte)1 : (byte)0;
      _pos++;
    }

    /// <summary>
    /// Writes an integer to the OutChunk.
    /// </summary>
    /// <param name="toWrite">The integer to write.</param>
    public void WriteInt(int toWrite)
    {
      // Contract.Ensures(Length == Contract.OldValue(Length) + 4);
      
      byte[] bytes = BitConverter.GetBytes(toWrite);
      // Contract.Assert(bytes.Length == 4);
      WriteBytes(bytes, 0, 4);
    }

    /// <summary>
    /// Writes a 64-bit integer to the OutChunk.
    /// </summary>
    /// <param name="toWrite">The 64-bit integer to write.</param>
    public void WriteLong(long toWrite)
    {
      // Contract.Ensures(Length == Contract.OldValue(Length) + sizeof(long));

      byte[] bytes = BitConverter.GetBytes(toWrite);
      // Contract.Assert(bytes.Length == sizeof(long));
      WriteBytes(bytes,0,sizeof(long));
    }

    /// <summary>
    /// Converts a string to a byte array (one character per byte), and then
    /// writes the string to the buffer by first writing the number of 
    /// characters in the string, and then writing the bytes.
    /// </summary>
    /// <param name="toWrite">The string to write to the buffer.</param>
    public void WriteString(string toWrite)
    {
      // Contract.Ensures(Length == Contract.OldValue(Length) + toWrite.Length + sizeof(int));

      UTF8Encoding encoding = new UTF8Encoding();
      byte[] stringBytes = encoding.GetBytes(toWrite);
      WriteInt(stringBytes.Length);
      WriteBytes(stringBytes, 0, stringBytes.Length);
    }

    /// <summary>
    /// Doubles the capacity of the chunk so that more data can be written.
    /// </summary>
    private void Grow()
    {
      // Contract.Ensures(_bytes.Length == Contract.OldValue(_bytes.Length) * 2);

      byte[] newBytes = new byte[_bytes.Length * 2];
      Array.Copy(_bytes, newBytes, _bytes.Length);
      _bytes = newBytes;
    }
  }
}
