﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmptyModuleCreator
{
  public partial class ModuleCreatorControl : UserControl
  {
    public ModuleCreatorControl()
    {
      InitializeComponent();
    }

    public string[] GetNewModuleArgs()
    {
      return new string[] {
        _moduleNameTextBox.Text,
      };
    }
  }
}
