﻿namespace EmptyModuleCreator
{
  partial class ModuleCreatorControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this._moduleNameTextBox = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(4, 10);
      this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(95, 17);
      this.label1.TabIndex = 3;
      this.label1.Text = "Module Name";
      // 
      // _moduleNameTextBox
      // 
      this._moduleNameTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
      this._moduleNameTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
      this._moduleNameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
      this._moduleNameTextBox.Location = new System.Drawing.Point(109, 7);
      this._moduleNameTextBox.Margin = new System.Windows.Forms.Padding(4);
      this._moduleNameTextBox.Name = "_moduleNameTextBox";
      this._moduleNameTextBox.Size = new System.Drawing.Size(260, 22);
      this._moduleNameTextBox.TabIndex = 2;
      // 
      // ModuleCreatorControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this._moduleNameTextBox);
      this.Name = "ModuleCreatorControl";
      this.Size = new System.Drawing.Size(380, 39);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox _moduleNameTextBox;
  }
}
