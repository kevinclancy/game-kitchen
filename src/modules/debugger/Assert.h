#ifndef LOVE_DEBUGGER_ASSERT_H_
#define LOVE_DEBUGGER_ASSERT_H_

#if defined( _DEBUG )
#define _assert(x) \
  do { \
    if(!(x)) { __debugbreak(); }\
  } while(0)
#else
#define _assert(x) (void)(sizeof(x))
#endif

#endif
