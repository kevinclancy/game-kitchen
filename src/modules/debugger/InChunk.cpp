/**
* Copyright (c) 2006-2011 LOVE Development Team
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
**/

#include "InChunk.h"
#include "Assert.h"

namespace love
{

namespace debugger
{

InChunk::InChunk(char * data, int len, bool _ownsData) 
	: length(len), bytes(data), pos(0), ownsData(_ownsData)
{

}

InChunk::~InChunk()
{
	if(ownsData)
		delete[] bytes;
}

char InChunk::ReadChar()
{
	char ret = bytes[pos];
	pos++;
	return ret;
}

InChunk* InChunk::ReadBytes(int numBytes)
{
	char* newBytes = new char[numBytes];
	std::copy(bytes+pos,bytes+pos+numBytes,newBytes);

	pos += numBytes;

	InChunk* ret = new InChunk(newBytes,numBytes,true);
	return ret;
}

int InChunk::ReadInt()
{
	int ret = *( (int *)(bytes + pos) );
	pos += sizeof(int);
	return ret;
}

long long InChunk::ReadInt64()
{
	long long ret = *( (long long *)(bytes + pos) );
	pos += sizeof(long long);
	return ret;
}

std::string InChunk::ReadString()
{
	int stringLength = *( (int *)(bytes + pos) ); 
	pos += 4;
	std::string ret(bytes + pos, bytes + pos + stringLength);
	pos += stringLength;
	return ret;
}

bool InChunk::ReadBool()
{
	char oneOrZero = bytes[pos];
	pos++;
	_assert(oneOrZero == 1 || oneOrZero == 0);
	return (oneOrZero == 1) ? true : false;
}

bool InChunk::Empty() const
{
	return pos == length;
}

} // debugger

} // love