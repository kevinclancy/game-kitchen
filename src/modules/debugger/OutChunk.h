/**
* Copyright (c) 2006-2011 LOVE Development Team
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
**/

#ifndef LOVE_DEBUGGER_OUT_CHUNK_H
#define LOVE_DEBUGGER_OUT_CHUNK_H

#include <string>

namespace love
{

namespace debugger
{

/// A char array which encodes and writes sequence of primitive data values, 
/// such as strings, integers, and booleans, to a char array. This data is then
/// transferred over the network to a consumer. OutChunks are used in situations 
/// in which the consumer knows the order and types of the contained fields; 
/// hence, the aforementioned metadata is not stored in the outchunk 
/// itself. Values are written to the outchunk by calling its write methods:
/// WriteInt, WriteString, etc.
class OutChunk
{
private:
	/// The size of the char array pointed to by bytes
	int length;

	/// The offset from bytes of the next value to be written.
	int pos;

	/// A pointer to the bytes stored in the chunk
	char * bytes;

	/// Doubles the size of the bytes array and preserves its current data.
	void Grow();
public:
	/// Creates a byte chunk corresponding to len bytes beginning at data.
	OutChunk();
	
	~OutChunk();

	char * GetBytes();
	int GetLength();

	/// Reads a char from the chunk.
	void WriteChar(char toWrite);

	/// Reads an integer from the chunk.
  void WriteInt(int toWrite);

	/// Writes a 64-bit integer
	void WriteInt64(long long toWrite);

	/// Reads a string from the chunk.
	void WriteString(const std::string & toWrite);
	
	/// Writes a bool.
	void WriteBool(bool toWrite);

	
};

} //love

} //debugger

#endif