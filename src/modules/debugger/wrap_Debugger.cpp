#include "wrap_Debugger.h"
#include "Debugger.h"
#include "Assert.h"

#include <string>
#include <map>
#include <set>

#include <common/Module.h>

extern "C" {
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
}

#include <libraries/lion/lion.h>

namespace 
{

love::debugger::Debugger * debuggerInstance = NULL;

void hookHandler(lua_State * L, lua_Debug * ar)
{
	switch(ar->event)
	{
	case LUA_HOOKLINE:
		debuggerInstance->handleLine(L, ar);
	}
}

typedef love::debugger::Debugger::MessageType MessageType;

char host[] = "localhost";
int app_port   = 57777;
int debugger_port   = 57778;

typedef int LuaErrorHandler(lua_State * L);

int assertHandler(lua_State * L)
{
	_assert(false);
	return 0;
}

int debugHandler(lua_State * L)
{
	debuggerInstance->handleError(L);
	exit(0);
	return 0;
}

LuaErrorHandler * errorHandler = debugHandler;

}

// TODO: use lion_set_handler instead?
int lion_userinput(lion_t * handle, void * user_data, 
                   int status, int size, char * line)
{
	switch( status ) {
		
	case LION_CONNECTION_LOST:
		printf("Connection '%p' was lost.\n", handle);
		break;
		
	case LION_CONNECTION_CLOSED:
		printf("Connection '%p' was gracefully closed.\n", handle);
		break;
	case LION_BUFFER_USED:
		_assert(false);
		break;
	case LION_CONNECTION_NEW:
		printf("Connection '%p' has a new connection...\n", handle);
		debuggerInstance->acceptIncomingConnection(handle);
		break;
	case LION_CONNECTION_CONNECTED:
		printf("Connection '%p' is connected. \n", handle);
		lion_listen(&app_port, 0, 0, NULL);
		break;
	case LION_BINARY:
		debuggerInstance->receiveMessages(line, size);
		break;
	default:
		break;
	}

	return 0;
}

namespace love
{
namespace debugger
{

int w_update(lua_State * L)
{
	debuggerInstance->update();

	return 0;
}

int w_errorHandler(lua_State * L)
{
	errorHandler(L);
	exit(0);
	return 0;
}

int w_debugThread(lua_State* L)
{
	luaL_checktype(L,1,LUA_TTHREAD);
	luaL_checktype(L,2,LUA_TSTRING);
	
	_assert(lua_isthread(L,-2));
	lua_State* thrd = lua_tothread(L,-2);
	const char* msg = lua_tostring(L,-1);
	debuggerInstance->debugThread(thrd,msg);

	lua_pop(L,2);

	return 0;
}

int w_debugger_print(lua_State* L)
{
	if (debuggerInstance == NULL) return 0;
	
	std::string result = "";

	for (int i = 1; i < lua_gettop(L); ++i)
	{
		lua_getglobal(L,"tostring");
		lua_pushvalue(L,i);
		lua_call(L,1,1);

		result += lua_tostring(L,lua_gettop(L)) + std::string("    ");

		lua_pop(L,1);
	}

	lua_getglobal(L,"tostring");
	lua_pushvalue(L,lua_gettop(L)-1);
	lua_call(L,1,1);

	result += lua_tostring(L,lua_gettop(L));

	lua_pop(L,lua_gettop(L));
	
	debuggerInstance->sendDebugLine(result.c_str());

	return 0;
}

int w_enterLevelEditMode(lua_State* L)
{
	luaL_checktype(L,1,LUA_TSTRING);

	if (debuggerInstance == NULL) 
	{
		lua_pop(L,1);
		return 0;
	}
	
	const char* levelName = lua_tostring(L,1);

	debuggerInstance->enterLevelEditMode(levelName);

	lua_pop(L,1);

	return 0;
}

int w_enterLevelPlayMode(lua_State* L)
{
	if (debuggerInstance == NULL) return 0;

	luaL_checktype(L,1,LUA_TSTRING);

	if (debuggerInstance == NULL) 
	{
		lua_pop(L,1);
		return 0;
	}

	const char* levelName = lua_tostring(L,1);

	debuggerInstance->enterLevelPlayMode(levelName);
	
	lua_pop(L,1);

	return 0;
}

int w_makeNewModule(lua_State* L)
{
	if (debuggerInstance == NULL) return 0;

	// module plugin name
	luaL_checktype(L,1,LUA_TSTRING);
	// a sequence of strings - arguments to the module creator plugin
	luaL_checktype(L,2,LUA_TTABLE);

	debuggerInstance->makeNewModule(L);
	
	lua_pop(L,2);

	return 0;
}

int w_openModule(lua_State* L)
{
	if (debuggerInstance == NULL) return 0;

	const char* moduleName = lua_tostring(L,1);

	debuggerInstance->openModule(moduleName);
	
	lua_pop(L,1);

	return 0;
}

int w_requestEmbeddedScreenSize(lua_State* L)
{
	if (debuggerInstance == NULL) return 0;

	debuggerInstance->requestEmbeddedScreenSize();

	return 0;
}

static const luaL_Reg functions[] = {
	{ "update", w_update },
	{ "errorHandler", w_errorHandler },
	{ "debugThread", w_debugThread },
	{ "enterLevelEditMode", w_enterLevelEditMode },
	{ "enterLevelPlayMode", w_enterLevelPlayMode },
	{ "makeNewModule", w_makeNewModule },
	{ "openModule", w_openModule },
	{ "requestEmbeddedScreenSize", w_requestEmbeddedScreenSize },
	{ 0, 0 }
};

static const lua_CFunction types[] = {
	0
};


int luaopen_love_debugger(lua_State * L)
{
	debuggerInstance = new Debugger(L);
	
	WrappedModule w;
	w.module = debuggerInstance;
	w.name = "debugger";
	w.type = MODULE_ID;
	w.functions = functions;
	w.types = types;

	int n = luax_register_module(L, w);
	
	int success = lion_init();
	_assert(success == 0);

	lion_compress_level(0);
	
	lua_sethook(L, hookHandler, LUA_MASKLINE, 0);

	debuggerInstance->connect(host, debugger_port);

	return n;
}

} // debugger
} // love