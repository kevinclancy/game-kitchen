#include "OutChunk.h"

namespace love
{

namespace debugger
{

void OutChunk::Grow()
{
	length = length * 2;
	char * newBytes = new char[length];
	std::copy(bytes, bytes+pos, newBytes);
	delete [] bytes;
	bytes = newBytes;
}

OutChunk::OutChunk()
	: length(10), pos(0), bytes(new char[10])
{
}

OutChunk::~OutChunk()
{
	delete [] bytes;
}

char * OutChunk::GetBytes()
{
	return bytes;
}

int OutChunk::GetLength()
{
	return pos;
}

void OutChunk::WriteChar(char toWrite)
{
	if (pos+1 > length)
		Grow();

	bytes[pos] = toWrite;
	pos++;
}

void OutChunk::WriteInt(int toWrite)
{
	while(pos + 4 > length)
		Grow();

	*( (int *)(bytes + pos) ) = toWrite;
	pos += 4;
}

void OutChunk::WriteString(const std::string & toWrite)
{
	while (pos + sizeof(int) + toWrite.length() > length)
		Grow();

	*( (int *)(bytes + pos) ) = toWrite.length();
	pos += 4;
	std::copy(toWrite.begin(), toWrite.end(), bytes + pos);
	pos += toWrite.length();
}

void OutChunk::WriteBool(bool toWrite)
{
	if (pos+1 > length)
		Grow();

	bytes[pos] = toWrite ? 1 : 0;
	pos++;
}

void OutChunk::WriteInt64(long long toWrite)
{
	while(pos + sizeof(long long) > length)
		Grow();

	*( (long long *)(bytes + pos) ) = toWrite;
	pos += sizeof(long long);
}

} // debugger

} // love