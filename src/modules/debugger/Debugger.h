/**
* Copyright (c) 2006-2011 LOVE Development Team
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
**/

#ifndef LOVE_DEBUGGER_DEBUGGER_H
#define LOVE_DEBUGGER_DEBUGGER_H

#include <string>
#include <map>
#include <set>

#include <common/Module.h>

extern "C" {
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
}

#include <libraries/lion/lion.h>

#include "OutChunk.h"
#include "InChunk.h"

namespace love
{

namespace debugger
{
	class Debugger : public Module
	{
	private:
		/// The debugger is a state machine. DebuggerState enumerates its states.
		typedef enum DebuggerState
		{
			CONNECTING,
			/// In this state, the debugger runs the game until either a breakpoint
			/// is hit, an error occurs, or the game exits. 
			RUNNING,
			/// Roughly, the game is executing one line in a file, and will break upon
			/// hitting the subsequent one.
			STEPPING_OVER,
			/// The game is executing until a new line begins executing (which, taking
			/// recursive calls into account, may not be before the current one 
			/// finishes.)
			STEPPING_IN,
			/// The game is paused and responding to requests about the stack,
			/// globals, etc.
			BREAKING
		};

		typedef enum Constants
		{
			MAX_CHILDREN = 50  
		};

		/// The operation that the debugger is currently performing.
		DebuggerState debuggerState;

		/// Maps a project-relative file name, line number pair to a
		/// boolean signifying whether or not a breakpoint is set at that position.
		std::map<std::string, std::map<int, bool> > breakpointMap; 

		/// The activation record that is currently being debugged.
		lua_Debug * currentActivationRecord;
		/// The thread that is currently being debugged.
		lua_State * luaState;

		/// The global state, so that we can access lua when no thread is being debugged
		lua_State * globalState;

		/// If we are stepping over, this is the stack depth we are stepping at.
		int stepDepth;
		/// If we are stepping over, this is file we are stepping through.
		std::string stepToFile;

		/// Modules which implement class systems. We allow the user to 
		/// specify whether or not the debugger should ignore these,
		/// a sensisble thing to do considering that most programmers
		/// do not want to step through the code used to implement
		/// the class systems they are using.
		std::set<std::string> classSystemModules;

		/// whether or not to ignore class system modules
		bool ignoringClassSystemModules;

		/// A connection used to receive data from love studio.
		lion_t * outgoing;
		/// A connection used to send data to love studio.
		lion_t * incoming;

		/// The synchronously received message, if any
		InChunk* currentSyncMsg;

		/// Stall until a new message of type SYNC is received, returning its body.
		/// Don't forget to delete it when you're done!
		InChunk* receiveSync();

		/// Assumes a table is on top of the stack. Returns the number of entries
		/// in this table.
		int numEntries(lua_State * L);

		/// Returns the depth of the stack in the given thread.
		int stackDepth(lua_State * L);

		/// Sends love studio a command telling it to break, along with a
		/// description of the stack and an a specified popup message.
		/// Pass NULL for message to refrain from triggering a popup message.
		void sendBreak(const char* message);

		// ** breaking state **
		// The following methods may only be called while the debugger is breaking.

		/// Send the children of the table located at the address ptr to
		/// the specified debug browser. Since the same table can occur in 
		/// multiple locations in a debug browser, we use path, a sequence
		/// of keys separated by '.' characters, to specify the location of 
		/// the location we are interested in.
		void sendChildren(long long ptr, std::string path, char browser);
		
		/// Send the entries of the global table to the globals debug browser.
		void sendGlobals(lua_State * L);
		
		/// Send each local in the current activation record to the debugger.
		void sendLocals(lua_State * L, lua_Debug * ar, int debugLevel);

		/// Send each upvalue in the current activation record to the debugger.
		void sendUpvalues(lua_State * L, lua_Debug * ar, int debugLevel);

		/// Sends a large chunk of data, waiting until the buffer empties out
		/// to continue
		void sendChunk(connection_t* connection, char* buffer, int length);

		// ** end breaking
	public:
		/// Identifies the purpose and data format of a message.
		/// TODO: separate incoming and outgoing in different enums.
		typedef enum MessageType
		{
			/// in: A breakpoint has been added
			BREAKPOINT_ADDED,
			/// in: A breakpoint has been removed.
			BREAKPOINT_REMOVED,
			/// out: A breakpoint has been hit.
			BREAKPOINT_HIT,
			/// in: the user has signalled the debugger to resume
			RESUME,
			/// out: describes the children of a lua table being inspected by the
			/// debug browser.
			CHILDREN_DATA,
			/// in: the user has requested information about the children of
			/// a lua table from the debug browser.
			REQUEST_CHILDREN,
			/// in: The user has signalled the debugger to step over
			STEP_OVER,
			/// in: the user has signalled the debugger to step in
			STEP_IN,
			/// All debugging info has been received from love studio, and 
			/// execution may begin
			INIT_FINISH,
			///in: Love Studio has signaled the target to shut down. 
			TERMINATE,
			///out: A debug line has been received from the target
			DEBUG_PRINT,
			/// out: The game has entered level-editing mode, and hence
			/// all level code is safe to edit.
			/// TODO: can we enforce this somehow?
			/// the best bet would be to run a "lua search" style
			/// program. If entries exist afterward that did not exist before,
			/// complain. Complaining isn't really enough, though, as we could
			/// still end up with bugs due to editing level code while it is 
			/// active at runtime. We would simply have to exit love.exe to prevent
			/// errors.
			ENTER_LEVEL_EDIT_MODE,

			/// out: The game has entered level-playing/testing mode, and hence
			/// no level code is safe to edit.
			ENTER_LEVEL_PLAY_MODE,

			///out: requests the size of the "Game" panel in love studio
			REQUEST_EMBEDDED_SCREEN_SIZE,

			MAKE_NEW_MODULE,

			OPEN_MODULE,

			SYNC
		};

		/// Labels for browser messages--both incoming and outgoing--which tell 
		/// love studio which browser to forward outgoing messages to. 
		typedef enum BrowserIds
		{
			LOCALS,
			GLOBALS,
			UPVALUES
		};

		Debugger(lua_State* L);
		~Debugger();

		/// overload from module class
		ModuleType getModuleType() const;

		/// Returns true iff connections with love studio are fully established.
		bool isConnected() const { return debuggerState != CONNECTING; }

		/// Initiates the sequence of events that establishes our incoming
		/// and outgoing connections. Blocks until the connections have been 
		/// established.
		void connect(char * host, int port);
		/// Accepts an incoming connection from love studio.
		void acceptIncomingConnection(lion_t * handle);

		/// Responds to debug client requests.
		void update();
		/// Allows the user to browse the lua state before closing the program
		void handleError(lua_State * L);

		/// Called whenever the lua VM hits a new line -- figures out whether
		/// or not to break, and does so when appropriate.
		void handleLine(lua_State * L, lua_Debug * ar);

		/// Returns false in the common case where the app is running
		/// and no breakpoints have been set.
		bool canBreak() const;

		/// Records the given project-relative file name and line number
		/// as a breakpoint.
		void addBreakpoint(std::string fileName, int lineNum);

		/// Removes an existing breakpoint which is set at the given
		/// project-relative file name and line number.
		void removeBreakpoint(std::string fileName, int lineNum);

		/// Responds to all received messages from the incoming connection.
		void receiveMessages(char * line, int size); 

		/// debugs the given thread
		void debugThread(lua_State* L, std::string msg);

		/// Sends a debug message to the IDE debugger.
		void sendDebugLine(const char* msg);

		/// Notify love studio that the app has entered a state where level code is 
		/// not being run and therefore can be edited.
		void enterLevelEditMode(const char* levelName);

		/// Notify love studio that the app has entered a state where level code is 
		/// being run and therefore cannot be edited.
		void enterLevelPlayMode(const char* levelName);

		/// makes a new module inside the current level code directory
		/// modulePath is specified w.r.t. level code directory
		void makeNewModule(lua_State* L);

		/// Opens a module inside the current level code directory
		/// modulePath is specified w.r.t. the level code directory
		void openModule(const char* modulePath);

		/// Requests the size of the "Game" panel from love studio.
		void requestEmbeddedScreenSize();

		// overridden from Module
		virtual const char * getName() const { return "love.debugger"; }
	};

}

}

#endif