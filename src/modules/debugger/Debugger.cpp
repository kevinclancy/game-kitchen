#include "Debugger.h"
#include "Assert.h"
#include "InChunk.h"
#include "OutChunk.h"
#include "SDL_Events.h"
#include "module.h"

#include <algorithm>

extern "C" {
#include "lapi.h"
}

namespace love
{

namespace debugger
{

Debugger::Debugger(lua_State * L) :
	incoming(NULL), outgoing(NULL), debuggerState(CONNECTING), globalState(L),
	currentSyncMsg(NULL)
{
}

Debugger::~Debugger()
{
	lion_free();
}

Module::ModuleType Debugger::getModuleType() const
{
	return M_DEBUGGER;
}

void Debugger::addBreakpoint(std::string fileName, int lineNum)
{
	_assert(
		breakpointMap.count(fileName) == 0 || 
		breakpointMap[fileName].count(lineNum) == 0
	);

	breakpointMap[fileName][lineNum] = true;
}

void Debugger::removeBreakpoint(std::string fileName, int lineNum)
{
	_assert(breakpointMap.count(fileName) == 1);
	_assert(breakpointMap[fileName].count(lineNum) == 1);

	std::map<int, bool> & lineMap = breakpointMap[fileName];
	std::map<int, bool>::iterator it = lineMap.find(lineNum);
	lineMap.erase(it);

	if (lineMap.empty())
		breakpointMap.erase(breakpointMap.find(fileName));
}

void Debugger::receiveMessages(char * line, int size)
{
	InChunk messages(line, size);

	while (!messages.Empty())
	{
		char msgType = messages.ReadChar();

		if (msgType == INIT_FINISH)
		{
			_assert(debuggerState == CONNECTING);
			
			bool typeCheckingEnabled = messages.ReadBool();
			lua_pushboolean(globalState,typeCheckingEnabled ? 1 : 0);
			lua_setglobal(globalState,"typeCheckingEnabled");

			ignoringClassSystemModules = messages.ReadBool();
			int numClassSystemModules = messages.ReadInt();
			for (int i = 0; i < numClassSystemModules; ++i)
			{
				std::string moduleName = messages.ReadString();
				classSystemModules.insert(moduleName);
			}

			debuggerState = RUNNING;
		}
		if (msgType == BREAKPOINT_ADDED)
		{
			int lineNum = messages.ReadInt();
			std::string fileName = messages.ReadString();
			addBreakpoint(fileName, lineNum);
		}
		else if (msgType == BREAKPOINT_REMOVED)
		{
			int lineNum = messages.ReadInt();
			std::string fileName = messages.ReadString();
			removeBreakpoint(fileName,lineNum);
		}
		else if (msgType == RESUME)
		{
			debuggerState = RUNNING;
		}
		else if (msgType == STEP_OVER)
		{
			lua_getstack(luaState, 0, currentActivationRecord);
			lua_getinfo(luaState, "Sl", currentActivationRecord);

			if (currentActivationRecord->lastlinedefined == currentActivationRecord->currentline)
			{
				debuggerState = STEPPING_IN;
			}
			else
			{
				debuggerState = STEPPING_OVER;
				stepDepth = stackDepth(luaState);
				stepToFile = &currentActivationRecord->source[1];
			}
		}
		else if (msgType == STEP_IN)
		{
			debuggerState = STEPPING_IN;
		}
		else if (msgType == REQUEST_CHILDREN)
		{
			long long ptr = messages.ReadInt64();
			std::string parentPath = messages.ReadString();
			char browser = messages.ReadChar();
			int stackLevel = messages.ReadInt();

			if (ptr == 0LL)
			{
				switch (browser)
				{
				case LOCALS:
					sendLocals(luaState, currentActivationRecord, stackLevel);
					break;
				case GLOBALS:
					sendGlobals(luaState);
					break;
				case UPVALUES:
					sendUpvalues(luaState, currentActivationRecord, stackLevel);
					break;
				}
			}
			else
				sendChildren(ptr, parentPath, browser);
		}
		else if (msgType == TERMINATE)
		{
			lion_free();
			exit(0);
		}
		else if (msgType == SYNC)
		{
			int length = messages.ReadInt();
			currentSyncMsg = messages.ReadBytes(length);
		}
	}
}

int Debugger::stackDepth(lua_State * L)
{
	lua_Debug ar;
	int stackSize = 0;
	int current = 0;
	while (lua_getstack(L,current,&ar) == 1)
	{
		lua_getinfo(L, "S", &ar);

		// index at 1 to skip the @
		bool isClassSystemModule = classSystemModules.count(&ar.source[1]) > 0;

		if (ar.what != std::string("C") && ar.source != std::string("***") && 
			!(ignoringClassSystemModules && isClassSystemModule) )
			stackSize++;
		
		current++;
	}

	return stackSize;
}

InChunk* Debugger::receiveSync()
{
	// enter debug loop
	while (currentSyncMsg == NULL)
	{
		SDL_PumpEvents();

		SDL_Event e;
		while (SDL_PollEvent(&e)) {}

		// once a breakpoint is hit, it might be a better idea to 
		// do everything synchronously
		lion_poll(0,0);
	}

	InChunk* ret = currentSyncMsg;
	currentSyncMsg = NULL;
	return ret;
}

int Debugger::numEntries(lua_State * L)
{
	_assert(lua_istable(L,-1) == 1);
	int topInit = lua_gettop(L);
	int count = 0;

	/* table is in the stack at index 't' */
	lua_pushnil(L);  /* first key */
	while (lua_next(L, -2) != 0) {
		if (count == MAX_CHILDREN)
		{
			lua_pop(L,2);
			break;
		}

		++count;
		/* removes 'value'; keeps 'key' for next iteration */
		lua_pop(L, 1);
	}

	_assert(topInit = lua_gettop(L));
	return count;
}

void Debugger::sendChildren(
	long long ptr, 
	std::string path, 
	char browser
)
{
	lua_State * L = luaState;

	int topInit = lua_gettop(L);

	//push the table onto the stack
	TValue tv;
	tv.tt = LUA_TTABLE;
	tv.value.gc = (GCObject *)ptr;
	luaA_pushobject(luaState, &tv);
	int tableIndex = lua_gettop(L);

	OutChunk chunk;
	chunk.WriteChar(CHILDREN_DATA);
	chunk.WriteChar(browser);
	chunk.WriteString(path);

	int entryCount = numEntries(L);
	chunk.WriteInt(entryCount);

	//write the table's entries
  lua_pushnil(L);  /* first key */
  for (int i = 0; i < entryCount; ++i) {
		lua_next(L, tableIndex);
    /* uses 'key' (at index -2) and 'value' (at index -1) */
		
		//Get key string
		//TODO: if it isn't a string, surround it in square brackets
		lua_getglobal(L,"tostring");
		lua_pushvalue(L, -3); 
		int res = lua_pcall(L,1,1,0);
		if (res == LUA_ERRRUN)
		{
			lua_pop(L,1);
			lua_pushstring(L,"tostring error");
		}
		luaL_checkstring(L,-1);
		const char * keyString = lua_tostring(L, -1);
		chunk.WriteString(keyString);
		lua_pop(L,1);
		
		//stack: tbl, key, value
		_assert(lua_gettop(L) == topInit + 3);
		
		// call type on the the local
		lua_getglobal(L, "type");
		lua_pushvalue(L, -2);
		lua_call(L, 1, 1); 
		_assert(lua_isstring(L,-1) == 1);

		// write the type name and pop it
		const char * typeName = lua_tostring(L,-1);
		chunk.WriteString(typeName);
		lua_pop(L,1); 
		
		//stack: tbl, key, value
		_assert(lua_gettop(L) == topInit + 3);

		//Copy the local value, convert the copy to a string, write the string to
		//our chunk, and then pop the string.
		lua_getglobal(L,"tostring");
		lua_pushvalue(L, -2); 
		res = lua_pcall(L,1,1,0);
		if (res == LUA_ERRRUN)
		{
			lua_pop(L,1);
			lua_pushstring(L,"tostring error");
		}
		luaL_checkstring(L,-1);
		const char * valueString = lua_tostring(L, -1);
		
		// shorten value if it is too long, avoiding overflow
		char shortened[80];
		int len = strlen(valueString);
		std::copy(valueString,valueString+min(79,len), shortened);
		shortened[min(79,len)] = 0;

		chunk.WriteString(shortened);
		//pop the string (representing the local)
		lua_pop(L,1); 
		
		//stack: tbl, key, value
		_assert(lua_gettop(L) == topInit + 3);

		if (lua_istable(L,-1))
		{
			//Copy the table, convert it to a pointer
			lua_pushvalue(L, -1);
			//topointer returns NULL if the value is not a pointer type.
			//in that case, the debugger will know not to request children.
			long long childPtr = (long long)lua_topointer(L,-1);
			_assert(childPtr != 0LL);

			//write a bool indicating whether this item has a ptr 
			chunk.WriteBool(true);
			//write the ptr (or 0 if it has none)
			chunk.WriteInt64(childPtr);
			//pop the ptr (or 0)
			lua_pop(L,1);
		}
		else
		{
			//write a bool indicating whether this item has a ptr 
			chunk.WriteBool(false);
			//write the ptr (or 0 if it has none)
			chunk.WriteInt64(0LL);
		}

		//stack: tbl, key, value
		_assert(lua_gettop(L) == topInit + 3);

		//note relevant for "children fetching", which won't be done in this function:
		//with lua 5.2, we could use the pairs metamethod to traverse children
		//traversing children in 5.1 should be done with lua_next (see documentation)

    /* removes 'value'; keeps 'key' for next iteration */
    lua_pop(L, 1);
  }

	if (lua_gettop(L) == topInit+2)
	{
		//pop key
		lua_pop(L,1);
	}

	//pop table
	lua_pop(L,1);

	//Send the local to the debugger.
	int bytesSent = lion_output(outgoing, chunk.GetBytes(), chunk.GetLength());
	_assert(bytesSent == chunk.GetLength());
	_assert(lua_gettop(L) == topInit);
}

void Debugger::acceptIncomingConnection(lion_t * handle)
{
	incoming = lion_accept(handle, 0, 0, NULL, 0, 0);
	lion_set_buffersize(incoming, 65536);
	lion_enable_binary(incoming);
}

void Debugger::connect(char * host, int port)
{
	outgoing = lion_connect(
		host, 
		port, 
		NULL, 
		0, 
		0, 
		NULL
	);

	lion_set_buffersize(outgoing, 65536);

	_assert(outgoing != NULL);

	while(!isConnected())
		update();
}

void Debugger::update()
{
	lion_poll(0,0);
}

void Debugger::handleError(lua_State * L)
{
	debuggerState = BREAKING;

	luaState = L;
	currentActivationRecord = new lua_Debug;

	currentActivationRecord->source = "[=C]";

	lua_getstack(L,0,currentActivationRecord);
	lua_getinfo(L,"nSl",currentActivationRecord);

	//stack: love::debugger::errorHandler, studio_error_handler, cause of error
	sendBreak(lua_tostring(L,-1));
	
	// enter debug loop
	while (debuggerState == BREAKING)
	{
		SDL_PumpEvents();

		SDL_Event e;
		while (SDL_PollEvent(&e)) {}

		// once a breakpoint is hit, it might be a better idea to 
		// do everything synchronously
		lion_poll(0,0);
	}	
	
	delete currentActivationRecord;
	currentActivationRecord = NULL;
	luaState = NULL;
}

void Debugger::sendBreak(const char* breakMessage)
{
	OutChunk data;
	data.WriteChar(BREAKPOINT_HIT);
	data.WriteString(breakMessage ? breakMessage : "");

	lua_Debug ar;
	int stackSize = min(stackDepth(luaState),100);
	data.WriteInt(stackSize);

	int i = 0;
	int sentFrames = 0;
	while (lua_getstack(luaState,i,&ar) == 1 && sentFrames < stackSize)
	{
		int res = lua_getinfo(luaState, "nSl", &ar);
		_assert(res == 1);

		if (ar.what == std::string("C") || ar.source == std::string("***"))
		{
			++i;
			continue;
		}

		if (classSystemModules.count(&ar.source[1]) > 0 && ignoringClassSystemModules)
		{
			++i;
			continue;
		}

		data.WriteString(ar.name == NULL ? "unknown" : ar.name);
		data.WriteString(&ar.source[1]); //skip the leading @
		data.WriteInt(ar.currentline);
		data.WriteInt(i);

		++sentFrames;
		++i;
	}

	int bytesSent = lion_output(outgoing, data.GetBytes(), data.GetLength());
	_assert(bytesSent == data.GetLength());
}

void Debugger::sendGlobals(lua_State * L)
{
	int topInit = lua_gettop(L);
	//TODO: Lua 5.2 - We are going to have to retrieve the value of 
	//_ENV for this.
	lua_getglobal(L,"_G");
	long long ptr = (long long)lua_topointer(L,-1);
	_assert(ptr != 0);
	lua_pop(L,1);
	sendChildren(ptr,"Globals",1/*TODO: define a constant for this*/);

	_assert(topInit == lua_gettop(L));
}

void Debugger::sendUpvalues(
	lua_State * L, 
	lua_Debug * ar, 
	int stackLevel
)
{
	int noErrors = lua_getstack(L, stackLevel, ar);
	_assert(noErrors == 1);

	int topInit = lua_gettop(L);
	//TODO: send varargs as well (see lua reference for getlocal on how
	//to do this), make varargs debug browser

	OutChunk chunk;
	//msg type
	chunk.WriteChar(CHILDREN_DATA);
	//browser id
	chunk.WriteChar(UPVALUES);
	//parent path
	chunk.WriteString("Upvalues");

	//push the closure for the current activation record
	lua_getinfo(L, "f", ar);

	//count the number of upvalues and write to chunk
	int uvCount = 0;
	int uvIndex = 1;
	const char * uvName = lua_getupvalue(L, -1, uvIndex);
	while (uvName != NULL && uvIndex < 40)
	{
		if (uvName != std::string("(*temporary)"))
			uvCount++;

		++uvIndex;
		lua_pop(L,1);
		uvName = lua_getupvalue(L, -1, uvIndex);
	}
	chunk.WriteInt(uvCount);

	//write each upvalue to the chunk
	uvIndex = 1;
	uvName = lua_getupvalue(L, -1, uvIndex);
	while (uvName != NULL && uvIndex < 40)
	{
		if (uvName == std::string("(*temporary)"))
		{
			++uvIndex;
			lua_pop(L,1);
			uvName = lua_getupvalue(L, -1, uvIndex);
			continue;
		}

		chunk.WriteString(uvName);

		// call type on the the local
		lua_getglobal(L, "type");
		lua_pushvalue(L, -2);
		lua_call(L, 1, 1); 

		// stack: typeName, localValue
		_assert( lua_gettop(L) == topInit + 3 );
		
		// write the type name and pop it
		_assert( lua_isstring(L,-1) == 1 );
		const char * typeName = lua_tostring(L,-1);
		chunk.WriteString(typeName);
		lua_pop(L,1); 

		//stack: upvalue value
		_assert( lua_gettop(L) == topInit + 2 );

		//Copy the upvalue value, convert the copy to a string, write the string to
		//our chunk, and then pop the string.
		lua_getglobal(L,"tostring");
		lua_pushvalue(L, -2); 
		int res = lua_pcall(L,1,1,0);
		if (res == LUA_ERRRUN)
		{
			lua_pop(L,1);
			lua_pushstring(L,"tostring error");
		}
		luaL_checkstring(L,-1);
		const char * valueString = lua_tostring(L, -1);

		// shorten value if it is too long, avoiding overflow
		char shortened[80];
		int len = strlen(valueString);
		std::copy(valueString,valueString+min(79,len), shortened);
		shortened[min(79,len)] = 0;

		chunk.WriteString(shortened);

		//pop the string (representing the local)
		lua_pop(L,1); 

		if (lua_istable(L,-1))
		{
			//Copy the table, convert it to a pointer
			lua_pushvalue(L, -1);
			//topointer returns NULL if the value is not a pointer type.
			//in that case, the debugger will know not to request children.
			long long childPtr = (long long)lua_topointer(L,-1);
			_assert(childPtr != 0LL);

			//write a bool indicating whether this item has a ptr 
			chunk.WriteBool(true);
			//write the ptr (or 0 if it has none)
			chunk.WriteInt64(childPtr);
			//pop the ptr (or 0)
			lua_pop(L,1);
		}
		else
		{
			//write a bool indicating whether this item has a ptr 
			chunk.WriteBool(false);
			//write the ptr (or 0 if it has none)
			chunk.WriteInt64(0LL);
		}

		//pop the local
		lua_pop(L,1);

		//note relevant for "children fetching", which won't be done in this function:
		//with lua 5.2, we could use the pairs metamethod to traverse children
		//traversing children in 5.1 should be done with lua_next (see documentation)

		//get the next local, if it exists (otherwise, getlocal returns null)
		++uvIndex;
		uvName = lua_getupvalue(L, -1, uvIndex);
	}

	//pop the closure for the current activation record
	lua_pop(L,1);

	//Send the locals to the debugger.
	int bytesSent = lion_output(outgoing, chunk.GetBytes(), chunk.GetLength());
	_assert(bytesSent == chunk.GetLength());

	_assert(topInit == lua_gettop(L));
}

void sendChunk(connection_t* connection, char* buffer, int length)
{
	lion_output(connection,buffer, length);

}

void Debugger::sendLocals(
	lua_State * L, 
	lua_Debug * ar, 
	int stackLevel
)
{
	int noErrors = lua_getstack(L, stackLevel, ar);
	_assert(noErrors == 1);

	int topInit = lua_gettop(L);
	//TODO: send varargs as well (see lua reference for getlocal on how
	//to do this), make varargs debug browser

	OutChunk chunk;
	//msg type
	chunk.WriteChar(CHILDREN_DATA);
	//browser id
	chunk.WriteBool(LOCALS);
	//parent path
	chunk.WriteString("Locals");

	//count the number of locals and write to chunk
	int localCount = 0;
	int localIndex = 1;
	const char * localName = lua_getlocal(L, ar, localIndex);
	while (localName != NULL && localIndex < 40)
	{
		if (localName != std::string("(*temporary)"))
			localCount++;

		++localIndex;
		lua_pop(L,1);
		localName = lua_getlocal(L, ar, localIndex);
	}
	chunk.WriteInt(localCount);

	_assert(lua_gettop(L) == topInit);

	//write each local to the chunk
	localIndex = 1;
	localName = lua_getlocal(L, ar, localIndex);
	while (localName != NULL && localIndex < 40)
	{
		if (localName == std::string("(*temporary)"))
		{
			++localIndex;
			lua_pop(L,1);
			localName = lua_getlocal(L, ar, localIndex);
			continue;
		}

		chunk.WriteString(localName);

		// call type on the the local
		lua_getglobal(L, "type");
		lua_pushvalue(L, -2);
		lua_call(L, 1, 1); 

		// stack: typeName, localValue
		_assert( lua_gettop(L) == topInit + 2 );
		
		// write the type name and pop it
		_assert( lua_isstring(L,-1) == 1 );
		const char * typeName = lua_tostring(L,-1);
		chunk.WriteString(typeName);
		lua_pop(L,1); 

		//stack: localValue
		_assert( lua_gettop(L) == topInit + 1 );

		//Copy the local value, convert the copy to a string, write the string to
		//our chunk, and then pop the string.
		lua_getglobal(L,"tostring");
		lua_pushvalue(L, -2); 
		int res = lua_pcall(L,1,1,0);
		if (res == LUA_ERRRUN)
		{
			lua_pop(L,1);
			lua_pushstring(L,"tostring error");
		}
		luaL_checkstring(L,-1);
		const char * valueString = lua_tostring(L, -1);

		// shorten the string so that we do not get enormous strings overflowing the buffer
		char shortened[80];
		int len = strlen(valueString);
		std::copy(valueString,valueString+min(79,len), shortened);
		shortened[min(79,len)] = 0;

		chunk.WriteString(shortened);
		//pop the string (representing the local)
		lua_pop(L,1); 

		if (lua_istable(L,-1))
		{
			//Copy the table, convert it to a pointer
			lua_pushvalue(L, -1);
			//topointer returns NULL if the value is not a pointer type.
			//in that case, the debugger will know not to request children.
			long long childPtr = (long long)lua_topointer(L,-1);
			_assert(childPtr != 0LL);

			//write a bool indicating whether this item has a ptr 
			chunk.WriteBool(true);
			//write the ptr (or 0 if it has none)
			chunk.WriteInt64(childPtr);
			//pop the ptr (or 0)
			lua_pop(L,1);
		}
		else
		{
			//write a bool indicating whether this item has a ptr 
			chunk.WriteBool(false);
			//write the ptr (or 0 if it has none)
			chunk.WriteInt64(0LL);
		}

		//pop the local
		lua_pop(L,1);

		//get the next local, if it exists (otherwise, getlocal returns null)
		++localIndex;
		localName = lua_getlocal(L, ar, localIndex);
	}

	//Send the locals to the debugger.
	int bytesSent = lion_output(outgoing, chunk.GetBytes(), chunk.GetLength());
	_assert(bytesSent == chunk.GetLength());

	_assert(topInit == lua_gettop(L));
}

void Debugger::debugThread(lua_State * L, std::string msg)
{
	lua_Debug ar;
	lua_getstack(L, 0, &ar);
	lua_getinfo(L, "nS", &ar);

	if (stackDepth(L) == 0)
		return;

	debuggerState = BREAKING;

	luaState = L;
	currentActivationRecord = &ar;

	sendBreak(msg.c_str());
	
	// enter debug loop
	while (debuggerState == BREAKING)
	{
		SDL_PumpEvents();

		SDL_Event e;
		while (SDL_PollEvent(&e)) {}

		// once a breakpoint is hit, it might be a better idea to 
		// do everything synchronously
		lion_poll(0,0);
	}

	currentActivationRecord = 0;
}

bool Debugger::canBreak() const
{
	return !(debuggerState == RUNNING && breakpointMap.size() == 0);
}

void Debugger::handleLine(lua_State * L, lua_Debug* ar)
{	
	if (!canBreak())
		return;

	lua_getinfo(L, "nS", ar);

	const char* source = &ar->source[1];

	if (debuggerState == STEPPING_OVER)
	{
		if (luaState != L)
			return;

		// source is indexed by 1 to skip the leading @
		if (source != stepToFile || stackDepth(L) != stepDepth)
			return;
	}
	else if (debuggerState == RUNNING)
	{
		if (breakpointMap.count(source) == 0)
			return;

		if (breakpointMap[source].count(ar->currentline) == 0)
			return;
	}
	else if (debuggerState == BREAKING)
	{
		//We must be converting something to a string. We do not break
		//in this case.
		return;
	}

	if (stackDepth(L) == 0)
		return;

	if (classSystemModules.count(&(ar->source)[1]) > 0 && ignoringClassSystemModules)
		return;
	
	debuggerState = BREAKING;

	luaState = L;
	currentActivationRecord = ar;

	sendBreak("");
	
	// enter debug loop
	while (debuggerState == BREAKING)
	{
		SDL_PumpEvents();

		SDL_Event e;
		while (SDL_PollEvent(&e)) {}

		// once a breakpoint is hit, it might be a better idea to 
		// do everything synchronously
		lion_poll(0,0);
	}
}

void Debugger::sendDebugLine(const char* msg)
{
	OutChunk chunk;
	chunk.WriteChar(DEBUG_PRINT);
	chunk.WriteString(msg);
	lion_output(outgoing,chunk.GetBytes(),chunk.GetLength());
}

void Debugger::enterLevelEditMode(const char* levelName)
{
	OutChunk chunk;
	chunk.WriteChar(ENTER_LEVEL_EDIT_MODE);
	chunk.WriteString(levelName);
	lion_output(outgoing,chunk.GetBytes(),chunk.GetLength());
}

void Debugger::enterLevelPlayMode(const char* levelName)
{
	OutChunk chunk;
	chunk.WriteChar(ENTER_LEVEL_PLAY_MODE);
	chunk.WriteString(levelName);
	lion_output(outgoing,chunk.GetBytes(),chunk.GetLength());
}

void Debugger::makeNewModule(lua_State* L)
{
	luaL_checktype(L,1,LUA_TSTRING);
	luaL_checktype(L,2,LUA_TTABLE);

	int top = lua_gettop(L);

	const char* modulePluginName = lua_tostring(L,1);
	
	OutChunk chunk;
	chunk.WriteChar(MAKE_NEW_MODULE);
	chunk.WriteString(modulePluginName);
	
	int n = lua_objlen(L,2);
	chunk.WriteInt(n);

	for(int i = 1; i <= n; ++i) 
	{
		lua_pushnumber(L,i);
		// pop 1, push t[1]
		lua_gettable(L,2);

		if (lua_isnil(L,-1))
			break;
		else
		{
			const char* str = lua_tostring(L,-1);
			chunk.WriteString(str);
		}
	}	

	lua_pop(L,3);

	lion_output(outgoing,chunk.GetBytes(),chunk.GetLength());
	
	_assert(lua_gettop(L) == top);
}

void Debugger::openModule(const char* modulePath)
{
	OutChunk chunk;
	chunk.WriteChar(OPEN_MODULE);
	chunk.WriteString(modulePath);
	lion_output(outgoing,chunk.GetBytes(),chunk.GetLength());
}

/// Requests the size of the "Game" panel from love studio.
void Debugger::requestEmbeddedScreenSize()
{
	OutChunk chunk;
	chunk.WriteChar(REQUEST_EMBEDDED_SCREEN_SIZE);
	lion_output(outgoing,chunk.GetBytes(),chunk.GetLength());

	InChunk* sizeData = receiveSync();
	int width = sizeData->ReadInt();
	int height = sizeData->ReadInt();
	delete sizeData;

	if (width != -1)
	{
		lua_getglobal(globalState,"love");
		lua_getfield(globalState,-1,"window");
		lua_getfield(globalState,-1,"setMode");

		lua_pushnumber(globalState, width);
		lua_pushnumber(globalState, height);
		lua_pushnil(globalState);
		lua_pushnil(globalState);
		lua_pushnil(globalState);

		lua_call(globalState,5,1);

		bool success = lua_toboolean(globalState,-1) == 0 ? false : true;
				
		//result,graphics,love
		lua_pop(globalState,3);

		OutChunk response;
		response.WriteChar(SYNC);
		lion_output(outgoing,response.GetBytes(),response.GetLength());
	}
	else
	{
		OutChunk response;
		response.WriteChar(SYNC);
		lion_output(outgoing,response.GetBytes(),response.GetLength());
	}

}

}

}