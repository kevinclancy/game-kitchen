/**
* Copyright (c) 2006-2011 LOVE Development Team
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
**/

#ifndef LOVE_DEBUGGER_BYTE_CHUNK_H
#define LOVE_DEBUGGER_BYTE_CHUNK_H

#include <string>

namespace love
{

namespace debugger
{
//TODO: the method casing in the class is inconsistent with love

/// A char array which contains an encoded sequence of primitive data values, 
/// such as strings, integers, and booleans, to be read. ByteChunks are used 
/// in situations in which the order and types of the contained fields are known 
/// from context; hence, the aforementioned metadata is not stored in the inchunk 
/// itself. The values contained in the inchunk are extracted by calling its read 
/// methods: ReadString, ReadInt, etc. 
class InChunk
{
private:
	/// The number of bytes stored in the chunk
	const int length;

	/// The byte offset from bytes of the next byte to be read
	int pos;

	/// A pointer to the bytes stored in the chunk
	const char * const bytes;

	bool ownsData;
public:
	/// Creates a byte chunk corresponding to len bytes beginning at data.
	InChunk(char * data, int len, bool _ownsData=false);
	
	~InChunk();

	/// Reads a char from the chunk.
	char ReadChar();

	/// Reads an integer from the chunk.
	int ReadInt();

	/// Reads numBytes bytes into a new InChunk. 
	/// Don't forget to delete once you are done using it!
	InChunk* ReadBytes(int numBytes);

	/// Reads a 64-bit integer from the chunk.
	long long ReadInt64();

	/// Reads a string from the chunk.
	std::string ReadString();
	
	/// Reads a bool from the chunk. 
	bool ReadBool();

	bool Empty() const;
};

} //love

} //debugger

#endif