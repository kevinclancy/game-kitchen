


#ifndef LOVE_DEBUGGER_WRAP_DEBUGGER_H
#define LOVE_DEBUGGER_WRAP_DEBUGGER_H

#include "common/runtime.h"
#include <common/config.h>

extern "C" {
	#include <lua.h>
}

namespace love
{
namespace debugger
{
	/// Responds to any messages received from the debug proxy.
	int w_update(lua_State * L);
	/// Causes the love studio to break and display the current lua state.
	/// Waits for a resume message from the debug proxy, and then shuts down
	/// love.
	int w_errorHandler(lua_State * L);
	/// Debugs the thread at the top of the stack specified by the argument.
	int w_debugThread(lua_State* L);
	
	/// Tells IDE to enter level edit mode
	int w_enterLevelEditMode(lua_State* L);

	/// Tells IDE to enter level play mode
	int w_enterLevelPlayMode(lua_State* L);

	/// Tells IDE to make a new module
	int w_makeNewModule(lua_State* L);

	int w_openModule(lua_State* L);

	int w_requestEmbeddedScreenSize(lua_State* L);

	/// Sends a debug message to the IDE debugger.
	extern "C" LOVE_EXPORT int w_debugger_print(lua_State* L);

	extern "C" LOVE_EXPORT int luaopen_love_debugger(lua_State * L);
} // debugger
} // love

#endif //LOVE_DEBUGGER_WRAP_DEBUGGER_H