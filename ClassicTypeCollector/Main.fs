﻿module Main

open LuaAnalyzer.Type
open LuaAnalyzer.TypeChecker
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.Utils
open LuaAnalyzer.Context
open LuaAnalyzer.SubtypeGraphBuilder
open LuaAnalyzer.TypeCollector
open LuaAnalyzer.Annotations
open LuaAnalyzer.Operators

open System.ComponentModel.Composition

exception GlobalStyleModuleDefinition of Range

let errors : Ref< List<Error> > = ref []

let flushErrors () =
    errors := []

let addError fileName msg rng =
    errors := (fileName,msg,rng) :: !errors

/// True iff ast ends with the statement "return *name*"
let rec nameReturned (ast : TypedStatement) =
    match ast with
    | EndsWith (Return([NameExpr(x,_)],_)) ->
        Some x
    | _ ->
        None

let rec getName (currentFile : string) (ast : TypedStatement) =
    let retName = nameReturned ast

    let rec getNameRec (currentFile : string) (ast : TypedStatement) =
        match ast with
        | LocalAssign(
            _,
            [NameExpr(name,rng)],
            [Constructor([],_)],
            _
            ) when retName.IsSome && name = retName.Value ->
            Some (name,rng)
        | Assign(
            _,
            [NameExpr(name,rng)],
            [Constructor([],_)],
            _
            ) when retName.IsSome && name = retName.Value ->
            raise( GlobalStyleModuleDefinition(rng) )
        | Sequence(_,stats,_) ->
            Seq.tryPick (getNameRec currentFile) stats
        | _ ->
            None          

    getNameRec currentFile ast          

type FieldMap = Map<string, Field>
type MethodMap = Map<string, Method>


let typeGraphBuilder (modPath : string) (fileName : string) (ast : TypedStatement) =
    flushErrors()
    try
        match getName fileName ast with
        | Some(name,rng) ->
            Some (rng,[],[]), !errors
        | _ ->
            None, !errors
    with
    | GlobalStyleModuleDefinition(rng) ->
        addError fileName "Global style module definitions not allowed" rng
        None, !errors

let getDescription (ast : TypedStatement) =
    match ast with
    | Sequence(Some(desc,_,_,_),_,_) ->
        desc
    | _ ->
        ""

let rec getFieldsAndMethods (fileName : string) (moduleName : string) (className : string) (ast : TypedStatement) : FieldMap*MethodMap =
    match ast with
    | Assign(
        annotation,
        [BinOpExpr(OpSelect,NameExpr(x,_),String(fieldName,nameRng),_)], 
        [Function(ann,selfName,latent,fstFormal :: restFormals,varPars,rets,varRets,body,(startLoc,_))],
        _
      ) when x = className ->
        let (fstName, fstDesc, fstType) = fstFormal
        let desc = if ann.IsSome then fst4 ann.Value else ""
        if fstName = "self" then
            let methodTy = 
                FunctionTy(
                    desc, 
                    restFormals,
                    varPars,
                    rets, 
                    varRets,
                    true, 
                    latent,
                    (fileName, nameRng)
                )

            Map.empty, new MethodMap([(fieldName,{desc=desc;ty=methodTy;isAbstract=false;loc=(fileName,nameRng)})])
        else
            let fieldTy = 
                FunctionTy(
                    desc, 
                    fstFormal :: restFormals,
                    varPars,
                    rets, 
                    varRets,
                    false, 
                    latent,
                    (fileName, nameRng)
                )
            let field = {
                desc = desc
                ty = fieldTy
                dependencies = Set.empty
                isConst = true
                isVisible = true
                loc = (fileName,nameRng)
            }
            new FieldMap([(fieldName,field)]), Map.empty
    | Assign(
        annotation,
        [BinOpExpr(OpSelect,NameExpr(x,_),String(fieldName,nameRng),_)], 
        [expr],
        _
      ) when x = className ->
        let desc, vars, isConst = Type.InterpretVarAnnotation annotation 1

        match Type.InterpretVarAnnotation annotation 1 with
        | _,None :: _,isConst :: _->
            addError
                fileName 
                "non-function fields should be given type ascriptions"
                nameRng
            Map.empty, Map.empty
        | desc :: _,Some(ty) :: _,isConst :: _ ->
            let field = {
                desc = desc
                ty = ty
                dependencies = Set.empty
                isConst = isConst
                isVisible = true
                loc = (fileName,nameRng)
            }
            new FieldMap([(fieldName,field)]),Map.empty
        | _,_,_ ->
            failwith "unreachable"

    | Sequence(_,stats,_) ->
        let foldStat ((f,m) : FieldMap*MethodMap) (stat : TypedStatement) =
            let statf,statm = getFieldsAndMethods fileName moduleName className stat
            //TODO: detect duplicate method definitions
            (cover f statf, cover m statm)

        Seq.fold foldStat (Map.empty,Map.empty) stats
    | _ ->
        Map.empty, Map.empty

let externalTypeBuilder (env : TypeEnvironment) (modname : string) (path : string) (ast : TypedStatement) =
    flushErrors()
    let typeMap = env.typeMap
    match getName path ast with
    | Some (name, rng) ->
        let desc = getDescription ast
        let fields, methods = getFieldsAndMethods path modname name ast
        let ty = RecordTy(
            modname,
            (if desc = "" then "No description given for " + modname + "." else desc),
            false,
            false,
            Closed,
            MetamethodSet.empty, 
            fields,
            [],
            methods,
            [],
            (path,rng)
        )

        // Note that for classic modules, the constructor type *is* the instance type
        Some (ty,ty), !errors
    | _ ->
        raise( CollectorError "could not build external type" )

let internalTypeBuilder (ctxt : Context)
                        (modname : string) 
                        (fileName : string)
                        (ast : TypedStatement) =
    
    Map.empty, [], []

let decorate (ctxt : Context) (modname : string) (ast : TypedStatement) =
    let typeMap = ctxt.tenv.typeMap
    let className = 
        match getName "" ast with
        | Some(className,_) -> className
        // we cannot throw an exception here, because it may be that the file's syntax is malformed
        | _ -> "***unknown742***"

    let rec decRec (ast : TypedStatement) =
        match ast with
        | Sequence(ann,stats,rng) ->
            let mappedStats = new GenList<TypedStatement>(Seq.map decRec stats)
            Sequence(ann,mappedStats,rng)
        | LocalAssign(
            annotation,
            [NameExpr(name,nrng)],
            [Constructor([],crng)],
            rng
            ) when name = className ->

            let newClassTy = RecordTy(
                "newmodule",
                "a module in the process of being defined",
                true,
                false,
                Open,
                MetamethodSet.empty,
                Map.empty,
                [],
                Map.empty,
                [],
                NoLocation
            )

            LocalAssign(
                annotation,
                [NameExpr(name,nrng)],
                [Ascription(Constructor([],crng),newClassTy,crng)],
                rng
            ) 
        | Assign(
            annotation,
            [BinOpExpr(OpSelect,NameExpr(x,rcl),String(methName,rmth),rind)], 
            [Function(desc,funcName,latent,fstFormal :: restFormals,varPars,rets,varRets,body,funRng)],
            rng
            ) when x = className ->

            let (fstName,fstDesc,fstTy) = fstFormal
            
            if fstName = "self" then 
                let self' = "self","",ctxt.tenv.typeMap.Item modname

                Assign(
                    annotation,
                    [BinOpExpr(OpSelect,NameExpr(x,rcl),String(methName,rmth),rind)],
                    [Function(desc,funcName,latent,self' :: restFormals,varPars,rets,varRets,body,funRng)],
                    rng
                )
            else
                 Assign(
                    annotation,
                    [BinOpExpr(OpSelect,NameExpr(x,rcl),String(methName,rmth),rind)],
                    [Function(desc,funcName,latent,fstFormal :: restFormals,varPars,rets,varRets,body,funRng)],
                    rng
                )               
        | x ->
            x

    decRec ast

let detectMiscErrors (ctxt : Context) (modulePath : string) (filePath : string) (ast : TypedStatement) =
    ()

[<Export(typeof<LuaAnalyzer.Plugins.ITypeCollectorPlugin>)>]
type Initializer() =
    interface LuaAnalyzer.Plugins.ITypeCollectorPlugin with
        member self.Init () =
            addTypeCollector {
                name = "Classic Module System"
                typeGraphBuilder = typeGraphBuilder
                typeGraphIsTree = true
                externalTypeBuilder = externalTypeBuilder
                internalTypeBuilder = internalTypeBuilder
                decorate = decorate
                detectMiscErrors = detectMiscErrors
            }

        member self.GetLibPaths () =
            Seq.empty

        member self.GetName () =
            "Classic Module"


