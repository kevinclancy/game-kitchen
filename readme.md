﻿An IDE for the popular game development framework [LÖVE][site].
See the [wiki][wiki] for details.

[site]: http://love2d.org
[wiki]: https://bitbucket.org/kevinclancy/game-kitchen/wiki/